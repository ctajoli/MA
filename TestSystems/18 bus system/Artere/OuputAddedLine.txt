# records dumped
$TOLAC 0.01 ;
$TOLREAC 0.01 ;
$NBITMA 20 ;
$MISBLOC 0 ;
$MISADJ 1 ;
$PLIM 1 ;
$DIVDET 0 ;
BUS TN 20 0. 0. 0. 0 ;
BUS N0 20 0. 0. 0. 0 ;
BUS N1 0.4 9.4999999E-02 3.1250000E-02 0. 0 ;
BUS N2 0.4 0. 0. 0. 0 ;
BUS N3 0.4 0. 0. 0. 0 ;
BUS N4 0.4 0. 0. 0. 0 ;
BUS N5 0.4 0. 0. 0. 0 ;
BUS N6 0.4 0. 0. 0. 0 ;
BUS N7 0.4 0. 0. 0. 0 ;
BUS N8 0.4 0. 0. 0. 0 ;
BUS N9 0.4 0. 0. 0. 0 ;
BUS N10 0.4 0. 0. 0. 0 ;
BUS N11 0.4 7.1249995E-03 2.3399999E-03 0. 0 ;
BUS N12 0.4 0. 0. 0. 0 ;
BUS N13 0.4 0. 0. 0. 0 ;
BUS N14 0.4 0. 0. 0. 0 ;
BUS N15 0.4 2.4700001E-02 8.1200004E-03 0. 0 ;
BUS N16 0.4 2.6125001E-02 8.5850004E-03 0. 0 ;
BUS N17 0.4 1.6625000E-02 5.4649999E-03 0. 0 ;
BUS N18 0.4 2.2325000E-02 7.3400000E-03 0. 0 ;
TRANSFO MAINTR N0 N1 0.0032 0.0128 0 0 100.0000 0 0.5 1 ;
LINE 'TN N0' TN N0 0 0 0 0 1 ;
LINE 'N1 N2' N1 N2 0.010045 0.005845 0 0 1 ;
LINE 'N2 N3' N2 N3 0.010045 0.005845 0 0 1 ;
LINE 'N3 N4' N3 N4 0.010045 0.005845 0 0 1 ;
LINE 'N4 N5' N4 N5 0.010045 0.005845 0 0 1 ;
LINE 'N5 N6' N5 N6 0.010045 0.005845 0 0 1 ;
LINE 'N6 N7' N6 N7 0.010045 0.005845 0 0 1 ;
LINE 'N7 N8' N7 N8 0.010045 0.005845 0 0 1 ;
LINE 'N8 N9' N8 N9 0.010045 0.005845 0 0 1 ;
LINE 'N9 N10' N9 N10 0.010045 0.005845 0 0 1 ;
LINE 'N3 N11' N3 N11 0.03456 0.01374 0 0 1 ;
LINE 'N4 N12' N4 N12 0.04032 0.01603 0 0 1 ;
LINE 'N12 N13' N12 N13 0.04032 0.01603 0 0 1 ;
LINE 'N13 N14' N13 N14 0.04032 0.01603 0 0 1 ;
LINE 'N14 N15' N14 N15 0.03456 0.01374 0 0 1 ;
LINE 'N6 N16' N6 N16 0.10368 0.04122 0 0 1 ;
LINE 'N9 N17' N9 N17 0.03456 0.01374 0 0 1 ;
LINE 'N10 N18' N10 N18 0.03456 0.01374 0 0 1 ;
GENER G0 TN TN 0.1919000 6.3100018E-02 1.000000 0 -999 999.0000 1 ;
SLACK TN ;
FNOM 50. ;
LFRESV TN 1.000000 0. ;
LFRESV N0 0.9999999 -1.9189999E-07 ;
LFRESV N1 0.9999715 -4.5279896E-05 ;
LFRESV N2 0.9927245 -1.5855753E-03 ;
LFRESV N3 0.9854774 -3.1258706E-03 ;
LFRESV N4 0.9787632 -4.5527890E-03 ;
LFRESV N5 0.9738963 -5.5871690E-03 ;
LFRESV N6 0.9690294 -6.6215494E-03 ;
LFRESV N7 0.9661163 -7.2405278E-03 ;
LFRESV N8 0.9632032 -7.8595057E-03 ;
LFRESV N9 0.9602901 -8.4784841E-03 ;
LFRESV N10 0.9586204 -8.8332295E-03 ;
LFRESV N11 0.9837375 -3.2322900E-03 ;
LFRESV N12 0.9717253 -4.9811802E-03 ;
LFRESV N13 0.9646874 -5.4095718E-03 ;
LFRESV N14 0.9576495 -5.8379630E-03 ;
LFRESV N15 0.9516169 -6.2051555E-03 ;
LFRESV N16 0.9498887 -7.7889222E-03 ;
LFRESV N17 0.9562298 -8.7257158E-03 ;
LFRESV N18 0.9531679 -9.1649489E-03 ;
