# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 11:35:02 2021

@author: Carlo
"""
#%% Imports 
import pyramses
# import cmath
# import math
import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition, mark_inset, zoomed_inset_axes)
# import pandas as pd

#%% Function definition


# def get_measurements_central():
#     w = ram.getObs('SYN', 'G', 'S')
#     f = w[0]*50
#     if f <= 48.5:
#         f_level = -3
#     elif f <= 49.2:
#         f_level = -2
#     elif f <= 49.9:
#         f_level = -1
#     elif f < 50.1:
#         f_level = 0
#     elif f < 50.8:
#         f_level = 1
#     elif f < 51.5:
#         f_level = 2
#     else: f_level = 3
# #   rocof still missing
#     return f_level

def get_limits_atl(ATL): # having two functions we could use them at different frequencies.
    lim_v_min = ram.getObs('INJ', ATL, 'F_v_min')
    lim_v_max = ram.getObs('INJ', ATL, 'F_v_max')
    lim_p_min = ram.getObs('INJ', ATL, 'F_p_min')
    lim_p_max = ram.getObs('INJ', ATL, 'F_p_max')
    
    return lim_v_min[0], lim_v_max[0], lim_p_min[0], lim_p_max[0]

# def update_emergency_level(t, ATL, level_diff):
#     ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' lvl ' + level_diff + ' 0')
#     return none

def update_neighbourhood_help(t, ATL, v_diff, p_diff):
    ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' P_n ' + str(p_diff) + ' 0')
    ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' V_n ' + str(v_diff) + ' 0')
     

#%% Execution

## Draw random parameters
# import import_ipynb
# import Functions

# from Functions import drawParametersStrongGrid
# from Functions import drawParametersWeakGrid

# p_atl = [1]  # ATL protection
# s_atl = [1]  # ATL support
# p_pv =  [-1]  # PV protection
# s_pv =  [-1]  # PV support

# # Functions.drawParametersStrongGrid(p_atl[0],s_atl[0],p_pv[0],s_pv[0])
# Functions.drawParametersWeakGrid(p_atl[0],s_atl[0],p_pv[0],s_pv[0])


##Initial set points
# ram = pyramses.sim()
# ram = pyramses.sim(r"C:\Users\ctajoli\OneDrive - ETH Zurich\MA Carlo\URAMSES\URAMSES-1.2\URAMSES-1.2\Release_intel_w64")
# ram = pyramses.sim(r"C:\Users\Carlo\OneDrive - ETH Zurich\MA Carlo\URAMSES\URAMSES-1.2\URAMSES-1.2\Release_intel_w64")
ram = pyramses.sim(r"C:\\Users\\Carlo\\OneDrive - ETH Zurich\\MA Carlo\\URAMSES-3.40b\\Release_intel_w64")
case = pyramses.cfg("cmd.txt")

# store results, i is number of monte carlo simulation
case.addTrj("out.trj")


ret = ram.execSim(case,0)
# listofsyncs = ram.getAllCompNames("SYNC")

# Islanding
# ram.addDisturb(5.000, 'FAULT BUS N0 0 0')
# ram.addDisturb(0.600, 'BREAKER BRANCH N10-N18 1 0')
# ram.addDisturb(0.600, 'BREAKER BRANCH N4-N5 1 0')

# Short circuit
# ram.addDisturb(5.000, 'VFAULT BUS N5 0.95') this does not work

# ram.addDisturb(0.5, 'FAULT BUS N1 0 0.1')
# ram.addDisturb(2.5, 'CLEAR BUS N1')

# Frequency event
ram.addDisturb(2, 'CHGPRM INJ L0 P0 -0.15 0.1')
# ram.addDisturb(2, 'CHGPRM INJ L0 P0 +0.25 0.5')
# ram.addDisturb(3, 'CHGPRM INJ TL1 P_n 1 0')

# Test short circuit currents
# ram.addDisturb(0.5, 'FAULT BUS N10 0 0.1')
# ram.addDisturb(2.5, 'CLEAR BUS N10')  # units should disconnect, PVs reconnect after some time


# ram.addDisturb(1, 'CLEAR BUS N10')    # if protection on, units should stay connected
 


Thorizon = 10
Tsample = 0.5

# try:
#     ret = ram.contSim(Thorizon) # Run simulation
#     ram.endSim()
# except:
#     print(ram.getLastErr())

LVloadlist = ['TL1', 'TL11', 'TL15', 'TL16', 'TL17', 'TL18'] 
count_v = 0
count_p = 0

for t in np.arange(0.5,Thorizon,Tsample):
    
    # Simulates until the next sampling point. If there is an error, then exit the loop printing the error.
    try:
        ret = ram.contSim(t)
       

    except:
        print(ram.getLastErr())
        break
#     old_level = level 
#     level = get_measurements_central()
#     level_diff = level-old_level
    count_v_old = count_v
    count_p_old = count_p
    for ATL in LVloadlist: # measure loop. LVloadlist must be all the laads for the given LV system, so they have to have the correct name 
        lim_v_min, lim_v_max, lim_p_min, lim_p_max = get_limits_atl(ATL)
        count_v += lim_v_max - lim_v_min
        count_p += lim_p_max - lim_p_min
    v_diff = count_v - count_v_old
    p_diff = count_p - count_p_old
    
   
    
    for ATL in LVloadlist: # update loop. separate because this way the count goes through all the LV before updating
        # ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' P_n ' + str(p_diff) + ' 0')
#         update_emergency_level(t, ATL, level_diff)
        update_neighbourhood_help(t-0.1, ATL, v_diff, p_diff)
        
ram.endSim()
#%% Plots

pyramses.extractor(case.getTrj()).getInj('TL11').F_hlp_p_m2.plot()
pyramses.extractor(case.getTrj()).getInj('TL11').dPnb.plot()

## Plot L0 power
ext = pyramses.extractor(case.getTrj())
# open Monte Carlo results 1
# ext = pyramses.extractor('out1.trj')
plt.style.use("ggplot")

# disturbance
ext.getInj('L0').P.plot()
ext.getInj('L0').Q.plot()



## TN node overview
fig = plt.figure(None, (10,6))
ax1, ax2, ax3 = fig.subplots(3,1, sharex = True)

w_coi = ext.getSync('G').SC
w = ext.getSync('G').S
pg = ext.getSync('G').P
qg = ext.getSync('G').Q

pt = ext.getBranch('MAINTR').PF
qt = ext.getBranch('MAINTR').QF

ax1.plot(w_coi.time, w_coi.value*50+50, label='w_coi')
ax1.plot(w.time, w.value*50, label='w')
ax2.plot(pg.time, pg.value, label='grid')
ax2.plot(pt.time, pt.value, label='transformer')
ax3.plot(qg.time, qg.value, label='grid')
ax3.plot(qt.time, qt.value, label='transformer')

ax1.legend(loc="upper right")
ax2.legend(loc="upper right")
ax3.legend(loc="upper right")
ax1.set_title('Frequency')
ax2.set_title('Active Power')
ax3.set_title('Reactive Power')

plt.savefig('TN.pdf')  



## Loads
loads = ['L1','L11', 'L15', 'L16', 'L17', 'L18']
fig = plt.figure(None, (10,6))
ax1, ax2 = fig.subplots(2,1, sharex = True)

for load in loads:
    p = ext.getInj(load).P
    q = ext.getInj(load).Q
    ax1.plot(p.time, p.value*1000, label=load)
    ax2.plot(q.time, q.value*1000, label=load)

    ax1.legend(loc="upper right")
    ax2.legend(loc="upper right")
    ax1.set_title('Active Power in kW')
    ax2.set_title('Reactive Power in kW')
    
    plt.savefig('Loads.pdf')
    
    
## Voltages
buses = ['N0','N1', 'N11','N15' ,'N16', 'N17', 'N18']
fig = plt.figure(None, (10,6))
ax1 = fig.subplots(1,1, sharex = True)

for bus in buses:
    v = ext.getBus(bus).mag
    ax1.plot(v.time, v.value, label=bus)
    ax1.legend(loc="upper left")
    ax1.set_title('Voltage Magnitude')

plt.savefig('Vn.pdf')  


##ATLs 
atls = ['TL11', 'TL15', 'TL16', 'TL17', 'TL18'] 
# atls = ['TL11']
# atls = ['TL1']
fig = plt.figure(None, (10,6)) 
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3,2,figsize=(10,8), sharex=True)

for tl in atls: 
    p = ext.getInj(tl).P
    punit = ext.getInj(tl).Punit
    pref =  ext.getInj(tl).Pref
    q = ext.getInj(tl).Q
    wm = ext.getInj(tl).wm
    f = ext.getInj(tl).f
    df = ext.getInj(tl).rocof 
    status = ext.getInj(tl).status
    wm_ref = ext.getInj(tl).wm_ref_lim

    ax1.plot(p.time, p.value*1000, label=tl)
#     ax1.plot(punit.time, punit.value, label=tl)
#     ax1.plot(pref.time, pref.value, label=tl)
    ax1.legend(loc="upper right")
    ax1.set_title('Active Power in kW')
    ax2.plot(q.time, q.value*1000, label=tl)
    ax2.legend(loc="upper right")
    ax2.set_title('Reactive Power in kW')
    ax3.plot(wm.time, wm.value, label=tl)
#     ax3.plot(wm_ref.time, wm_ref.value, label=tl)
    ax3.legend(loc="upper right")
    ax3.set_title('Rotational Speed')
    ax4.plot(df.time, df.value, label=tl)
    ax4.legend(loc="upper right")
    ax4.set_title('rocof')
    ax5.plot(f.time, f.value, label=tl)
    ax5.legend(loc="upper right")
    ax5.set_title('Frequency in Hz')
    ax6.plot(status.time, status.value, label=tl)
    ax6.legend(loc="upper right")
    ax6.set_title('Status')
    
plt.savefig('ATL.pdf')  



## protection tripping
# ffl = ext.getInj('TL11').Ffl
# ffh = ext.getInj('TL11').Ffh
# ffr =ext.getInj('TL11').Ffr
# fvl =ext.getInj('TL11').Fvl
# fvh =ext.getInj('TL11').Fvh
status = ext.getInj('TL17').status

# Pref_lim = ext.getInj('TL17').Plim
P = ext.getInj('TL17').Punit

wm_ref = ext.getInj('TL17').wm_ref_lim
wm = ext.getInj('TL17').wm



fig = plt.figure(None, (10,6))
ax1, ax2, ax3 = fig.subplots(3,1, sharex=True)

ax1.plot(status.time, status.value, label='status')
# ax1.set_xlim([1.5,2.1])

ax1.set_title('status')
ax2.set_title('separate functions')
ax3.set_title('active power limiter')
# ax3.set_title('rotational speed limiter')

ax1.legend(loc="upper right")


# individual functions
# ax2.plot(ffl.time, ffl.value, label='underfrequency')
# ax2.plot(ffh.time, ffh.value, label='overfrequency')
# ax2.plot(ffr.time, ffr.value, label='rocof')
# ax2.plot(fvl.time, fvl.value, label='undervoltage')
# ax2.plot(fvh.time, fvh.value, label='overvoltage')
# ax2.legend(loc="upper right")

# active power limit
ax3.plot(P.time, P.value, label='P')
# ax3.plot(Pref_lim.time, Pref_lim.value, label='Preflim')

ax2.plot(wm_ref.time, wm_ref.value, label='wm_ref')
# ax2.plot(wm_ref_lim.time, wm_ref_lim.value, label='wm_ref_lim')
ax2.plot(wm.time, wm.value, label='wm')
ax2.legend(loc="upper right")

ax3.legend(loc="upper right")


##VSCs 
vscs = ['PV11','PV15', 'PV16','PV17', 'PV18'] 
fig = plt.figure(None, (10,6)) 
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2,figsize=(10,8), sharex=True)

for vsc in vscs: 
    p = ext.getInj(vsc).Pgen 
    q = ext.getInj(vsc).Qgen 
    w = ext.getInj(vsc).fmfilt
    df = ext.getInj(vsc).rocof
    #Fvl = ext.getInj(vsc).Fvl

    ax1.plot(p.time, p.value*1000, label=vsc)
    ax1.legend(loc="upper right")
    ax1.set_title('Active Power in kW')
    ax2.plot(q.time, q.value*1000, label=vsc)
    ax2.legend(loc="upper right")
    ax2.set_title('Reactive Power in kW')
    ax3.plot(w.time, w.value, label=vsc)
    ax3.legend(loc="upper right")
    ax3.set_title('Filtered Frequency')
    ax4.plot(df.time, df.value, label=vsc)
    ax4.legend(loc="upper right")
    ax4.set_title('Rocof')
    
plt.savefig('PV.pdf')  


#vscs = ['PV11']
atls=['TL18', 'TL1']
fig = plt.figure(None, (10,6)) 
fig, ((ax1)) = plt.subplots(1,1,figsize=(10,8), sharex=True)

for vsc in vscs: 
    f = ext.getInj(vsc).fmfilt
    ax1.plot(f.time, f.value, label=vsc)

for tl in atls:
    f = ext.getInj(tl).f
    ax1.plot(f.time, f.value, label = tl)
w_coi = ext.getSync('G').S
ax1.plot(w_coi.time, w_coi.value*50, label='w_coi')
ax1.legend(loc="upper right")
ax1.set_title('Frequency')

# ax1.set_xlim([0, 15])



## Induction machines
ims = ['IM1','IM11', 'IM15', 'IM16', 'IM17', 'IM18'] 
fig = plt.figure(None, (10,6)) 
fig, ((ax1)) = plt.subplots(1,1,figsize=(10,8), sharex=True)

for im in ims: 
    p = ext.getInj(im).P 

    ax1.plot(p.time, p.value*1000, label=im)
    ax1.legend(loc="upper right")
    ax1.set_title('Active Power in kW')
    
plt.savefig('IM.pdf')  


## TN node overview
lines = ['TN-N0','N3-N4', 'N4-N5', 'N6-N7', 'N9-N10']
fig = plt.figure(None, (10,6))
ax1, ax2 = fig.subplots(2,1, sharex = True)


for line in lines:
    pt = ext.getBranch(line).PF
    qt = ext.getBranch(line).QF

    ax1.plot(pt.time, pt.value*1000, label=line)
    ax2.plot(qt.time, qt.value*1000, label=line)

ax1.legend(loc="upper right")
ax2.legend(loc="upper right")

ax1.set_title('Active Power')
ax2.set_title('Reactive Power')

plt.savefig('Lines.pdf')  



## Get minimum and maximum frequency
w_coi = ext.getSync('G').S

fmax = w_coi.value[np.argmax(w_coi.value)]*50
fmin = w_coi.value[np.argmin(w_coi.value)]*50

print('min frequency: '+str(fmin))
print('max frequency: '+str(fmax))


# ROCOF
from numpy import diff
df =  diff(w_coi.value)/diff(w_coi.time)
plt.plot(w_coi.time,np.gradient(w_coi.value*50,w_coi.time))

df = np.gradient(w_coi.value*50,w_coi.time)
dfmax = df[np.nanargmax(df)]
dfmin = df[np.nanargmin(df)]

print('min rocof: '+str(dfmin))
print('max rocof: '+str(dfmax))


