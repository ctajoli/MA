import pyramses
import cmath
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition, mark_inset, zoomed_inset_axes)
from matplotlib import rc
import pandas as pd
import datetime
import seaborn as sns
import json, codecs
from save_figure import save_figure


# Set plot options
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
cm = 1/2.54
sns.set_context("paper", font_scale = 0.7, rc={"grid.linewidth": 0.6})
rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
rc('font', **{'family': 'serif', 'serif': ['Palatino']})
rc('text', usetex=True)
palette = ["#1269b0","#a8322d", "#91056a", '#edb120','#72791c', '#6f6f64', '#007a96', '#1f407a','#485a2c']
sns.set_palette(palette)
# sns.cubehelix_palette(start=.5, rot=-.5, as_cmap=True)
names = ['weak', 'strong']

def plot_strong_weak(types, directory, n_var, fig_name):
    """
    Plots different trj files in comparison, for a weak grid on the left and a strong one on the right. 
    The trj files need to be calles 'strong_<type>' or 'weak_<type>'
    """
    
    fig,axs = plt.subplots(n_var,len(names), figsize = (20,2*3), sharex='col', sharey = 'row')
    # fig.suptitle("\detokenize{"+ fig_title +"}")
    fig.subplots_adjust(hspace=0.15, wspace = 0.05)
    axs = axs.ravel()
    


    for typ in types:
        for j, name in enumerate(names):
            ext=pyramses.extractor(directory + name + '_' + typ + ".trj")
            # print(name + '_' + typ)
            data = pd.DataFrame(data = ext.getSync('G').S.time, columns=["time"])
            # data["$P_unit$"] = ext1[-1].getInj('TL1').Punit.value
            # data["$wm_ref$"] = ext1[-1].getInj('TL1').wm_ref_lim.value
            data["$f$ in Hz"] = (ext.getSync('G').S.value)*50
            # data["$i_Q$"] = ext1[-1].getInj('TL1').iQ.value
            # data["$i_P$"] = ext1[-1].getInj('TL1').iP.value
            data["$v$ in p.u."] = ext.getBus('114115').mag.value
            # data["$dQ_sum$"] = ext1[-1].getInj('TL1').dQ_sum.value
            for i, ax in enumerate(axs):
                if i%2 == 1 and j%2 == 1: # right graph and strong 
                    sns.lineplot(ax=ax, y=data.columns[int(i/2 + 1)], x="time", data=data,linewidth=0.5, label=typ, legend = False)
                elif i%2 != 1 and j%2 != 1: # left graph and weak
                    sns.lineplot(ax=ax, y=data.columns[int(i/2 + 1)], x="time", data=data,linewidth=0.5, label=typ, legend = False)
                
        #       ax = sns.lineplot(ax=ax2, y="iP", x="time", data=data,linewidth=0.5, zorder = z_order)
                ax.set_xlim([1.75,7.25])
         
                # if i%2 == 1:
                #     ax.yaxis.set_label_position("right")
    #     # ext2.append(pyramses.extractor(directory+ "ki_p hyst,kpp1,5\\" + names_hyst[j]+".trj"))
    #     # data = pd.DataFrame(data = ext2[-1].getSync('G').S.time, columns=["time"])
    #     # data["Punit"] = ext2[-1].getInj('TL1').Punit.value
    #     # data["wmref"] = ext2[-1].getInj('TL1').wm_ref_lim.value
    #     # data["f"] = ext2[-1].getSync('G').S.value
    #         ax = sns.lineplot(ax=ax3, y="dQsum", x="time", data=data,linewidth=0.5, label=round(var,3), zorder = z_order)
    #         ax = sns.lineplot(ax=ax4, y="iQ", x="time", data=data,linewidth=0.5, zorder = z_order)
        # If z_order is +=1 the line on top is the last one. If z_order -= 1 it is the first one
        # z_order -= 1
    fig.align_ylabels(axs[:])
    axs[1].legend(loc = 'upper right', bbox_to_anchor=(1, 1.1), ncol=len(types))
    axs[0].set_title('Weak grid')
    axs[1].set_title('Strong grid')
    parameters_left = '\n'.join(('H = 1.5 s',
                                 '$\Delta P$ = -1.5 MW',
                                 "ATLs' load share = 20\%"
                                ))
    parameters_right = '\n'.join(('H = 6 s',
                                  '$\Delta P$ = -1.5 MW',
                                  "ATLs' load share = 20\%"
                                 ))
    props = dict(boxstyle='square', facecolor='white', alpha=0.5)
    axs[0].text(0.98, 0.95,parameters_left, bbox=props, transform=axs[0].transAxes, ha = 'right', va = 'top')
    axs[1].text(0.98, 0.95,parameters_right, bbox=props, transform=axs[1].transAxes, ha = 'right', va = 'top')
    
    save_figure(directory, fig_name, fig)

#     str_i = "" 
#     i = 0
    
#     while True:
#         if not os.path.exists(directory + fig_name + str_i + '.png'):
#             fig.savefig(directory + fig_name + str_i + '.png',bbox_inches='tight', dpi = 300)
            
#             break
#         else:
#             i += 1
#             str_i = " " + str(i) 