import os
def save_figure(directory, fig_name, fig):
    str_i = "" 
    i = 0
    while True:
        if not os.path.exists(directory + fig_name + str_i + '.png'):
            fig.savefig(directory + fig_name + str_i + '.png',bbox_inches='tight', dpi = 300)
            
            break
        else:
            i += 1
            str_i = " " + str(i) 