import pandas as pd
import numpy as np  

def write_load_file(artere, model, typ, bus_name, params_LV, P, Q, variable_parameters, Sb=None, pf=None, init_values = None):
    """
    artere = str, 'artere-' or ''
    typ = str, for example 'TL' 
    bus_name = numpy array [len(MV)*len(LV),]
    params_LV = array from excel, starting after P and Q (column 8)
    P = numpy array [len(MV)*len(LV),]
    Q = numpy array [len(MV)*len(LV),]
    variable_parameters = dictionary with the ranges of min and max of the parameters to randomize
    Sb, pf = numpy array [len(MV)*len(LV),], only for ATLs
    init_values = dictionary of np array [len(MV)*len(LV),], only for ATLs
    """
    with open(artere + typ +'_list_test.dat', 'w') as f:
    
        for i, bus in enumerate(bus_name):
            name = typ + str("{0:.0f}".format(bus))
            parameters = str(' ')
            for param in params_LV[:-1]: # skips the ';', since it would be recognized as string
                if isinstance(param, str): # if the parameter is a string the bus number is added to the string to be later defined individually. 
                    parameters = parameters + param + '_' + name + ' '
                else:
                    parameters = parameters + str(param) + ' '
            parameters = parameters + ';' # the ';' is added again
            # f.write('INJEC ATLv4 ' + ATL_name + ' ' + str(bus) + ' 0 0 ' + str(P_ATL[i]) + ' ' + str(Q_ATL[i]) + ' ' + '0.006' + parameters + '\n')
            f.write('INJEC ' + model + ' ' + name + ' {:.0f} 0 0 {:.5f} {:.5f}'.format(bus, P[i], Q[i]) + parameters + '\n')

    with open(artere + typ + '_list_test.dat', 'r') as file:
        filedata = file.read()
        # filedata = f.read()
        for i, bus in enumerate(bus_name):
            name = typ + str("{0:.0f}".format(bus))
            if isinstance(Sb, np.ndarray):
                filedata = filedata.replace('Sb_' + name, str("{:.5f}".format(Sb[i])))
            if isinstance(pf, np.ndarray):
                filedata = filedata.replace('pf_' + name, str("{:.3f}".format(pf[i])))
            if isinstance(init_values, dict):
                for init_val in init_values:
                    filedata = filedata.replace(init_val + '_' + name, str("{:.0f}".format(init_values[init_val][i])))
            for var_par in variable_parameters:
                filedata = filedata.replace(var_par + '_' + name, str("{:.3f}".format(np.random.uniform(variable_parameters[var_par][0],variable_parameters[var_par][1]))))
    with open(artere + typ + '_list_test_out.dat', 'w') as file:
        file.write(filedata)

def convert_to_pu(par_perc, P_max, P_min):
    """It converts the power parameter given in percentage of max-min range to the per unit value"""
    par_pu = par_perc * (P_max - P_min) + P_min
    return par_pu

def check_sign(pl_check):
    pl = np.zeros(len(pl_check))
    for i in range(len(pl)):
        pl[i] = 1 if pl_check[i] >= 0 else 0
    return pl




file = 'Extended model.xlsx'
xl = pd.ExcelFile(file)
artere_check = False
artere = 'artere-' if artere_check == True else ''

rand_check = True
rand = ' rand' if (rand_check and not artere_check) else '' 

ATL_model = 'ATLv5'
# ATL_model = 'ATL'
# MV section
MV_buses_loads = xl.parse('MV imp') ['busname']
P_IMP_MV = xl.parse('MV imp') ['P']
Q_IMP_MV = xl.parse('MV imp') ['Q']
P_IND_MV = xl.parse('MV ind') ['P']
Q_IND_MV = xl.parse('MV ind') ['Q']
P_PV_MV = xl.parse('MV PV') ['P']
Q_PV_MV = xl.parse('MV PV') ['Q']
P_ATL_MV = xl.parse('MV ATL') ['P']
# LV section
LV_buses_loads = xl.parse(artere+'LV imp') ['busname']
LV_buses_PV = xl.parse(artere+'LV PV') ['busname']
P_IMP_LV = xl.parse(artere+'LV imp') ['P']
Q_IMP_LV = xl.parse(artere+'LV imp') ['Q']
P_IND_LV = xl.parse(artere+'LV ind') ['P']
Q_IND_LV = xl.parse(artere+'LV ind') ['Q']
P_PV_LV = xl.parse(artere+'LV PV') ['P']
Q_PV_LV = xl.parse(artere+'LV PV') ['Q']
# P_ATL_LV = xl.parse(artere+'LV ATLv4') ['P']
P_ATL_LV = xl.parse(artere+'LV ' + ATL_model) ['P']
# Sb_old_ATL_LV = xl.parse(artere+'LV ATLv4') ['Sb']
Sb_old_ATL_LV = xl.parse(artere+'LV ' + ATL_model) ['Sb']

# params_ATL_LV = xl.parse(artere+'LV ATLv4'+rand).iloc[1][8:]
params_ATL_LV = xl.parse(artere+'LV ' + ATL_model + rand).iloc[1][8:]
params_IMP_LV = xl.parse(artere+'LV imp' + rand).iloc[1][8:]
params_IND_LV = xl.parse(artere+'LV ind' + rand).iloc[1][8:]


#Total MV load
P_load_MV = P_IMP_MV + P_IND_MV + P_ATL_MV
Q_load_MV = Q_IMP_MV + Q_IND_MV

#Total LV load
P_load_LV = P_IMP_LV + P_IND_LV + P_ATL_LV
Q_load_LV = Q_IMP_LV + Q_IND_LV


# Shares by bus
P_bus_LV_share = P_load_LV/sum(P_load_LV)
Q_bus_LV_share = Q_load_LV/sum(Q_load_LV)
P_bus_LV_share_PV = P_PV_LV/sum(P_PV_LV)

P_bus_tot = np.zeros(len(MV_buses_loads)*len(LV_buses_loads),)
Q_bus_tot = np.zeros(len(MV_buses_loads)*len(LV_buses_loads),)
bus_name = np.zeros(len(MV_buses_loads)*len(LV_buses_loads),)
count = 0

for i, MVbus in enumerate(MV_buses_loads):
    for j, LVbus in enumerate(LV_buses_loads):
        bus_name[count] = int(100 * MVbus + LVbus)
        P_bus_tot[count] = P_load_MV[i]*P_bus_LV_share[j]
        Q_bus_tot[count] = Q_load_MV[i]*Q_bus_LV_share[j]
        count += 1

np.random.seed(1)
P_ind_share_min = 0.001
P_ind_share_max = 0.1
P_ind_share = np.random.rand(len(P_bus_tot),) * (P_ind_share_max - P_ind_share_min) + P_ind_share_min
Q_ind_share_min = 0.001
Q_ind_share_max = 0.1
Q_ind_share = np.random.rand(len(Q_bus_tot),) * (Q_ind_share_max - Q_ind_share_min) + Q_ind_share_min
P_ATL_share_min = 0.1
P_ATL_share_max = 0.3
P_ATL_share = np.random.rand(len(P_bus_tot),) * (P_ATL_share_max - P_ATL_share_min) + P_ATL_share_min
pf_ATL_min = 0.95
pf_ATL_max = 1
pf_ATL = np.random.rand(len(P_bus_tot),) * (pf_ATL_max - pf_ATL_min) + pf_ATL_min
sb_ATL_factor_min = 1/0.95 # the active power is between 0.3 and 0.95 of the nominal power.
sb_ATL_factor_max = 1/0.4
sb_ATL_factor = np.random.rand(len(P_bus_tot),) * (sb_ATL_factor_max - sb_ATL_factor_min) + sb_ATL_factor_min


P_ind = P_bus_tot * P_ind_share
Q_ind = Q_bus_tot * Q_ind_share
P_ATL = P_bus_tot * P_ATL_share
Q_ATL = (P_ATL / pf_ATL) * np.sqrt(1-np.square(pf_ATL))
Sb_ATL = -P_ATL * sb_ATL_factor
P_imp = P_bus_tot - (P_ind + P_ATL)
Q_imp = Q_bus_tot - (Q_ind + Q_ATL)

if ATL_model == 'ATLv5':
    p_unit = -P_ATL/Sb_ATL # equal to Pref_lim in codegen at t = 0
    P_max = params_ATL_LV['P_max']
    P_min = params_ATL_LV['P_min']
    pl1_2_check = p_unit - convert_to_pu(params_ATL_LV['l_1_neg_min_par'], P_max, P_min)
    pl1_4_check = convert_to_pu(params_ATL_LV['l_1_neg_max_par'], P_max, P_min) - p_unit
    pl1_6_check = p_unit - convert_to_pu(params_ATL_LV['l_1_pos_min_par'], P_max, P_min)
    pl1_8_check = convert_to_pu(params_ATL_LV['l_1_pos_max_par'], P_max, P_min) - p_unit
    pl1_2_0 = check_sign(pl1_2_check)
    pl1_4_0 = check_sign(pl1_4_check)
    pl1_6_0 = check_sign(pl1_6_check)
    pl1_8_0 = check_sign(pl1_8_check)

    pl2_2_check = p_unit - convert_to_pu(params_ATL_LV['l_2_neg_min_par'], P_max, P_min)
    pl2_4_check = convert_to_pu(params_ATL_LV['l_2_neg_max_par'], P_max, P_min) - p_unit
    pl2_6_check = p_unit - convert_to_pu(params_ATL_LV['l_2_pos_min_par'], P_max, P_min)
    pl2_8_check = convert_to_pu(params_ATL_LV['l_2_pos_max_par'], P_max, P_min) - p_unit
    pl2_2_0 = check_sign(pl2_2_check)
    pl2_4_0 = check_sign(pl2_4_check)
    pl2_6_0 = check_sign(pl2_6_check)
    pl2_8_0 = check_sign(pl2_8_check)

    Vdb_p = params_ATL_LV['Vdb_p']
    Vdb_m = params_ATL_LV['Vdb_m']
    lines = [[]]
    lines_in = []
    complete_bus_list = []
    V = np.zeros(len(bus_name))
    F_help_high_0 = np.zeros(len(bus_name))
    F_help_low_0 = np.zeros(len(bus_name))
    with open('base.dat','r') as f:
        for l in f:
            line = l
            lines.append(line.split())
    for i in range(len(lines)):
        if len(lines[i]) > 3:
            lines_in.append(lines[i]) 
    j = 0
    for i in range(len(lines_in)):
        if lines_in[j][0] != 'LFRESV':
            lines_in.pop(j)
        else: j +=1
    for i in range(len(lines_in)):
        complete_bus_list.append(lines_in[i][1])
    for i, bus in enumerate(bus_name):
        position = complete_bus_list.index(str("{0:.0f}".format(bus)))
        V[i] = lines_in[position][2]

        F_help_high_0[i] = 1 if V[i] >= Vdb_p else 0
        F_help_low_0[i] = 1 if V[i] <= Vdb_m else 0



    init_values_ATL = {
        'pl1_2_0': pl1_2_0,
        'pl1_4_0': pl1_4_0,
        'pl1_6_0': pl1_6_0,
        'pl1_8_0': pl1_8_0,
        'pl2_2_0': pl2_2_0,
        'pl2_4_0': pl2_4_0,
        'pl2_6_0': pl2_6_0,
        'pl2_8_0': pl2_8_0,

        'F_help_high_0': F_help_high_0,
        'F_help_low_0': F_help_low_0
    }

variable_parameters_ATL = {
    'ra': [0.01, 0.1],
    'H': [0.03, 0.5],
    'b': [0.0005, 0.002],
    'rt': [0.005, 0.05],
    'lt': [0.1, 0.9]
}
variable_parameters_IMP = {
    'alpha1': [1, 2],
    'beta1': [1.5, 3]
}
variable_parameters_IND = {
    'Rs': [0.03, 0.13],
    'RR': [0.03, 0.13],
    'LSR': [2.5, 4],
    'LLS': [0.07, 0.15],
    'LLR': [0.06, 0.15],
    'H': [0.4, 0.6],    
    'LF': [0.4, 0.6]
}
variable_parameters_PV = {
    'Iprate': [0.2, 0.5],
    'Tg': [0.1, 0.3],
    'tau': [50, 100]
}

if ATL_model == 'ATLv5':
    write_load_file(artere, ATL_model, 'ATL', bus_name, params_ATL_LV, P_ATL, Q_ATL, variable_parameters_ATL, Sb_ATL, pf_ATL, init_values_ATL)
else:
    write_load_file(artere, ATL_model, 'ATL', bus_name, params_ATL_LV, P_ATL, Q_ATL, variable_parameters_ATL, Sb_ATL, pf_ATL)

write_load_file(artere, 'LOAD', 'IMP', bus_name, params_IMP_LV, P_imp, Q_imp, variable_parameters_IMP)
write_load_file(artere, 'INDMACH1', 'IND', bus_name, params_IND_LV, P_ind, Q_ind, variable_parameters_IND)


    
    
    
    
    
    
    