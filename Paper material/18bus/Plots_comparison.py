#!/usr/bin/env python
# coding: utf-8

# # Import

# In[1]:


import pyramses
# import cmath
# import math
# import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1.inset_locator import (
#     inset_axes,
#     InsetPosition,
#     mark_inset,
#     zoomed_inset_axes,
# )
from matplotlib import rc
import pandas as pd
import datetime
import seaborn as sns
import os


# # Functions

# In[2]:


def save_figure(fig, directory, fig_name):
    str_i = ""
    i = 0
    while True:
        if not os.path.exists(directory + fig_name + str_i + ".png"):
            fig.savefig(
                directory + fig_name + str_i + ".png",
                bbox_inches="tight", dpi=300
            )

            break
        else:
            i += 1
            str_i = " " + str(i)


# # Plot options

# In[3]:


# Set plot options
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
cm = 1 / 2.54
sns.set_context("paper", font_scale=0.7, rc={"grid.linewidth": 0.6})
rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
rc("font", **{"family": "serif", "serif": ["Palatino"]})
rc("text", usetex=True)
palette1 = [
    "#1269b0",
    "#a8322d",
    "#edb120",
    "#72791c",
    "#91056a",
    "#6f6f64",
    "#007a96",
    "#1f407a",
    "#485a2c",
]
palette = [
    "#1269b0",
    "#a8322d",
    "#91056a",
    "#edb120",
    "#72791c",
    "#6f6f64",
    "#007a96",
    "#1f407a",
    "#485a2c",
]
sns.set_palette(palette1)


# # Data options

# In[4]:


# TIME DOMAIN PLOTS
today = datetime.date.today()
date = '2022-07-13'
directory = ".\\" + date + "\\"
fig_name = "Base 18"

names = [""]
types = ["normal", "noJump", "noVhelp"]
variables = ["time [s]", "$f$ [Hz]", "$v$ [pu]", "$P$ [kW]", "$Q$ [kW]"]

events_left = {
    "Power drop": 2,
    "LoE = 1": 2.5,
    "2 units shut down (centralized control)": 3.65,
    "2 units shut down (droop control)": 5.1,
}
events_right = {
    "Power drop": 2,
    "$LoE = 3$, $V_n = -4$": 2.5,
    "$LoE = 2$, $P_n = -1$, No-help ridethrough end": 3,
    "$V_n = 0$": 3.5,
}
parameters_left = "\n".join(
    (
        "H = 1.5 s",
        r"$\Delta P$ = -1.5 MW",
        # "ATLs' load share = 20\%"
    )
)
parameters_right = "\n".join(
    ("H = 1.5 s", r"$\Delta P$ = -0.25 MW", r"ATLs' load share = 15\%")
)


# # Data extraction

# In[5]:


ext = {}
data = pd.DataFrame()
# for typ in types:
#     ext[typ] = pyramses.extractor(directory + typ + ".trj")


A = []
B = []
C = variables * 6
D = []
for name in names:
    for var in variables:
        for typ in types:
            A += [name]
for name in names:
    for typ in types:
        for var in variables:
            B += [typ]

for name in names:
    for typ in types:
        filename = name + "_" + typ
        ext = pyramses.extractor(directory + filename + ".trj")
        D.append(ext.getSync("G").S.time)
        D.append(ext.getSync("G").S.value * 50)
        D.append(ext.getBus("N15").mag.value)
        D.append(ext.getInj("TL15").P.value * 1000)
        D.append(ext.getInj("TL15").Q.value * 1000)
        # D.append(ext.getInj('PV15').Pgen.value*1000)

df = pd.DataFrame(zip(A, B, C, D), columns=["name", "type", "value", "data"])
df.set_index(["name", "type", "value"], inplace=True)
df


# # Plotting

# In[6]:


fig, axs = plt.subplots(
    len(variables) - 1,
    1,
    figsize=(20, (len(variables) - 1) * 3),
    sharex="col",
    sharey=False,
)
# fig.suptitle("\detokenize{"+ fig_title +"}")
fig.subplots_adjust(hspace=0.15, wspace=0.07)
axs = axs.ravel()

for typ in types:
    # for i, var in enumerate(variables[1:]):
    #     sns.lineplot(ax=axs[i], y=var, x=variables[0],
    #                   data=df['data'][''][typ],
    #                   linewidth=0.5, label=typ, legend = False)
    sns.lineplot(
        ax=axs[0],
        y=variables[1],
        x=variables[0],
        data=df["data"][""][typ],
        linewidth=0.5,
        label=typ,
        legend=False,
    )
    sns.lineplot(
        ax=axs[1],
        y=variables[2],
        x=variables[0],
        data=df["data"][""][typ],
        linewidth=0.5,
        label=typ,
        legend=False,
    )
    sns.lineplot(
        ax=axs[2],
        y=variables[3],
        x=variables[0],
        data=df["data"][""][typ],
        linewidth=0.5,
        label=typ,
        legend=False,
    )
    sns.lineplot(
        ax=axs[3],
        y=variables[4],
        x=variables[0],
        data=df["data"][""][typ],
        linewidth=0.5,
        label=typ,
        legend=False,
    )

# for event, time in events_left.items():
#     for ax in axs[0],axs[2]:
#         ax.axvline(x = time, color = 'grey', linewidth=0.5)
# axs[0].text(time+0.05, 50, event)
# to correct the height for the data present.
for event, time in events_right.items():
    for ax in axs[0], axs[2], axs[1], axs[3]:
        ax.axvline(x=time, color="grey", linewidth=0.5)
    axs[0].text(time + 0.02, 50, event)
    # to correct the height for the data present.
# axs[2].set_xlim([1.75,5.25])
axs[0].set_xlim([1.75, 5.25])
fig.align_ylabels(axs[:])
axs[0].legend(loc="upper right", bbox_to_anchor=(1, 1), ncol=len(types))
# axs[0].set_title('Weak grid')
# axs[1].set_title('Strong grid')

props = dict(boxstyle="square", facecolor="white", alpha=0.5)
# axs[0].text(0.98, 0.95,parameters_left, bbox=props,
#             transform=axs[0].transAxes, ha = 'right', va = 'top')
axs[0].text(
    0.995,
    0.87,
    parameters_right,
    bbox=props,
    transform=axs[0].transAxes,
    ha="right",
    va="top",
)
save_figure(fig, directory, fig_name)


# In[7]:


# names = ['SC_atl_noDroop']
# # Loop over some different simulation results stored in different trj files
# fig1, (ax2,ax3,ax4) = plt.subplots(3,1, figsize =(9*cm,12.5*cm), sharex=True)

# for name in names:
#     # Plot results for changing load factor
#     ext = pyramses.extractor(out_dir+name+".trj")
#     data = pd.DataFrame(data = ext.getSync('G').S.time-2, columns=["time"])
# #     data["pt"] = ext.getBranch('TxMAIN').PF.value*1000
#     data["wm"] = ext.getInj('TL11').wm.value
#     data["patl"] = ext.getInj('TL11').P.value*1000
#     data["f"] = ext.getSync('G').S.value*50
# #     ax1 = sns.lineplot(ax=ax1, y="pt", x="time", data=data,linewidth=0.5)
#     ax2 = sns.lineplot(ax=ax2, y="wm", x="time", data=data,linewidth=0.5)
#     ax3 = sns.lineplot(ax=ax3, y="patl", x="time", data=data,linewidth=0.5)
#     ax4 = sns.lineplot(ax=ax4, y="f", x="time", data=data,linewidth=0.5)

#     # Plot results for ATL Share


# # Axis labels
# # ax1.set_ylabel(r'$P_t$ in kW')
# ax2.set_ylabel(r'$\omega_m$ in p.u.')
# ax3.set_ylabel(r'$P_{atl}$ in kW')
# ax4.set_ylabel(r'$f$ in Hz')
# ax4.set_xlabel(r"time in s")


# # Axis limits
# # ax1.set_xlim([-0.25,5.25])
# ax2.set_xlim([-0.25,5.25])
# ax3.set_xlim([-0.25,5.25])
# ax4.set_xlim([-0.25,5.25])

# #     #Add labels to each plot
# #     props = dict(boxstyle='square', facecolor='white', alpha=0.5)

# ax1.text(0.98, 0.1,'load factor changes', bbox=props,
#          transform=ax1.transAxes, ha = 'right', va='bottom')
# ax2.text(0.98, 0.1,'base power changes', bbox=props,
#          transform=ax2.transAxes, ha = 'right', va = 'bottom')
# ax3.text(0.98, 0.1,'ATL load share changes', bbox=props,
#          transform=ax3.transAxes, ha = 'right',va = 'bottom')
# ax4.text(0.98, 0.1,'all load shares change', bbox=props,
#          transform=ax4.transAxes, ha = 'right',va = 'bottom')


# fig1.tight_layout()
# fig1.savefig('NameFigur.pdf',bbox_inches='tight')
