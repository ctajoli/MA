#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pyramses
import numpy as np
import datetime
import matplotlib.pyplot as plt
from PIL import Image

today = datetime.date.today()
out_dir = ".\\" + str(today) + "\\"
ext = pyramses.extractor(out_dir+"out.trj")
plt.style.use("ggplot")


# In[9]:

img = Image.open(out_dir + "Loads 18 28.png")
w, h = img.size
print(w)


# In[ ]:


# ATLs
atls = ["TL11", "TL15", "TL16", "TL17", "TL18"]
# atls = ['TL1']
# atls = ['TL17']
# atls = ['TL11']
# atls = ['TL18']
fig = plt.figure(None, (10, 6))
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = plt.subplots(
    4, 2, figsize=(10, 8), sharex=True
)

for tl in atls:
    p = ext.getInj(tl).P
    punit = ext.getInj(tl).Punit
    q = ext.getInj(tl).Q
    qunit = ext.getInj(tl).Qunit
    iP = ext.getInj(tl).iP
    iQ = ext.getInj(tl).iQ
    ix = ext.getInj(tl).ix
    iy = ext.getInj(tl).iy

    ax1.plot(p.time, p.value * 1000, label=tl)
    ax1.legend(loc="upper right")
    ax1.set_title("Active Power in kW")

    ax2.plot(q.time, q.value * 1000, label=tl)
    ax2.legend(loc="upper right")
    ax2.set_title("Reactive Power in kW")

    ax3.plot(punit.time, punit.value, label=tl)
    ax3.legend(loc="upper right")
    ax3.set_title("Active Power in pu")

    ax4.plot(qunit.time, qunit.value, label=tl)
    ax4.legend(loc="upper right")
    ax4.set_title("Reactive Power in pu")

    ax5.plot(iP.time, iP.value, label=tl)
    ax5.legend(loc="upper right")
    ax5.set_title("active current")

    ax6.plot(iQ.time, iQ.value, label=tl)
    ax6.legend(loc="upper right")
    ax6.set_title("reactive current")

    ax7.plot(ix.time, ix.value, label=tl)
    ax7.legend(loc="upper right")
    ax7.set_title("ix")

    ax8.plot(iy.time, iy.value, label=tl)
    ax8.legend(loc="upper right")
    ax8.set_title("iy")

    ax5.set_xlim([1.4, 3.6])
    # ax1.set_ylim([0.1, 0.5])
plt.savefig(out_dir + r"\ATL.pdf")


# In[ ]:

list1 = ["active"]
for lis in list1:
    print(lis)


# In[7]:

np.arange(48.5, 50.5, 0.5)
