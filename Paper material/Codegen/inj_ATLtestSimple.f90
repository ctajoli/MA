!  MODEL NAME : inj_ATLtestSimple       
!  MODEL DESCRIPTION FILE : ATLtestSimple.txt
!  Data :
!       prm(  1)=  Sb                              ! base power of the unit
!       prm(  2)=  Sbs                             ! base power of the system
!       prm(  3)=  vdc_star                        ! DC link voltage reference
!       prm(  4)=  pf                              ! setpoint for power factor at the terminal
!       prm(  5)=  tau                             ! Pll time constant
!       prm(  6)=  Vminpll                         ! PLL freezing voltage, PLL freezes below this value
!       prm(  7)=  tau_f                           ! filter constant for frequency
!       prm(  8)=  kp_v                            ! DC link voltage control parameters
!       prm(  9)=  ki_v
!       prm( 10)=  kp_c                            ! rectifier current control parameters
!       prm( 11)=  ki_c
!       prm( 12)=  kp_p                            ! power control parameters
!       prm( 13)=  ki_p
!       prm( 14)=  kp_w                            ! motor speed control parameters
!       prm( 15)=  ki_w
!       prm( 16)=  w_cc                            ! motor current control bandwidth
!       prm( 17)=  rt                              ! terminal impedance
!       prm( 18)=  lt
!       prm( 19)=  cdc                             ! DC link capacitance
!       prm( 20)=  kw                              ! speed/torque control constant
!       prm( 21)=  kT                              ! motor torque constant
!       prm( 22)=  ra                              ! motor anchor/stator resistance
!       prm( 23)=  H                               ! motor inertia
!       prm( 24)=  b                               ! motor friction coefficient
!       prm( 25)=  Tnm                             ! compressor torque at nominal speed
!       prm( 26)=  iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
!       prm( 27)=  wm_min                          ! limits on rotational speed
!       prm( 28)=  wm_max
!       prm( 29)=  Vmax                            ! voltage range during which unit needs to stay connected
!       prm( 30)=  Vmin
!       prm( 31)=  Vint
!       prm( 32)=  Vr
!       prm( 33)=  tLVRT1
!       prm( 34)=  tLVRT2
!       prm( 35)=  tLVRTint
!       prm( 36)=  Vtrip
!       prm( 37)=  fmin                            ! frequency control regime
!       prm( 38)=  fmax
!       prm( 39)=  Trocof                          ! delay for ROCOF measurement
!       prm( 40)=  dfmax                           ! maximum permissable ROCOF
!       prm( 41)=  VPmin                           ! power limit below 0.9 pu of Voltage
!       prm( 42)=  VPmax                           ! power limit above 0.93 pu voltage
!       prm( 43)=  LVRT                            ! enable or disable LVRT
!       prm( 44)=  Tm                              ! measurement delay
!       prm( 45)=  protection                      ! Flag for protection, -1 for off, 1 for on
!       prm( 46)=  support                         ! Flag for grid support, -1 for off, 1 for on
!       prm( 47)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 48)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 49)=  s_f                             ! security factor for unit limit control
!       prm( 50)=  lvl                             ! level of emergency of the central controller
!       prm( 51)=  pl1_3_0                        ! flags that varies for the different units
!       prm( 52)=  pl1_4_0
!       prm( 53)=  F_help_high_0
!       prm( 54)=  F_help_low_0
!  Parameters :
!       prm( 55)=  w0  
!       prm( 56)=  wb  
!       prm( 57)=  t2   Torque polynomial parameters, set to constant torque
!       prm( 58)=  t1  
!       prm( 59)=  t0  
!       prm( 60)=  Tlim  
!       prm( 61)=  Downlim  
!       prm( 62)=  Downlimdisc  
!       prm( 63)=  UplimdeltaP  
!       prm( 64)=  DownlimdeltaP  
!       prm( 65)=  Uplimdis  
!       prm( 66)=  downlimdis  
!       prm( 67)=  theta0  
!       prm( 68)=  P0   initial active power
!       prm( 69)=  Q0   initial reactive power
!       prm( 70)=  P0_unit  
!       prm( 71)=  Q0_unit  
!       prm( 72)=  V0   initial voltage magnitude at bus
!       prm( 73)=  iP0   current alignment
!       prm( 74)=  iQ0  
!       prm( 75)=  vd0   voltage alignment
!       prm( 76)=  vq0  
!       prm( 77)=  md0   modulation indices
!       prm( 78)=  mq0  
!       prm( 79)=  idc0   DC link current
!       prm( 80)=  iT0  
!       prm( 81)=  wm0  
!       prm( 82)=  Te0  
!       prm( 83)=  Tc0  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vd                    
!       x(  4)=  vq                    
!       x(  5)=  theta                 
!       x(  6)=  vdc_ref               
!       x(  7)=  V                     
!       x(  8)=  vxm                   
!       x(  9)=  vym                   
!       x( 10)=  Vm                    
!       x( 11)=  iP                    
!       x( 12)=  diP                   
!       x( 13)=  iQ                    
!       x( 14)=  diQ                   
!       x( 15)=  P                     
!       x( 16)=  dp                    
!       x( 17)=  Punit                 
!       x( 18)=  Q                     
!       x( 19)=  Qunit                 
!       x( 20)=  Pref_lim               limited P reference
!       x( 21)=  dw_pll                
!       x( 22)=  dw_pllf               
!       x( 23)=  deltaf                
!       x( 24)=  w_pll                 
!       x( 25)=  f                     
!       x( 26)=  fi                    
!       x( 27)=  vdc                   
!       x( 28)=  dvdc                  
!       x( 29)=  iP_ref                
!       x( 30)=  Plim                  
!       x( 31)=  Plimin                
!       x( 32)=  iQ_ref                
!       x( 33)=  mdr                   
!       x( 34)=  mqr                   
!       x( 35)=  md                    
!       x( 36)=  mq                    
!       x( 37)=  dvd                   
!       x( 38)=  dvq                   
!       x( 39)=  idc                   
!       x( 40)=  didc                  
!       x( 41)=  wm_ref                
!       x( 42)=  wm_ref_lim            
!       x( 43)=  wm_ref_lim_in         
!       x( 44)=  iT_ref                
!       x( 45)=  iT                    
!       x( 46)=  dwm                   
!       x( 47)=  wm                    
!       x( 48)=  Te                    
!       x( 49)=  dT                    
!       x( 50)=  F_pll                 
!       x( 51)=  dv_pll                
!       x( 52)=  PLLmulta              
!       x( 53)=  PLLmultb              
!       x( 54)=  deltafvh              
!       x( 55)=  z1                    
!       x( 56)=  Fvhi                  
!       x( 57)=  Fvh                   
!       x( 58)=  iPs                   
!       x( 59)=  iQs                   
!       x( 60)=  Pref                  
!       x( 61)=  x10                   
!       x( 62)=  z                     
!       x( 63)=  Fvl                   
!       x( 64)=  Fvli                  
!       x( 65)=  deltafl               
!       x( 66)=  flagla                
!       x( 67)=  flaglb                
!       x( 68)=  Ffl                   
!       x( 69)=  Ffli                  
!       x( 70)=  deltafh               
!       x( 71)=  flagha                
!       x( 72)=  flaghb                
!       x( 73)=  Ffh                   
!       x( 74)=  Ffhi                  
!       x( 75)=  rocof                 
!       x( 76)=  abrocof               
!       x( 77)=  flagra                
!       x( 78)=  flagrb                
!       x( 79)=  deltarocof            
!       x( 80)=  Ffri                  
!       x( 81)=  Ffr                   
!       x( 82)=  Plim_min              
!       x( 83)=  status                 status of the device, 1 for on
!       x( 84)=  p1                    
!       x( 85)=  p2                    
!       x( 86)=  p3                    
!       x( 87)=  s1                    
!       x( 88)=  s2                    
!       x( 89)=  Fpl1                   placeholder for value 1 in switch block
!       x( 90)=  Fpl0                   placeholder for value 0 in switch block
!       x( 91)=  V_lim_min              unit controller states
!       x( 92)=  V_lim_max             
!       x( 93)=  P_lim_min             
!       x( 94)=  P_lim_max             
!       x( 95)=  pl1                   
!       x( 96)=  F_v_min_in            
!       x( 97)=  pl2                   
!       x( 98)=  F_v_max_in            
!       x( 99)=  F_v_opp               
!       x(100)=  pl3                   
!       x(101)=  F_p_min_in            
!       x(102)=  pl4                   
!       x(103)=  F_p_max_in            
!       x(104)=  F_v_min               
!       x(105)=  F_v_max               
!       x(106)=  F_p_min               
!       x(107)=  F_p_max               
!       x(108)=  F_lim                 
!       x(109)=  dP_lim                
!       x(110)=  dQ_lim                
!       x(111)=  dQ_sum                
!       x(112)=  dQ                    
!       x(113)=  dP_sum                

!.........................................................................................................

subroutine inj_ATLtestSimple(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 54
      nbaddpar= 29
      parname(  1)='Sb'
      parname(  2)='Sbs'
      parname(  3)='vdc_star'
      parname(  4)='pf'
      parname(  5)='tau'
      parname(  6)='Vminpll'
      parname(  7)='tau_f'
      parname(  8)='kp_v'
      parname(  9)='ki_v'
      parname( 10)='kp_c'
      parname( 11)='ki_c'
      parname( 12)='kp_p'
      parname( 13)='ki_p'
      parname( 14)='kp_w'
      parname( 15)='ki_w'
      parname( 16)='w_cc'
      parname( 17)='rt'
      parname( 18)='lt'
      parname( 19)='cdc'
      parname( 20)='kw'
      parname( 21)='kT'
      parname( 22)='ra'
      parname( 23)='H'
      parname( 24)='b'
      parname( 25)='Tnm'
      parname( 26)='iF'
      parname( 27)='wm_min'
      parname( 28)='wm_max'
      parname( 29)='Vmax'
      parname( 30)='Vmin'
      parname( 31)='Vint'
      parname( 32)='Vr'
      parname( 33)='tLVRT1'
      parname( 34)='tLVRT2'
      parname( 35)='tLVRTint'
      parname( 36)='Vtrip'
      parname( 37)='fmin'
      parname( 38)='fmax'
      parname( 39)='Trocof'
      parname( 40)='dfmax'
      parname( 41)='VPmin'
      parname( 42)='VPmax'
      parname( 43)='LVRT'
      parname( 44)='Tm'
      parname( 45)='protection'
      parname( 46)='support'
      parname( 47)='P_max'
      parname( 48)='P_min'
      parname( 49)='s_f'
      parname( 50)='lvl'
      parname( 51)='pl1_3_0'
      parname( 52)='pl1_4_0'
      parname( 53)='F_help_high_0'
      parname( 54)='F_help_low_0'
      parname( 55)='w0'
      parname( 56)='wb'
      parname( 57)='t2'
      parname( 58)='t1'
      parname( 59)='t0'
      parname( 60)='Tlim'
      parname( 61)='Downlim'
      parname( 62)='Downlimdisc'
      parname( 63)='UplimdeltaP'
      parname( 64)='DownlimdeltaP'
      parname( 65)='Uplimdis'
      parname( 66)='downlimdis'
      parname( 67)='theta0'
      parname( 68)='P0'
      parname( 69)='Q0'
      parname( 70)='P0_unit'
      parname( 71)='Q0_unit'
      parname( 72)='V0'
      parname( 73)='iP0'
      parname( 74)='iQ0'
      parname( 75)='vd0'
      parname( 76)='vq0'
      parname( 77)='md0'
      parname( 78)='mq0'
      parname( 79)='idc0'
      parname( 80)='iT0'
      parname( 81)='wm0'
      parname( 82)='Te0'
      parname( 83)='Tc0'
      adix=  1
      adiy=  2
      nbxvar=122
      nbzvar= 23

!........................................................................................
   case (define_obs)
      nbobs= 20
      obsname(  1)='iP'
      obsname(  2)='iQ'
      obsname(  3)='vd'
      obsname(  4)='vq'
      obsname(  5)='P'
      obsname(  6)='Punit'
      obsname(  7)='Pref'
      obsname(  8)='Q'
      obsname(  9)='f'
      obsname( 10)='wm'
      obsname( 11)='Vm'
      obsname( 12)='Pref_lim'
      obsname( 13)='wm_ref_lim'
      obsname( 14)='status'
      obsname( 15)='rocof'
      obsname( 16)='Ffl'
      obsname( 17)='Ffh'
      obsname( 18)='Fvl'
      obsname( 19)='Fvh'
      obsname( 20)='Ffr'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x( 11)              
      obs(  2)=x( 13)              
      obs(  3)=x(  3)              
      obs(  4)=x(  4)              
      obs(  5)=x( 15)              
      obs(  6)=x( 17)              
      obs(  7)=x( 60)              
      obs(  8)=x( 18)              
      obs(  9)=x( 25)              
      obs( 10)=x( 47)              
      obs( 11)=x( 10)              
      obs( 12)=x( 20)              
      obs( 13)=x( 42)              
      obs( 14)=x( 83)              
      obs( 15)=x( 75)              
      obs( 16)=x( 68)              
      obs( 17)=x( 73)              
      obs( 18)=x( 63)              
      obs( 19)=x( 57)              
      obs( 20)=x( 81)              

!........................................................................................
   case (initialize)

!w0 = 1
      prm( 55)= 1

!wb = 2*pi*fnom
      prm( 56)= 2*pi*fnom

!t2 = 0
      prm( 57)= 0

!t1 = 0
      prm( 58)= 0

!t0 = 1
      prm( 59)= 1

!Tlim = 0.01
      prm( 60)= 0.01

!Downlim = -9999
      prm( 61)= -9999

!Downlimdisc = 0
      prm( 62)= 0

!UplimdeltaP = 9999
      prm( 63)= 9999

!DownlimdeltaP = 0
      prm( 64)= 0

!Uplimdis = 0
      prm( 65)= 0

!downlimdis = -9999
      prm( 66)= -9999

!theta0 = atan([vy]/[vx])
      prm( 67)= atan(vy/vx)

!P0 = ([vx]*[ix]+[vy]*[iy])
      prm( 68)= (vx*ix+vy*iy)

!Q0 = [vy]*[ix]-[vx]*[iy]
      prm( 69)= vy*ix-vx*iy

!P0_unit = -{P0}*{Sbs}/{Sb}
      prm( 70)= -prm( 68)*prm(  2)/prm(  1)

!Q0_unit = -{Q0}*{Sbs}/{Sb}
      prm( 71)= -prm( 69)*prm(  2)/prm(  1)

!V0 = dsqrt([vx]**2+[vy]**2)
      prm( 72)= dsqrt(vx**2+vy**2)

!iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}
      prm( 73)=  (-ix*cos(prm( 67))-iy*sin(prm( 67)))*prm(  2)/prm(  1)

!iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
      prm( 74)=  (-ix*sin(prm( 67))+iy*cos(prm( 67)))*prm(  2)/prm(  1)

!vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})
      prm( 75)= vx*cos(prm( 67))+vy*sin(prm( 67))

!vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
      prm( 76)= vx*sin(prm( 67))-vy*cos(prm( 67))

!md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})
      prm( 77)= 1/prm(  3)*(prm( 75)+prm( 18)*prm( 55)*prm( 74)-prm( 17)*prm( 73))

!mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
      prm( 78)= 1/prm(  3)*(prm( 76)-prm( 18)*prm( 55)*prm( 73)-prm( 17)*prm( 74))

!idc0 = {md0}*{iP0}+{mq0}*{iQ0}
      prm( 79)= prm( 77)*prm( 73)+prm( 78)*prm( 74)

!iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
      prm( 80)= prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)) + dsqrt((prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)))**2-(prm( 22)*prm( 24)*prm( 26)**2-prm( 24)*prm( 79)*prm(  3))/(prm( 21)**2+prm( 22)*prm( 24)))

!wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
      prm( 81)= 1/prm( 24)*(prm( 21)*prm( 80)-prm( 25))

!Te0 = {iT0}*{kT}
      prm( 82)= prm( 80)*prm( 21)

!Tc0 = {Tnm}
      prm( 83)= prm( 25)

!vd =  {vd0}
      x(  3)= prm( 75)

!vq =  {vq0}
      x(  4)= prm( 76)

!theta =  {theta0}
      x(  5)= prm( 67)

!vdc_ref = {vdc_star}
      x(  6)=prm(  3)

!V =  {V0}
      x(  7)= prm( 72)

!vxm =  [vx]
      x(  8)= vx

!vym =  [vy]
      x(  9)= vy

!Vm =  {V0}
      x( 10)= prm( 72)

!iP =   {iP0}
      x( 11)=  prm( 73)

!diP =  0
      x( 12)= 0

!iQ =   {iQ0}
      x( 13)=  prm( 74)

!diQ =  0
      x( 14)= 0

!P =  {P0}
      x( 15)= prm( 68)

!dp =  0
      x( 16)= 0

!Punit =  {P0_unit}
      x( 17)= prm( 70)

!Q =  {Q0}
      x( 18)= prm( 69)

!Qunit =  {Q0_unit}
      x( 19)= prm( 71)

!Pref_lim =  {P0_unit}
      x( 20)= prm( 70)

!dw_pll =  0
      x( 21)= 0

!dw_pllf =  0
      x( 22)= 0

!deltaf =  0
      x( 23)= 0

!w_pll =  {wb}
      x( 24)= prm( 56)

!f =  fnom
      x( 25)= fnom

!fi =  fnom
      x( 26)= fnom

!vdc =  {vdc_star}
      x( 27)= prm(  3)

!dvdc =  0
      x( 28)= 0

!iP_ref =  {iP0}
      x( 29)= prm( 73)

!Plim =  {VPmax}
      x( 30)= prm( 42)

!Plimin =  {VPmax}
      x( 31)= prm( 42)

!iQ_ref =  {iQ0}
      x( 32)= prm( 74)

!mdr =  {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
      x( 33)= prm( 18)/prm(  3)*prm( 74)*prm( 55)-prm( 77)

!mqr =  -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
      x( 34)= -prm( 18)/prm(  3)*prm( 73)*prm( 55)-prm( 78)

!md =  {md0}
      x( 35)= prm( 77)

!mq =  {mq0}
      x( 36)= prm( 78)

!dvd =  {iP0}*{rt}
      x( 37)= prm( 73)*prm( 17)

!dvq =  {iQ0}*{rt}
      x( 38)= prm( 74)*prm( 17)

!idc =  {idc0}
      x( 39)= prm( 79)

!didc =  {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
      x( 40)= prm( 77)*prm( 73)+prm( 78)*prm( 74)-prm( 79)

!wm_ref =  {wm0}
      x( 41)= prm( 81)

!wm_ref_lim =  {wm0}
      x( 42)= prm( 81)

!wm_ref_lim_in =  {wm0}
      x( 43)= prm( 81)

!iT_ref =  {iT0}
      x( 44)= prm( 80)

!iT =  {iT0}
      x( 45)= prm( 80)

!dwm =  0
      x( 46)= 0

!wm =  {wm0}
      x( 47)= prm( 81)

!Te =  {kT}*{iT0}
      x( 48)= prm( 21)*prm( 80)

!dT =  {kT}*{iT0}-{Tc0}
      x( 49)= prm( 21)*prm( 80)-prm( 83)

!F_pll =  1
      x( 50)= 1

!dv_pll =  {V0}-{Vminpll}
      x( 51)= prm( 72)-prm(  6)

!PLLmulta =  1
      x( 52)= 1

!PLLmultb =  0
      x( 53)= 0

!deltafvh =   {V0}-{Vmax}
      x( 54)=  prm( 72)-prm( 29)

!z1 =  0
      x( 55)= 0

!Fvhi =  1
      x( 56)= 1

!Fvh =  1
      x( 57)= 1

!iPs =  {iP0}
      x( 58)= prm( 73)

!iQs =  {iQ0}
      x( 59)= prm( 74)

!Pref =  {P0_unit}
      x( 60)= prm( 70)

!x10 =  -{V0}
      x( 61)= -prm( 72)

!z = 0
      x( 62)=0

!Fvl =  1
      x( 63)= 1

!Fvli =  1
      x( 64)= 1

!deltafl =  [f] - {fmin}
      x( 65)= x( 25) - prm( 37)

!flagla =  1
      x( 66)= 1

!flaglb =  0
      x( 67)= 0

!Ffl =  1
      x( 68)= 1

!Ffli =  1
      x( 69)= 1

!deltafh =  {fmax} - [f]
      x( 70)= prm( 38) - x( 25)

!flagha =  1
      x( 71)= 1

!flaghb =  0
      x( 72)= 0

!Ffh =  1
      x( 73)= 1

!Ffhi =  1
      x( 74)= 1

!rocof =  0
      x( 75)= 0

!abrocof =  0
      x( 76)= 0

!flagra =  1
      x( 77)= 1

!flagrb =  0
      x( 78)= 0

!deltarocof =  {dfmax} - 0
      x( 79)= prm( 40) - 0

!Ffri =  1
      x( 80)= 1

!Ffr =  1
      x( 81)= 1

!Plim_min =  0
      x( 82)= 0

!status =  1
      x( 83)= 1

!p1 =  {protection}
      x( 84)= prm( 45)

!p2 =  1
      x( 85)= 1

!p3 =  1
      x( 86)= 1

!s1 =  {support}
      x( 87)= prm( 46)

!s2 =  {wm0}
      x( 88)= prm( 81)

!Fpl1 =  1
      x( 89)= 1

!Fpl0 =  0
      x( 90)= 0

!V_lim_min =  {Vmin} * {s_f}
      x( 91)= prm( 30) * prm( 49)

!V_lim_max =  {Vmax} / {s_f}
      x( 92)= prm( 29) / prm( 49)

!P_lim_min =  {P_min} * {s_f}
      x( 93)= prm( 48) * prm( 49)

!P_lim_max =  {P_max} / {s_f}
      x( 94)= prm( 47) / prm( 49)

!pl1 =  {Vmin} * {s_f} - {V0}
      x( 95)= prm( 30) * prm( 49) - prm( 72)

!F_v_min_in =  0
      x( 96)= 0

!pl2 =  {V0} - {Vmax} / {s_f}
      x( 97)= prm( 72) - prm( 29) / prm( 49)

!F_v_max_in =  0
      x( 98)= 0

!F_v_opp =  1
      x( 99)= 1

!pl3 =  {P_min} * {s_f} - {P0_unit}
      x(100)= prm( 48) * prm( 49) - prm( 70)

!F_p_min_in =  0
      x(101)= 0

!pl4 =  {P0_unit} - {P_max} / {s_f}
      x(102)= prm( 70) - prm( 47) / prm( 49)

!F_p_max_in =  0
      x(103)= 0

!F_v_min =  0
      x(104)= 0

!F_v_max =  0
      x(105)= 0

!F_p_min =  0
      x(106)= 0

!F_p_max =  0
      x(107)= 0

!F_lim =  0
      x(108)= 0

!dP_lim =  0
      x(109)= 0

!dQ_lim =  0
      x(110)= 0

!dQ_sum =  0
      x(111)= 0

!dQ =  -{Q0_unit}
      x(112)= -prm( 71)

!dP_sum =  0
      x(113)= 0

!& algeq                                             ! voltage magnitude
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)= 10
      tc(  2)=prm( 44)

!& tf1p
      eqtyp(  3)=  8
      tc(  3)=prm( 44)

!& tf1p
      eqtyp(  4)=  9
      tc(  4)=prm( 44)

!& algeq                                             ! voltage alignment
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& algeq                                             ! compute ix
      eqtyp(  7)=0

!& algeq                                             ! compute iy
      eqtyp(  8)=0

!& algeq
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq                                             ! compute powers
      eqtyp( 11)=0

!& algeq
      eqtyp( 12)=0

!& algeq                                             ! compute powers
      eqtyp( 13)=0

!& algeq
      eqtyp( 14)=0

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         eqtyp( 15)=0
      else
         eqtyp( 15)=  5
         tc( 15)=1.d0
      endif

!& pictl                                             ! PLL
      eqtyp( 16)=114
      x(114)=x( 24)
      eqtyp( 17)=0

!& algeq                                             ! frequency deviation
      eqtyp( 18)=0

!& algeq
      eqtyp( 19)=0

!& algeq                                             ! compute and filter frequency
      eqtyp( 20)=0

!& tf1p
      eqtyp( 21)= 25
      tc( 21)=prm(  7)

!& algeq                                             ! PLL freezing
      eqtyp( 22)=0

!& algeq
      eqtyp( 23)=0

!& algeq
      eqtyp( 24)=0

!& swsign
      eqtyp( 25)=0
      if(x( 51)>=0.)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq
      eqtyp( 26)=0

!& algeq                                             ! DC voltage control
      eqtyp( 27)=0

!& pictl
      eqtyp( 28)=115
      x(115)=x( 29)
      eqtyp( 29)=0

!& algeq                                             ! p-axis current control
      eqtyp( 30)=0

!& pictl
      eqtyp( 31)=116
      x(116)=x( 33)
      eqtyp( 32)=0

!& algeq                                             ! q-axis current control
      eqtyp( 33)=0

!& algeq
      eqtyp( 34)=0

!& pictl
      eqtyp( 35)=117
      x(117)=x( 34)
      eqtyp( 36)=0

!& algeq                                             ! md
      eqtyp( 37)=0

!& algeq                                             ! mq
      eqtyp( 38)=0

!& algeq                                             ! voltage over d-axis terminal impedance
      eqtyp( 39)=0

!& algeq                                             ! voltage over q-axis terminal impedance
      eqtyp( 40)=0

!& tf1p                                              ! d-axis current
      eqtyp( 41)= 11
      tc( 41)=prm( 18)/(prm( 56)*prm( 17))

!& tf1p                                              ! q-axis current
      eqtyp( 42)= 13
      tc( 42)=prm( 18)/(prm( 56)*prm( 17))

!& algeq                                             ! DC link current
      eqtyp( 43)=0

!& algeq                                             ! DC link
      eqtyp( 44)=0

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 56)< 0.005)then
         eqtyp( 45)=0
      else
         eqtyp( 45)= 27
         tc( 45)=prm( 19)/prm( 56)
      endif

!& limvb                                              ! power limiter
      eqtyp( 46)=0
      if(x( 60)>x( 30))then
         z(  2)=1
      elseif(x( 60)<x( 82))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq                                             ! power mismatch
      eqtyp( 47)=0

!& pictl                                             ! power control
      eqtyp( 48)=118
      x(118)=x( 41)
      eqtyp( 49)=0

!& lim                                               ! limit speed control input
      eqtyp( 50)=0
      if(x( 41)>prm( 28))then
         z(  3)=1
      elseif(x( 41)<prm( 27))then
         z(  3)=-1
      else
         z(  3)=0
      endif

!& algeq                                             ! speed control input
      eqtyp( 51)=0

!& pictl                                             ! speed control
      eqtyp( 52)=119
      x(119)=x( 44)
      eqtyp( 53)=0

!& tf1p                                              ! motor current control
      eqtyp( 54)= 45
      tc( 54)=1/prm( 16)

!& algeq                                             ! torque equations
      eqtyp( 55)=0

!& algeq
      eqtyp( 56)=0

!& tf1p                                              ! motor inertia
      eqtyp( 57)= 47
      tc( 57)=2*prm( 23)/prm( 24)

!& algeq
      eqtyp( 58)=0

!& pwlin4            ! Overvoltage Protection
      eqtyp( 59)=0
      if(x( 54)<(-999))then
         z(  4)=1
      elseif(x( 54)>=999)then
         z(  4)=   3
      elseif((-999)<=x( 54) .and. x( 54)<0.)then
         z(  4)=  1
      elseif(0.<=x( 54) .and. x( 54)<0.)then
         z(  4)=  2
      elseif(0.<=x( 54) .and. x( 54)<999)then
         z(  4)=  3
      endif

!& algeq
      eqtyp( 60)=0

!& hyst
      eqtyp( 61)=0
      if(x( 56)>1.1)then
         z(  5)=1
      elseif(x( 56)<0.9)then
         z(  5)=-1
      else
         if(1.>= 0.)then
            z(  5)=1
         else
            z(  5)=-1
         endif
      endif

!& algeq                     ! LVRT
      eqtyp( 62)=0

!& timer5
      eqtyp( 63)=0
      eqtyp( 64)=0
      z(  6)=-1
      x(120)=0.

!& algeq
      eqtyp( 65)=0

!& hyst
      eqtyp( 66)=0
      if(x( 64)>1.1)then
         z(  7)=1
      elseif(x( 64)<0.9)then
         z(  7)=-1
      else
         if(1.>= 0.)then
            z(  7)=1
         else
            z(  7)=-1
         endif
      endif

!& algeq                         ! status variable that can switch the entire unit on or off
      eqtyp( 67)=0

!& algeq
      eqtyp( 68)=0

!& algeq
      eqtyp( 69)=0

!& swsign
      eqtyp( 70)=0
      if(x( 84)>=0.)then
         z(  8)=1
      else
         z(  8)=2
      endif

!& algeq                     ! switch support on and off
      eqtyp( 71)=0

!& algeq
      eqtyp( 72)=0

!& swsign
      eqtyp( 73)=0
      if(x( 87)>=0.)then
         z(  9)=1
      else
         z(  9)=2
      endif

!& algeq                 ! underfrequency protection
      eqtyp( 74)=0

!& algeq
      eqtyp( 75)=0

!& algeq
      eqtyp( 76)=0

!& swsign
      eqtyp( 77)=0
      if(x( 65)>=0.)then
         z( 10)=1
      else
         z( 10)=2
      endif

!& hyst
      eqtyp( 78)=0
      if(x( 69)>1.1)then
         z( 11)=1
      elseif(x( 69)<0.9)then
         z( 11)=-1
      else
         if(1.>= 0.)then
            z( 11)=1
         else
            z( 11)=-1
         endif
      endif

!& algeq                 ! overfrequency protection
      eqtyp( 79)=0

!& algeq
      eqtyp( 80)=0

!& algeq
      eqtyp( 81)=0

!& swsign
      eqtyp( 82)=0
      if(x( 70)>=0.)then
         z( 12)=1
      else
         z( 12)=2
      endif

!& hyst
      eqtyp( 83)=0
      if(x( 74)>1.1)then
         z( 13)=1
      elseif(x( 74)<0.9)then
         z( 13)=-1
      else
         if(1.>= 0.)then
            z( 13)=1
         else
            z( 13)=-1
         endif
      endif

!& algeq                ! frequency deviation in Hz
      eqtyp( 84)=0

!& tfder1p               ! Rocof measurement in Hz/s
      x(121)=x( 23)
      eqtyp( 85)=121
      tc( 85)=prm( 39)
      eqtyp( 86)=0

!& abs
      eqtyp( 87)=0
      if(x( 75)>0. )then
         z( 14)=1
      else
         z( 14)=-1
      endif

!& algeq                 ! Rocof protection
      eqtyp( 88)=0

!& algeq
      eqtyp( 89)=0

!& algeq
      eqtyp( 90)=0

!& swsign
      eqtyp( 91)=0
      if(x( 79)>=0.)then
         z( 15)=1
      else
         z( 15)=2
      endif

!& hyst
      eqtyp( 92)=0
      if(x( 80)>1.1)then
         z( 16)=1
      elseif(x( 80)<0.9)then
         z( 16)=-1
      else
         if(1.>= 0.)then
            z( 16)=1
         else
            z( 16)=-1
         endif
      endif

!& pwlin4            ! limit active power during undervoltage
      eqtyp( 93)=0
      if(x( 10)<0)then
         z( 17)=1
      elseif(x( 10)>=1.5)then
         z( 17)=   3
      elseif(0<=x( 10) .and. x( 10)<0.9)then
         z( 17)=  1
      elseif(0.9<=x( 10) .and. x( 10)<0.93)then
         z( 17)=  2
      elseif(0.93<=x( 10) .and. x( 10)<1.5)then
         z( 17)=  3
      endif

!& algeq
      eqtyp( 94)=0

!& tf1p2lim
      if(prm( 60)< 0.001)then
         prm( 60)=0.d0
         prm( 64)=-huge(0.d0)
         prm( 63)= huge(0.d0)
         prm( 64)=-huge(0.d0)
         prm( 63)= huge(0.d0)
      endif
      if(1*x( 31)-x( 30)>prm( 63)*prm( 60))then
         z( 18)=1
      elseif(1*x( 31)-x( 30)<prm( 64)*prm( 60))then
         z( 18)=-1
      else
         z( 18)=0
      endif
      eqtyp( 95)=0
      if(x( 30)>prm( 63))then
         z( 19)=1
         eqtyp( 96)=0
      elseif(x( 30)<prm( 64))then
         z( 19)=-1
         eqtyp( 96)=0
      else
         z( 19)=0
         eqtyp( 96)= 30
      endif
      tc( 96)=prm( 60)

!& algeq
      eqtyp( 97)=0

!& algeq ! TESTING STARTS HERE	 central control, positive and negative variation
      eqtyp( 98)=0

!& algeq
      eqtyp( 99)=0

!& algeq		! UNIT LIMIT	!s_f = security factor
      eqtyp(100)=0

!& algeq
      eqtyp(101)=0

!& algeq
      eqtyp(102)=0

!& algeq
      eqtyp(103)=0

!& algeq !should it be V or Vm?	! voltage low limit flag
      eqtyp(104)=0

!& swsign
      eqtyp(105)=0
      if(x( 95)>=0.)then
         z( 20)=1
      else
         z( 20)=2
      endif

!& algeq !should it be V or Vm?	! voltage high limit flag
      eqtyp(106)=0

!& swsign
      eqtyp(107)=0
      if(x( 97)>=0.)then
         z( 21)=1
      else
         z( 21)=2
      endif

!& algeq ! F_v_opp = 0 if the voltage is smaller than F_v_min_in or bigger than F_v_max_in
      eqtyp(108)=0

!& algeq							! power low limit flag
      eqtyp(109)=0

!& swsign
      eqtyp(110)=0
      if(x(100)>=0.)then
         z( 22)=1
      else
         z( 22)=2
      endif

!& algeq							! power high limit flag
      eqtyp(111)=0

!& swsign
      eqtyp(112)=0
      if(x(102)>=0.)then
         z( 23)=1
      else
         z( 23)=2
      endif

!& algeq
      eqtyp(113)=0

!& algeq
      eqtyp(114)=0

!& algeq
      eqtyp(115)=0

!& algeq
      eqtyp(116)=0

!& algeq							! final limit flag
      eqtyp(117)=0

!& algeq
      eqtyp(118)=0

!& algeq
      eqtyp(119)=0

!& algeq
      eqtyp(120)=0

!& algeq
      eqtyp(121)=0

!& algeq		! dQ MUST BE LINKED TO THE MOTOR SPEED!
      eqtyp(122)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq                                             ! voltage magnitude
      f(  1)=dsqrt(vx**2+vy**2) - x(  7)

!& tf1p
      f(  2)=(-x( 10)+1.*x(  7))

!& tf1p
      f(  3)=(-x(  8)+1.*vx    )

!& tf1p
      f(  4)=(-x(  9)+1.*vy    )

!& algeq                                             ! voltage alignment
      f(  5)=-x(  3) + x(  8)*cos(x(  5))+x(  9)*sin(x(  5))

!& algeq
      f(  6)=-x(  4) - x(  8)*sin(x(  5))+x(  9)*cos(x(  5))

!& algeq                                             ! compute ix
      f(  7)=-x(  1) + (-x( 58)*cos(x(  5))-x( 59)*sin(x(  5)))*prm(  1)/prm(  2)

!& algeq                                             ! compute iy
      f(  8)=-x(  2) + (-x( 58)*sin(x(  5))+x( 59)*sin(x(  5)))*prm(  1)/prm(  2)

!& algeq
      f(  9)=-x( 58) + x( 11)*x( 83)

!& algeq
      f( 10)=-x( 59) + x( 13)*x( 83)

!& algeq                                             ! compute powers
      f( 11)=-x( 15)*prm(  2)/prm(  1) -x( 17)                            ! + x(  1)*vx+x(  2)*vy

!& algeq
      f( 12)=-x( 18)*prm(  2)/prm(  1) - x( 19)                           ! -vx*x(  2) + vy*x(  1)

!& algeq                                             ! compute powers
      f( 13)=-x( 17) + x(  3)*x( 58) + x(  4)*x( 59)                              ! - x( 15)*prm(  2)/prm(  1)

!& algeq
      f( 14)=-x( 19) + x(  3)*x( 59) - x(  4)*x( 58)                              ! - x( 18)*prm(  2)/prm(  1)

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         f( 15)=x( 22)-x(  5)
      else
         f( 15)=x( 22)
      endif

!& pictl                                             ! PLL
      f( 16)=0.1/(prm(  5)*0.001)**2                                                                                                                                                                                                                                                                                     *x(  4)
      f( 17)=0.5/(prm(  5)*0.001)                                                                                                                                                                                                                                                                                        *x(  4)+x(114)-x( 24)

!& algeq                                             ! frequency deviation
      f( 18)=x( 21) - x( 24) + omega*prm( 56)

!& algeq
      f( 19)=x( 22) -x( 21)*x( 50)

!& algeq                                             ! compute and filter frequency
      f( 20)=-x( 26)+x( 24)/prm( 56)*fnom

!& tf1p
      f( 21)=(-x( 25)+1.*x( 26))

!& algeq                                             ! PLL freezing
      f( 22)=-x( 51) + x( 10)-prm(  6)

!& algeq
      f( 23)=x( 53)

!& algeq
      f( 24)=-x( 52) + 1.d0

!& swsign
      select case (z(  1))
         case(1)
            f( 25)=x( 50)-x( 52)
         case(2)
            f( 25)=x( 50)-x( 53)
      end select

!& algeq
      f( 26)=-x(  6) + prm(  3)*x( 83)

!& algeq                                             ! DC voltage control
      f( 27)=-x( 28) + x( 83)*x(  6) - x( 27)

!& pictl
      f( 28)=prm(  9)                                                                                                                                                                                                                                                                                                    *x( 28)
      f( 29)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 28)+x(115)-x( 29)

!& algeq                                             ! p-axis current control
      f( 30)=-x( 12) + x( 29) -x( 11)

!& pictl
      f( 31)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 12)
      f( 32)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 12)+x(116)-x( 33)

!& algeq                                             ! q-axis current control
      f( 33)=-x( 32) + x( 29)*dsqrt(1-prm(  4))

!& algeq
      f( 34)=-x( 14) + x( 32) -x( 13)

!& pictl
      f( 35)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 14)
      f( 36)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 14)+x(117)-x( 34)

!& algeq                                             ! md
      f( 37)=-x( 35) + x( 83)*(-x( 33) + prm( 18)/max(1.d-3,prm(  3))*x( 25)/fnom*x( 13))

!& algeq                                             ! mq
      f( 38)=-x( 36) + x( 83)*(-x( 34) - prm( 18)/max(1.d-3,prm(  3))*x( 25)/fnom*x( 11))

!& algeq                                             ! voltage over d-axis terminal impedance
      f( 39)=-x( 37) + x(  3) - x( 35) * x( 27) + prm( 18)*omega*x( 13)

!& algeq                                             ! voltage over q-axis terminal impedance
      f( 40)=-x( 38) + x(  4) - x( 36) * x( 27) - prm( 18)*omega*x( 11)

!& tf1p                                              ! d-axis current
      f( 41)=(-x( 11)+1/prm( 17)*x( 37))

!& tf1p                                              ! q-axis current
      f( 42)=(-x( 13)+1/prm( 17)*x( 38))

!& algeq                                             ! DC link current
      f( 43)=-x( 39) + x( 83)*(x( 47)*prm( 21)*x( 45) + prm( 22)*x( 45)**2) / max(1.d-3,x( 27))

!& algeq                                             ! DC link
      f( 44)=-x( 40)+x( 35)*x( 11)+x( 36)*x( 13)-x( 39)

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 56)< 0.005)then
         f( 45)=x( 40)-x( 27)
      else
         f( 45)=x( 40)
      endif

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            f( 46)=x( 20)-x( 60)
         case(-1)
            f( 46)=x( 20)-x( 82)
         case(1)
            f( 46)=x( 20)-x( 30)
      end select

!& algeq                                             ! power mismatch
      f( 47)=-x( 16) + x( 20)-x( 17) + x(113)

!& pictl                                             ! power control
      f( 48)=prm( 13)                                                                                                                                                                                                                                                                                                    *x( 16)
      f( 49)=prm( 12)                                                                                                                                                                                                                                                                                                    *x( 16)+x(118)-x( 41)

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            f( 50)=x( 43)-x( 41)
         case(-1)
            f( 50)=x( 43)-prm( 27)
         case(1)
            f( 50)=x( 43)-prm( 28)
      end select

!& algeq                                             ! speed control input
      f( 51)=-x( 46) + prm( 20)*(x( 83)*x( 42)-x( 47))

!& pictl                                             ! speed control
      f( 52)=prm( 15)                                                                                                                                                                                                                                                                                                    *x( 46)
      f( 53)=prm( 14)                                                                                                                                                                                                                                                                                                    *x( 46)+x(119)-x( 44)

!& tf1p                                              ! motor current control
      f( 54)=(-x( 45)+1*x( 44))

!& algeq                                             ! torque equations
      f( 55)=-x( 48) + prm( 21)*x( 45)

!& algeq
      f( 56)=-x( 49) + x( 48)-prm( 83)

!& tf1p                                              ! motor inertia
      f( 57)=(-x( 47)+1/prm( 24)*x( 49))

!& algeq
      f( 58)=x( 54) - x( 10) +prm( 29)

!& pwlin4            ! Overvoltage Protection
      select case (z(  4))
         case (  1)
            f( 59)=0.+ ( (0.-0.)*(x( 54)-(-999))/(0.-(-999)) ) -x( 55)
         case (  2)
            f( 59)=0.+ ( (1.-0.)*(x( 54)-0.)/(0.-0.) ) -x( 55)
         case (  3)
            f( 59)=1.+ ( (1.-1.)*(x( 54)-0.)/(999-0.) ) -x( 55)
      end select

!& algeq
      f( 60)=x( 56) -1 + x( 55)

!& hyst
      if(z(  5) == 1)then
         f( 61)=x( 57)-1.-(1.-1.)*(x( 56)-1.1)/(1.1-0.9)
      else
         f( 61)=x( 57)-0.-(0.-0.)*(x( 56)-0.9)/(1.1-0.9)
      endif

!& algeq                     ! LVRT
      f( 62)=x( 10) + x( 61)

!& timer5
      select case (z(  6))
         case (-1)
            f( 63)=x( 62)
            f( 64)=x(120)
         case (0)
            f( 63)=x( 62)
            f( 64)= 1.
         case (1)
            f( 63)=x( 62)-1.
            f( 64)= 0.
      end select

!& algeq
      f( 65)=x( 64) -1 + x( 62)

!& hyst
      if(z(  7) == 1)then
         f( 66)=x( 63)-1.-(1.-1.)*(x( 64)-1.1)/(1.1-0.9)
      else
         f( 66)=x( 63)-0.-(0.-0.)*(x( 64)-0.9)/(1.1-0.9)
      endif

!& algeq                         ! status variable that can switch the entire unit on or off
      f( 67)=x( 85) - x( 57)*x( 63)*x( 68)*x( 73)*x( 81)

!& algeq
      f( 68)=x( 84) - prm( 45)

!& algeq
      f( 69)=x( 86) - 1

!& swsign
      select case (z(  8))
         case(1)
            f( 70)=x( 83)-x( 85)
         case(2)
            f( 70)=x( 83)-x( 86)
      end select

!& algeq                     ! switch support on and off
      f( 71)=x( 87) - prm( 46)

!& algeq
      f( 72)=x( 88) - prm( 81)

!& swsign
      select case (z(  9))
         case(1)
            f( 73)=x( 42)-x( 43)
         case(2)
            f( 73)=x( 42)-x( 88)
      end select

!& algeq                 ! underfrequency protection
      f( 74)=x( 66) - 1

!& algeq
      f( 75)=x( 67)

!& algeq
      f( 76)=x( 65) - x( 25) + prm( 37)

!& swsign
      select case (z( 10))
         case(1)
            f( 77)=x( 69)-x( 66)
         case(2)
            f( 77)=x( 69)-x( 67)
      end select

!& hyst
      if(z( 11) == 1)then
         f( 78)=x( 68)-1.-(1.-1.)*(x( 69)-1.1)/(1.1-0.9)
      else
         f( 78)=x( 68)-0.-(0.-0.)*(x( 69)-0.9)/(1.1-0.9)
      endif

!& algeq                 ! overfrequency protection
      f( 79)=x( 71) - 1

!& algeq
      f( 80)=x( 72)

!& algeq
      f( 81)=x( 70) - prm( 38) + x( 25)

!& swsign
      select case (z( 12))
         case(1)
            f( 82)=x( 74)-x( 71)
         case(2)
            f( 82)=x( 74)-x( 72)
      end select

!& hyst
      if(z( 13) == 1)then
         f( 83)=x( 73)-1.-(1.-1.)*(x( 74)-1.1)/(1.1-0.9)
      else
         f( 83)=x( 73)-0.-(0.-0.)*(x( 74)-0.9)/(1.1-0.9)
      endif

!& algeq                ! frequency deviation in Hz
      f( 84)=-x( 23) + x( 25)-fnom

!& tfder1p               ! Rocof measurement in Hz/s
      f( 85)=-x(121)+x( 23)
      if (prm( 39)< 0.005)then
         f( 86)=1/prm( 39)*x( 23)-x( 75)
      else
         f( 86)=1/prm( 39)*(x( 23)-x(121))-x( 75)
      endif

!& abs
      if(z( 14) == 1 )then
         f( 87)=x( 76)-x( 75)
      else
         f( 87)=x( 76)+x( 75)
      endif

!& algeq                 ! Rocof protection
      f( 88)=x( 77) - 1

!& algeq
      f( 89)=x( 78)

!& algeq
      f( 90)=-x( 79) +prm( 40) -x( 76)

!& swsign
      select case (z( 15))
         case(1)
            f( 91)=x( 80)-x( 77)
         case(2)
            f( 91)=x( 80)-x( 78)
      end select

!& hyst
      if(z( 16) == 1)then
         f( 92)=x( 81)-1.-(1.-1.)*(x( 80)-1.1)/(1.1-0.9)
      else
         f( 92)=x( 81)-0.-(0.-0.)*(x( 80)-0.9)/(1.1-0.9)
      endif

!& pwlin4            ! limit active power during undervoltage
      select case (z( 17))
         case (  1)
            f( 93)=prm( 41)+ ( (prm( 41)-prm( 41))*(x( 10)-0)/(0.9-0) ) -x( 31)
         case (  2)
            f( 93)=prm( 41)+ ( (prm( 42)-prm( 41))*(x( 10)-0.9)/(0.93-0.9) ) -x( 31)
         case (  3)
            f( 93)=prm( 42)+ ( (prm( 42)-prm( 42))*(x( 10)-0.93)/(1.5-0.93) ) -x( 31)
      end select

!& algeq
      f( 94)=x( 82)

!& tf1p2lim
      select case (z( 18))
         case(0)
            f( 95)=x(122)-1*x( 31)+x( 30)
         case(1)
            f( 95)=x(122)-prm( 63)*prm( 60)
         case(-1)
            f( 95)=x(122)-prm( 64)*prm( 60)
      end select
      select case (z( 19))
         case(0)
            f( 96)=x(122)
         case(1)
            f( 96)=x( 30)-prm( 63)
         case(-1)
            f( 96)=x( 30)-prm( 64)
      end select

!& algeq
      f( 97)=- x( 60) + x( 83)*prm( 70)

!& algeq ! TESTING STARTS HERE	 central control, positive and negative variation
      f( 98)=x( 90)

!& algeq
      f( 99)=x( 89) - 1

!& algeq		! UNIT LIMIT	!s_f = security factor
      f(100)=-x( 91) + prm( 30)*prm( 49)

!& algeq
      f(101)=-x( 92) + prm( 29)/prm( 49)

!& algeq
      f(102)=-x( 93) + prm( 48)*prm( 49)

!& algeq
      f(103)=-x( 94) + prm( 47)/prm( 49)

!& algeq !should it be V or Vm?	! voltage low limit flag
      f(104)=-x( 95) + x( 91) - x( 10)

!& swsign
      select case (z( 20))
         case(1)
            f(105)=x( 96)-x( 89)
         case(2)
            f(105)=x( 96)-x( 90)
      end select

!& algeq !should it be V or Vm?	! voltage high limit flag
      f(106)=-x( 97) + x( 10) - x( 92)

!& swsign
      select case (z( 21))
         case(1)
            f(107)=x( 98)-x( 89)
         case(2)
            f(107)=x( 98)-x( 90)
      end select

!& algeq ! F_v_opp = 0 if the voltage is smaller than F_v_min_in or bigger than F_v_max_in
      f(108)=-x( 99) + (1-x( 96))*(1-x( 98))

!& algeq							! power low limit flag
      f(109)=-x(100) + x( 93) - x( 17)

!& swsign
      select case (z( 22))
         case(1)
            f(110)=x(101)-x( 99)
         case(2)
            f(110)=x(101)-x( 90)
      end select

!& algeq							! power high limit flag
      f(111)=-x(102) + x( 17) - x( 94)

!& swsign
      select case (z( 23))
         case(1)
            f(112)=x(103)-x( 99)
         case(2)
            f(112)=x(103)-x( 90)
      end select

!& algeq
      f(113)=-x(104) + x( 96)*x( 83)

!& algeq
      f(114)=-x(105) + x( 98)*x( 83)

!& algeq
      f(115)=-x(106) + x(101)*x( 83)

!& algeq
      f(116)=-x(107) + x(103)*x( 83)

!& algeq							! final limit flag
      f(117)=-x(108) + 1-(1-x(104))*(1-x(105))*(1-x(106))*(1-x(107))

!& algeq
      f(118)=-x(109) + (x( 17) - x( 20))*x(108)

!& algeq
      f(119)=-x(113) + x(109)

!& algeq
      f(120)=-x(110) + x( 19)*x(108)

!& algeq
      f(121)=-x(111) + x(110)

!& algeq		! dQ MUST BE LINKED TO THE MOTOR SPEED!
      f(122)=-x(112) - x( 19) + x(111)

!........................................................................................
   case (update_disc)

!& algeq                                             ! voltage magnitude

!& tf1p

!& tf1p

!& tf1p

!& algeq                                             ! voltage alignment

!& algeq

!& algeq                                             ! compute ix

!& algeq                                             ! compute iy

!& algeq

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& int                                               ! voltage alignment angle, PLL angle

!& pictl                                             ! PLL

!& algeq                                             ! frequency deviation

!& algeq

!& algeq                                             ! compute and filter frequency

!& tf1p

!& algeq                                             ! PLL freezing

!& algeq

!& algeq

!& swsign
      select case (z(  1))
         case(1)
            if(x( 51)<0.)then
               z(  1)=2
            endif
         case(2)
            if(x( 51)>=0.)then
               z(  1)=1
            endif
      end select

!& algeq

!& algeq                                             ! DC voltage control

!& pictl

!& algeq                                             ! p-axis current control

!& pictl

!& algeq                                             ! q-axis current control

!& algeq

!& pictl

!& algeq                                             ! md

!& algeq                                             ! mq

!& algeq                                             ! voltage over d-axis terminal impedance

!& algeq                                             ! voltage over q-axis terminal impedance

!& tf1p                                              ! d-axis current

!& tf1p                                              ! q-axis current

!& algeq                                             ! DC link current

!& algeq                                             ! DC link

!& int                                               ! DC link voltage

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            if(x( 60)>x( 30))then
               z(  2)=1
            elseif(x( 60)<x( 82))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 60)>x( 82))then
               z(  2)=0
            endif
         case(1)
            if(x( 60)<x( 30))then
               z(  2)=0
            endif
      end select

!& algeq                                             ! power mismatch

!& pictl                                             ! power control

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            if(x( 41)>prm( 28))then
               z(  3)=1
            elseif(x( 41)<prm( 27))then
               z(  3)=-1
            endif
         case(-1)
            if(x( 41)>prm( 27))then
               z(  3)=0
            endif
         case(1)
            if(x( 41)<prm( 28))then
               z(  3)=0
            endif
      end select

!& algeq                                             ! speed control input

!& pictl                                             ! speed control

!& tf1p                                              ! motor current control

!& algeq                                             ! torque equations

!& algeq

!& tf1p                                              ! motor inertia

!& algeq

!& pwlin4            ! Overvoltage Protection
      if(x( 54)<(-999))then
         z(  4)=1
      elseif(x( 54)>=999)then
         z(  4)=  3
      elseif((-999)<=x( 54) .and. x( 54)<0.)then
         z(  4)=  1
      elseif(0.<=x( 54) .and. x( 54)<0.)then
         z(  4)=  2
      elseif(0.<=x( 54) .and. x( 54)<999)then
         z(  4)=  3
      endif

!& algeq

!& hyst
      if (z(  5) == -1)then
         if(x( 56)>1.1)then
            z(  5)=1
         endif
      else
         if(x( 56)<0.9)then
            z(  5)=-1
         endif
      endif

!& algeq                     ! LVRT

!& timer5
      if(z(  6) == -1)then
         if(x( 61) >= (-prm( 32)))then
            z(  6)=0
            eqtyp( 64)=120
         endif
      else
         if(x( 61) < (-prm( 32)))then
            z(  6)=-1
            eqtyp( 64)=0
         endif
      endif
      if(z(  6) == 0)then
         if(x( 61) > (-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36))))then
            if(x(120) > 0.)then
               z(  6)=1
            endif
         elseif(x( 61) > (-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36))))then
            if(x(120) > prm( 33)+(0.-prm( 33))*(x( 61)-(-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36))))/((-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36)))-(-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 61) > (-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36))))then
            if(x(120) > prm( 33)+(prm( 33)-prm( 33))*(x( 61)-(-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36))))/((-(prm( 43)*prm( 30) + (1-prm( 43))*prm( 36)))-(-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 61) > (-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36))))then
            if(x(120) > prm( 35)+(prm( 33)-prm( 35))*(x( 61)-(-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36))))/((-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36)))-(-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 61) > (-prm( 32)))then
            if(x(120) > prm( 34)+(prm( 35)-prm( 34))*(x( 61)-(-prm( 32)))/((-(prm( 43)*prm( 31) + (1-prm( 43))*prm( 36)))-(-prm( 32))))then
               z(  6)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  7) == -1)then
         if(x( 64)>1.1)then
            z(  7)=1
         endif
      else
         if(x( 64)<0.9)then
            z(  7)=-1
         endif
      endif

!& algeq                         ! status variable that can switch the entire unit on or off

!& algeq

!& algeq

!& swsign
      select case (z(  8))
         case(1)
            if(x( 84)<0.)then
               z(  8)=2
            endif
         case(2)
            if(x( 84)>=0.)then
               z(  8)=1
            endif
      end select

!& algeq                     ! switch support on and off

!& algeq

!& swsign
      select case (z(  9))
         case(1)
            if(x( 87)<0.)then
               z(  9)=2
            endif
         case(2)
            if(x( 87)>=0.)then
               z(  9)=1
            endif
      end select

!& algeq                 ! underfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z( 10))
         case(1)
            if(x( 65)<0.)then
               z( 10)=2
            endif
         case(2)
            if(x( 65)>=0.)then
               z( 10)=1
            endif
      end select

!& hyst
      if (z( 11) == -1)then
         if(x( 69)>1.1)then
            z( 11)=1
         endif
      else
         if(x( 69)<0.9)then
            z( 11)=-1
         endif
      endif

!& algeq                 ! overfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z( 12))
         case(1)
            if(x( 70)<0.)then
               z( 12)=2
            endif
         case(2)
            if(x( 70)>=0.)then
               z( 12)=1
            endif
      end select

!& hyst
      if (z( 13) == -1)then
         if(x( 74)>1.1)then
            z( 13)=1
         endif
      else
         if(x( 74)<0.9)then
            z( 13)=-1
         endif
      endif

!& algeq                ! frequency deviation in Hz

!& tfder1p               ! Rocof measurement in Hz/s

!& abs
      if (z( 14) == -1 )then
         if(x( 75)> blocktol1 )then
            z( 14)=1
         endif
      else
         if(x( 75)< - blocktol1 )then
            z( 14)=-1
         endif
      endif

!& algeq                 ! Rocof protection

!& algeq

!& algeq

!& swsign
      select case (z( 15))
         case(1)
            if(x( 79)<0.)then
               z( 15)=2
            endif
         case(2)
            if(x( 79)>=0.)then
               z( 15)=1
            endif
      end select

!& hyst
      if (z( 16) == -1)then
         if(x( 80)>1.1)then
            z( 16)=1
         endif
      else
         if(x( 80)<0.9)then
            z( 16)=-1
         endif
      endif

!& pwlin4            ! limit active power during undervoltage
      if(x( 10)<0)then
         z( 17)=1
      elseif(x( 10)>=1.5)then
         z( 17)=  3
      elseif(0<=x( 10) .and. x( 10)<0.9)then
         z( 17)=  1
      elseif(0.9<=x( 10) .and. x( 10)<0.93)then
         z( 17)=  2
      elseif(0.93<=x( 10) .and. x( 10)<1.5)then
         z( 17)=  3
      endif

!& algeq

!& tf1p2lim
      select case (z( 18))
         case(0)
            if(x(122)>prm( 63)*prm( 60))then
               z( 18)=1
            elseif(x(122)<prm( 64)*prm( 60))then
               z( 18)=-1
            endif
         case(1)
            if(1*x( 31)-x( 30)<prm( 63)*prm( 60))then
               z( 18)= 0
            endif
         case(-1)
            if(1*x( 31)-x( 30)>prm( 64)*prm( 60))then
               z( 18)= 0
            endif
      end select
      select case (z( 19))
         case(0)
            if(x( 30)>prm( 63))then
               z( 19)=1
               eqtyp( 96)=0
            elseif(x( 30)<prm( 64))then
               z( 19)=-1
               eqtyp( 96)=0
            endif
         case(1)
            if (x(122)<0.)then
               z( 19)= 0
               eqtyp( 96)= 30
            endif
         case(-1)
            if(x(122)>0.)then
               z( 19)= 0
               eqtyp( 96)= 30
            endif
      end select

!& algeq

!& algeq ! TESTING STARTS HERE	 central control, positive and negative variation

!& algeq

!& algeq		! UNIT LIMIT	!s_f = security factor

!& algeq

!& algeq

!& algeq

!& algeq !should it be V or Vm?	! voltage low limit flag

!& swsign
      select case (z( 20))
         case(1)
            if(x( 95)<0.)then
               z( 20)=2
            endif
         case(2)
            if(x( 95)>=0.)then
               z( 20)=1
            endif
      end select

!& algeq !should it be V or Vm?	! voltage high limit flag

!& swsign
      select case (z( 21))
         case(1)
            if(x( 97)<0.)then
               z( 21)=2
            endif
         case(2)
            if(x( 97)>=0.)then
               z( 21)=1
            endif
      end select

!& algeq ! F_v_opp = 0 if the voltage is smaller than F_v_min_in or bigger than F_v_max_in

!& algeq							! power low limit flag

!& swsign
      select case (z( 22))
         case(1)
            if(x(100)<0.)then
               z( 22)=2
            endif
         case(2)
            if(x(100)>=0.)then
               z( 22)=1
            endif
      end select

!& algeq							! power high limit flag

!& swsign
      select case (z( 23))
         case(1)
            if(x(102)<0.)then
               z( 23)=2
            endif
         case(2)
            if(x(102)>=0.)then
               z( 23)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq							! final limit flag

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq		! dQ MUST BE LINKED TO THE MOTOR SPEED!
   end select

end subroutine inj_ATLtestSimple
