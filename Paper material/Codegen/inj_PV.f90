!  MODEL NAME : inj_PV                  
!  MODEL DESCRIPTION FILE : PV.txt
!  Data :
!       prm(  1)=  Imax
!       prm(  2)=  IN
!       prm(  3)=  ratemax
!       prm(  4)=  Tg
!       prm(  5)=  Tm
!       prm(  6)=  tLVRT1
!       prm(  7)=  tLVRT2
!       prm(  8)=  tLVRTint
!       prm(  9)=  Vmax
!       prm( 10)=  kpll
!       prm( 11)=  Vminpll
!       prm( 12)=  a
!       prm( 13)=  Vmin
!       prm( 14)=  Vint
!       prm( 15)=  fmin
!       prm( 16)=  fmax
!       prm( 17)=  fstart
!       prm( 18)=  b
!       prm( 19)=  fr
!       prm( 20)=  Tr
!       prm( 21)=  Re
!       prm( 22)=  Xe
!       prm( 23)=  CM1
!       prm( 24)=  kRCI
!       prm( 25)=  kRCA
!       prm( 26)=  m
!       prm( 27)=  n
!       prm( 28)=  dbmin
!       prm( 29)=  dbmax
!       prm( 30)=  HVRT
!       prm( 31)=  LVRT
!       prm( 32)=  CM2
!       prm( 33)=  CM3
!       prm( 34)=  BM
!  Parameters :
!       prm( 35)=  vxlv  
!       prm( 36)=  vylv  
!       prm( 37)=  Iqref  
!       prm( 38)=  Pext  
!       prm( 39)=  Uplim  
!       prm( 40)=  Downlim  
!       prm( 41)=  Vref  
!       prm( 42)=  UplimdeltaP  
!       prm( 43)=  DownlimdeltaP  
!       prm( 44)=  Tlim  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  Qreal                 
!       x(  4)=  PLLPhaseAngle         
!       x(  5)=  Vt                    
!       x(  6)=  Vm                    
!       x(  7)=  x2                    
!       x(  8)=  Ip                    
!       x(  9)=  Iq                    
!       x( 10)=  Ipcmd                 
!       x( 11)=  Iqcmd                 
!       x( 12)=  Iqmax                 
!       x( 13)=  Iqmin                 
!       x( 14)=  Ipmax                 
!       x( 15)=  Ipmin                 
!       x( 16)=  x4                    
!       x( 17)=  DeltaW                
!       x( 18)=  x1                    
!       x( 19)=  vq                    
!       x( 20)=  vd                    
!       x( 21)=  Pgen                  
!       x( 22)=  Qgen                  
!       x( 23)=  Iqext                 
!       x( 24)=  Fvl                   
!       x( 25)=  Fvli                  
!       x( 26)=  Fvhi                  
!       x( 27)=  Fvh                   
!       x( 28)=  z1                     Entrée de l'hystérésis pour HVRT
!       x( 29)=  x6                    
!       x( 30)=  x8                    
!       x( 31)=  x10                    Rentrée du timer (-Vm) pour LVRT
!       x( 32)=  x11                    Rentrée du HVRT control
!       x( 33)=  z                      Sortie du TIMER
!       x( 34)=  x5                    
!       x( 35)=  deltaV                 Rentré du SWITCH pour contrôle de Pflag
!       x( 36)=  Pflag                 
!       x( 37)=  Pflaga                 Switch input a
!       x( 38)=  Pflagb                 Switch input b
!       x( 39)=  vxl                   
!       x( 40)=  vyl                   
!       x( 41)=  vxlm                  
!       x( 42)=  vylm                  
!       x( 43)=  Iqrefa                 Switch to disable Iqref in case of a fault
!       x( 44)=  Iqrefb                
!       x( 45)=  Iqadd                 
!       x( 46)=  omegam                
!       x( 47)=  fm                    
!       x( 48)=  fmfilt                
!       x( 49)=  Ffli                   Current multiplier (input of hysteresis)
!       x( 50)=  Ffl                   
!       x( 51)=  Ffhi                  
!       x( 52)=  Ffh                    Current multiplier (output of hysteresis)
!       x( 53)=  deltaP                
!       x( 54)=  deltaPfin              Active power correction
!       x( 55)=  Ptot                  
!       x( 56)=  deltafl               
!       x( 57)=  flagla                
!       x( 58)=  flaglb                
!       x( 59)=  deltafh               
!       x( 60)=  flagha                
!       x( 61)=  flaghb                
!       x( 62)=  Iqrefcmda             
!       x( 63)=  Iqrefcmdb             
!       x( 64)=  Iqrefcmd              
!       x( 65)=  deltaVm               
!       x( 66)=  PLLmulta              
!       x( 67)=  PLLmultb              
!       x( 68)=  mult                  
!       x( 69)=  deltaVPLL             
!       x( 70)=  wpll                  
!       x( 71)=  g                     
!       x( 72)=  tr                    
!       x( 73)=  Fr                    
!       x( 74)=  fvla                  
!       x( 75)=  fvlb                  
!       x( 76)=  deltafvl              
!       x( 77)=  deltaVCM3             
!       x( 78)=  deltaIQ               
!       x( 79)=  x21                   
!       x( 80)=  deltaPlim             
!       x( 81)=  deltafcomp            
!       x( 82)=  fcomp                 
!       x( 83)=  fa                    
!       x( 84)=  fb                    
!       x( 85)=  bma                   
!       x( 86)=  bmb                   
!       x( 87)=  bm                    

!.........................................................................................................

subroutine inj_PV(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 34
      nbaddpar= 10
      parname(  1)='Imax'
      parname(  2)='IN'
      parname(  3)='ratemax'
      parname(  4)='Tg'
      parname(  5)='Tm'
      parname(  6)='tLVRT1'
      parname(  7)='tLVRT2'
      parname(  8)='tLVRTint'
      parname(  9)='Vmax'
      parname( 10)='kpll'
      parname( 11)='Vminpll'
      parname( 12)='a'
      parname( 13)='Vmin'
      parname( 14)='Vint'
      parname( 15)='fmin'
      parname( 16)='fmax'
      parname( 17)='fstart'
      parname( 18)='b'
      parname( 19)='fr'
      parname( 20)='Tr'
      parname( 21)='Re'
      parname( 22)='Xe'
      parname( 23)='CM1'
      parname( 24)='kRCI'
      parname( 25)='kRCA'
      parname( 26)='m'
      parname( 27)='n'
      parname( 28)='dbmin'
      parname( 29)='dbmax'
      parname( 30)='HVRT'
      parname( 31)='LVRT'
      parname( 32)='CM2'
      parname( 33)='CM3'
      parname( 34)='BM'
      parname( 35)='vxlv'
      parname( 36)='vylv'
      parname( 37)='Iqref'
      parname( 38)='Pext'
      parname( 39)='Uplim'
      parname( 40)='Downlim'
      parname( 41)='Vref'
      parname( 42)='UplimdeltaP'
      parname( 43)='DownlimdeltaP'
      parname( 44)='Tlim'
      adix=  1
      adiy=  2
      nbxvar= 92
      nbzvar= 25

!........................................................................................
   case (define_obs)
      nbobs= 19
      obsname(  1)='Ip'
      obsname(  2)='Iq'
      obsname(  3)='PLLPhaseAngle'
      obsname(  4)='Pgen'
      obsname(  5)='Qgen'
      obsname(  6)='Qreal'
      obsname(  7)='Vm'
      obsname(  8)='Vt'
      obsname(  9)='Ipmax'
      obsname( 10)='Iqmax'
      obsname( 11)='Iqadd'
      obsname( 12)='Iqext'
      obsname( 13)='Iqcmd'
      obsname( 14)='Ptot'
      obsname( 15)='deltaPlim'
      obsname( 16)='deltaPfin'
      obsname( 17)='deltaP'
      obsname( 18)='fmfilt'
      obsname( 19)='fm'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x(  8)              
      obs(  2)=x(  9)              
      obs(  3)=x(  4)              
      obs(  4)=x( 21)              
      obs(  5)=x( 22)              
      obs(  6)=x(  3)              
      obs(  7)=x(  6)              
      obs(  8)=x(  5)              
      obs(  9)=x( 14)              
      obs( 10)=x( 12)              
      obs( 11)=x( 45)              
      obs( 12)=x( 23)              
      obs( 13)=x( 11)              
      obs( 14)=x( 55)              
      obs( 15)=x( 80)              
      obs( 16)=x( 54)              
      obs( 17)=x( 53)              
      obs( 18)=x( 48)              
      obs( 19)=x( 47)              

!........................................................................................
   case (initialize)

!vxlv = [vx] + {Re}*[ix] - {Xe}*[iy]
      prm( 35)= vx + prm( 21)*ix - prm( 22)*iy

!vylv = [vy] + {Re}*[iy] + {Xe}*[ix]
      prm( 36)= vy + prm( 21)*iy + prm( 22)*ix

!Iqref = [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      prm( 37)= ix*sin(atan(prm( 36)/prm( 35))) - iy*cos(atan(prm( 36)/prm( 35)))

!Pext = {vxlv}*[ix] + {vylv}*[iy]
      prm( 38)= prm( 35)*ix + prm( 36)*iy

!Uplim = 9999
      prm( 39)= 9999

!Downlim = -9999
      prm( 40)= -9999

!Vref = sqrt({vxlv}**2 + {vylv}**2 )
      prm( 41)= sqrt(prm( 35)**2 + prm( 36)**2 )

!UplimdeltaP = 9999
      prm( 42)= 9999

!DownlimdeltaP = 0
      prm( 43)= 0

!Tlim = 0.01
      prm( 44)= 0.01

!Qreal =  {vylv}*[ix] - {vxlv}*[iy]
      x(  3)= prm( 36)*x(  1) - prm( 35)*x(  2)

!PLLPhaseAngle =  atan({vylv}/{vxlv})
      x(  4)= atan(prm( 36)/prm( 35))

!Vt =  sqrt({vxlv}**2+{vylv}**2)
      x(  5)= sqrt(prm( 35)**2+prm( 36)**2)

!Vm =  sqrt({vxlv}**2+{vylv}**2)
      x(  6)= sqrt(prm( 35)**2+prm( 36)**2)

!x2 =  sqrt({vxlv}**2+{vylv}**2)
      x(  7)= sqrt(prm( 35)**2+prm( 36)**2)

!Ip =  [ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv}))
      x(  8)= x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35)))

!Iq =  [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      x(  9)= x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35)))

!Ipcmd =  [ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv}))
      x( 10)= x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35)))

!Iqcmd =  [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      x( 11)= x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35)))

!Iqmax =  sqrt({IN}**2 - ([ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv})))**2 )
      x( 12)= sqrt(prm(  2)**2 - (x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35))))**2 )

!Iqmin =  -sqrt({Imax}**2 - ([ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv})))**2)
      x( 13)= -sqrt(prm(  1)**2 - (x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35))))**2)

!Ipmax =  {IN}
      x( 14)= prm(  2)

!Ipmin =  -0.001
      x( 15)= -0.001

!x4 =  ({vxlv}*[ix] + {vylv}*[iy])/sqrt({vxlv}**2+{vylv}**2)
      x( 16)= (prm( 35)*x(  1) + prm( 36)*x(  2))/sqrt(prm( 35)**2+prm( 36)**2)

!DeltaW =  -({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv})))*{kpll}
      x( 17)= -(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35))))*prm( 10)

!x1 =  ({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv})))*{kpll}
      x( 18)= (prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35))))*prm( 10)

!vq =  {vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))
      x( 19)= prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))

!vd =  {vxlv}*cos(atan({vylv}/{vxlv})) + {vylv}*sin(atan({vylv}/{vxlv}))
      x( 20)= prm( 35)*cos(atan(prm( 36)/prm( 35))) + prm( 36)*sin(atan(prm( 36)/prm( 35)))

!Pgen =  ([ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv})))*({vxlv}*cos(atan({vylv}/{vxlv})) + {vylv}*sin(atan({vylv}/{vxlv})))
      x( 21)= (x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35))))*(prm( 35)*cos(atan(prm( 36)/prm( 35))) + prm( 36)*sin(atan(prm( 36)/prm( 35))))

!Qgen =  ({vxlv}*cos(atan({vylv}/{vxlv})) + {vylv}*sin(atan({vylv}/{vxlv})))*([ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv})))
      x( 22)= (prm( 35)*cos(atan(prm( 36)/prm( 35))) + prm( 36)*sin(atan(prm( 36)/prm( 35))))*(x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35))))

!Iqext =  0.
      x( 23)= 0.

!Fvl =  1
      x( 24)= 1

!Fvli =  1
      x( 25)= 1

!Fvhi =  1
      x( 26)= 1

!Fvh =  1
      x( 27)= 1

!z1 =  0.
      x( 28)= 0.

!x6 =  [ix]*cos(atan({vylv}/{vxlv})) + [iy]*sin(atan({vylv}/{vxlv}))
      x( 29)= x(  1)*cos(atan(prm( 36)/prm( 35))) + x(  2)*sin(atan(prm( 36)/prm( 35)))

!x8 =  [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      x( 30)= x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35)))

!x10 =  -sqrt({vxlv}**2+{vylv}**2)
      x( 31)= -sqrt(prm( 35)**2+prm( 36)**2)

!x11 =  sqrt({vxlv}**2+{vylv}**2) - {Vmax}
      x( 32)= sqrt(prm( 35)**2+prm( 36)**2) - prm(  9)

!z =  0.
      x( 33)= 0.

!x5 =  [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      x( 34)= x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35)))

!deltaV =  sqrt({vxlv}**2+{vylv}**2) - 0.9
      x( 35)= sqrt(prm( 35)**2+prm( 36)**2) - 0.9

!Pflag =  1
      x( 36)= 1

!Pflaga =  1
      x( 37)= 1

!Pflagb =  0.
      x( 38)= 0.

!vxl =  {vxlv}
      x( 39)= prm( 35)

!vyl =  {vylv}
      x( 40)= prm( 36)

!vxlm =  {vxlv}
      x( 41)= prm( 35)

!vylm =  {vylv}
      x( 42)= prm( 36)

!Iqrefa =  {Iqref}
      x( 43)= prm( 37)

!Iqrefb =  {Iqref}
      x( 44)= prm( 37)

!Iqadd =  {Iqref}
      x( 45)= prm( 37)

!omegam =  -0.5*({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))) + 1
      x( 46)= -0.5*(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))) + 1

!fm =  50*(-0.5*({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))) + 1)
      x( 47)= 50*(-0.5*(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))) + 1)

!fmfilt =  50*(-0.5*({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))) + 1)
      x( 48)= 50*(-0.5*(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))) + 1)

!Ffli =  1.
      x( 49)= 1.

!Ffl =  1.
      x( 50)= 1.

!Ffhi =  1.
      x( 51)= 1.

!Ffh =  1.
      x( 52)= 1.

!deltaP =  {b}*{Pext}*(50*(-({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))) + 1)  - {fstart})/50
      x( 53)= prm( 18)*prm( 38)*(50*(-(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))) + 1)  - prm( 17))/50

!deltaPfin =  0
      x( 54)= 0

!Ptot =  {Pext}
      x( 55)= prm( 38)

!deltafl =  50*[omega] - {fmin}
      x( 56)= 50*omega - prm( 15)

!flagla =  1
      x( 57)= 1

!flaglb =  0
      x( 58)= 0

!deltafh =  {fmax} - 50*[omega]
      x( 59)= prm( 16) - 50*omega

!flagha =  1
      x( 60)= 1

!flaghb =  0
      x( 61)= 0

!Iqrefcmda =  1
      x( 62)= 1

!Iqrefcmdb =  0
      x( 63)= 0

!Iqrefcmd =  1
      x( 64)= 1

!deltaVm =  1.1 - sqrt({vxlv}**2+{vylv}**2)
      x( 65)= 1.1 - sqrt(prm( 35)**2+prm( 36)**2)

!PLLmulta =  1
      x( 66)= 1

!PLLmultb =  0
      x( 67)= 0

!mult =  1
      x( 68)= 1

!deltaVPLL =  sqrt({vxlv}**2+{vylv}**2) - {Vminpll}
      x( 69)= sqrt(prm( 35)**2+prm( 36)**2) - prm( 11)

!wpll =  -({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv})))*{kpll}
      x( 70)= -(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35))))*prm( 10)

!g =  0
      x( 71)= 0

!tr =  0
      x( 72)= 0

!Fr =  1
      x( 73)= 1

!fvla =  1
      x( 74)= 1

!fvlb =  1
      x( 75)= 1

!deltafvl =  -{Tr}
      x( 76)= -prm( 20)

!deltaVCM3 =  0
      x( 77)= 0

!deltaIQ =  0
      x( 78)= 0

!x21 =  [ix]*sin(atan({vylv}/{vxlv})) - [iy]*cos(atan({vylv}/{vxlv}))
      x( 79)= x(  1)*sin(atan(prm( 36)/prm( 35))) - x(  2)*cos(atan(prm( 36)/prm( 35)))

!deltaPlim =  0
      x( 80)= 0

!deltafcomp =  50*(-({vxlv}*sin(atan({vylv}/{vxlv})) - {vylv}*cos(atan({vylv}/{vxlv}))) + 1) - {fr}
      x( 81)= 50*(-(prm( 35)*sin(atan(prm( 36)/prm( 35))) - prm( 36)*cos(atan(prm( 36)/prm( 35)))) + 1) - prm( 19)

!fcomp =  0
      x( 82)= 0

!fa =  1
      x( 83)= 1

!fb =  0
      x( 84)= 0

!bma =  1
      x( 85)= 1

!bmb =  1- {BM}
      x( 86)= 1- prm( 34)

!bm =  1
      x( 87)= 1

!& algeq
      eqtyp(  1)=0

!& algeq
      eqtyp(  2)=0

!& tf1p
      eqtyp(  3)=  6
      tc(  3)=prm(  5)

!& max1v1c
      eqtyp(  4)=0
      if(x(  6)<0.01)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& limvb
      eqtyp(  7)=0
      if(x( 16)>x( 14))then
         z(  2)=1
      elseif(x( 16)<x( 15))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq
      eqtyp(  8)=0

!& algeq
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& swsign
      eqtyp( 11)=0
      if(x( 35)>=0.)then
         z(  3)=1
      else
         z(  3)=2
      endif

!& tf1p2lim
      if(prm(  4)< 0.001)then
         prm(  4)=0.d0
         prm( 40)=-huge(0.d0)
         prm( 39)= huge(0.d0)
         prm( 40)=-huge(0.d0)
         prm(  3)= huge(0.d0)
      endif
      if(1*x( 10)-x(  8)>prm(  3)*prm(  4))then
         z(  4)=1
      elseif(1*x( 10)-x(  8)<prm( 40)*prm(  4))then
         z(  4)=-1
      else
         z(  4)=0
      endif
      eqtyp( 12)=0
      if(x(  8)>prm( 39))then
         z(  5)=1
         eqtyp( 13)=0
      elseif(x(  8)<prm( 40))then
         z(  5)=-1
         eqtyp( 13)=0
      else
         z(  5)=0
         eqtyp( 13)=  8
      endif
      tc( 13)=prm(  4)

!& algeq
      eqtyp( 14)=0

!& algeq
      eqtyp( 15)=0

!& algeq
      eqtyp( 16)=0

!& algeq
      eqtyp( 17)=0

!& limvb
      eqtyp( 18)=0
      if(x( 79)>x( 12))then
         z(  6)=1
      elseif(x( 79)<x( 13))then
         z(  6)=-1
      else
         z(  6)=0
      endif

!& algeq
      eqtyp( 19)=0

!& tf1p
      eqtyp( 20)=  9
      tc( 20)=prm(  4)

!& algeq
      eqtyp( 21)=0

!& db
      eqtyp( 22)=0
      if(x(  6)>prm( 29))then
         z(  7)=1
      elseif(x(  6)<prm( 28))then
         z(  7)=-1
      else
         z(  7)=0
      endif

!& algeq
      eqtyp( 23)=0

!& timer5
      eqtyp( 24)=0
      eqtyp( 25)=0
      z(  8)=-1
      x( 89)=0.

!& algeq
      eqtyp( 26)=0

!& hyst
      eqtyp( 27)=0
      if(x( 25)>1.1)then
         z(  9)=1
      elseif(x( 25)<0.9)then
         z(  9)=-1
      else
         if(1.>= 0.)then
            z(  9)=1
         else
            z(  9)=-1
         endif
      endif

!& algeq
      eqtyp( 28)=0

!& inlim
      if (1>= 0.005)then
         tc( 29)=1
      endif
      if (x( 72)>5)then
         z( 10)=1
         eqtyp( 29)=0
      elseif (x( 72)<0.) then
         z( 10)=-1
         eqtyp( 29)=0
      else
         z( 10)=0
         if (1>= 0.005)then
            eqtyp( 29)= 72
         else
            eqtyp( 29)=0
         endif
      endif

!& algeq
      eqtyp( 30)=0

!& algeq
      eqtyp( 31)=0

!& algeq
      eqtyp( 32)=0

!& swsign
      eqtyp( 33)=0
      if(x( 76)>=0.)then
         z( 11)=1
      else
         z( 11)=2
      endif

!& algeq
      eqtyp( 34)=0

!& pwlin4
      eqtyp( 35)=0
      if(x( 32)<(-999))then
         z( 12)=1
      elseif(x( 32)>=999)then
         z( 12)=   3
      elseif((-999)<=x( 32) .and. x( 32)<0.)then
         z( 12)=  1
      elseif(0.<=x( 32) .and. x( 32)<0.)then
         z( 12)=  2
      elseif(0.<=x( 32) .and. x( 32)<999)then
         z( 12)=  3
      endif

!& algeq
      eqtyp( 36)=0

!& hyst
      eqtyp( 37)=0
      if(x( 26)>1.1)then
         z( 13)=1
      elseif(x( 26)<0.9)then
         z( 13)=-1
      else
         if(1.>= 0.)then
            z( 13)=1
         else
            z( 13)=-1
         endif
      endif

!& algeq
      eqtyp( 38)=0

!& algeq
      eqtyp( 39)=0

!& algeq
      eqtyp( 40)=0

!& swsign
      eqtyp( 41)=0
      if(x( 35)>=0.)then
         z( 14)=1
      else
         z( 14)=2
      endif

!& algeq
      eqtyp( 42)=0

!& algeq
      eqtyp( 43)=0

!& algeq
      eqtyp( 44)=0

!& swsign
      eqtyp( 45)=0
      if(x( 69)>=0.)then
         z( 15)=1
      else
         z( 15)=2
      endif

!& int
      if (1.< 0.005)then
         eqtyp( 46)=0
      else
         eqtyp( 46)=  4
         tc( 46)=1.
      endif

!& algeq
      eqtyp( 47)=0

!& algeq
      eqtyp( 48)=0

!& pictl
      eqtyp( 49)= 90
      x( 90)=x( 18)
      eqtyp( 50)=0

!& tf1p
      eqtyp( 51)= 41
      tc( 51)=prm(  5)

!& tf1p
      eqtyp( 52)= 42
      tc( 52)=prm(  5)

!& algeq
      eqtyp( 53)=0

!& algeq
      eqtyp( 54)=0

!& swsign
      eqtyp( 55)=0
      if(x( 35)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& algeq
      eqtyp( 56)=0

!& algeq
      eqtyp( 57)=0

!& algeq
      eqtyp( 58)=0

!& swsign
      eqtyp( 59)=0
      if(x( 65)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& algeq
      eqtyp( 60)=0

!& algeq
      eqtyp( 61)=0

!& tf1p
      eqtyp( 62)= 48
      tc( 62)=1.

!& algeq
      eqtyp( 63)=0

!& algeq
      eqtyp( 64)=0

!& algeq
      eqtyp( 65)=0

!& swsign
      eqtyp( 66)=0
      if(x( 56)>=0.)then
         z( 18)=1
      else
         z( 18)=2
      endif

!& algeq
      eqtyp( 67)=0

!& algeq
      eqtyp( 68)=0

!& algeq
      eqtyp( 69)=0

!& swsign
      eqtyp( 70)=0
      if(x( 59)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& hyst
      eqtyp( 71)=0
      if(x( 49)>1.1)then
         z( 20)=1
      elseif(x( 49)<0.9)then
         z( 20)=-1
      else
         if(1.>= 0.)then
            z( 20)=1
         else
            z( 20)=-1
         endif
      endif

!& hyst
      eqtyp( 72)=0
      if(x( 51)>1.1)then
         z( 21)=1
      elseif(x( 51)<0.9)then
         z( 21)=-1
      else
         if(1.>= 0.)then
            z( 21)=1
         else
            z( 21)=-1
         endif
      endif

!& algeq
      eqtyp( 73)=0

!& lim
      eqtyp( 74)=0
      if(x( 53)>999)then
         z( 22)=1
      elseif(x( 53)<0)then
         z( 22)=-1
      else
         z( 22)=0
      endif

!& tf1p2lim
      if(prm( 44)< 0.001)then
         prm( 44)=0.d0
         prm( 43)=-huge(0.d0)
         prm( 42)= huge(0.d0)
         prm( 43)=-huge(0.d0)
         prm( 42)= huge(0.d0)
      endif
      if(1*x( 54)-x( 80)>prm( 42)*prm( 44))then
         z( 23)=1
      elseif(1*x( 54)-x( 80)<prm( 43)*prm( 44))then
         z( 23)=-1
      else
         z( 23)=0
      endif
      eqtyp( 75)=0
      if(x( 80)>prm( 42))then
         z( 24)=1
         eqtyp( 76)=0
      elseif(x( 80)<prm( 43))then
         z( 24)=-1
         eqtyp( 76)=0
      else
         z( 24)=0
         eqtyp( 76)= 80
      endif
      tc( 76)=prm( 44)

!& algeq
      eqtyp( 77)=0

!& algeq
      eqtyp( 78)=0

!& algeq
      eqtyp( 79)=0

!& swsign
      eqtyp( 80)=0
      if(x( 81)>=0.)then
         z( 25)=1
      else
         z( 25)=2
      endif

!& algeq
      eqtyp( 81)=0

!& pictl
      eqtyp( 82)= 92
      x( 92)=x( 78)
      eqtyp( 83)=0

!& algeq
      eqtyp( 84)=0

!& algeq
      eqtyp( 85)=0

!& algeq
      eqtyp( 86)=0

!& algeq
      eqtyp( 87)=0

!& algeq
      eqtyp( 88)=0

!& algeq
      eqtyp( 89)=0

!& algeq
      eqtyp( 90)=0

!& algeq
      eqtyp( 91)=0

!& algeq
      eqtyp( 92)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq
      f(  1)=x(  3) - x( 40)*x(  1) + x( 39)*x(  2)

!& algeq
      f(  2)=x(  5) - sqrt(x( 39)**2+x( 40)**2)

!& tf1p
      f(  3)=(-x(  6)+1.*x(  5))

!& max1v1c
      select case (z(  1))
         case(1)
            f(  4)=0.01-x(  7)
         case(2)
            f(  4)=x(  6)-x(  7)
      end select

!& algeq
      f(  5)=x( 55)/x(  7) - x( 16)

!& algeq
      f(  6)=x( 15) + 0.001

!& limvb
      select case (z(  2))
         case(0)
            f(  7)=x( 29)-x( 16)
         case(-1)
            f(  7)=x( 29)-x( 15)
         case(1)
            f(  7)=x( 29)-x( 14)
      end select

!& algeq
      f(  8)=x( 29)*x( 73)*x( 27)*x( 50)*x( 52)*x( 87) - x( 10)

!& algeq
      f(  9)=x( 85) - 1

!& algeq
      f( 10)=x( 86) + prm( 34) - 1

!& swsign
      select case (z(  3))
         case(1)
            f( 11)=x( 87)-x( 85)
         case(2)
            f( 11)=x( 87)-x( 86)
      end select

!& tf1p2lim
      select case (z(  4))
         case(0)
            f( 12)=x( 88)-1*x( 10)+x(  8)
         case(1)
            f( 12)=x( 88)-prm(  3)*prm(  4)
         case(-1)
            f( 12)=x( 88)-prm( 40)*prm(  4)
      end select
      select case (z(  5))
         case(0)
            f( 13)=x( 88)
         case(1)
            f( 13)=x(  8)-prm( 39)
         case(-1)
            f( 13)=x(  8)-prm( 40)
      end select

!& algeq
      f( 14)=x( 12) -x( 36)*dsqrt(max(0.d0,prm(  2)**2 - x( 10)**2)) - (1-x( 36))*(prm( 32)*prm(  1)*0.707 + (1-prm( 32))*prm(  1)*(1- prm( 33)) + prm( 33)*dsqrt(max(0.d0,prm(  1)**2 - x( 10)**2)))

!& algeq
      f( 15)=x( 13) + x( 36)*dsqrt(max(0.d0,prm(  1)**2 - x( 10)**2)) + (1-x( 36))*prm(  2)

!& algeq
      f( 16)=-x( 23) + x( 45) - x( 34)

!& algeq
      f( 17)=x( 79) - x( 34) - x( 78)*prm( 33)*(1 - x( 36))

!& limvb
      select case (z(  6))
         case(0)
            f( 18)=x( 30)-x( 79)
         case(-1)
            f( 18)=x( 30)-x( 13)
         case(1)
            f( 18)=x( 30)-x( 12)
      end select

!& algeq
      f( 19)=x( 30)*x( 73)*x( 27)*x( 50)*x( 52) - x( 11)

!& tf1p
      f( 20)=(-x(  9)+1.*x( 11))

!& algeq
      f( 21)=x( 14) - x( 36)*prm(  2) -  (1-x( 36))*dsqrt(max(0.d0,prm(  1)**2 - x( 11)**2))

!& db
      select case (z(  7))
         case(0)
            f( 22)=x( 23)
         case(-1)
            f( 22)=x( 23)-(-prm( 26)*prm(  2)*(prm( 23) + prm( 32) + prm( 33)))-(prm( 24)*(prm( 23) + prm( 32) + prm( 33)))*(x(  6)-prm( 28))
         case(1)
            f( 22)=x( 23)-prm( 27)*prm(  2)-prm( 25)*prm( 30)*(x(  6)-prm( 29))
      end select

!& algeq
      f( 23)=x(  6) + x( 31)

!& timer5
      select case (z(  8))
         case (-1)
            f( 24)=x( 33)
            f( 25)=x( 89)
         case (0)
            f( 24)=x( 33)
            f( 25)= 1.
         case (1)
            f( 24)=x( 33)-1.
            f( 25)= 0.
      end select

!& algeq
      f( 26)=x( 25) -1 + x( 33)

!& hyst
      if(z(  9) == 1)then
         f( 27)=x( 24)-1.-(1.-1.)*(x( 25)-1.1)/(1.1-0.9)
      else
         f( 27)=x( 24)-0.-(0.-0.)*(x( 25)-0.9)/(1.1-0.9)
      endif

!& algeq
      f( 28)=x( 71) - 1 + x( 24)

!& inlim
      if (1>= 0.005)then
         select case (z( 10))
            case(0)
               f( 29)=x( 71)
            case(1)
               f( 29)=x( 72)-5
            case(-1)
               f( 29)=x( 72)-0.
         end select
      else
         select case (z( 10))
            case(0)
               f( 29)=x( 71)-x( 72)
            case(1)
               f( 29)=x( 72)-5
            case(-1)
               f( 29)=x( 72)-0.
         end select
      endif

!& algeq
      f( 30)=x( 74) - 1

!& algeq
      f( 31)=x( 75) - 1 + x( 71)

!& algeq
      f( 32)=x( 76) + prm( 20) - x( 72)

!& swsign
      select case (z( 11))
         case(1)
            f( 33)=x( 73)-x( 74)
         case(2)
            f( 33)=x( 73)-x( 75)
      end select

!& algeq
      f( 34)=x( 32) - x(  5) + prm(  9)

!& pwlin4
      select case (z( 12))
         case (  1)
            f( 35)=0.+ ( (0.-0.)*(x( 32)-(-999))/(0.-(-999)) ) -x( 28)
         case (  2)
            f( 35)=0.+ ( (1.-0.)*(x( 32)-0.)/(0.-0.) ) -x( 28)
         case (  3)
            f( 35)=1.+ ( (1.-1.)*(x( 32)-0.)/(999-0.) ) -x( 28)
      end select

!& algeq
      f( 36)=x( 26) -1 + x( 28)

!& hyst
      if(z( 13) == 1)then
         f( 37)=x( 27)-1.-(1.-1.)*(x( 26)-1.1)/(1.1-0.9)
      else
         f( 37)=x( 27)-0.-(0.-0.)*(x( 26)-0.9)/(1.1-0.9)
      endif

!& algeq
      f( 38)=x( 35) - x(  6) + 0.9

!& algeq
      f( 39)=x( 37) - 1

!& algeq
      f( 40)=x( 38)

!& swsign
      select case (z( 14))
         case(1)
            f( 41)=x( 36)-x( 37)
         case(2)
            f( 41)=x( 36)-x( 38)
      end select

!& algeq
      f( 42)=x( 66) - 1

!& algeq
      f( 43)=x( 67)

!& algeq
      f( 44)=x( 69) - x(  6) + prm( 11)

!& swsign
      select case (z( 15))
         case(1)
            f( 45)=x( 68)-x( 66)
         case(2)
            f( 45)=x( 68)-x( 67)
      end select

!& int
      if (1.< 0.005)then
         f( 46)=x( 70)-x(  4)
      else
         f( 46)=x( 70)
      endif

!& algeq
      f( 47)=x( 18) + x( 17)

!& algeq
      f( 48)=x( 70) - x( 17)*x( 68)

!& pictl
      f( 49)=0.                                                                                                                                                                                                                                                                                                          *x( 19)
      f( 50)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 19)+x( 90)-x( 18)

!& tf1p
      f( 51)=(-x( 41)+1.*x( 39))

!& tf1p
      f( 52)=(-x( 42)+1.*x( 40))

!& algeq
      f( 53)=x( 43) - prm( 37)*x( 64)

!& algeq
      f( 54)=x( 44) - (1 - prm( 34))*prm( 37)

!& swsign
      select case (z( 16))
         case(1)
            f( 55)=x( 45)-x( 43)
         case(2)
            f( 55)=x( 45)-x( 44)
      end select

!& algeq
      f( 56)=x( 62) - 1

!& algeq
      f( 57)=x( 63)

!& algeq
      f( 58)=x( 65) - 1.1 + x(  6)

!& swsign
      select case (z( 17))
         case(1)
            f( 59)=x( 64)-x( 62)
         case(2)
            f( 59)=x( 64)-x( 63)
      end select

!& algeq
      f( 60)=x( 46) - 1 - x( 17)/314

!& algeq
      f( 61)=x( 47) - 50*x( 46)

!& tf1p
      f( 62)=(-x( 48)+1.*x( 47))

!& algeq
      f( 63)=x( 57) - 1

!& algeq
      f( 64)=x( 58)

!& algeq
      f( 65)=x( 56) - x( 48) + prm( 15)

!& swsign
      select case (z( 18))
         case(1)
            f( 66)=x( 49)-x( 57)
         case(2)
            f( 66)=x( 49)-x( 58)
      end select

!& algeq
      f( 67)=x( 60) - 1

!& algeq
      f( 68)=x( 61)

!& algeq
      f( 69)=x( 59) - prm( 16) + x( 48)

!& swsign
      select case (z( 19))
         case(1)
            f( 70)=x( 51)-x( 60)
         case(2)
            f( 70)=x( 51)-x( 61)
      end select

!& hyst
      if(z( 20) == 1)then
         f( 71)=x( 50)-1.-(1.-1.)*(x( 49)-1.1)/(1.1-0.9)
      else
         f( 71)=x( 50)-0.-(0.-0.)*(x( 49)-0.9)/(1.1-0.9)
      endif

!& hyst
      if(z( 21) == 1)then
         f( 72)=x( 52)-1.-(1.-1.)*(x( 51)-1.1)/(1.1-0.9)
      else
         f( 72)=x( 52)-0.-(0.-0.)*(x( 51)-0.9)/(1.1-0.9)
      endif

!& algeq
      f( 73)=x( 53) - prm( 18)*prm( 38)*(x( 48) - prm( 17))/50

!& lim
      select case (z( 22))
         case(0)
            f( 74)=x( 54)-x( 53)
         case(-1)
            f( 74)=x( 54)-0
         case(1)
            f( 74)=x( 54)-999
      end select

!& tf1p2lim
      select case (z( 23))
         case(0)
            f( 75)=x( 91)-1*x( 54)+x( 80)
         case(1)
            f( 75)=x( 91)-prm( 42)*prm( 44)
         case(-1)
            f( 75)=x( 91)-prm( 43)*prm( 44)
      end select
      select case (z( 24))
         case(0)
            f( 76)=x( 91)
         case(1)
            f( 76)=x( 80)-prm( 42)
         case(-1)
            f( 76)=x( 80)-prm( 43)
      end select

!& algeq
      f( 77)=x( 83) - 1

!& algeq
      f( 78)=x( 84)

!& algeq
      f( 79)=x( 81) - x( 48) + prm( 19)

!& swsign
      select case (z( 25))
         case(1)
            f( 80)=x( 82)-x( 83)
         case(2)
            f( 80)=x( 82)-x( 84)
      end select

!& algeq
      f( 81)=x( 77) - prm( 41) + x(  6)

!& pictl
      f( 82)=10                                                                                                                                                                                                                                                                                                          *x( 77)
      f( 83)=10                                                                                                                                                                                                                                                                                                          *x( 77)+x( 92)-x( 78)

!& algeq
      f( 84)=x( 55) + x( 54)*(1-x( 82)) + x( 80)*x( 82) - prm( 38)

!& algeq
      f( 85)=x( 39) - prm( 21)*x(  1) + prm( 22)*x(  2) - vx

!& algeq
      f( 86)=x( 40) - prm( 21)*x(  2) - prm( 22)*x(  1) - vy

!& algeq
      f( 87)=x( 20) - x( 41)*cos(x(  4)) - x( 42)*sin(x(  4))

!& algeq
      f( 88)=x( 19) - x( 41)*sin(x(  4)) + x( 42)*cos(x(  4))

!& algeq
      f( 89)=x(  8) - x(  1)*cos(x(  4)) - x(  2)*sin(x(  4))

!& algeq
      f( 90)=x(  9) - x(  1)*sin(x(  4)) + x(  2)*cos(x(  4))

!& algeq
      f( 91)=x( 21) - x( 20)*x(  8)

!& algeq
      f( 92)=x( 22) - x( 20)*x(  9)

!........................................................................................
   case (update_disc)

!& algeq

!& algeq

!& tf1p

!& max1v1c
      select case (z(  1))
         case(1)
            if(x(  6)>0.01)then
               z(  1)=2
            endif
         case(2)
            if(0.01>x(  6))then
               z(  1)=1
            endif
      end select

!& algeq

!& algeq

!& limvb
      select case (z(  2))
         case(0)
            if(x( 16)>x( 14))then
               z(  2)=1
            elseif(x( 16)<x( 15))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 16)>x( 15))then
               z(  2)=0
            endif
         case(1)
            if(x( 16)<x( 14))then
               z(  2)=0
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z(  3))
         case(1)
            if(x( 35)<0.)then
               z(  3)=2
            endif
         case(2)
            if(x( 35)>=0.)then
               z(  3)=1
            endif
      end select

!& tf1p2lim
      select case (z(  4))
         case(0)
            if(x( 88)>prm(  3)*prm(  4))then
               z(  4)=1
            elseif(x( 88)<prm( 40)*prm(  4))then
               z(  4)=-1
            endif
         case(1)
            if(1*x( 10)-x(  8)<prm(  3)*prm(  4))then
               z(  4)= 0
            endif
         case(-1)
            if(1*x( 10)-x(  8)>prm( 40)*prm(  4))then
               z(  4)= 0
            endif
      end select
      select case (z(  5))
         case(0)
            if(x(  8)>prm( 39))then
               z(  5)=1
               eqtyp( 13)=0
            elseif(x(  8)<prm( 40))then
               z(  5)=-1
               eqtyp( 13)=0
            endif
         case(1)
            if (x( 88)<0.)then
               z(  5)= 0
               eqtyp( 13)=  8
            endif
         case(-1)
            if(x( 88)>0.)then
               z(  5)= 0
               eqtyp( 13)=  8
            endif
      end select

!& algeq

!& algeq

!& algeq

!& algeq

!& limvb
      select case (z(  6))
         case(0)
            if(x( 79)>x( 12))then
               z(  6)=1
            elseif(x( 79)<x( 13))then
               z(  6)=-1
            endif
         case(-1)
            if(x( 79)>x( 13))then
               z(  6)=0
            endif
         case(1)
            if(x( 79)<x( 12))then
               z(  6)=0
            endif
      end select

!& algeq

!& tf1p

!& algeq

!& db
      select case (z(  7))
         case(0)
            if(x(  6)>prm( 29))then
               z(  7)=1
            elseif(x(  6)<prm( 28))then
               z(  7)=-1
            endif
         case(-1)
            if(x(  6)>prm( 28))then
               z(  7)=0
            endif
         case(1)
            if(x(  6)<prm( 29))then
               z(  7)=0
            endif
      end select

!& algeq

!& timer5
      if(z(  8) == -1)then
         if(x( 31) >= (-prm( 12)))then
            z(  8)=0
            eqtyp( 25)= 89
         endif
      else
         if(x( 31) < (-prm( 12)))then
            z(  8)=-1
            eqtyp( 25)=0
         endif
      endif
      if(z(  8) == 0)then
         if(x( 31) > (-(prm( 31)*prm( 13) + (1-prm( 31))*0.8)))then
            if(x( 89) > 0.)then
               z(  8)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 13) + (1-prm( 31))*0.8)))then
            if(x( 89) > prm(  6)+(0.-prm(  6))*(x( 31)-(-(prm( 31)*prm( 13) + (1-prm( 31))*0.8)))/((-(prm( 31)*prm( 13) + (1-prm( 31))*0.8))-(-(prm( 31)*prm( 13) + (1-prm( 31))*0.8))))then
               z(  8)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 14) + (1-prm( 31))*0.8)))then
            if(x( 89) > prm(  6)+(prm(  6)-prm(  6))*(x( 31)-(-(prm( 31)*prm( 14) + (1-prm( 31))*0.8)))/((-(prm( 31)*prm( 13) + (1-prm( 31))*0.8))-(-(prm( 31)*prm( 14) + (1-prm( 31))*0.8))))then
               z(  8)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 14) + (1-prm( 31))*0.8)))then
            if(x( 89) > prm(  8)+(prm(  6)-prm(  8))*(x( 31)-(-(prm( 31)*prm( 14) + (1-prm( 31))*0.8)))/((-(prm( 31)*prm( 14) + (1-prm( 31))*0.8))-(-(prm( 31)*prm( 14) + (1-prm( 31))*0.8))))then
               z(  8)=1
            endif
         elseif(x( 31) > (-prm( 12)))then
            if(x( 89) > prm(  7)+(prm(  8)-prm(  7))*(x( 31)-(-prm( 12)))/((-(prm( 31)*prm( 14) + (1-prm( 31))*0.8))-(-prm( 12))))then
               z(  8)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  9) == -1)then
         if(x( 25)>1.1)then
            z(  9)=1
         endif
      else
         if(x( 25)<0.9)then
            z(  9)=-1
         endif
      endif

!& algeq

!& inlim
      if (1>= 0.005)then
         select case (z( 10))
            case(0)
               if(x( 72)<0.)then
                  z( 10)=-1
                  eqtyp( 29)=0
               elseif(x( 72)>5)then
                  z( 10)= 1
                  eqtyp( 29)=0
               endif
            case(1)
               if(x( 71)<0.)then
                  z( 10)=0
                  eqtyp( 29)= 72
               endif
            case(-1)
               if(x( 71)>0.)then
                  z( 10)=0
                  eqtyp( 29)= 72
               endif
         end select
      else
         select case (z( 10))
            case(0)
               if(x( 72)<0.)then
                  z( 10)=-1
               elseif(x( 72)>5)then
                  z( 10)= 1
               endif
            case(1)
               if(x( 71)<5)then
                  z( 10)=0
               endif
            case(-1)
               if(x( 71)>0.)then
                  z( 10)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 11))
         case(1)
            if(x( 76)<0.)then
               z( 11)=2
            endif
         case(2)
            if(x( 76)>=0.)then
               z( 11)=1
            endif
      end select

!& algeq

!& pwlin4
      if(x( 32)<(-999))then
         z( 12)=1
      elseif(x( 32)>=999)then
         z( 12)=  3
      elseif((-999)<=x( 32) .and. x( 32)<0.)then
         z( 12)=  1
      elseif(0.<=x( 32) .and. x( 32)<0.)then
         z( 12)=  2
      elseif(0.<=x( 32) .and. x( 32)<999)then
         z( 12)=  3
      endif

!& algeq

!& hyst
      if (z( 13) == -1)then
         if(x( 26)>1.1)then
            z( 13)=1
         endif
      else
         if(x( 26)<0.9)then
            z( 13)=-1
         endif
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 14))
         case(1)
            if(x( 35)<0.)then
               z( 14)=2
            endif
         case(2)
            if(x( 35)>=0.)then
               z( 14)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 15))
         case(1)
            if(x( 69)<0.)then
               z( 15)=2
            endif
         case(2)
            if(x( 69)>=0.)then
               z( 15)=1
            endif
      end select

!& int

!& algeq

!& algeq

!& pictl

!& tf1p

!& tf1p

!& algeq

!& algeq

!& swsign
      select case (z( 16))
         case(1)
            if(x( 35)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x( 35)>=0.)then
               z( 16)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 17))
         case(1)
            if(x( 65)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x( 65)>=0.)then
               z( 17)=1
            endif
      end select

!& algeq

!& algeq

!& tf1p

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 18))
         case(1)
            if(x( 56)<0.)then
               z( 18)=2
            endif
         case(2)
            if(x( 56)>=0.)then
               z( 18)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 19))
         case(1)
            if(x( 59)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x( 59)>=0.)then
               z( 19)=1
            endif
      end select

!& hyst
      if (z( 20) == -1)then
         if(x( 49)>1.1)then
            z( 20)=1
         endif
      else
         if(x( 49)<0.9)then
            z( 20)=-1
         endif
      endif

!& hyst
      if (z( 21) == -1)then
         if(x( 51)>1.1)then
            z( 21)=1
         endif
      else
         if(x( 51)<0.9)then
            z( 21)=-1
         endif
      endif

!& algeq

!& lim
      select case (z( 22))
         case(0)
            if(x( 53)>999)then
               z( 22)=1
            elseif(x( 53)<0)then
               z( 22)=-1
            endif
         case(-1)
            if(x( 53)>0)then
               z( 22)=0
            endif
         case(1)
            if(x( 53)<999)then
               z( 22)=0
            endif
      end select

!& tf1p2lim
      select case (z( 23))
         case(0)
            if(x( 91)>prm( 42)*prm( 44))then
               z( 23)=1
            elseif(x( 91)<prm( 43)*prm( 44))then
               z( 23)=-1
            endif
         case(1)
            if(1*x( 54)-x( 80)<prm( 42)*prm( 44))then
               z( 23)= 0
            endif
         case(-1)
            if(1*x( 54)-x( 80)>prm( 43)*prm( 44))then
               z( 23)= 0
            endif
      end select
      select case (z( 24))
         case(0)
            if(x( 80)>prm( 42))then
               z( 24)=1
               eqtyp( 76)=0
            elseif(x( 80)<prm( 43))then
               z( 24)=-1
               eqtyp( 76)=0
            endif
         case(1)
            if (x( 91)<0.)then
               z( 24)= 0
               eqtyp( 76)= 80
            endif
         case(-1)
            if(x( 91)>0.)then
               z( 24)= 0
               eqtyp( 76)= 80
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 25))
         case(1)
            if(x( 81)<0.)then
               z( 25)=2
            endif
         case(2)
            if(x( 81)>=0.)then
               z( 25)=1
            endif
      end select

!& algeq

!& pictl

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq
   end select

end subroutine inj_PV
