!  MODEL NAME : inj_ATLnoPLL            
!  MODEL DESCRIPTION FILE : ATLnoPLL.txt
!  Data :
!       prm(  1)=  Sb                              ! base power of the unit
!       prm(  2)=  Sbs                             ! base power of the system
!       prm(  3)=  vdc_star                        ! DC link voltage reference
!       prm(  4)=  pf                              ! setpoint for power factor at the terminal
!       prm(  5)=  tau                             ! Pll time constant
!       prm(  6)=  Vminpll                         ! PLL freezing voltage, PLL freezes below this value
!       prm(  7)=  tau_f                           ! filter constant for frequency
!       prm(  8)=  kp_v                            ! DC link voltage control parameters
!       prm(  9)=  ki_v
!       prm( 10)=  kp_c                            ! rectifier current control parameters
!       prm( 11)=  ki_c
!       prm( 12)=  kp_p                            ! power control parameters
!       prm( 13)=  ki_p
!       prm( 14)=  kp_w                            ! motor speed control parameters
!       prm( 15)=  ki_w
!       prm( 16)=  w_cc                            ! motor current control bandwidth
!       prm( 17)=  rt                              ! terminal impedance
!       prm( 18)=  lt
!       prm( 19)=  cdc                             ! DC link capacitance
!       prm( 20)=  kw                              ! speed/torque control constant
!       prm( 21)=  kT                              ! motor torque constant
!       prm( 22)=  ra                              ! motor anchor/stator resistance
!       prm( 23)=  H                               ! motor inertia
!       prm( 24)=  b                               ! motor friction coefficient
!       prm( 25)=  Tnm                             ! compressor torque at nominal speed
!       prm( 26)=  iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
!       prm( 27)=  wm_min                          ! limits on rotational speed
!       prm( 28)=  wm_max
!       prm( 29)=  Vmax                            ! voltage range during which unit needs to stay connected
!       prm( 30)=  Vmin
!       prm( 31)=  Vint
!       prm( 32)=  Vr
!       prm( 33)=  tLVRT1
!       prm( 34)=  tLVRT2
!       prm( 35)=  tLVRTint
!       prm( 36)=  Vtrip
!       prm( 37)=  fmin                            ! frequency control regime
!       prm( 38)=  fmax
!       prm( 39)=  Trocof                          ! delay for ROCOF measurement
!       prm( 40)=  dfmax                           ! maximum permissable ROCOF
!       prm( 41)=  VPmax                           ! power limit above 0.93 pu voltage
!       prm( 42)=  LVRT                            ! enable or disable LVRT
!       prm( 43)=  Tm                              ! measurement delay
!       prm( 44)=  protection                      ! Flag for protection, -1 for off, 1 for on
!       prm( 45)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 46)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 47)=  dPc_1_0                         ! start power for positive level 1 central controller
!       prm( 48)=  dPc_2_0                         ! start power for positive level 2 central controller
!       prm( 49)=  l_1_p_min_par                 ! power level for the activation of the central controller help in pu
!       prm( 50)=  l_1_p_max_par
!       prm( 51)=  l_1_n_min_par
!       prm( 52)=  l_1_n_max_par
!       prm( 53)=  l_2_p_min_par
!       prm( 54)=  l_2_p_max_par
!       prm( 55)=  l_2_n_min_par
!       prm( 56)=  l_2_n_max_par
!       prm( 57)=  V_n                             ! number of neighbours at voltage limit
!       prm( 58)=  P_n                             ! number of neighbours at power limit
!       prm( 59)=  Vdb_p                           ! higher voltage deadband value
!       prm( 60)=  Vdb_m                           ! lower voltage deadband value
!       prm( 61)=  ro_v                            ! neighbourhood factor for voltage
!       prm( 62)=  ro_p                            ! neighbourhood factor for power
!       prm( 63)=  Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
!       prm( 64)=  Q_min                           ! min reactive power of the unit in pu, IT WIL
!       prm( 65)=  s_f                             ! security factor for unit limit control
!       prm( 66)=  lvl                             ! level of emergency of the central controller
!       prm( 67)=  pl1_2_0                        ! flags that varies for the different units
!       prm( 68)=  pl1_4_0
!       prm( 69)=  pl1_6_0
!       prm( 70)=  pl1_8_0
!       prm( 71)=  pl2_2_0                        ! flags that varies for the different units
!       prm( 72)=  pl2_4_0
!       prm( 73)=  pl2_6_0
!       prm( 74)=  pl2_8_0
!       prm( 75)=  F_help_high_0
!       prm( 76)=  F_help_low_0
!       prm( 77)=  Tp1
!       prm( 78)=  Tp2
!       prm( 79)=  nadir
!       prm( 80)=  V_max_nb
!       prm( 81)=  V_min_nb
!       prm( 82)=  dPs_rate_max
!  Parameters :
!       prm( 83)=  w0  
!       prm( 84)=  wb  
!       prm( 85)=  t2   Torque polynomial parameters, set to constant torque
!       prm( 86)=  t1  
!       prm( 87)=  t0  
!       prm( 88)=  Downlim  
!       prm( 89)=  Uplim  
!       prm( 90)=  Downlimdisc  
!       prm( 91)=  Uplimdis  
!       prm( 92)=  downlimdis  
!       prm( 93)=  theta0  
!       prm( 94)=  P0   initial active power
!       prm( 95)=  Q0   initial reactive power
!       prm( 96)=  P0_unit  
!       prm( 97)=  Q0_unit  
!       prm( 98)=  V0   initial voltage magnitude at bus
!       prm( 99)=  iP0   current alignment
!       prm(100)=  iQ0  
!       prm(101)=  vd0   voltage alignment
!       prm(102)=  vq0  
!       prm(103)=  md0   modulation indices
!       prm(104)=  mq0  
!       prm(105)=  idc0   DC link current
!       prm(106)=  iT0  
!       prm(107)=  wm0  
!       prm(108)=  Te0  
!       prm(109)=  Tc0  
!       prm(110)=  {dPs_rate_min}  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vd                    
!       x(  4)=  vq                    
!       x(  5)=  theta                 
!       x(  6)=  vdc_ref               
!       x(  7)=  V                     
!       x(  8)=  vxm                   
!       x(  9)=  vym                   
!       x( 10)=  Vm                    
!       x( 11)=  iP                    
!       x( 12)=  diP                   
!       x( 13)=  iQ                    
!       x( 14)=  diQ                   
!       x( 15)=  P                     
!       x( 16)=  dp                    
!       x( 17)=  Punit                 
!       x( 18)=  Q                     
!       x( 19)=  Qunit                 
!       x( 20)=  Pref_lim               limited P reference
!       x( 21)=  dw_pll                
!       x( 22)=  dw_pllf               
!       x( 23)=  deltaf                
!       x( 24)=  w_pll                 
!       x( 25)=  f                     
!       x( 26)=  fi                    
!       x( 27)=  vdc                   
!       x( 28)=  dvdc                  
!       x( 29)=  iP_ref                
!       x( 30)=  Plim                  
!       x( 31)=  iQ_ref                
!       x( 32)=  mdr                   
!       x( 33)=  mqr                   
!       x( 34)=  md                    
!       x( 35)=  mq                    
!       x( 36)=  dvd                   
!       x( 37)=  dvq                   
!       x( 38)=  idc                   
!       x( 39)=  didc                  
!       x( 40)=  wm_ref                
!       x( 41)=  wm_ref_lim            
!       x( 42)=  iT_ref                
!       x( 43)=  iT                    
!       x( 44)=  dwm                   
!       x( 45)=  wm                    
!       x( 46)=  Te                    
!       x( 47)=  dT                    
!       x( 48)=  F_pll                 
!       x( 49)=  dv_pll                
!       x( 50)=  PLLmulta              
!       x( 51)=  PLLmultb              
!       x( 52)=  deltafvh              
!       x( 53)=  z1                    
!       x( 54)=  Fvhi                  
!       x( 55)=  Fvh                   
!       x( 56)=  iPs                   
!       x( 57)=  iQs                   
!       x( 58)=  Pref                  
!       x( 59)=  x10                   
!       x( 60)=  z                     
!       x( 61)=  Fvl                   
!       x( 62)=  Fvli                  
!       x( 63)=  deltafl               
!       x( 64)=  flagla                
!       x( 65)=  flaglb                
!       x( 66)=  Ffl                   
!       x( 67)=  Ffli                  
!       x( 68)=  deltafh               
!       x( 69)=  flagha                
!       x( 70)=  flaghb                
!       x( 71)=  Ffh                   
!       x( 72)=  Ffhi                  
!       x( 73)=  rocof                 
!       x( 74)=  abrocof               
!       x( 75)=  flagra                
!       x( 76)=  flagrb                
!       x( 77)=  deltarocof            
!       x( 78)=  Ffri                  
!       x( 79)=  Ffr                   
!       x( 80)=  Plim_min              
!       x( 81)=  status                 status of the device, 1 for on
!       x( 82)=  p1                    
!       x( 83)=  p2                    
!       x( 84)=  p3                    
!       x( 85)=  Fpl1                   placeholder for value 1 in switch block
!       x( 86)=  Fpl0                   placeholder for value 0 in switch block
!       x( 87)=  V_lim_min              unit controller states
!       x( 88)=  V_lim_max             
!       x( 89)=  P_lim_min             
!       x( 90)=  P_lim_max             
!       x( 91)=  pl1                   
!       x( 92)=  F_v_min_in            
!       x( 93)=  pl2                   
!       x( 94)=  F_v_max_in            
!       x( 95)=  F_v_opp               
!       x( 96)=  pl3                   
!       x( 97)=  F_p_min_in            
!       x( 98)=  pl4                   
!       x( 99)=  F_p_max_in            
!       x(100)=  F_v_min               
!       x(101)=  F_v_max               
!       x(102)=  F_p_min               
!       x(103)=  F_p_max               
!       x(104)=  F_lim                 
!       x(105)=  dP_lim                
!       x(106)=  dQ_lim                
!       x(107)=  dQ_sum                
!       x(108)=  dP_sum1               
!       x(109)=  dP_sum2               
!       x(110)=  dP_sum                
!       x(111)=  nadir_var             
!       x(112)=  P_n_var                neighbourhood controller states
!       x(113)=  P_n_var_neg           
!       x(114)=  V_n_var               
!       x(115)=  V_n_var_neg           
!       x(116)=  V_n_abs               
!       x(117)=  pl5                   
!       x(118)=  F_help_v              
!       x(119)=  F_help_v_p            
!       x(120)=  F_help_v_m            
!       x(121)=  F_help_p_p            
!       x(122)=  F_help_p_m            
!       x(123)=  F_hlp_high            
!       x(124)=  F_hlp_low             
!       x(125)=  F_hlp_v_p2            
!       x(126)=  F_hlp_v_m2            
!       x(127)=  F_hlp_p_p2            
!       x(128)=  F_hlp_p_m2            
!       x(129)=  dQp                   
!       x(130)=  dQm                   
!       x(131)=  dQnb                  
!       x(132)=  dPp                   
!       x(133)=  dPm                   
!       x(134)=  dPnb                  
!       x(135)=  iQnb                  
!       x(136)=  dPc_1_neg              central controller states
!       x(137)=  dPc_1_pos             
!       x(138)=  dPc_2_pos             
!       x(139)=  dPc_2_neg             
!       x(140)=  dPc_3_pos             
!       x(141)=  dPc_3_neg             
!       x(142)=  level                 
!       x(143)=  dPc_1_in              
!       x(144)=  dPc_2_in              
!       x(145)=  dPc_1                 
!       x(146)=  dPc_2                 
!       x(147)=  dPc_3                 
!       x(148)=  dPc_0                 
!       x(149)=  pl1_1                 
!       x(150)=  pl1_2                 
!       x(151)=  pl1_3                 
!       x(152)=  pl1_4                 
!       x(153)=  F_l_1_neg             
!       x(154)=  pl1_5                 
!       x(155)=  pl1_6                 
!       x(156)=  pl1_7                 
!       x(157)=  pl1_8                 
!       x(158)=  F_l_1_pos             
!       x(159)=  F_l_1                 
!       x(160)=  dPc_1_s_fl            
!       x(161)=  pl2_1                 
!       x(162)=  pl2_2                 
!       x(163)=  pl2_3                 
!       x(164)=  pl2_4                 
!       x(165)=  F_l_2_neg             
!       x(166)=  pl2_5                 
!       x(167)=  pl2_6                 
!       x(168)=  pl2_7                 
!       x(169)=  pl2_8                 
!       x(170)=  F_l_2_pos             
!       x(171)=  F_l_2                 
!       x(172)=  dPc_2_s_fl            
!       x(173)=  l_abs                 
!       x(174)=  l_switch              
!       x(175)=  dPc                   
!       x(176)=  theta_est             
!       x(177)=  TP2                   

!.........................................................................................................

subroutine inj_ATLnoPLL(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 82
      nbaddpar= 28
      parname(  1)='Sb'
      parname(  2)='Sbs'
      parname(  3)='vdc_star'
      parname(  4)='pf'
      parname(  5)='tau'
      parname(  6)='Vminpll'
      parname(  7)='tau_f'
      parname(  8)='kp_v'
      parname(  9)='ki_v'
      parname( 10)='kp_c'
      parname( 11)='ki_c'
      parname( 12)='kp_p'
      parname( 13)='ki_p'
      parname( 14)='kp_w'
      parname( 15)='ki_w'
      parname( 16)='w_cc'
      parname( 17)='rt'
      parname( 18)='lt'
      parname( 19)='cdc'
      parname( 20)='kw'
      parname( 21)='kT'
      parname( 22)='ra'
      parname( 23)='H'
      parname( 24)='b'
      parname( 25)='Tnm'
      parname( 26)='iF'
      parname( 27)='wm_min'
      parname( 28)='wm_max'
      parname( 29)='Vmax'
      parname( 30)='Vmin'
      parname( 31)='Vint'
      parname( 32)='Vr'
      parname( 33)='tLVRT1'
      parname( 34)='tLVRT2'
      parname( 35)='tLVRTint'
      parname( 36)='Vtrip'
      parname( 37)='fmin'
      parname( 38)='fmax'
      parname( 39)='Trocof'
      parname( 40)='dfmax'
      parname( 41)='VPmax'
      parname( 42)='LVRT'
      parname( 43)='Tm'
      parname( 44)='protection'
      parname( 45)='P_max'
      parname( 46)='P_min'
      parname( 47)='dPc_1_0'
      parname( 48)='dPc_2_0'
      parname( 49)='l_1_p_min_par'
      parname( 50)='l_1_p_max_par'
      parname( 51)='l_1_n_min_par'
      parname( 52)='l_1_n_max_par'
      parname( 53)='l_2_p_min_par'
      parname( 54)='l_2_p_max_par'
      parname( 55)='l_2_n_min_par'
      parname( 56)='l_2_n_max_par'
      parname( 57)='V_n'
      parname( 58)='P_n'
      parname( 59)='Vdb_p'
      parname( 60)='Vdb_m'
      parname( 61)='ro_v'
      parname( 62)='ro_p'
      parname( 63)='Q_max'
      parname( 64)='Q_min'
      parname( 65)='s_f'
      parname( 66)='lvl'
      parname( 67)='pl1_2_0'
      parname( 68)='pl1_4_0'
      parname( 69)='pl1_6_0'
      parname( 70)='pl1_8_0'
      parname( 71)='pl2_2_0'
      parname( 72)='pl2_4_0'
      parname( 73)='pl2_6_0'
      parname( 74)='pl2_8_0'
      parname( 75)='F_help_high_0'
      parname( 76)='F_help_low_0'
      parname( 77)='Tp1'
      parname( 78)='Tp2'
      parname( 79)='nadir'
      parname( 80)='V_max_nb'
      parname( 81)='V_min_nb'
      parname( 82)='dPs_rate_max'
      parname( 83)='w0'
      parname( 84)='wb'
      parname( 85)='t2'
      parname( 86)='t1'
      parname( 87)='t0'
      parname( 88)='Downlim'
      parname( 89)='Uplim'
      parname( 90)='Downlimdisc'
      parname( 91)='Uplimdis'
      parname( 92)='downlimdis'
      parname( 93)='theta0'
      parname( 94)='P0'
      parname( 95)='Q0'
      parname( 96)='P0_unit'
      parname( 97)='Q0_unit'
      parname( 98)='V0'
      parname( 99)='iP0'
      parname(100)='iQ0'
      parname(101)='vd0'
      parname(102)='vq0'
      parname(103)='md0'
      parname(104)='mq0'
      parname(105)='idc0'
      parname(106)='iT0'
      parname(107)='wm0'
      parname(108)='Te0'
      parname(109)='Tc0'
      parname(110)='{dPs_rate_min}'
      adix=  1
      adiy=  2
      nbxvar=186
      nbzvar= 47

!........................................................................................
   case (define_obs)
      nbobs= 30
      obsname(  1)='iP'
      obsname(  2)='iQ'
      obsname(  3)='vd'
      obsname(  4)='vq'
      obsname(  5)='P'
      obsname(  6)='Punit'
      obsname(  7)='Pref'
      obsname(  8)='Q'
      obsname(  9)='f'
      obsname( 10)='wm'
      obsname( 11)='Vm'
      obsname( 12)='Pref_lim'
      obsname( 13)='wm_ref_lim'
      obsname( 14)='status'
      obsname( 15)='rocof'
      obsname( 16)='F_v_min'
      obsname( 17)='F_v_max'
      obsname( 18)='F_p_min'
      obsname( 19)='F_p_max'
      obsname( 20)='F_hlp_high'
      obsname( 21)='F_hlp_low'
      obsname( 22)='F_hlp_p_p2'
      obsname( 23)='F_hlp_p_m2'
      obsname( 24)='dPnb'
      obsname( 25)='dQnb'
      obsname( 26)='P_n_var'
      obsname( 27)='V_n_var'
      obsname( 28)='dP_sum'
      obsname( 29)='dP_lim'
      obsname( 30)='TP2'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x( 11)              
      obs(  2)=x( 13)              
      obs(  3)=x(  3)              
      obs(  4)=x(  4)              
      obs(  5)=x( 15)              
      obs(  6)=x( 17)              
      obs(  7)=x( 58)              
      obs(  8)=x( 18)              
      obs(  9)=x( 25)              
      obs( 10)=x( 45)              
      obs( 11)=x( 10)              
      obs( 12)=x( 20)              
      obs( 13)=x( 41)              
      obs( 14)=x( 81)              
      obs( 15)=x( 73)              
      obs( 16)=x(100)              
      obs( 17)=x(101)              
      obs( 18)=x(102)              
      obs( 19)=x(103)              
      obs( 20)=x(123)              
      obs( 21)=x(124)              
      obs( 22)=x(127)              
      obs( 23)=x(128)              
      obs( 24)=x(134)              
      obs( 25)=x(131)              
      obs( 26)=x(112)              
      obs( 27)=x(114)              
      obs( 28)=x(110)              
      obs( 29)=x(105)              
      obs( 30)=x(177)              

!........................................................................................
   case (initialize)

!w0 = 1
      prm( 83)= 1

!wb = 2*pi*fnom
      prm( 84)= 2*pi*fnom

!t2 = 0
      prm( 85)= 0

!t1 = 0
      prm( 86)= 0

!t0 = 1
      prm( 87)= 1

!Downlim = -9999
      prm( 88)= -9999

!Uplim = 9999
      prm( 89)= 9999

!Downlimdisc = 0
      prm( 90)= 0

!Uplimdis = 0
      prm( 91)= 0

!downlimdis = -9999
      prm( 92)= -9999

!theta0 = atan([vy]/[vx])
      prm( 93)= atan(vy/vx)

!P0 = ([vx]*[ix]+[vy]*[iy])
      prm( 94)= (vx*ix+vy*iy)

!Q0 = [vy]*[ix]-[vx]*[iy]
      prm( 95)= vy*ix-vx*iy

!P0_unit = -{P0}*{Sbs}/{Sb}
      prm( 96)= -prm( 94)*prm(  2)/prm(  1)

!Q0_unit = -{Q0}*{Sbs}/{Sb}
      prm( 97)= -prm( 95)*prm(  2)/prm(  1)

!V0 = dsqrt([vx]**2+[vy]**2)
      prm( 98)= dsqrt(vx**2+vy**2)

!iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}
      prm( 99)=  (-ix*cos(prm( 93))-iy*sin(prm( 93)))*prm(  2)/prm(  1)

!iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
      prm(100)=  (-ix*sin(prm( 93))+iy*cos(prm( 93)))*prm(  2)/prm(  1)

!vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})
      prm(101)= vx*cos(prm( 93))+vy*sin(prm( 93))

!vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
      prm(102)= vx*sin(prm( 93))-vy*cos(prm( 93))

!md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})
      prm(103)= 1/prm(  3)*(prm(101)+prm( 18)*prm( 83)*prm(100)-prm( 17)*prm( 99))

!mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
      prm(104)= 1/prm(  3)*(prm(102)-prm( 18)*prm( 83)*prm( 99)-prm( 17)*prm(100))

!idc0 = {md0}*{iP0}+{mq0}*{iQ0}
      prm(105)= prm(103)*prm( 99)+prm(104)*prm(100)

!iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
      prm(106)= prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)) + dsqrt((prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)))**2-(prm( 22)*prm( 24)*prm( 26)**2-prm( 24)*prm(105)*prm(  3))/(prm( 21)**2+prm( 22)*prm( 24)))

!wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
      prm(107)= 1/prm( 24)*(prm( 21)*prm(106)-prm( 25))

!Te0 = {iT0}*{kT}
      prm(108)= prm(106)*prm( 21)

!Tc0 = {Tnm}
      prm(109)= prm( 25)

!{dPs_rate_min} = -{dPs_rate_max}
      prm(110)= -prm( 82)

!vd =  {vd0}
      x(  3)= prm(101)

!vq =  {vq0}
      x(  4)= prm(102)

!theta =  {theta0}
      x(  5)= prm( 93)

!vdc_ref = {vdc_star}
      x(  6)=prm(  3)

!V =  {V0}
      x(  7)= prm( 98)

!vxm =  [vx]
      x(  8)= vx

!vym =  [vy]
      x(  9)= vy

!Vm =  {V0}
      x( 10)= prm( 98)

!iP =   {iP0}
      x( 11)=  prm( 99)

!diP =  0
      x( 12)= 0

!iQ =   {iQ0}
      x( 13)=  prm(100)

!diQ =  0
      x( 14)= 0

!P =  {P0}
      x( 15)= prm( 94)

!dp =  0
      x( 16)= 0

!Punit =  {P0_unit}
      x( 17)= prm( 96)

!Q =  {Q0}
      x( 18)= prm( 95)

!Qunit =  {Q0_unit}
      x( 19)= prm( 97)

!Pref_lim =  {P0_unit}
      x( 20)= prm( 96)

!dw_pll =  0
      x( 21)= 0

!dw_pllf =  0
      x( 22)= 0

!deltaf =  0
      x( 23)= 0

!w_pll =  {wb}
      x( 24)= prm( 84)

!f =  fnom
      x( 25)= fnom

!fi =  fnom
      x( 26)= fnom

!vdc =  {vdc_star}
      x( 27)= prm(  3)

!dvdc =  0
      x( 28)= 0

!iP_ref =  {iP0}
      x( 29)= prm( 99)

!Plim =  {VPmax}
      x( 30)= prm( 41)

!iQ_ref =  {iQ0}
      x( 31)= prm(100)

!mdr =  {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
      x( 32)= prm( 18)/prm(  3)*prm(100)*prm( 83)-prm(103)

!mqr =  -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
      x( 33)= -prm( 18)/prm(  3)*prm( 99)*prm( 83)-prm(104)

!md =  {md0}
      x( 34)= prm(103)

!mq =  {mq0}
      x( 35)= prm(104)

!dvd =  {iP0}*{rt}
      x( 36)= prm( 99)*prm( 17)

!dvq =  {iQ0}*{rt}
      x( 37)= prm(100)*prm( 17)

!idc =  {idc0}
      x( 38)= prm(105)

!didc =  {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
      x( 39)= prm(103)*prm( 99)+prm(104)*prm(100)-prm(105)

!wm_ref =  {wm0}
      x( 40)= prm(107)

!wm_ref_lim =  {wm0}
      x( 41)= prm(107)

!iT_ref =  {iT0}
      x( 42)= prm(106)

!iT =  {iT0}
      x( 43)= prm(106)

!dwm =  0
      x( 44)= 0

!wm =  {wm0}
      x( 45)= prm(107)

!Te =  {kT}*{iT0}
      x( 46)= prm( 21)*prm(106)

!dT =  {kT}*{iT0}-{Tc0}
      x( 47)= prm( 21)*prm(106)-prm(109)

!F_pll =  1
      x( 48)= 1

!dv_pll =  {V0}-{Vminpll}
      x( 49)= prm( 98)-prm(  6)

!PLLmulta =  1
      x( 50)= 1

!PLLmultb =  0
      x( 51)= 0

!deltafvh =   {V0}-{Vmax}
      x( 52)=  prm( 98)-prm( 29)

!z1 =  0
      x( 53)= 0

!Fvhi =  1
      x( 54)= 1

!Fvh =  1
      x( 55)= 1

!iPs =  {iP0}
      x( 56)= prm( 99)

!iQs =  {iQ0}
      x( 57)= prm(100)

!Pref =  {P0_unit}
      x( 58)= prm( 96)

!x10 =  -{V0}
      x( 59)= -prm( 98)

!z = 0
      x( 60)=0

!Fvl =  1
      x( 61)= 1

!Fvli =  1
      x( 62)= 1

!deltafl =  [f] - {fmin}
      x( 63)= x( 25) - prm( 37)

!flagla =  1
      x( 64)= 1

!flaglb =  0
      x( 65)= 0

!Ffl =  1
      x( 66)= 1

!Ffli =  1
      x( 67)= 1

!deltafh =  {fmax} - [f]
      x( 68)= prm( 38) - x( 25)

!flagha =  1
      x( 69)= 1

!flaghb =  0
      x( 70)= 0

!Ffh =  1
      x( 71)= 1

!Ffhi =  1
      x( 72)= 1

!rocof =  0
      x( 73)= 0

!abrocof =  0
      x( 74)= 0

!flagra =  1
      x( 75)= 1

!flagrb =  0
      x( 76)= 0

!deltarocof =  {dfmax} - 0
      x( 77)= prm( 40) - 0

!Ffri =  1
      x( 78)= 1

!Ffr =  1
      x( 79)= 1

!Plim_min =  0
      x( 80)= 0

!status =  1
      x( 81)= 1

!p1 =  {protection}
      x( 82)= prm( 44)

!p2 =  1
      x( 83)= 1

!p3 =  1
      x( 84)= 1

!Fpl1 =  1
      x( 85)= 1

!Fpl0 =  0
      x( 86)= 0

!V_lim_min =  {Vmin} * {s_f}
      x( 87)= prm( 30) * prm( 65)

!V_lim_max =  {Vmax} / {s_f}
      x( 88)= prm( 29) / prm( 65)

!P_lim_min =  {P_min} * {s_f}
      x( 89)= prm( 46) * prm( 65)

!P_lim_max =  {P_max} / {s_f}
      x( 90)= prm( 45) / prm( 65)

!pl1 =  {Vmin} * {s_f} - {V0}
      x( 91)= prm( 30) * prm( 65) - prm( 98)

!F_v_min_in =  0
      x( 92)= 0

!pl2 =  {V0} - {Vmax} / {s_f}
      x( 93)= prm( 98) - prm( 29) / prm( 65)

!F_v_max_in =  0
      x( 94)= 0

!F_v_opp =  1
      x( 95)= 1

!pl3 =  {P_min} * {s_f} - {P0_unit}
      x( 96)= prm( 46) * prm( 65) - prm( 96)

!F_p_min_in =  0
      x( 97)= 0

!pl4 =  {P0_unit} - {P_max} / {s_f}
      x( 98)= prm( 96) - prm( 45) / prm( 65)

!F_p_max_in =  0
      x( 99)= 0

!F_v_min =  0
      x(100)= 0

!F_v_max =  0
      x(101)= 0

!F_p_min =  0
      x(102)= 0

!F_p_max =  0
      x(103)= 0

!F_lim =  0
      x(104)= 0

!dP_lim =  0
      x(105)= 0

!dQ_lim =  0
      x(106)= 0

!dQ_sum =  0
      x(107)= 0

!dP_sum1 =  0
      x(108)= 0

!dP_sum2 =  0
      x(109)= 0

!dP_sum =  0
      x(110)= 0

!nadir_var =  -1
      x(111)= -1

!P_n_var =  {P_n}
      x(112)= prm( 58)

!P_n_var_neg =  -{P_n}
      x(113)= -prm( 58)

!V_n_var =  {V_n}
      x(114)= prm( 57)

!V_n_var_neg =  -{V_n}
      x(115)= -prm( 57)

!V_n_abs =  {V_n}
      x(116)= prm( 57)

!pl5 =  {V_n} - 1
      x(117)= prm( 57) - 1

!F_help_v =  0
      x(118)= 0

!F_help_v_p =  0
      x(119)= 0

!F_help_v_m =  0
      x(120)= 0

!F_help_p_p =  0
      x(121)= 0

!F_help_p_m =  0
      x(122)= 0

!F_hlp_high =  {F_help_high_0}
      x(123)= prm( 75)

!F_hlp_low =  {F_help_low_0}
      x(124)= prm( 76)

!F_hlp_v_p2 =  [F_help_v_p] * [F_hlp_high]
      x(125)= x(119) * x(123)

!F_hlp_v_m2 =  [F_help_v_m ]* [F_hlp_low]
      x(126)= x(120)* x(124)

!F_hlp_p_p2 =  [F_help_p_p] * (1-[F_help_v])
      x(127)= x(121) * (1-x(118))

!F_hlp_p_m2 =  [F_help_p_m] * (1-[F_help_v])
      x(128)= x(122) * (1-x(118))

!dQp =  0
      x(129)= 0

!dQm =  0
      x(130)= 0

!dQnb =  0
      x(131)= 0

!dPp =  0
      x(132)= 0

!dPm =  0
      x(133)= 0

!dPnb =  0
      x(134)= 0

!iQnb =  0
      x(135)= 0

!dPc_1_neg =  - {dPc_1_0}
      x(136)= - prm( 47)

!dPc_1_pos =  {dPc_1_0}
      x(137)= prm( 47)

!dPc_2_pos =  {dPc_2_0}
      x(138)= prm( 48)

!dPc_2_neg =  - {dPc_2_0}
      x(139)= - prm( 48)

!dPc_3_pos =  {P_max}/{s_f} - {P0_unit}
      x(140)= prm( 45)/prm( 65) - prm( 96)

!dPc_3_neg =  {P0_unit} - {P_min}*{s_f}
      x(141)= prm( 96) - prm( 46)*prm( 65)

!level =  {lvl}
      x(142)= prm( 66)

!dPc_1_in =  {dPc_1_0}
      x(143)= prm( 47)

!dPc_2_in =  {dPc_2_0}
      x(144)= prm( 48)

!dPc_1 =  {dPc_1_0}
      x(145)= prm( 47)

!dPc_2 =  {dPc_2_0}
      x(146)= prm( 48)

!dPc_3 =  {P_max}/{s_f} - {P0_unit}
      x(147)= prm( 45)/prm( 65) - prm( 96)

!dPc_0 =  0
      x(148)= 0

!pl1_1 =  {P0_unit} - {l_1_n_min_par}
      x(149)= prm( 96) - prm( 51)

!pl1_2 =  {pl1_2_0}
      x(150)= prm( 67)

!pl1_3 =  {l_1_n_max_par} - {P0_unit}
      x(151)= prm( 52) - prm( 96)

!pl1_4 =  {pl1_4_0}
      x(152)= prm( 68)

!F_l_1_neg =  {pl1_2_0} * {pl1_4_0}
      x(153)= prm( 67) * prm( 68)

!pl1_5 =  {P0_unit} - {l_1_p_min_par}
      x(154)= prm( 96) - prm( 49)

!pl1_6 =  {pl1_6_0}
      x(155)= prm( 69)

!pl1_7 =  {l_1_p_max_par} - {P0_unit}
      x(156)= prm( 50) - prm( 96)

!pl1_8 =  {pl1_8_0}
      x(157)= prm( 70)

!F_l_1_pos =  {pl1_6_0} * {pl1_8_0}
      x(158)= prm( 69) * prm( 70)

!F_l_1 =  [F_l_1_pos]
      x(159)= x(158)

!dPc_1_s_fl =  {dPc_1_0} * [F_l_1]
      x(160)= prm( 47) * x(159)

!pl2_1 =  {P0_unit} - {l_2_n_min_par}
      x(161)= prm( 96) - prm( 55)

!pl2_2 =  {pl2_2_0}
      x(162)= prm( 71)

!pl2_3 =  {l_2_n_max_par} - {P0_unit}
      x(163)= prm( 56) - prm( 96)

!pl2_4 =  {pl2_4_0}
      x(164)= prm( 72)

!F_l_2_neg =  {pl2_2_0} * {pl2_4_0}
      x(165)= prm( 71) * prm( 72)

!pl2_5 =  {P0_unit} - {l_2_p_min_par}
      x(166)= prm( 96) - prm( 53)

!pl2_6 =  {pl2_6_0}
      x(167)= prm( 73)

!pl2_7 =  {l_2_p_max_par} - {P0_unit}
      x(168)= prm( 54) - prm( 96)

!pl2_8 =  {pl2_8_0}
      x(169)= prm( 74)

!F_l_2_pos =  {pl2_6_0} * {pl2_8_0}
      x(170)= prm( 73) * prm( 74)

!F_l_2 =  [F_l_2_pos]
      x(171)= x(170)

!dPc_2_s_fl =  {dPc_2_0} * [F_l_2]
      x(172)= prm( 48) * x(171)

!l_abs =  0
      x(173)= 0

!l_switch =  1
      x(174)= 1

!dPc =  0
      x(175)= 0

!theta_est =  {theta0}
      x(176)= prm( 93)

!TP2 =  {Tp2}
      x(177)= prm( 78)

!& algeq                                             ! voltage magnitude
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)= 10
      tc(  2)=prm( 43)

!& tf1p
      eqtyp(  3)=  8
      tc(  3)=prm( 43)

!& tf1p
      eqtyp(  4)=  9
      tc(  4)=prm( 43)

!& algeq                                             ! voltage alignment
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& algeq
      eqtyp(  7)=0

!& algeq                                             ! compute ix
      eqtyp(  8)=0

!& algeq                                             ! compute iy
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq
      eqtyp( 11)=0

!& algeq                                             ! compute powers
      eqtyp( 12)=0

!& algeq
      eqtyp( 13)=0

!& algeq                                             ! compute powers
      eqtyp( 14)=0

!& algeq
      eqtyp( 15)=0

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         eqtyp( 16)=0
      else
         eqtyp( 16)=  5
         tc( 16)=1.d0
      endif

!& pictl                                             ! PLL
      eqtyp( 17)=178
      x(178)=x( 24)
      eqtyp( 18)=0

!& algeq                                             ! frequency deviation
      eqtyp( 19)=0

!& algeq
      eqtyp( 20)=0

!& algeq                                             ! compute and filter frequency
      eqtyp( 21)=0

!& tf1p
      eqtyp( 22)= 25
      tc( 22)=prm(  7)

!& algeq                                             ! PLL freezing
      eqtyp( 23)=0

!& algeq
      eqtyp( 24)=0

!& algeq
      eqtyp( 25)=0

!& swsign
      eqtyp( 26)=0
      if(x( 49)>=0.)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq
      eqtyp( 27)=0

!& algeq                                             ! DC voltage control
      eqtyp( 28)=0

!& pictl
      eqtyp( 29)=179
      x(179)=x( 29)
      eqtyp( 30)=0

!& algeq                                             ! p-axis current control
      eqtyp( 31)=0

!& pictl
      eqtyp( 32)=180
      x(180)=x( 32)
      eqtyp( 33)=0

!& algeq                                             ! q-axis current control
      eqtyp( 34)=0

!& algeq
      eqtyp( 35)=0

!& pictl
      eqtyp( 36)=181
      x(181)=x( 33)
      eqtyp( 37)=0

!& algeq                                             ! md
      eqtyp( 38)=0

!& algeq                                             ! mq
      eqtyp( 39)=0

!& algeq                                             ! voltage over d-axis terminal impedance
      eqtyp( 40)=0

!& algeq                                             ! voltage over q-axis terminal impedance
      eqtyp( 41)=0

!& tf1p                                              ! d-axis current
      eqtyp( 42)= 11
      tc( 42)=prm( 18)/(prm( 84)*prm( 17))

!& tf1p                                              ! q-axis current
      eqtyp( 43)= 13
      tc( 43)=prm( 18)/(prm( 84)*prm( 17))

!& algeq                                             ! DC link current
      eqtyp( 44)=0

!& algeq                                             ! DC link
      eqtyp( 45)=0

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 84)< 0.005)then
         eqtyp( 46)=0
      else
         eqtyp( 46)= 27
         tc( 46)=prm( 19)/prm( 84)
      endif

!& limvb                                              ! power limiter
      eqtyp( 47)=0
      if(x( 58)>x( 30))then
         z(  2)=1
      elseif(x( 58)<x( 80))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq                                             ! power mismatch
      eqtyp( 48)=0

!& pictl                                             ! power control
      eqtyp( 49)=182
      x(182)=x( 40)
      eqtyp( 50)=0

!& lim                                               ! limit speed control input
      eqtyp( 51)=0
      if(x( 40)>prm( 28))then
         z(  3)=1
      elseif(x( 40)<prm( 27))then
         z(  3)=-1
      else
         z(  3)=0
      endif

!& algeq                                             ! speed control input
      eqtyp( 52)=0

!& pictl                                             ! speed control
      eqtyp( 53)=183
      x(183)=x( 42)
      eqtyp( 54)=0

!& tf1p                                              ! motor current control
      eqtyp( 55)= 43
      tc( 55)=1/prm( 16)

!& algeq                                             ! torque equations
      eqtyp( 56)=0

!& algeq
      eqtyp( 57)=0

!& tf1p                                              ! motor inertia
      eqtyp( 58)= 45
      tc( 58)=2*prm( 23)/prm( 24)

!& algeq
      eqtyp( 59)=0

!& pwlin4                                            ! overvoltage Protection
      eqtyp( 60)=0
      if(x( 52)<(-999))then
         z(  4)=1
      elseif(x( 52)>=999)then
         z(  4)=   3
      elseif((-999)<=x( 52) .and. x( 52)<0.)then
         z(  4)=  1
      elseif(0.<=x( 52) .and. x( 52)<0.)then
         z(  4)=  2
      elseif(0.<=x( 52) .and. x( 52)<999)then
         z(  4)=  3
      endif

!& algeq
      eqtyp( 61)=0

!& hyst
      eqtyp( 62)=0
      if(x( 54)>1.1)then
         z(  5)=1
      elseif(x( 54)<0.9)then
         z(  5)=-1
      else
         if(1.>= 0.)then
            z(  5)=1
         else
            z(  5)=-1
         endif
      endif

!& algeq                                             ! LVRT
      eqtyp( 63)=0

!& timer5
      eqtyp( 64)=0
      eqtyp( 65)=0
      z(  6)=-1
      x(184)=0.

!& algeq
      eqtyp( 66)=0

!& hyst
      eqtyp( 67)=0
      if(x( 62)>1.1)then
         z(  7)=1
      elseif(x( 62)<0.9)then
         z(  7)=-1
      else
         if(1.>= 0.)then
            z(  7)=1
         else
            z(  7)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      eqtyp( 68)=0

!& algeq
      eqtyp( 69)=0

!& algeq
      eqtyp( 70)=0

!& swsign
      eqtyp( 71)=0
      if(x( 82)>=0.)then
         z(  8)=1
      else
         z(  8)=2
      endif

!& algeq                                             ! underfrequency protection
      eqtyp( 72)=0

!& algeq
      eqtyp( 73)=0

!& algeq
      eqtyp( 74)=0

!& swsign
      eqtyp( 75)=0
      if(x( 63)>=0.)then
         z(  9)=1
      else
         z(  9)=2
      endif

!& hyst
      eqtyp( 76)=0
      if(x( 67)>1.1)then
         z( 10)=1
      elseif(x( 67)<0.9)then
         z( 10)=-1
      else
         if(1.>= 0.)then
            z( 10)=1
         else
            z( 10)=-1
         endif
      endif

!& algeq                                             ! overfrequency protection
      eqtyp( 77)=0

!& algeq
      eqtyp( 78)=0

!& algeq
      eqtyp( 79)=0

!& swsign
      eqtyp( 80)=0
      if(x( 68)>=0.)then
         z( 11)=1
      else
         z( 11)=2
      endif

!& hyst
      eqtyp( 81)=0
      if(x( 72)>1.1)then
         z( 12)=1
      elseif(x( 72)<0.9)then
         z( 12)=-1
      else
         if(1.>= 0.)then
            z( 12)=1
         else
            z( 12)=-1
         endif
      endif

!& algeq                                             ! frequency deviation in Hz
      eqtyp( 82)=0

!& tfder1p                                           ! Rocof measurement in Hz/s
      x(185)=x( 23)
      eqtyp( 83)=185
      tc( 83)=prm( 39)
      eqtyp( 84)=0

!& abs
      eqtyp( 85)=0
      if(x( 73)>0. )then
         z( 13)=1
      else
         z( 13)=-1
      endif

!& algeq                                             ! Rocof protection
      eqtyp( 86)=0

!& algeq
      eqtyp( 87)=0

!& algeq
      eqtyp( 88)=0

!& swsign
      eqtyp( 89)=0
      if(x( 77)>=0.)then
         z( 14)=1
      else
         z( 14)=2
      endif

!& hyst
      eqtyp( 90)=0
      if(x( 78)>1.1)then
         z( 15)=1
      elseif(x( 78)<0.9)then
         z( 15)=-1
      else
         if(1.>= 0.)then
            z( 15)=1
         else
            z( 15)=-1
         endif
      endif

!& algeq                                             ! min and max power limiters
      eqtyp( 91)=0

!& algeq
      eqtyp( 92)=0

!& algeq                                             ! status check without droop
      eqtyp( 93)=0

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      eqtyp( 94)=0

!& algeq
      eqtyp( 95)=0

!& algeq												! NEIGHBOURHOOD CONTROL
      eqtyp( 96)=0

!& algeq
      eqtyp( 97)=0

!& swsign											! F_help_p_p is 1 if P_n > 0
      eqtyp( 98)=0
      if(x(113)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& swsign											! F_help_p_m is 1 if P_n < 0
      eqtyp( 99)=0
      if(x(112)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& hyst ! should it be V or Vm?
      eqtyp(100)=0
      if(x( 10)>prm( 59))then
         z( 18)=1
      elseif(x( 10)<prm( 60))then
         z( 18)=-1
      else
         if((-1.)>= 0.)then
            z( 18)=1
         else
            z( 18)=-1
         endif
      endif

!& hyst
      eqtyp(101)=0
      if(x( 10)>prm( 59))then
         z( 19)=1
      elseif(x( 10)<prm( 60))then
         z( 19)=-1
      else
         if(1.>= 0.)then
            z( 19)=1
         else
            z( 19)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      eqtyp(102)=0

!& abs
      eqtyp(103)=0
      if(x(114)>0. )then
         z( 20)=1
      else
         z( 20)=-1
      endif

!& algeq
      eqtyp(104)=0

!& swsign
      eqtyp(105)=0
      if(x(117)>=0.)then
         z( 21)=1
      else
         z( 21)=2
      endif

!& algeq												! voltage help required
      eqtyp(106)=0

!& swsign											! F_help_v_p is 1 if V_n > 0
      eqtyp(107)=0
      if(x(115)>=0.)then
         z( 22)=1
      else
         z( 22)=2
      endif

!& swsign											! F_help_v_m is 1 if V_n < 0
      eqtyp(108)=0
      if(x(114)>=0.)then
         z( 23)=1
      else
         z( 23)=2
      endif

!& algeq
      eqtyp(109)=0

!& algeq
      eqtyp(110)=0

!& algeq												! priority of voltage over power
      eqtyp(111)=0

!& algeq
      eqtyp(112)=0

!& algeq												! reactive power computation
      eqtyp(113)=0

!& algeq
      eqtyp(114)=0

!& algeq
      eqtyp(115)=0

!& algeq												! active power computation
      eqtyp(116)=0

!& algeq
      eqtyp(117)=0

!& algeq
      eqtyp(118)=0

!& algeq												! UNIT LIMIT	!s_f = security factor
      eqtyp(119)=0

!& algeq
      eqtyp(120)=0

!& algeq
      eqtyp(121)=0

!& algeq
      eqtyp(122)=0

!& algeq 											! voltage low limit flag
      eqtyp(123)=0

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      eqtyp(124)=0
      if(x( 91)>=0.)then
         z( 24)=1
      else
         z( 24)=2
      endif

!& algeq												! voltage high limit flag
      eqtyp(125)=0

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      eqtyp(126)=0
      if(x( 93)>=0.)then
         z( 25)=1
      else
         z( 25)=2
      endif

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      eqtyp(127)=0

!& algeq												! power low limit flag
      eqtyp(128)=0

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      eqtyp(129)=0
      if(x( 96)>=0.)then
         z( 26)=1
      else
         z( 26)=2
      endif

!& algeq												! power high limit flag
      eqtyp(130)=0

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      eqtyp(131)=0
      if(x( 98)>=0.)then
         z( 27)=1
      else
         z( 27)=2
      endif

!& algeq												! if the units are off the limit flags go to 0
      eqtyp(132)=0

!& algeq
      eqtyp(133)=0

!& algeq
      eqtyp(134)=0

!& algeq
      eqtyp(135)=0

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1
      eqtyp(136)=0

!& algeq
      eqtyp(137)=0

!& tf1p
      eqtyp(138)=109
      tc(138)=prm( 78)

!& algeq
      eqtyp(139)=0

!& swsign
      eqtyp(140)=0
      if(x(111)>=0.)then
         z( 28)=1
      else
         z( 28)=2
      endif

!& tf1p2lim
      if(prm( 77)< 0.001)then
         prm( 77)=0.d0
         prm( 88)=-huge(0.d0)
         prm( 89)= huge(0.d0)
         prm(110)=-huge(0.d0)
         prm( 82)= huge(0.d0)
      endif
      if(1*x(108)-x(110)>prm( 82)*prm( 77))then
         z( 29)=1
      elseif(1*x(108)-x(110)<prm(110)*prm( 77))then
         z( 29)=-1
      else
         z( 29)=0
      endif
      eqtyp(141)=0
      if(x(110)>prm( 89))then
         z( 30)=1
         eqtyp(142)=0
      elseif(x(110)<prm( 88))then
         z( 30)=-1
         eqtyp(142)=0
      else
         z( 30)=0
         eqtyp(142)=110
      endif
      tc(142)=prm( 77)

!& algeq
      eqtyp(143)=0

!& tf1p
      eqtyp(144)=107
      tc(144)=prm( 77)

!& algeq
      eqtyp(145)=0

!& algeq												! CENTRAL CONTROL, positive and negative variation
      eqtyp(146)=0

!& algeq
      eqtyp(147)=0

!& algeq
      eqtyp(148)=0

!& algeq
      eqtyp(149)=0

!& algeq
      eqtyp(150)=0

!& algeq
      eqtyp(151)=0

!& algeq
      eqtyp(152)=0

!& swsign
      eqtyp(153)=0
      if(x(142)>=0.)then
         z( 31)=1
      else
         z( 31)=2
      endif

!& swsign
      eqtyp(154)=0
      if(x(142)>=0.)then
         z( 32)=1
      else
         z( 32)=2
      endif

!& swsign
      eqtyp(155)=0
      if(x(142)>=0.)then
         z( 33)=1
      else
         z( 33)=2
      endif

!& limvb
      eqtyp(156)=0
      if(x(143)>x(140))then
         z( 34)=1
      elseif(x(143)<x(141))then
         z( 34)=-1
      else
         z( 34)=0
      endif

!& limvb
      eqtyp(157)=0
      if(x(144)>x(140))then
         z( 35)=1
      elseif(x(144)<x(141))then
         z( 35)=-1
      else
         z( 35)=0
      endif

!& algeq !dPc_0 is equal to 0
      eqtyp(158)=0

!& algeq												! level 1 low negative
      eqtyp(159)=0

!& swsign
      eqtyp(160)=0
      if(x(149)>=0.)then
         z( 36)=1
      else
         z( 36)=2
      endif

!& algeq												! level 1 high negative
      eqtyp(161)=0

!& swsign
      eqtyp(162)=0
      if(x(151)>=0.)then
         z( 37)=1
      else
         z( 37)=2
      endif

!& algeq												! level 1 negative flag
      eqtyp(163)=0

!& algeq												! level 1 low positive
      eqtyp(164)=0

!& swsign
      eqtyp(165)=0
      if(x(154)>=0.)then
         z( 38)=1
      else
         z( 38)=2
      endif

!& algeq												! level 1 high positive
      eqtyp(166)=0

!& swsign
      eqtyp(167)=0
      if(x(156)>=0.)then
         z( 39)=1
      else
         z( 39)=2
      endif

!& algeq												! level 1 positive flag
      eqtyp(168)=0

!& swsign
      eqtyp(169)=0
      if(x(142)>=0.)then
         z( 40)=1
      else
         z( 40)=2
      endif

!& algeq
      eqtyp(170)=0

!& algeq												! level 2 low negative
      eqtyp(171)=0

!& swsign
      eqtyp(172)=0
      if(x(161)>=0.)then
         z( 41)=1
      else
         z( 41)=2
      endif

!& algeq												! level 2 high negative
      eqtyp(173)=0

!& swsign
      eqtyp(174)=0
      if(x(163)>=0.)then
         z( 42)=1
      else
         z( 42)=2
      endif

!& algeq												! level 2 negative flag
      eqtyp(175)=0

!& algeq												! level 2 low positive
      eqtyp(176)=0

!& swsign
      eqtyp(177)=0
      if(x(166)>=0.)then
         z( 43)=1
      else
         z( 43)=2
      endif

!& algeq												! level 2 high positive
      eqtyp(178)=0

!& swsign
      eqtyp(179)=0
      if(x(168)>=0.)then
         z( 44)=1
      else
         z( 44)=2
      endif

!& algeq												! level 2 positive flag
      eqtyp(180)=0

!& swsign
      eqtyp(181)=0
      if(x(142)>=0.)then
         z( 45)=1
      else
         z( 45)=2
      endif

!& algeq
      eqtyp(182)=0

!& abs												! absolute of the level
      eqtyp(183)=0
      if(x(142)>0. )then
         z( 46)=1
      else
         z( 46)=-1
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      eqtyp(184)=0

!& switch4											! choice between the levels
      eqtyp(185)=0
      z( 47)=max(1,min(  4,nint(x(174))))

!& algeq
      eqtyp(186)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq                                             ! voltage magnitude
      f(  1)=dsqrt(vx**2+vy**2) - x(  7)

!& tf1p
      f(  2)=(-x( 10)+1.*x(  7))

!& tf1p
      f(  3)=(-x(  8)+1.*vx    )

!& tf1p
      f(  4)=(-x(  9)+1.*vy    )

!& algeq                                             ! voltage alignment
      f(  5)=-x(  3) + x(  8)*cos(x(176))+x(  9)*sin(x(176))

!& algeq
      f(  6)=-x(  4) - x(  8)*sin(x(176))+x(  9)*cos(x(176))

!& algeq
      f(  7)=-x(176) + atan(x(  9)/x(  8))

!& algeq                                             ! compute ix
      f(  8)=-x(  1) + (-x( 56)*cos(x(176))+x( 57)*sin(x(176)))*prm(  1)/prm(  2)

!& algeq                                             ! compute iy
      f(  9)=-x(  2) + (-x( 56)*sin(x(176))-x( 57)*cos(x(176)))*prm(  1)/prm(  2)

!& algeq
      f( 10)=-x( 56) + x( 11)*x( 81)

!& algeq
      f( 11)=-x( 57) + x( 13)*x( 81)

!& algeq                                             ! compute powers
      f( 12)=-x( 15)*prm(  2)/prm(  1) -x( 17)                            ! + x(  1)*vx+x(  2)*vy

!& algeq
      f( 13)=-x( 18)*prm(  2)/prm(  1) - x( 19)                           ! -vx*x(  2) + vy*x(  1)

!& algeq                                             ! compute powers
      f( 14)=-x( 17) + x(  3)*x( 56) + x(  4)*x( 57)                              ! - x( 15)*prm(  2)/prm(  1)

!& algeq
      f( 15)=-x( 19) + x(  3)*x( 57) - x(  4)*x( 56)                              ! - x( 18)*prm(  2)/prm(  1)

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         f( 16)=x( 22)-x(  5)
      else
         f( 16)=x( 22)
      endif

!& pictl                                             ! PLL
      f( 17)=0.1/(prm(  5)*0.001)**2                                                                                                                                                                                                                                                                                     *x(  4)
      f( 18)=0.5/(prm(  5)*0.001)                                                                                                                                                                                                                                                                                        *x(  4)+x(178)-x( 24)

!& algeq                                             ! frequency deviation
      f( 19)=x( 21) - x( 24) + omega*prm( 84)

!& algeq
      f( 20)=x( 22) -x( 21)*x( 48)

!& algeq                                             ! compute and filter frequency
      f( 21)=-x( 26)+x( 24)/prm( 84)*fnom

!& tf1p
      f( 22)=(-x( 25)+1.*x( 26))

!& algeq                                             ! PLL freezing
      f( 23)=-x( 49) + x( 10)-prm(  6)

!& algeq
      f( 24)=x( 51)

!& algeq
      f( 25)=-x( 50) + 1.d0

!& swsign
      select case (z(  1))
         case(1)
            f( 26)=x( 48)-x( 50)
         case(2)
            f( 26)=x( 48)-x( 51)
      end select

!& algeq
      f( 27)=-x(  6) + prm(  3)*x( 81)

!& algeq                                             ! DC voltage control
      f( 28)=-x( 28) + x( 81)*x(  6) - x( 27)

!& pictl
      f( 29)=prm(  9)                                                                                                                                                                                                                                                                                                    *x( 28)
      f( 30)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 28)+x(179)-x( 29)

!& algeq                                             ! p-axis current control
      f( 31)=-x( 12) + x( 29) -x( 11)

!& pictl
      f( 32)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 12)
      f( 33)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 12)+x(180)-x( 32)

!& algeq                                             ! q-axis current control
      f( 34)=-x( 31) ! + x( 29)*dsqrt(1-prm(  4))

!& algeq
      f( 35)=-x( 14) + x( 31) -x( 13) + x(135)

!& pictl
      f( 36)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 14)
      f( 37)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 14)+x(181)-x( 33)

!& algeq                                             ! md
      f( 38)=-x( 34) + x( 81)*(-x( 32) + prm( 18)/max(1.d-3,prm(  3))*omega*x( 13))

!& algeq                                             ! mq
      f( 39)=-x( 35) + x( 81)*(-x( 33) - prm( 18)/max(1.d-3,prm(  3))*omega*x( 11))

!& algeq                                             ! voltage over d-axis terminal impedance
      f( 40)=-x( 36) + x(  3) - x( 34) * x( 27) + prm( 18)*omega*x( 13)

!& algeq                                             ! voltage over q-axis terminal impedance
      f( 41)=-x( 37) + x(  4) - x( 35) * x( 27) - prm( 18)*omega*x( 11)

!& tf1p                                              ! d-axis current
      f( 42)=(-x( 11)+1/prm( 17)*x( 36))

!& tf1p                                              ! q-axis current
      f( 43)=(-x( 13)+1/prm( 17)*x( 37))

!& algeq                                             ! DC link current
      f( 44)=-x( 38) + x( 81)*(x( 45)*prm( 21)*x( 43) + prm( 22)*x( 43)**2) / max(1.d-3,x( 27))

!& algeq                                             ! DC link
      f( 45)=-x( 39)+x( 34)*x( 11)+x( 35)*x( 13)-x( 38)

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 84)< 0.005)then
         f( 46)=x( 39)-x( 27)
      else
         f( 46)=x( 39)
      endif

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            f( 47)=x( 20)-x( 58)
         case(-1)
            f( 47)=x( 20)-x( 80)
         case(1)
            f( 47)=x( 20)-x( 30)
      end select

!& algeq                                             ! power mismatch
      f( 48)=-x( 16) + x( 20)-x( 17) + x(110)

!& pictl                                             ! power control
      f( 49)=prm( 13)                                                                                                                                                                                                                                                                                                    *x( 16)
      f( 50)=prm( 12)                                                                                                                                                                                                                                                                                                    *x( 16)+x(182)-x( 40)

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            f( 51)=x( 41)-x( 40)
         case(-1)
            f( 51)=x( 41)-prm( 27)
         case(1)
            f( 51)=x( 41)-prm( 28)
      end select

!& algeq                                             ! speed control input
      f( 52)=-x( 44) + prm( 20)*(x( 81)*x( 41)-x( 45))

!& pictl                                             ! speed control
      f( 53)=prm( 15)                                                                                                                                                                                                                                                                                                    *x( 44)
      f( 54)=prm( 14)                                                                                                                                                                                                                                                                                                    *x( 44)+x(183)-x( 42)

!& tf1p                                              ! motor current control
      f( 55)=(-x( 43)+1*x( 42))

!& algeq                                             ! torque equations
      f( 56)=-x( 46) + prm( 21)*x( 43)

!& algeq
      f( 57)=-x( 47) + x( 46)-prm(109)

!& tf1p                                              ! motor inertia
      f( 58)=(-x( 45)+1/prm( 24)*x( 47))

!& algeq
      f( 59)=x( 52) - x( 10) +prm( 29)

!& pwlin4                                            ! overvoltage Protection
      select case (z(  4))
         case (  1)
            f( 60)=0.+ ( (0.-0.)*(x( 52)-(-999))/(0.-(-999)) ) -x( 53)
         case (  2)
            f( 60)=0.+ ( (1.-0.)*(x( 52)-0.)/(0.-0.) ) -x( 53)
         case (  3)
            f( 60)=1.+ ( (1.-1.)*(x( 52)-0.)/(999-0.) ) -x( 53)
      end select

!& algeq
      f( 61)=x( 54) -1 + x( 53)

!& hyst
      if(z(  5) == 1)then
         f( 62)=x( 55)-1.-(1.-1.)*(x( 54)-1.1)/(1.1-0.9)
      else
         f( 62)=x( 55)-0.-(0.-0.)*(x( 54)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! LVRT
      f( 63)=x( 10) + x( 59)

!& timer5
      select case (z(  6))
         case (-1)
            f( 64)=x( 60)
            f( 65)=x(184)
         case (0)
            f( 64)=x( 60)
            f( 65)= 1.
         case (1)
            f( 64)=x( 60)-1.
            f( 65)= 0.
      end select

!& algeq
      f( 66)=x( 62) -1 + x( 60)

!& hyst
      if(z(  7) == 1)then
         f( 67)=x( 61)-1.-(1.-1.)*(x( 62)-1.1)/(1.1-0.9)
      else
         f( 67)=x( 61)-0.-(0.-0.)*(x( 62)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      f( 68)=x( 83) - x( 55)*x( 61)*x( 66)*x( 71)*x( 79)

!& algeq
      f( 69)=x( 82) - prm( 44)

!& algeq
      f( 70)=x( 84) - 1

!& swsign
      select case (z(  8))
         case(1)
            f( 71)=x( 81)-x( 83)
         case(2)
            f( 71)=x( 81)-x( 84)
      end select

!& algeq                                             ! underfrequency protection
      f( 72)=x( 64) - 1

!& algeq
      f( 73)=x( 65)

!& algeq
      f( 74)=x( 63) - x( 25) + prm( 37)

!& swsign
      select case (z(  9))
         case(1)
            f( 75)=x( 67)-x( 64)
         case(2)
            f( 75)=x( 67)-x( 65)
      end select

!& hyst
      if(z( 10) == 1)then
         f( 76)=x( 66)-1.-(1.-1.)*(x( 67)-1.1)/(1.1-0.9)
      else
         f( 76)=x( 66)-0.-(0.-0.)*(x( 67)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! overfrequency protection
      f( 77)=x( 69) - 1

!& algeq
      f( 78)=x( 70)

!& algeq
      f( 79)=x( 68) - prm( 38) + x( 25)

!& swsign
      select case (z( 11))
         case(1)
            f( 80)=x( 72)-x( 69)
         case(2)
            f( 80)=x( 72)-x( 70)
      end select

!& hyst
      if(z( 12) == 1)then
         f( 81)=x( 71)-1.-(1.-1.)*(x( 72)-1.1)/(1.1-0.9)
      else
         f( 81)=x( 71)-0.-(0.-0.)*(x( 72)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! frequency deviation in Hz
      f( 82)=-x( 23) + x( 25)-fnom

!& tfder1p                                           ! Rocof measurement in Hz/s
      f( 83)=-x(185)+x( 23)
      if (prm( 39)< 0.005)then
         f( 84)=1/prm( 39)*x( 23)-x( 73)
      else
         f( 84)=1/prm( 39)*(x( 23)-x(185))-x( 73)
      endif

!& abs
      if(z( 13) == 1 )then
         f( 85)=x( 74)-x( 73)
      else
         f( 85)=x( 74)+x( 73)
      endif

!& algeq                                             ! Rocof protection
      f( 86)=x( 75) - 1

!& algeq
      f( 87)=x( 76)

!& algeq
      f( 88)=-x( 77) +prm( 40) -x( 74)

!& swsign
      select case (z( 14))
         case(1)
            f( 89)=x( 78)-x( 75)
         case(2)
            f( 89)=x( 78)-x( 76)
      end select

!& hyst
      if(z( 15) == 1)then
         f( 90)=x( 79)-1.-(1.-1.)*(x( 78)-1.1)/(1.1-0.9)
      else
         f( 90)=x( 79)-0.-(0.-0.)*(x( 78)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! min and max power limiters
      f( 91)=x( 80)

!& algeq
      f( 92)=-x( 30) + prm( 41)

!& algeq                                             ! status check without droop
      f( 93)=- x( 58) +  x( 81)*prm( 96)

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      f( 94)=x( 86)

!& algeq
      f( 95)=x( 85) - 1

!& algeq												! NEIGHBOURHOOD CONTROL
      f( 96)=-x(112) + prm( 58)

!& algeq
      f( 97)=-x(113) - prm( 58)

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z( 16))
         case(1)
            f( 98)=x(121)-x( 86)
         case(2)
            f( 98)=x(121)-x( 85)
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z( 17))
         case(1)
            f( 99)=x(122)-x( 86)
         case(2)
            f( 99)=x(122)-x( 85)
      end select

!& hyst ! should it be V or Vm?
      if(z( 18) == 1)then
         f(100)=x(123)-1.-(1.-1.)*(x( 10)-prm( 59))/(prm( 59)-prm( 60))
      else
         f(100)=x(123)-0.-(0.-0.)*(x( 10)-prm( 60))/(prm( 59)-prm( 60))
      endif

!& hyst
      if(z( 19) == 1)then
         f(101)=x(124)-0.-(0.-0.)*(x( 10)-prm( 59))/(prm( 59)-prm( 60))
      else
         f(101)=x(124)-1.-(1.-1.)*(x( 10)-prm( 60))/(prm( 59)-prm( 60))
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      f(102)=-x(114) + prm( 57)

!& abs
      if(z( 20) == 1 )then
         f(103)=x(116)-x(114)
      else
         f(103)=x(116)+x(114)
      endif

!& algeq
      f(104)=-x(117) + x(116) - 1

!& swsign
      select case (z( 21))
         case(1)
            f(105)=x(118)-x( 85)
         case(2)
            f(105)=x(118)-x( 86)
      end select

!& algeq												! voltage help required
      f(106)=-x(115) - prm( 57)

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 22))
         case(1)
            f(107)=x(119)-x( 86)
         case(2)
            f(107)=x(119)-x( 85)
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 23))
         case(1)
            f(108)=x(120)-x( 86)
         case(2)
            f(108)=x(120)-x( 85)
      end select

!& algeq
      f(109)=-x(125) + x(119)*x(123)

!& algeq
      f(110)=-x(126) + x(120)*x(124)

!& algeq												! priority of voltage over power
      f(111)=-x(127) + x(121)*(1-x(118))

!& algeq
      f(112)=-x(128) + x(122)*(1-x(118))

!& algeq												! reactive power computation
      f(113)=-x(129) + prm( 57)*prm( 61)*prm( 63)*(prm( 63)-x( 19))/(prm( 63)-prm( 64))*x(125)

!& algeq
      f(114)=-x(130) + prm( 57)*prm( 61)*prm( 63)*(x( 19)-prm( 64))/(prm( 63)-prm( 64))*x(126)

!& algeq
      f(115)=-x(131) + x(129) + x(130)

!& algeq												! active power computation
      f(116)=-x(132) + prm( 58)*prm( 62)*prm( 45)*(prm( 45)-x( 17))/(prm( 45)-prm( 46))*x(127)

!& algeq
      f(117)=-x(133) + prm( 58)*prm( 62)*prm( 45)*(x( 17)-prm( 46))/(prm( 45)-prm( 46))*x(128)

!& algeq
      f(118)=-x(134) + x(132) + x(133)

!& algeq												! UNIT LIMIT	!s_f = security factor
      f(119)=-x( 87) + prm( 81)*prm( 65)

!& algeq
      f(120)=-x( 88) + prm( 80)/prm( 65)

!& algeq
      f(121)=-x( 89) + prm( 46)*prm( 65)

!& algeq
      f(122)=-x( 90) + prm( 45)/prm( 65)

!& algeq 											! voltage low limit flag
      f(123)=-x( 91) + x( 87) - x( 10)

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 24))
         case(1)
            f(124)=x( 92)-x( 85)
         case(2)
            f(124)=x( 92)-x( 86)
      end select

!& algeq												! voltage high limit flag
      f(125)=-x( 93) + x( 10) - x( 88)

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 25))
         case(1)
            f(126)=x( 94)-x( 85)
         case(2)
            f(126)=x( 94)-x( 86)
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      f(127)=-x( 95) + (1-x( 92))*(1-x( 94))

!& algeq												! power low limit flag
      f(128)=-x( 96) + x( 89) - x( 17)

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 26))
         case(1)
            f(129)=x( 97)-x( 95)
         case(2)
            f(129)=x( 97)-x( 86)
      end select

!& algeq												! power high limit flag
      f(130)=-x( 98) + x( 17) - x( 90)

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 27))
         case(1)
            f(131)=x( 99)-x( 95)
         case(2)
            f(131)=x( 99)-x( 86)
      end select

!& algeq												! if the units are off the limit flags go to 0
      f(132)=-x(100) + x( 92)*x( 81)

!& algeq
      f(133)=-x(101) + x( 94)*x( 81)

!& algeq
      f(134)=-x(102) + x( 97)*x( 81)

!& algeq
      f(135)=-x(103) + x( 99)*x( 81)

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1
      f(136)=-x(104) + 1-(1-x(100))*(1-x(101))*(1-x(102))*(1-x(103))

!& algeq
      f(137)=-x(105) + (x(134) + x(175)) ! * (1 - x(104)) ! + (x( 17) - x( 20)) * x(104)

!& tf1p
      f(138)=(-x(109)+1*x(105))

!& algeq
      f(139)=-x(111) + prm( 79)

!& swsign
      select case (z( 28))
         case(1)
            f(140)=x(108)-x(109)
         case(2)
            f(140)=x(108)-x(105)
      end select

!& tf1p2lim
      select case (z( 29))
         case(0)
            f(141)=x(186)-1*x(108)+x(110)
         case(1)
            f(141)=x(186)-prm( 82)*prm( 77)
         case(-1)
            f(141)=x(186)-prm(110)*prm( 77)
      end select
      select case (z( 30))
         case(0)
            f(142)=x(186)
         case(1)
            f(142)=x(110)-prm( 89)
         case(-1)
            f(142)=x(110)-prm( 88)
      end select

!& algeq
      f(143)=-x(106) + x(131) ! * (1 - x(104))

!& tf1p
      f(144)=(-x(107)+1*x(106))

!& algeq
      f(145)=-x(135) + x(107)

!& algeq												! CENTRAL CONTROL, positive and negative variation
      f(146)=-x(137) + prm( 47)

!& algeq
      f(147)=-x(138) + prm( 48)

!& algeq
      f(148)=x(136) + x(137)

!& algeq
      f(149)=x(139) + x(138)

!& algeq
      f(150)=-x(140) + prm( 45)/prm( 65) - x( 20)

!& algeq
      f(151)=-x(141) - x( 20) + prm( 46)*prm( 65)

!& algeq
      f(152)=-x(142) + prm( 66)

!& swsign
      select case (z( 31))
         case(1)
            f(153)=x(143)-x(137)
         case(2)
            f(153)=x(143)-x(136)
      end select

!& swsign
      select case (z( 32))
         case(1)
            f(154)=x(144)-x(138)
         case(2)
            f(154)=x(144)-x(139)
      end select

!& swsign
      select case (z( 33))
         case(1)
            f(155)=x(147)-x(140)
         case(2)
            f(155)=x(147)-x(141)
      end select

!& limvb
      select case (z( 34))
         case(0)
            f(156)=x(145)-x(143)
         case(-1)
            f(156)=x(145)-x(141)
         case(1)
            f(156)=x(145)-x(140)
      end select

!& limvb
      select case (z( 35))
         case(0)
            f(157)=x(146)-x(144)
         case(-1)
            f(157)=x(146)-x(141)
         case(1)
            f(157)=x(146)-x(140)
      end select

!& algeq !dPc_0 is equal to 0
      f(158)=x(148)

!& algeq												! level 1 low negative
      f(159)=-x(149) + x( 20) - (prm( 51) * (prm( 45)-prm( 46)) + prm( 46))

!& swsign
      select case (z( 36))
         case(1)
            f(160)=x(150)-x( 85)
         case(2)
            f(160)=x(150)-x( 86)
      end select

!& algeq												! level 1 high negative
      f(161)=-x(151) + (prm( 52) * (prm( 45)-prm( 46)) + prm( 46)) - x( 20)

!& swsign
      select case (z( 37))
         case(1)
            f(162)=x(152)-x( 85)
         case(2)
            f(162)=x(152)-x( 86)
      end select

!& algeq												! level 1 negative flag
      f(163)=-x(153) + (x(150) * x(152))

!& algeq												! level 1 low positive
      f(164)=-x(154) + x( 20) - (prm( 49) * (prm( 45)-prm( 46)) + prm( 46))

!& swsign
      select case (z( 38))
         case(1)
            f(165)=x(155)-x( 85)
         case(2)
            f(165)=x(155)-x( 86)
      end select

!& algeq												! level 1 high positive
      f(166)=-x(156) + (prm( 50) * (prm( 45)-prm( 46)) + prm( 46)) - x( 20)

!& swsign
      select case (z( 39))
         case(1)
            f(167)=x(157)-x( 85)
         case(2)
            f(167)=x(157)-x( 86)
      end select

!& algeq												! level 1 positive flag
      f(168)=-x(158) + (x(155) * x(157))

!& swsign
      select case (z( 40))
         case(1)
            f(169)=x(159)-x(158)
         case(2)
            f(169)=x(159)-x(153)
      end select

!& algeq
      f(170)=-x(160) + x(145) * x(159)

!& algeq												! level 2 low negative
      f(171)=-x(161) + x( 20) - (prm( 55) * (prm( 45)-prm( 46)) + prm( 46))

!& swsign
      select case (z( 41))
         case(1)
            f(172)=x(162)-x( 85)
         case(2)
            f(172)=x(162)-x( 86)
      end select

!& algeq												! level 2 high negative
      f(173)=-x(163) + (prm( 56) * (prm( 45)-prm( 46)) + prm( 46)) - x( 20)

!& swsign
      select case (z( 42))
         case(1)
            f(174)=x(164)-x( 85)
         case(2)
            f(174)=x(164)-x( 86)
      end select

!& algeq												! level 2 negative flag
      f(175)=-x(165) + (x(162) * x(164))

!& algeq												! level 2 low positive
      f(176)=-x(166) + x( 20) - (prm( 53) * (prm( 45)-prm( 46)) + prm( 46))

!& swsign
      select case (z( 43))
         case(1)
            f(177)=x(167)-x( 85)
         case(2)
            f(177)=x(167)-x( 86)
      end select

!& algeq												! level 2 high positive
      f(178)=-x(168) + (prm( 54) * (prm( 45)-prm( 46)) + prm( 46)) - x( 20)

!& swsign
      select case (z( 44))
         case(1)
            f(179)=x(169)-x( 85)
         case(2)
            f(179)=x(169)-x( 86)
      end select

!& algeq												! level 2 positive flag
      f(180)=-x(170) + (x(167) * x(169))

!& swsign
      select case (z( 45))
         case(1)
            f(181)=x(171)-x(170)
         case(2)
            f(181)=x(171)-x(165)
      end select

!& algeq
      f(182)=-x(172) + x(146) * x(171)

!& abs												! absolute of the level
      if(z( 46) == 1 )then
         f(183)=x(173)-x(142)
      else
         f(183)=x(173)+x(142)
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      f(184)=-x(174) + x(173) + 1

!& switch4											! choice between the levels
      select case (z( 47))
         case(  1)
            f(185)=x(175)-x(148)
         case(  2)
            f(185)=x(175)-x(160)
         case(  3)
            f(185)=x(175)-x(172)
         case(  4)
            f(185)=x(175)-x(147)
      end select

!& algeq
      f(186)=-x(177) + prm( 78)

!........................................................................................
   case (update_disc)

!& algeq                                             ! voltage magnitude

!& tf1p

!& tf1p

!& tf1p

!& algeq                                             ! voltage alignment

!& algeq

!& algeq

!& algeq                                             ! compute ix

!& algeq                                             ! compute iy

!& algeq

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& int                                               ! voltage alignment angle, PLL angle

!& pictl                                             ! PLL

!& algeq                                             ! frequency deviation

!& algeq

!& algeq                                             ! compute and filter frequency

!& tf1p

!& algeq                                             ! PLL freezing

!& algeq

!& algeq

!& swsign
      select case (z(  1))
         case(1)
            if(x( 49)<0.)then
               z(  1)=2
            endif
         case(2)
            if(x( 49)>=0.)then
               z(  1)=1
            endif
      end select

!& algeq

!& algeq                                             ! DC voltage control

!& pictl

!& algeq                                             ! p-axis current control

!& pictl

!& algeq                                             ! q-axis current control

!& algeq

!& pictl

!& algeq                                             ! md

!& algeq                                             ! mq

!& algeq                                             ! voltage over d-axis terminal impedance

!& algeq                                             ! voltage over q-axis terminal impedance

!& tf1p                                              ! d-axis current

!& tf1p                                              ! q-axis current

!& algeq                                             ! DC link current

!& algeq                                             ! DC link

!& int                                               ! DC link voltage

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            if(x( 58)>x( 30))then
               z(  2)=1
            elseif(x( 58)<x( 80))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 58)>x( 80))then
               z(  2)=0
            endif
         case(1)
            if(x( 58)<x( 30))then
               z(  2)=0
            endif
      end select

!& algeq                                             ! power mismatch

!& pictl                                             ! power control

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            if(x( 40)>prm( 28))then
               z(  3)=1
            elseif(x( 40)<prm( 27))then
               z(  3)=-1
            endif
         case(-1)
            if(x( 40)>prm( 27))then
               z(  3)=0
            endif
         case(1)
            if(x( 40)<prm( 28))then
               z(  3)=0
            endif
      end select

!& algeq                                             ! speed control input

!& pictl                                             ! speed control

!& tf1p                                              ! motor current control

!& algeq                                             ! torque equations

!& algeq

!& tf1p                                              ! motor inertia

!& algeq

!& pwlin4                                            ! overvoltage Protection
      if(x( 52)<(-999))then
         z(  4)=1
      elseif(x( 52)>=999)then
         z(  4)=  3
      elseif((-999)<=x( 52) .and. x( 52)<0.)then
         z(  4)=  1
      elseif(0.<=x( 52) .and. x( 52)<0.)then
         z(  4)=  2
      elseif(0.<=x( 52) .and. x( 52)<999)then
         z(  4)=  3
      endif

!& algeq

!& hyst
      if (z(  5) == -1)then
         if(x( 54)>1.1)then
            z(  5)=1
         endif
      else
         if(x( 54)<0.9)then
            z(  5)=-1
         endif
      endif

!& algeq                                             ! LVRT

!& timer5
      if(z(  6) == -1)then
         if(x( 59) >= (-prm( 32)))then
            z(  6)=0
            eqtyp( 65)=184
         endif
      else
         if(x( 59) < (-prm( 32)))then
            z(  6)=-1
            eqtyp( 65)=0
         endif
      endif
      if(z(  6) == 0)then
         if(x( 59) > (-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))then
            if(x(184) > 0.)then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))then
            if(x(184) > prm( 33)+(0.-prm( 33))*(x( 59)-(-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))then
            if(x(184) > prm( 33)+(prm( 33)-prm( 33))*(x( 59)-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))then
            if(x(184) > prm( 35)+(prm( 33)-prm( 35))*(x( 59)-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-prm( 32)))then
            if(x(184) > prm( 34)+(prm( 35)-prm( 34))*(x( 59)-(-prm( 32)))/((-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))-(-prm( 32))))then
               z(  6)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  7) == -1)then
         if(x( 62)>1.1)then
            z(  7)=1
         endif
      else
         if(x( 62)<0.9)then
            z(  7)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off

!& algeq

!& algeq

!& swsign
      select case (z(  8))
         case(1)
            if(x( 82)<0.)then
               z(  8)=2
            endif
         case(2)
            if(x( 82)>=0.)then
               z(  8)=1
            endif
      end select

!& algeq                                             ! underfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z(  9))
         case(1)
            if(x( 63)<0.)then
               z(  9)=2
            endif
         case(2)
            if(x( 63)>=0.)then
               z(  9)=1
            endif
      end select

!& hyst
      if (z( 10) == -1)then
         if(x( 67)>1.1)then
            z( 10)=1
         endif
      else
         if(x( 67)<0.9)then
            z( 10)=-1
         endif
      endif

!& algeq                                             ! overfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z( 11))
         case(1)
            if(x( 68)<0.)then
               z( 11)=2
            endif
         case(2)
            if(x( 68)>=0.)then
               z( 11)=1
            endif
      end select

!& hyst
      if (z( 12) == -1)then
         if(x( 72)>1.1)then
            z( 12)=1
         endif
      else
         if(x( 72)<0.9)then
            z( 12)=-1
         endif
      endif

!& algeq                                             ! frequency deviation in Hz

!& tfder1p                                           ! Rocof measurement in Hz/s

!& abs
      if (z( 13) == -1 )then
         if(x( 73)> blocktol1 )then
            z( 13)=1
         endif
      else
         if(x( 73)< - blocktol1 )then
            z( 13)=-1
         endif
      endif

!& algeq                                             ! Rocof protection

!& algeq

!& algeq

!& swsign
      select case (z( 14))
         case(1)
            if(x( 77)<0.)then
               z( 14)=2
            endif
         case(2)
            if(x( 77)>=0.)then
               z( 14)=1
            endif
      end select

!& hyst
      if (z( 15) == -1)then
         if(x( 78)>1.1)then
            z( 15)=1
         endif
      else
         if(x( 78)<0.9)then
            z( 15)=-1
         endif
      endif

!& algeq                                             ! min and max power limiters

!& algeq

!& algeq                                             ! status check without droop

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation

!& algeq

!& algeq												! NEIGHBOURHOOD CONTROL

!& algeq

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z( 16))
         case(1)
            if(x(113)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x(113)>=0.)then
               z( 16)=1
            endif
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z( 17))
         case(1)
            if(x(112)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x(112)>=0.)then
               z( 17)=1
            endif
      end select

!& hyst ! should it be V or Vm?
      if (z( 18) == -1)then
         if(x( 10)>prm( 59))then
            z( 18)=1
         endif
      else
         if(x( 10)<prm( 60))then
            z( 18)=-1
         endif
      endif

!& hyst
      if (z( 19) == -1)then
         if(x( 10)>prm( 59))then
            z( 19)=1
         endif
      else
         if(x( 10)<prm( 60))then
            z( 19)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0

!& abs
      if (z( 20) == -1 )then
         if(x(114)> blocktol1 )then
            z( 20)=1
         endif
      else
         if(x(114)< - blocktol1 )then
            z( 20)=-1
         endif
      endif

!& algeq

!& swsign
      select case (z( 21))
         case(1)
            if(x(117)<0.)then
               z( 21)=2
            endif
         case(2)
            if(x(117)>=0.)then
               z( 21)=1
            endif
      end select

!& algeq												! voltage help required

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 22))
         case(1)
            if(x(115)<0.)then
               z( 22)=2
            endif
         case(2)
            if(x(115)>=0.)then
               z( 22)=1
            endif
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 23))
         case(1)
            if(x(114)<0.)then
               z( 23)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 23)=1
            endif
      end select

!& algeq

!& algeq

!& algeq												! priority of voltage over power

!& algeq

!& algeq												! reactive power computation

!& algeq

!& algeq

!& algeq												! active power computation

!& algeq

!& algeq

!& algeq												! UNIT LIMIT	!s_f = security factor

!& algeq

!& algeq

!& algeq

!& algeq 											! voltage low limit flag

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 24))
         case(1)
            if(x( 91)<0.)then
               z( 24)=2
            endif
         case(2)
            if(x( 91)>=0.)then
               z( 24)=1
            endif
      end select

!& algeq												! voltage high limit flag

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 25))
         case(1)
            if(x( 93)<0.)then
               z( 25)=2
            endif
         case(2)
            if(x( 93)>=0.)then
               z( 25)=1
            endif
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max

!& algeq												! power low limit flag

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 26))
         case(1)
            if(x( 96)<0.)then
               z( 26)=2
            endif
         case(2)
            if(x( 96)>=0.)then
               z( 26)=1
            endif
      end select

!& algeq												! power high limit flag

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 27))
         case(1)
            if(x( 98)<0.)then
               z( 27)=2
            endif
         case(2)
            if(x( 98)>=0.)then
               z( 27)=1
            endif
      end select

!& algeq												! if the units are off the limit flags go to 0

!& algeq

!& algeq

!& algeq

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1

!& algeq

!& tf1p

!& algeq

!& swsign
      select case (z( 28))
         case(1)
            if(x(111)<0.)then
               z( 28)=2
            endif
         case(2)
            if(x(111)>=0.)then
               z( 28)=1
            endif
      end select

!& tf1p2lim
      select case (z( 29))
         case(0)
            if(x(186)>prm( 82)*prm( 77))then
               z( 29)=1
            elseif(x(186)<prm(110)*prm( 77))then
               z( 29)=-1
            endif
         case(1)
            if(1*x(108)-x(110)<prm( 82)*prm( 77))then
               z( 29)= 0
            endif
         case(-1)
            if(1*x(108)-x(110)>prm(110)*prm( 77))then
               z( 29)= 0
            endif
      end select
      select case (z( 30))
         case(0)
            if(x(110)>prm( 89))then
               z( 30)=1
               eqtyp(142)=0
            elseif(x(110)<prm( 88))then
               z( 30)=-1
               eqtyp(142)=0
            endif
         case(1)
            if (x(186)<0.)then
               z( 30)= 0
               eqtyp(142)=110
            endif
         case(-1)
            if(x(186)>0.)then
               z( 30)= 0
               eqtyp(142)=110
            endif
      end select

!& algeq

!& tf1p

!& algeq

!& algeq												! CENTRAL CONTROL, positive and negative variation

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 31))
         case(1)
            if(x(142)<0.)then
               z( 31)=2
            endif
         case(2)
            if(x(142)>=0.)then
               z( 31)=1
            endif
      end select

!& swsign
      select case (z( 32))
         case(1)
            if(x(142)<0.)then
               z( 32)=2
            endif
         case(2)
            if(x(142)>=0.)then
               z( 32)=1
            endif
      end select

!& swsign
      select case (z( 33))
         case(1)
            if(x(142)<0.)then
               z( 33)=2
            endif
         case(2)
            if(x(142)>=0.)then
               z( 33)=1
            endif
      end select

!& limvb
      select case (z( 34))
         case(0)
            if(x(143)>x(140))then
               z( 34)=1
            elseif(x(143)<x(141))then
               z( 34)=-1
            endif
         case(-1)
            if(x(143)>x(141))then
               z( 34)=0
            endif
         case(1)
            if(x(143)<x(140))then
               z( 34)=0
            endif
      end select

!& limvb
      select case (z( 35))
         case(0)
            if(x(144)>x(140))then
               z( 35)=1
            elseif(x(144)<x(141))then
               z( 35)=-1
            endif
         case(-1)
            if(x(144)>x(141))then
               z( 35)=0
            endif
         case(1)
            if(x(144)<x(140))then
               z( 35)=0
            endif
      end select

!& algeq !dPc_0 is equal to 0

!& algeq												! level 1 low negative

!& swsign
      select case (z( 36))
         case(1)
            if(x(149)<0.)then
               z( 36)=2
            endif
         case(2)
            if(x(149)>=0.)then
               z( 36)=1
            endif
      end select

!& algeq												! level 1 high negative

!& swsign
      select case (z( 37))
         case(1)
            if(x(151)<0.)then
               z( 37)=2
            endif
         case(2)
            if(x(151)>=0.)then
               z( 37)=1
            endif
      end select

!& algeq												! level 1 negative flag

!& algeq												! level 1 low positive

!& swsign
      select case (z( 38))
         case(1)
            if(x(154)<0.)then
               z( 38)=2
            endif
         case(2)
            if(x(154)>=0.)then
               z( 38)=1
            endif
      end select

!& algeq												! level 1 high positive

!& swsign
      select case (z( 39))
         case(1)
            if(x(156)<0.)then
               z( 39)=2
            endif
         case(2)
            if(x(156)>=0.)then
               z( 39)=1
            endif
      end select

!& algeq												! level 1 positive flag

!& swsign
      select case (z( 40))
         case(1)
            if(x(142)<0.)then
               z( 40)=2
            endif
         case(2)
            if(x(142)>=0.)then
               z( 40)=1
            endif
      end select

!& algeq

!& algeq												! level 2 low negative

!& swsign
      select case (z( 41))
         case(1)
            if(x(161)<0.)then
               z( 41)=2
            endif
         case(2)
            if(x(161)>=0.)then
               z( 41)=1
            endif
      end select

!& algeq												! level 2 high negative

!& swsign
      select case (z( 42))
         case(1)
            if(x(163)<0.)then
               z( 42)=2
            endif
         case(2)
            if(x(163)>=0.)then
               z( 42)=1
            endif
      end select

!& algeq												! level 2 negative flag

!& algeq												! level 2 low positive

!& swsign
      select case (z( 43))
         case(1)
            if(x(166)<0.)then
               z( 43)=2
            endif
         case(2)
            if(x(166)>=0.)then
               z( 43)=1
            endif
      end select

!& algeq												! level 2 high positive

!& swsign
      select case (z( 44))
         case(1)
            if(x(168)<0.)then
               z( 44)=2
            endif
         case(2)
            if(x(168)>=0.)then
               z( 44)=1
            endif
      end select

!& algeq												! level 2 positive flag

!& swsign
      select case (z( 45))
         case(1)
            if(x(142)<0.)then
               z( 45)=2
            endif
         case(2)
            if(x(142)>=0.)then
               z( 45)=1
            endif
      end select

!& algeq

!& abs												! absolute of the level
      if (z( 46) == -1 )then
         if(x(142)> blocktol1 )then
            z( 46)=1
         endif
      else
         if(x(142)< - blocktol1 )then
            z( 46)=-1
         endif
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1

!& switch4											! choice between the levels
      z( 47)=max(1,min(  4,nint(x(174))))

!& algeq
   end select

end subroutine inj_ATLnoPLL
