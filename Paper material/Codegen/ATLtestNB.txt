inj
ATLtestNB

%data
Sb                              ! base power of the unit
Sbs                             ! base power of the system
vdc_star                        ! DC link voltage reference
pf                              ! setpoint for power factor at the terminal
tau                             ! Pll time constant
Vminpll                         ! PLL freezing voltage, PLL freezes below this value
tau_f                           ! filter constant for frequency
kp_v                            ! DC link voltage control parameters
ki_v
kp_c                            ! rectifier current control parameters
ki_c
kp_p                            ! power control parameters
ki_p
kp_w                            ! motor speed control parameters
ki_w
w_cc                            ! motor current control bandwidth
rt                              ! terminal impedance
lt
cdc                             ! DC link capacitance
kw                              ! speed/torque control constant
kT                              ! motor torque constant
ra                              ! motor anchor/stator resistance
H                               ! motor inertia
b                               ! motor friction coefficient
Tnm                             ! compressor torque at nominal speed
iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
wm_min                          ! limits on rotational speed
wm_max
Vmax                            ! voltage range during which unit needs to stay connected
Vmin
Vint
Vr
tLVRT1
tLVRT2
tLVRTint
Vtrip
fmin                            ! frequency control regime
fmax
Trocof                          ! delay for ROCOF measurement
dfmax                           ! maximum permissable ROCOF
VPmax                           ! power limit above 0.93 pu voltage
LVRT                            ! enable or disable LVRT
Tm                              ! measurement delay

protection                      ! Flag for protection, -1 for off, 1 for on


P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!

V_n                             ! number of neighbours at voltage limit
P_n                             ! number of neighbours at power limit
Vdb_p                           ! higher voltage deadband value
Vdb_m                           ! lower voltage deadband value
ro_v                            ! neighbourhood factor for voltage
ro_p                            ! neighbourhood factor for power
Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
Q_min                           ! min reactive power of the unit in pu, IT WIL

s_f                             ! security factor for unit limit control
lvl                             ! level of emergency of the central controller
pl1_3_0                        ! flags that varies for the different units
pl1_4_0
F_help_high_0
F_help_low_0


%parameters

w0 = 1
wb = 2*pi*fnom
t2 = 0                          ! Torque polynomial parameters, set to constant torque
t1 = 0
t0 = 1

Downlim = -9999
Downlimdisc = 0
Uplimdis = 0
downlimdis = -9999


theta0 = atan([vy]/[vx])
P0 = ([vx]*[ix]+[vy]*[iy])                                                      ! initial active power 
Q0 = [vy]*[ix]-[vx]*[iy]                                                        ! initial reactive power
P0_unit = -{P0}*{Sbs}/{Sb}
Q0_unit = -{Q0}*{Sbs}/{Sb}
V0 = dsqrt([vx]**2+[vy]**2)                                                     ! initial voltage magnitude at bus
iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}                      ! current alignment
iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})                                     ! voltage alignment
vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})                            ! modulation indices
mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
idc0 = {md0}*{iP0}+{mq0}*{iQ0}                                                  ! DC link current
iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
Te0 = {iT0}*{kT}
Tc0 = {Tnm}


%states
vd = {vd0}
vq = {vq0}
theta = {theta0}
vdc_ref ={vdc_star}
V = {V0}
vxm = [vx]
vym = [vy]
Vm = {V0}
iP =  {iP0}
diP = 0
iQ =  {iQ0}
diQ = 0
P = {P0}
dp = 0
Punit = {P0_unit}
Q = {Q0}
Qunit = {Q0_unit}
Pref_lim = {P0_unit}          ! limited P reference
dw_pll = 0
dw_pllf = 0
deltaf = 0
w_pll = {wb}
f = fnom
fi = fnom
vdc = {vdc_star}
dvdc = 0
iP_ref = {iP0}
Plim = {VPmax}
iQ_ref = {iQ0}
mdr = {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
mqr = -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
md = {md0}
mq = {mq0}
dvd = {iP0}*{rt}
dvq = {iQ0}*{rt}
idc = {idc0}
didc = {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
wm_ref = {wm0}
wm_ref_lim = {wm0}
iT_ref = {iT0}
iT = {iT0}
dwm = 0
wm = {wm0}
Te = {kT}*{iT0}
dT = {kT}*{iT0}-{Tc0}
F_pll = 1
dv_pll = {V0}-{Vminpll}
PLLmulta = 1
PLLmultb = 0
deltafvh =  {V0}-{Vmax}
z1 = 0
Fvhi = 1
Fvh = 1
iPs = {iP0}
iQs = {iQ0}
Pref = {P0_unit}
x10 = -{V0}
z=0
Fvl = 1
Fvli = 1
deltafl = [f] - {fmin}
flagla = 1
flaglb = 0
Ffl = 1
Ffli = 1
deltafh = {fmax} - [f]
flagha = 1
flaghb = 0
Ffh = 1
Ffhi = 1
rocof = 0
abrocof = 0
flagra = 1
flagrb = 0
deltarocof  = {dfmax} - 0
Ffri = 1
Ffr = 1
Plim_min = 0

status  = 1             ! status of the device, 1 for on
p1 = {protection}
p2 = 1
p3 = 1



Fpl1 = 1                                ! placeholder for value 1 in switch block
Fpl0 = 0                                ! placeholder for value 0 in switch block


V_lim_min = {Vmin} * {s_f}              ! unit controller states
V_lim_max = {Vmax} / {s_f}
P_lim_min = {P_min} * {s_f}
P_lim_max = {P_max} / {s_f}
pl1 = {Vmin} * {s_f} - {V0}
F_v_min_in = 0
pl2 = {V0} - {Vmax} / {s_f}
F_v_max_in = 0
F_v_opp = 1
pl3 = {P_min} * {s_f} - {P0_unit}
F_p_min_in = 0
pl4 = {P0_unit} - {P_max} / {s_f}
F_p_max_in = 0
F_v_min = 0
F_v_max = 0
F_p_min = 0
F_p_max = 0
F_lim = 0
dP_lim = 0 
dQ_lim = 0
dQ_sum = 0
dQ = -{Q0_unit}
dP_sum = 0

P_n_var = {P_n}                         ! neighbourhood controller states
V_n_var = {V_n}
V_n_abs = {V_n}
pl5 = {V_n} - 1
F_help_v = 0
F_help_p_p = 1
F_help_p_m = 0
F_hlp_high = {F_help_high_0}
F_hlp_low = {F_help_low_0}
F_hlp_p_p2 = [F_help_p_p] * (1-[F_help_v])
F_hlp_p_m2 = [F_help_p_m] * (1-[F_help_v])
dQp = 0
dQm = 0
dQnb = 0
dPp = 0
dPm = 0
dPnb = 0


%observables
iP
iQ
vd
vq
P
Punit
Pref
Q
f
wm
Vm
Pref_lim
wm_ref_lim
status
rocof

F_v_min
F_v_max
F_p_min
F_p_max
F_hlp_high
F_hlp_low
F_hlp_p_p2
F_hlp_p_m2
dPnb
dQnb


%models

& algeq                                             ! voltage magnitude
dsqrt([vx]**2+[vy]**2) - [V] 

& tf1p
V
Vm
1.
{Tm}
& tf1p
vx
vxm
1.
{Tm}
& tf1p
vy
vym
1.
{Tm}

& algeq                                             ! voltage alignment         
-[vd] + [vxm]*cos([theta])+[vym]*sin([theta])
& algeq
-[vq] - [vxm]*sin([theta])+[vym]*cos([theta])


& algeq                                             ! compute ix
-[ix] + (-[iPs]*cos([theta])-[iQs]*sin([theta]))*{Sb}/{Sbs}
& algeq                                             ! compute iy
-[iy] + (-[iPs]*sin([theta])+[iQs]*sin([theta]))*{Sb}/{Sbs}

& algeq
-[iPs] + [iP]*[status]

& algeq
-[iQs] + [iQ]*[status]


& algeq                                             ! compute powers
-[P]*{Sbs}/{Sb} -[Punit]                            ! + [ix]*[vx]+[iy]*[vy]
& algeq             
-[Q]*{Sbs}/{Sb} - [Qunit]                           ! -[vx]*[iy] + [vy]*[ix]


& algeq                                             ! compute powers
-[Punit] + [vd]*[iPs] + [vq]*[iQs]                              ! - [P]*{Sbs}/{Sb}
& algeq             
-[Qunit] + [vd]*[iQs] - [vq]*[iPs]                              ! - [Q]*{Sbs}/{Sb}



& int                                               ! voltage alignment angle, PLL angle
dw_pllf
theta
1.d0
& pictl                                             ! PLL
vq
w_pll
0.1/({tau}*0.001)**2
0.5/({tau}*0.001)

& algeq                                             ! frequency deviation
[dw_pll] - [w_pll] + [omega]*{wb}

& algeq
[dw_pllf] -[dw_pll]*[F_pll]

& algeq                                             ! compute and filter frequency
-[fi]+[w_pll]/{wb}*fnom
& tf1p
fi
f
1.
{tau_f}


& algeq                                             ! PLL freezing 
-[dv_pll] + [Vm]-{Vminpll}
& algeq
[PLLmultb] 
& algeq
-[PLLmulta] + 1.d0
& swsign
PLLmulta
PLLmultb
dv_pll
F_pll

& algeq 
-[vdc_ref] + {vdc_star}*[status]

& algeq                                             ! DC voltage control
-[dvdc] + [status]*[vdc_ref] - [vdc]

& pictl
dvdc
iP_ref
{ki_v}
{kp_v}

& algeq                                             ! p-axis current control
-[diP] + [iP_ref] -[iP]
& pictl
diP
mdr
{ki_c}
{kp_c}
& algeq                                             ! q-axis current control
-[iQ_ref] + [iP_ref]*dsqrt(1-{pf})
& algeq
-[diQ] + [iQ_ref] -[iQ]
& pictl
diQ
mqr
{ki_c}
{kp_c}


& algeq                                             ! md
-[md] + [status]*(-[mdr] + {lt}/max(1.d-3,{vdc_star})*[f]/fnom*[iQ])
& algeq                                             ! mq
-[mq] + [status]*(-[mqr] - {lt}/max(1.d-3,{vdc_star})*[f]/fnom*[iP])


& algeq                                             ! voltage over d-axis terminal impedance
-[dvd] + [vd] - [md] * [vdc] + {lt}*[omega]*[iQ]
& algeq                                             ! voltage over q-axis terminal impedance
-[dvq] + [vq] - [mq] * [vdc] - {lt}*[omega]*[iP]


& tf1p                                              ! d-axis current
dvd
iP
1/{rt}
{lt}/({wb}*{rt})
& tf1p                                              ! q-axis current
dvq
iQ
1/{rt}
{lt}/({wb}*{rt})


& algeq                                             ! DC link current
-[idc] + [status]*([wm]*{kT}*[iT] + {ra}*[iT]**2) / max(1.d-3,[vdc])


& algeq                                             ! DC link
-[didc]+[md]*[iP]+[mq]*[iQ]-[idc]


& int                                               ! DC link voltage
didc
vdc
{cdc}/{wb}


& limvb                                              ! power limiter
Pref
Plim_min
Plim
Pref_lim


& algeq                                             ! power mismatch
-[dp] + [Pref_lim]-[Punit]


& pictl                                             ! power control
dp
wm_ref
{ki_p}
{kp_p}


& lim                                               ! limit speed control input
wm_ref
wm_ref_lim
{wm_min}
{wm_max}


& algeq                                             ! speed control input
-[dwm] + {kw}*([status]*[wm_ref_lim]-[wm])


& pictl                                             ! speed control
dwm
iT_ref
ki_w
kp_w


& tf1p                                              ! motor current control
iT_ref
iT
1
1/{w_cc}


& algeq                                             ! torque equations
-[Te] + {kT}*[iT]
& algeq
-[dT] + [Te]-{Tc0}


& tf1p                                              ! motor inertia
dT
wm
1/{b}
2*{H}/{b}


& algeq
[deltafvh] - [Vm] +{Vmax}


& pwlin4                                            ! overvoltage Protection
deltafvh
z1
-999
0.
0.
0.
0.
1.
999
1.
& algeq 
[Fvhi] -1 + [z1] 
& hyst
Fvhi
Fvh
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! LVRT
[Vm] + [x10]
& timer5
x10
z
-{Vr}
{tLVRT2}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRTint}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
0.

& algeq 
[Fvli] -1 + [z] 

& hyst
Fvli
Fvl
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! status variable that can switch the entire unit on or off
[p2] - [Fvh]*[Fvl]*[Ffl]*[Ffh]*[Ffr]

& algeq
[p1] - {protection}
& algeq
[p3] - 1

& swsign
p2
p3
p1
status


& algeq                                             ! underfrequency protection
[flagla] - 1
& algeq 
[flaglb] 
& algeq 
[deltafl] - [f] + {fmin}
& swsign
flagla
flaglb
deltafl
Ffli
& hyst
Ffli
Ffl
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! overfrequency protection
[flagha] - 1
& algeq 
[flaghb]
& algeq 
[deltafh] - {fmax} + [f]
& swsign
flagha
flaghb
deltafh
Ffhi
& hyst
Ffhi
Ffh
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! frequency deviation in Hz
-[deltaf] + [f]-fnom


& tfder1p                                           ! Rocof measurement in Hz/s
deltaf
rocof
1/{Trocof}
{Trocof}

& abs
rocof
abrocof


& algeq                                             ! Rocof protection
[flagra] - 1
& algeq 
[flagrb]
& algeq 
-[deltarocof] +{dfmax} -[abrocof]
& swsign
flagra
flagrb
deltarocof
Ffri
& hyst
Ffri
Ffr
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! min and max power limiters
[Plim_min]
& algeq
-[Plim] + {VPmax}


& algeq                                             ! status check without droop
- [Pref] +  [status]*{P0_unit}





& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
[Fpl0]
& algeq
[Fpl1] - 1



& algeq                                             ! NEIGHBOURHOOD	! V_n is the sum of the voltage limit signals of the neighbours, with +1 for upper limit and -1 for lower limit	! P_n is the same for the power limit.
-[P_n_var] + {P_n}
& swsign 
Fpl1
Fpl0
P_n_var
F_help_p_p

& algeq
-[F_help_p_m] + 1 - [F_help_p_p]



& swsign 
Fpl1
Fpl0
V_n_var
F_hlp_high

& algeq
-[F_hlp_low] + 1 - [F_hlp_high]

& algeq
-[V_n_var] + {V_n}
& abs
V_n_var
V_n_abs
& algeq
-[pl5] + [V_n_abs] - 1
& swsign
Fpl1
Fpl0
pl5
F_help_v

& algeq												! priority of voltage over power
-[F_hlp_p_p2] + [F_help_p_p]*(1-[F_help_v])
& algeq
-[F_hlp_p_m2] + [F_help_p_m]*(1-[F_help_v])

& algeq												! reactive power computation
-[dQp] + {V_n}*{ro_v}*{Q_max}*({Q_max}-[Qunit])/({Q_max}-{Q_min})*[F_hlp_high]
& algeq
-[dQm] + {V_n}*{ro_v}*{Q_max}*([Qunit]-{Q_min})/({Q_max}-{Q_min})*[F_hlp_low]
& algeq
-[dQnb] + [dQp] + [dQm]

& algeq												! active power computation
-[dPp] + {P_n}*{ro_p}*{P_max}*({P_max}-[Punit])/({P_max}-{P_min})*[F_hlp_p_p2]
& algeq
-[dPm] + {P_n}*{ro_p}*{P_max}*([Punit]-{P_min})/({P_max}-{P_min})*[F_hlp_p_m2]
& algeq
-[dPnb] + [dPp] + [dPm]


& algeq												! UNIT LIMIT	!s_f = security factor
-[V_lim_min] + {Vmin}*{s_f}
& algeq
-[V_lim_max] + {Vmax}/{s_f}
& algeq
-[P_lim_min] + {P_min}*{s_f}
& algeq
-[P_lim_max] + {P_max}/{s_f}

& algeq 											!should it be V or Vm?	! voltage low limit flag
-[pl1] + [V_lim_min] - [Vm]
& swsign
Fpl1
Fpl0
pl1
F_v_min_in

& algeq !should it be V or Vm?						! voltage high limit flag
-[pl2] + [Vm] - [V_lim_max]
& swsign
Fpl1
Fpl0
pl2
F_v_max_in

& algeq 											! F_v_opp = 0 if the voltage is smaller than V_lim_min or bigger than V_lim_max
-[F_v_opp] + (1-[F_v_min_in])*(1-[F_v_max_in])


& algeq												! power low limit flag
-[pl3] + [P_lim_min] - [Punit]
& swsign
F_v_opp
Fpl0
pl3
F_p_min_in

& algeq												! power high limit flag
-[pl4] + [Punit] - [P_lim_max]
& swsign
F_v_opp
Fpl0
pl4
F_p_max_in

& algeq
-[F_v_min] + [F_v_min_in]*[status]
& algeq
-[F_v_max] + [F_v_max_in]*[status]
& algeq
-[F_p_min] + [F_p_min_in]*[status]
& algeq
-[F_p_max] + [F_p_max_in]*[status]
& algeq												! final limit flag F_lim doesn't work, since when V_n and P_n are 0 the max flags are 1!
-[F_lim] + 1-(1-[F_v_min])*(1-[F_v_max])*(1-[F_p_min])*(1-[F_p_max])

& algeq
-[dP_lim] + ([Punit] - [Pref_lim] + [dPnb])*[F_lim]
& algeq
-[dP_sum] + [dP_lim]


& algeq
-[dQ_lim] + ([Qunit] + [dQnb])*[F_lim]
& algeq
-[dQ_sum] + [dQ_lim]

& algeq												! dQ MUST BE LINKED TO THE MOTOR SPEED!
-[dQ] - [Qunit] + [dQ_sum]




