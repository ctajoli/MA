!  MODEL NAME : inj_IBGtesting2         
!  MODEL DESCRIPTION FILE : IBGtest2.txt
!  Data :
!       prm(  1)=  Imax
!       prm(  2)=  IN
!       prm(  3)=  Iprate
!       prm(  4)=  Tg
!       prm(  5)=  Tm
!       prm(  6)=  tLVRT1
!       prm(  7)=  tLVRT2
!       prm(  8)=  tLVRTint
!       prm(  9)=  Vmax
!       prm( 10)=  tau              ! response time of the PLL in milliseconds
!       prm( 11)=  Vminpll          ! Voltage magnitude under which the PLL is blocked
!       prm( 12)=  a
!       prm( 13)=  Vmin
!       prm( 14)=  Vint
!       prm( 15)=  fmin
!       prm( 16)=  fmax
!       prm( 17)=  fstart
!       prm( 18)=  b
!       prm( 19)=  fr
!       prm( 20)=  Tr      ! Time after which units are allowed to reconnect to the network
!       prm( 21)=  Re
!       prm( 22)=  Xe
!       prm( 23)=  CM1
!       prm( 24)=  kRCI
!       prm( 25)=  kRCA
!       prm( 26)=  m
!       prm( 27)=  n
!       prm( 28)=  dbmin
!       prm( 29)=  dbmax
!       prm( 30)=  HVRT
!       prm( 31)=  LVRT
!       prm( 32)=  CM2
!       prm( 33)=  Vtrip
!       prm( 34)=  fdbup
!       prm( 35)=  fdbdn
!       prm( 36)=  Rup
!       prm( 37)=  Rdn
!       prm( 38)=  Trocof                          ! delay for ROCOF measurement
!       prm( 39)=  dfmax                           ! maximum permissable ROCOF
!       prm( 40)=  protection                      -1 to switch off, 1 to switch on
!       prm( 41)=  support
!       prm( 42)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 43)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 44)=  V_n                             ! number of neighbours at voltage limit
!       prm( 45)=  P_n                             ! number of neighbours at power limit
!       prm( 46)=  Vdb_p                           ! higher voltage deadband value
!       prm( 47)=  Vdb_m                           ! lower voltage deadband value
!       prm( 48)=  ro_v1                            ! neighbourhood factor for voltage
!       prm( 49)=  ro_v2
!       prm( 50)=  ro_p                            ! neighbourhood factor for power
!       prm( 51)=  Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
!       prm( 52)=  Q_min                           ! min reactive power of the unit in pu, IT WIL
!       prm( 53)=  F_help_high_0
!       prm( 54)=  F_help_low_0
!       prm( 55)=  Tp1
!       prm( 56)=  dPs_rate_max
!       prm( 57)=  dQs_rate_max
!       prm( 58)=  V_help
!       prm( 59)=  dPc_1_0
!       prm( 60)=  dPc_2_0
!       prm( 61)=  s_f2
!       prm( 62)=  lvl
!       prm( 63)=  l_1_n_min_par
!       prm( 64)=  l_1_n_max_par
!       prm( 65)=  l_1_p_min_par
!       prm( 66)=  l_1_p_max_par
!       prm( 67)=  l_2_n_min_par
!       prm( 68)=  l_2_n_max_par
!       prm( 69)=  l_2_p_min_par
!       prm( 70)=  l_2_p_max_par
!       prm( 71)=  pl1_2_0                        ! flags that varies for the different units
!       prm( 72)=  pl1_4_0
!       prm( 73)=  pl1_6_0
!       prm( 74)=  pl1_8_0
!       prm( 75)=  pl2_2_0                        ! flags that varies for the different units
!       prm( 76)=  pl2_4_0
!       prm( 77)=  pl2_6_0
!       prm( 78)=  pl2_8_0
!  Parameters :
!       prm( 79)=  vxlv  
!       prm( 80)=  vylv  
!       prm( 81)=  Vref  
!       prm( 82)=  Pext  
!       prm( 83)=  Qext  
!       prm( 84)=  Iqref  
!       prm( 85)=  Ipref  
!       prm( 86)=  kpll  
!       prm( 87)=  theta_PLL  
!       prm( 88)=  Uplim  
!       prm( 89)=  Downlim  
!       prm( 90)=  Downlimdisc  
!       prm( 91)=  UplimdeltaP  
!       prm( 92)=  DownlimdeltaP  
!       prm( 93)=  Tlim  
!       prm( 94)=  Uplimdis  
!       prm( 95)=  downlimdis  
!       prm( 96)=  ratemax  
!       prm( 97)=  rate  
!       prm( 98)=  fref  
!       prm( 99)=  dPs_rate_min  
!       prm(100)=  dQs_rate_min  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vxl                   
!       x(  4)=  vyl                   
!       x(  5)=  Vt                    
!       x(  6)=  PLLPhaseAngle         
!       x(  7)=  Vm                    
!       x(  8)=  x2                    
!       x(  9)=  Ip                    
!       x( 10)=  Iq                    
!       x( 11)=  Ipcmd                 
!       x( 12)=  Iqcmd                 
!       x( 13)=  Iqmax                 
!       x( 14)=  Iqmin                 
!       x( 15)=  Ipmax                 
!       x( 16)=  Ipmin                 
!       x( 17)=  x4                    
!       x( 18)=  DeltaW                
!       x( 19)=  DeltaWf               
!       x( 20)=  vq                    
!       x( 21)=  vd                    
!       x( 22)=  Pgen                  
!       x( 23)=  Qgen                  
!       x( 24)=  Iqext                 
!       x( 25)=  Fvl                   
!       x( 26)=  Fvli                  
!       x( 27)=  Fvhi                  
!       x( 28)=  Fvh                   
!       x( 29)=  z1                    
!       x( 30)=  x5                    
!       x( 31)=  Iptemp                
!       x( 32)=  Iqtemp                
!       x( 33)=  x10                   
!       x( 34)=  x11                   
!       x( 35)=  z                     
!       x( 36)=  deltaV                
!       x( 37)=  Pflag                 
!       x( 38)=  Pflagi                
!       x( 39)=  Pflaga                 Switch input a
!       x( 40)=  Pflagb                 Switch input b
!       x( 41)=  vxlm                  
!       x( 42)=  vylm                  
!       x( 43)=  omegam                
!       x( 44)=  fm                    
!       x( 45)=  fmfilt                
!       x( 46)=  Ffli                   Current multiplier (input of hysteresis)
!       x( 47)=  Ffl                   
!       x( 48)=  Ffhi                  
!       x( 49)=  Ffh                   
!       x( 50)=  deltaP                
!       x( 51)=  deltaPfin              Active power correction
!       x( 52)=  Ptot                  
!       x( 53)=  deltafl               
!       x( 54)=  flagla                
!       x( 55)=  flaglb                
!       x( 56)=  deltafh               
!       x( 57)=  flagha                
!       x( 58)=  flaghb                
!       x( 59)=  PLLmulta              
!       x( 60)=  PLLmultb              
!       x( 61)=  mult                  
!       x( 62)=  deltaVPLL             
!       x( 63)=  wpll                  
!       x( 64)=  g                     
!       x( 65)=  tr                    
!       x( 66)=  Fr                    
!       x( 67)=  Frtemp                
!       x( 68)=  fvla                  
!       x( 69)=  fvlb                  
!       x( 70)=  deltafvl              
!       x( 71)=  deltaPlim             
!       x( 72)=  deltafcomp            
!       x( 73)=  fcomp                 
!       x( 74)=  fa                    
!       x( 75)=  fb                    
!       x( 76)=  fcompf                
!       x( 77)=  w1                    
!       x( 78)=  w2                    
!       x( 79)=  w3                    
!       x( 80)=  w4                    
!       x( 81)=  w5                    
!       x( 82)=  w6                    
!       x( 83)=  w7                    
!       x( 84)=  w8                    
!       x( 85)=  deltaf                
!       x( 86)=  rocof                 
!       x( 87)=  abrocof               
!       x( 88)=  flagra                
!       x( 89)=  flagrb                
!       x( 90)=  deltarocof            
!       x( 91)=  Ffri                  
!       x( 92)=  Ffr                   
!       x( 93)=  status                
!       x( 94)=  p1                    
!       x( 95)=  p2                    
!       x( 96)=  p3                    
!       x( 97)=  s1                    
!       x( 98)=  s2                    
!       x( 99)=  s3                    
!       x(100)=  Fpl1                   placeholder for value 1 in switch block
!       x(101)=  Fpl0                   placeholder for value 0 in switch block
!       x(102)=  dP_lim                 unit controller states
!       x(103)=  dQ_lim                
!       x(104)=  dQ_sum                
!       x(105)=  dP_sum                
!       x(106)=  P_n_var                neighbourhood controller states
!       x(107)=  P_n_var_neg           
!       x(108)=  V_n_var               
!       x(109)=  V_n_abs               
!       x(110)=  pl5                   
!       x(111)=  F_help_v              
!       x(112)=  F_help_p_p            
!       x(113)=  F_help_p_m            
!       x(114)=  F_hlp_high            
!       x(115)=  F_hlp_low             
!       x(116)=  F_hlp_p_p2            
!       x(117)=  F_hlp_p_m2            
!       x(118)=  dQp                   
!       x(119)=  dQm                   
!       x(120)=  dQnb                  
!       x(121)=  dPp                   
!       x(122)=  dPm                   
!       x(123)=  dPnb                  
!       x(124)=  iQnb                  
!       x(125)=  dPc_1_neg              central controller states
!       x(126)=  dPc_1_pos             
!       x(127)=  dPc_2_pos             
!       x(128)=  dPc_2_neg             
!       x(129)=  dPc_3_pos             
!       x(130)=  dPc_3_neg             
!       x(131)=  level                 
!       x(132)=  dPc_1_in              
!       x(133)=  dPc_2_in              
!       x(134)=  dPc_1                 
!       x(135)=  dPc_2                 
!       x(136)=  dPc_3                 
!       x(137)=  dPc_0                 
!       x(138)=  pl1_1                 
!       x(139)=  pl1_2                 
!       x(140)=  pl1_3                 
!       x(141)=  pl1_4                 
!       x(142)=  F_l_1_neg             
!       x(143)=  pl1_5                 
!       x(144)=  pl1_6                 
!       x(145)=  pl1_7                 
!       x(146)=  pl1_8                 
!       x(147)=  F_l_1_pos             
!       x(148)=  F_l_1                 
!       x(149)=  dPc_1_s_fl            
!       x(150)=  pl2_1                 
!       x(151)=  pl2_2                 
!       x(152)=  pl2_3                 
!       x(153)=  pl2_4                 
!       x(154)=  F_l_2_neg             
!       x(155)=  pl2_5                 
!       x(156)=  pl2_6                 
!       x(157)=  pl2_7                 
!       x(158)=  pl2_8                 
!       x(159)=  F_l_2_pos             
!       x(160)=  F_l_2                 
!       x(161)=  dPc_2_s_fl            
!       x(162)=  l_abs                 
!       x(163)=  l_switch              
!       x(164)=  dPc                   
!       x(165)=  dPc_1_in_f            
!       x(166)=  dPc_2_in_f            
!       x(167)=  dPc_no_bou            
!       x(168)=  under_min             
!       x(169)=  F_p_c_min             
!       x(170)=  over_max              
!       x(171)=  F_p_c_max             

!.........................................................................................................

subroutine inj_IBGtesting2(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 78
      nbaddpar= 22
      parname(  1)='Imax'
      parname(  2)='IN'
      parname(  3)='Iprate'
      parname(  4)='Tg'
      parname(  5)='Tm'
      parname(  6)='tLVRT1'
      parname(  7)='tLVRT2'
      parname(  8)='tLVRTint'
      parname(  9)='Vmax'
      parname( 10)='tau'
      parname( 11)='Vminpll'
      parname( 12)='a'
      parname( 13)='Vmin'
      parname( 14)='Vint'
      parname( 15)='fmin'
      parname( 16)='fmax'
      parname( 17)='fstart'
      parname( 18)='b'
      parname( 19)='fr'
      parname( 20)='Tr'
      parname( 21)='Re'
      parname( 22)='Xe'
      parname( 23)='CM1'
      parname( 24)='kRCI'
      parname( 25)='kRCA'
      parname( 26)='m'
      parname( 27)='n'
      parname( 28)='dbmin'
      parname( 29)='dbmax'
      parname( 30)='HVRT'
      parname( 31)='LVRT'
      parname( 32)='CM2'
      parname( 33)='Vtrip'
      parname( 34)='fdbup'
      parname( 35)='fdbdn'
      parname( 36)='Rup'
      parname( 37)='Rdn'
      parname( 38)='Trocof'
      parname( 39)='dfmax'
      parname( 40)='protection'
      parname( 41)='support'
      parname( 42)='P_max'
      parname( 43)='P_min'
      parname( 44)='V_n'
      parname( 45)='P_n'
      parname( 46)='Vdb_p'
      parname( 47)='Vdb_m'
      parname( 48)='ro_v1'
      parname( 49)='ro_v2'
      parname( 50)='ro_p'
      parname( 51)='Q_max'
      parname( 52)='Q_min'
      parname( 53)='F_help_high_0'
      parname( 54)='F_help_low_0'
      parname( 55)='Tp1'
      parname( 56)='dPs_rate_max'
      parname( 57)='dQs_rate_max'
      parname( 58)='V_help'
      parname( 59)='dPc_1_0'
      parname( 60)='dPc_2_0'
      parname( 61)='s_f2'
      parname( 62)='lvl'
      parname( 63)='l_1_n_min_par'
      parname( 64)='l_1_n_max_par'
      parname( 65)='l_1_p_min_par'
      parname( 66)='l_1_p_max_par'
      parname( 67)='l_2_n_min_par'
      parname( 68)='l_2_n_max_par'
      parname( 69)='l_2_p_min_par'
      parname( 70)='l_2_p_max_par'
      parname( 71)='pl1_2_0'
      parname( 72)='pl1_4_0'
      parname( 73)='pl1_6_0'
      parname( 74)='pl1_8_0'
      parname( 75)='pl2_2_0'
      parname( 76)='pl2_4_0'
      parname( 77)='pl2_6_0'
      parname( 78)='pl2_8_0'
      parname( 79)='vxlv'
      parname( 80)='vylv'
      parname( 81)='Vref'
      parname( 82)='Pext'
      parname( 83)='Qext'
      parname( 84)='Iqref'
      parname( 85)='Ipref'
      parname( 86)='kpll'
      parname( 87)='theta_PLL'
      parname( 88)='Uplim'
      parname( 89)='Downlim'
      parname( 90)='Downlimdisc'
      parname( 91)='UplimdeltaP'
      parname( 92)='DownlimdeltaP'
      parname( 93)='Tlim'
      parname( 94)='Uplimdis'
      parname( 95)='downlimdis'
      parname( 96)='ratemax'
      parname( 97)='rate'
      parname( 98)='fref'
      parname( 99)='dPs_rate_min'
      parname(100)='dQs_rate_min'
      adix=  1
      adiy=  2
      nbxvar=179
      nbzvar= 63

!........................................................................................
   case (define_obs)
      nbobs= 22
      obsname(  1)='Ip'
      obsname(  2)='Iq'
      obsname(  3)='Pgen'
      obsname(  4)='Qgen'
      obsname(  5)='Vm'
      obsname(  6)='PLLPhaseAngle'
      obsname(  7)='fmfilt'
      obsname(  8)='Fr'
      obsname(  9)='Fvh'
      obsname( 10)='Ffl'
      obsname( 11)='Ffh'
      obsname( 12)='Frtemp'
      obsname( 13)='Ipcmd'
      obsname( 14)='Vt'
      obsname( 15)='wpll'
      obsname( 16)='vq'
      obsname( 17)='vd'
      obsname( 18)='rocof'
      obsname( 19)='status'
      obsname( 20)='Fvl'
      obsname( 21)='dP_sum'
      obsname( 22)='dQnb'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x(  9)              
      obs(  2)=x( 10)              
      obs(  3)=x( 22)              
      obs(  4)=x( 23)              
      obs(  5)=x(  7)              
      obs(  6)=x(  6)              
      obs(  7)=x( 45)              
      obs(  8)=x( 66)              
      obs(  9)=x( 28)              
      obs( 10)=x( 47)              
      obs( 11)=x( 49)              
      obs( 12)=x( 67)              
      obs( 13)=x( 11)              
      obs( 14)=x(  5)              
      obs( 15)=x( 63)              
      obs( 16)=x( 20)              
      obs( 17)=x( 21)              
      obs( 18)=x( 86)              
      obs( 19)=x( 93)              
      obs( 20)=x( 25)              
      obs( 21)=x(105)              
      obs( 22)=x(120)              

!........................................................................................
   case (initialize)

!vxlv = [vx] + {Re}*[ix] - {Xe}*[iy]
      prm( 79)= vx + prm( 21)*ix - prm( 22)*iy

!vylv = [vy] + {Re}*[iy] + {Xe}*[ix]
      prm( 80)= vy + prm( 21)*iy + prm( 22)*ix

!Vref = dsqrt({vxlv}**2 + {vylv}**2)
      prm( 81)= dsqrt(prm( 79)**2 + prm( 80)**2)

!Pext = -{vxlv}*[ix]-{vylv}*[iy]
      prm( 82)= -prm( 79)*ix-prm( 80)*iy

!Qext = +{vxlv}*[iy]-{vylv}*[ix]
      prm( 83)= +prm( 79)*iy-prm( 80)*ix

!Iqref = -{Qext}/{Vref}
      prm( 84)= -prm( 83)/prm( 81)

!Ipref = -{Pext}/{Vref}
      prm( 85)= -prm( 82)/prm( 81)

!kpll = 10/({tau}*0.001)
      prm( 86)= 10/(prm( 10)*0.001)

!theta_PLL = atan({vylv}/{vxlv})
      prm( 87)= atan(prm( 80)/prm( 79))

!Uplim = 9999
      prm( 88)= 9999

!Downlim = -9999
      prm( 89)= -9999

!Downlimdisc = 0
      prm( 90)= 0

!UplimdeltaP = 9999
      prm( 91)= 9999

!DownlimdeltaP = 0
      prm( 92)= 0

!Tlim = 0.01
      prm( 93)= 0.01

!Uplimdis = 0
      prm( 94)= 0

!downlimdis = -9999
      prm( 95)= -9999

!ratemax = {Iprate}*{IN}
      prm( 96)= prm(  3)*prm(  2)

!rate = 0.1
      prm( 97)= 0.1

!fref = 1
      prm( 98)= 1

!dPs_rate_min = -{dPs_rate_max}
      prm( 99)= -prm( 56)

!dQs_rate_min = -{dQs_rate_max}
      prm(100)= -prm( 57)

!vxl =  {vxlv}
      x(  3)= prm( 79)

!vyl =  {vylv}
      x(  4)= prm( 80)

!Vt =  {Vref}
      x(  5)= prm( 81)

!PLLPhaseAngle =  {theta_PLL}
      x(  6)= prm( 87)

!Vm =  {Vref}
      x(  7)= prm( 81)

!x2 =  {Vref}
      x(  8)= prm( 81)

!Ip =  {Ipref}
      x(  9)= prm( 85)

!Iq =  {Iqref}
      x( 10)= prm( 84)

!Ipcmd =  {Ipref}
      x( 11)= prm( 85)

!Iqcmd =  {Iqref}
      x( 12)= prm( 84)

!Iqmax =  dsqrt({Imax}**2 - {Ipref}**2 )
      x( 13)= dsqrt(prm(  1)**2 - prm( 85)**2 )

!Iqmin =  - dsqrt({Imax}**2 - {Ipref}**2 )
      x( 14)= - dsqrt(prm(  1)**2 - prm( 85)**2 )

!Ipmax =  {IN}
      x( 15)= prm(  2)

!Ipmin =  -0.001
      x( 16)= -0.001

!x4 =  {Ipref}
      x( 17)= prm( 85)

!DeltaW =  0
      x( 18)= 0

!DeltaWf =   0
      x( 19)=  0

!vq =  -{vxlv}*sin({theta_PLL}) + {vylv}*cos({theta_PLL})
      x( 20)= -prm( 79)*sin(prm( 87)) + prm( 80)*cos(prm( 87))

!vd =  {vxlv}*cos({theta_PLL}) + {vylv}*sin({theta_PLL})
      x( 21)= prm( 79)*cos(prm( 87)) + prm( 80)*sin(prm( 87))

!Pgen =  -{Pext}
      x( 22)= -prm( 82)

!Qgen =  -{Qext}
      x( 23)= -prm( 83)

!Iqext =  0
      x( 24)= 0

!Fvl =  1
      x( 25)= 1

!Fvli =  1
      x( 26)= 1

!Fvhi =  1
      x( 27)= 1

!Fvh =  1
      x( 28)= 1

!z1 =  0.
      x( 29)= 0.

!x5 =  {Iqref}
      x( 30)= prm( 84)

!Iptemp =  {Ipref}
      x( 31)= prm( 85)

!Iqtemp =  {Iqref}
      x( 32)= prm( 84)

!x10 =  -{Vref}
      x( 33)= -prm( 81)

!x11 =  {Vref} - {Vmax}
      x( 34)= prm( 81) - prm(  9)

!z =  0.
      x( 35)= 0.

!deltaV =  {Vref} - {dbmin}
      x( 36)= prm( 81) - prm( 28)

!Pflag =  1
      x( 37)= 1

!Pflagi =  1
      x( 38)= 1

!Pflaga =  1
      x( 39)= 1

!Pflagb =  0.
      x( 40)= 0.

!vxlm =  {vxlv}
      x( 41)= prm( 79)

!vylm =  {vylv}
      x( 42)= prm( 80)

!omegam =  -0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1
      x( 43)= -0.5*(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87))) + 1

!fm =  50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)
      x( 44)= 50*(-0.5*(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87))) + 1)

!fmfilt =  50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)
      x( 45)= 50*(-0.5*(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87))) + 1)

!Ffli =  1.
      x( 46)= 1.

!Ffl =  1.
      x( 47)= 1.

!Ffhi =  1.
      x( 48)= 1.

!Ffh =  1.
      x( 49)= 1.

!deltaP =  {b}*-{Pext}*(50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)  - {fstart})/50
      x( 50)= prm( 18)*-prm( 82)*(50*(-(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87))) + 1)  - prm( 17))/50

!deltaPfin =  0
      x( 51)= 0

!Ptot =  -{Pext}
      x( 52)= -prm( 82)

!deltafl =  50*[omega] - {fmin}
      x( 53)= 50*omega - prm( 15)

!flagla =  1
      x( 54)= 1

!flaglb =  0
      x( 55)= 0

!deltafh =  {fmax} - 50*[omega]
      x( 56)= prm( 16) - 50*omega

!flagha =  1
      x( 57)= 1

!flaghb =  0
      x( 58)= 0

!PLLmulta =  1
      x( 59)= 1

!PLLmultb =  0
      x( 60)= 0

!mult =  1
      x( 61)= 1

!deltaVPLL =  {Vref} - {Vminpll}
      x( 62)= prm( 81) - prm( 11)

!wpll =  -({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL}))*{kpll} + 2*pi*50
      x( 63)= -(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87)))*prm( 86) + 2*pi*50

!g =  0
      x( 64)= 0

!tr =  0
      x( 65)= 0

!Fr =  1
      x( 66)= 1

!Frtemp =  1
      x( 67)= 1

!fvla =  1
      x( 68)= 1

!fvlb =  1
      x( 69)= 1

!deltafvl =  -{Tr}
      x( 70)= -prm( 20)

!deltaPlim =  0
      x( 71)= 0

!deltafcomp =  50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1) - {fr}
      x( 72)= 50*(-(prm( 79)*sin(prm( 87)) - prm( 80)*cos(prm( 87))) + 1) - prm( 19)

!fcomp =  0
      x( 73)= 0

!fa =  1
      x( 74)= 1

!fb =  0
      x( 75)= 0

!fcompf =  0
      x( 76)= 0

!w1 =  0
      x( 77)= 0

!w2 =  0
      x( 78)= 0

!w3 =  0
      x( 79)= 0

!w4 =  0
      x( 80)= 0

!w5 =  0
      x( 81)= 0

!w6 =  0
      x( 82)= 0

!w7 =  0
      x( 83)= 0

!w8 =  0
      x( 84)= 0

!deltaf =  0
      x( 85)= 0

!rocof =  0
      x( 86)= 0

!abrocof =  0
      x( 87)= 0

!flagra =  1
      x( 88)= 1

!flagrb =  0
      x( 89)= 0

!deltarocof =  {dfmax} - 0
      x( 90)= prm( 39) - 0

!Ffri =  1
      x( 91)= 1

!Ffr =  1
      x( 92)= 1

!status =  1
      x( 93)= 1

!p1 =  {protection}
      x( 94)= prm( 40)

!p2 =  1
      x( 95)= 1

!p3 =  1
      x( 96)= 1

!s1 = {support}
      x( 97)=prm( 41)

!s2 =  1
      x( 98)= 1

!s3 =  0
      x( 99)= 0

!Fpl1 =  1											
      x(100)= 1											

!Fpl0 =  0											
      x(101)= 0											

!dP_lim =  0											
      x(102)= 0											

!dQ_lim =  0
      x(103)= 0

!dQ_sum =  0
      x(104)= 0

!dP_sum =  0
      x(105)= 0

!P_n_var =  {P_n}										
      x(106)= prm( 45)										

!P_n_var_neg =  -{P_n}
      x(107)= -prm( 45)

!V_n_var =  {V_n}
      x(108)= prm( 44)

!V_n_abs =  {V_n}
      x(109)= prm( 44)

!pl5 =  {V_n} - 1
      x(110)= prm( 44) - 1

!F_help_v =  0
      x(111)= 0

!F_help_p_p =  0
      x(112)= 0

!F_help_p_m =  0
      x(113)= 0

!F_hlp_high =  {F_help_high_0}
      x(114)= prm( 53)

!F_hlp_low =  {F_help_low_0}
      x(115)= prm( 54)

!F_hlp_p_p2 =  [F_help_p_p] * (1-[F_help_v])
      x(116)= x(112) * (1-x(111))

!F_hlp_p_m2 =  [F_help_p_m] * (1-[F_help_v])
      x(117)= x(113) * (1-x(111))

!dQp =  0
      x(118)= 0

!dQm =  0
      x(119)= 0

!dQnb =  0
      x(120)= 0

!dPp =  0
      x(121)= 0

!dPm =  0
      x(122)= 0

!dPnb =  0
      x(123)= 0

!iQnb =  0
      x(124)= 0

!dPc_1_neg =  - {dPc_1_0}
      x(125)= - prm( 59)

!dPc_1_pos =  {dPc_1_0}
      x(126)= prm( 59)

!dPc_2_pos =  {dPc_2_0}
      x(127)= prm( 60)

!dPc_2_neg =  - {dPc_2_0}
      x(128)= - prm( 60)

!dPc_3_pos =  {IN}/{s_f2} - {Ipref}
      x(129)= prm(  2)/prm( 61) - prm( 85)

!dPc_3_neg =  - {Ipref}
      x(130)= - prm( 85)

!level =  {lvl}
      x(131)= prm( 62)

!dPc_1_in =  {dPc_1_0}
      x(132)= prm( 59)

!dPc_2_in =  {dPc_2_0}
      x(133)= prm( 60)

!dPc_1 =  {dPc_1_0}
      x(134)= prm( 59)

!dPc_2 =  {dPc_2_0}
      x(135)= prm( 60)

!dPc_3 =  {IN}/{s_f2} - {Ipref}
      x(136)= prm(  2)/prm( 61) - prm( 85)

!dPc_0 =  0
      x(137)= 0

!pl1_1 =  {Ipref} - ({l_1_n_min_par} * {IN})
      x(138)= prm( 85) - (prm( 63) * prm(  2))

!pl1_2 =  {pl1_2_0}
      x(139)= prm( 71)

!pl1_3 =  ({l_1_n_max_par} * {IN}) - {Ipref}
      x(140)= (prm( 64) * prm(  2)) - prm( 85)

!pl1_4 =  {pl1_4_0}
      x(141)= prm( 72)

!F_l_1_neg =  {pl1_2_0} * {pl1_4_0}
      x(142)= prm( 71) * prm( 72)

!pl1_5 =  {Ipref} - ({l_1_p_min_par} * {IN})
      x(143)= prm( 85) - (prm( 65) * prm(  2))

!pl1_6 =  {pl1_6_0}
      x(144)= prm( 73)

!pl1_7 =  ({l_1_p_max_par} * {IN}) - {Ipref}
      x(145)= (prm( 66) * prm(  2)) - prm( 85)

!pl1_8 =  {pl1_8_0}
      x(146)= prm( 74)

!F_l_1_pos =  {pl1_6_0} * {pl1_8_0}
      x(147)= prm( 73) * prm( 74)

!F_l_1 =  [F_l_1_pos]
      x(148)= x(147)

!dPc_1_s_fl =  {dPc_1_0} * [F_l_1]
      x(149)= prm( 59) * x(148)

!pl2_1 =  {Ipref} - ({l_2_n_min_par} * {IN})
      x(150)= prm( 85) - (prm( 67) * prm(  2))

!pl2_2 =  {pl2_2_0}
      x(151)= prm( 75)

!pl2_3 =  ({l_2_n_max_par} * {IN}) - {Ipref}
      x(152)= (prm( 68) * prm(  2)) - prm( 85)

!pl2_4 =  {pl2_4_0}
      x(153)= prm( 76)

!F_l_2_neg =  {pl2_2_0} * {pl2_4_0}
      x(154)= prm( 75) * prm( 76)

!pl2_5 =  {Ipref} - ({l_2_p_min_par} * {IN})
      x(155)= prm( 85) - (prm( 69) * prm(  2))

!pl2_6 =  {pl2_6_0}
      x(156)= prm( 77)

!pl2_7 =  ({l_2_p_max_par} * {IN}) - {Ipref}
      x(157)= (prm( 70) * prm(  2)) - prm( 85)

!pl2_8 =  {pl2_8_0}
      x(158)= prm( 78)

!F_l_2_pos =  {pl2_6_0} * {pl2_8_0}
      x(159)= prm( 77) * prm( 78)

!F_l_2 =  [F_l_2_pos]
      x(160)= x(159)

!dPc_2_s_fl =  {dPc_2_0} * [F_l_2]
      x(161)= prm( 60) * x(160)

!l_abs =  0
      x(162)= 0

!l_switch =  1
      x(163)= 1

!dPc =  0
      x(164)= 0

!dPc_1_in_f =  {dPc_1_0} * [F_l_1]
      x(165)= prm( 59) * x(148)

!dPc_2_in_f =  {dPc_2_0} * [F_l_2]
      x(166)= prm( 60) * x(160)

!dPc_no_bou =  0
      x(167)= 0

!under_min =  [dPc] +0.0001 - [dPc_3_neg]
      x(168)= x(164) +0.0001 - x(130)

!F_p_c_min =  0
      x(169)= 0

!over_max =  - [dPc] -0.0001 + [dPc_3_pos]
      x(170)= - x(164) -0.0001 + x(129)

!F_p_c_max =  0
      x(171)= 0

!& algeq
      eqtyp(  1)=0

!& algeq
      eqtyp(  2)=0

!& algeq
      eqtyp(  3)=0

!& tf1p                          ! voltage measurement
      eqtyp(  4)=  7
      tc(  4)=prm(  5)

!& max1v1c
      eqtyp(  5)=0
      if(x(  7)<0.01)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq                         ! maximum current computation
      eqtyp(  6)=0

!& algeq
      eqtyp(  7)=0

!& limvb
      eqtyp(  8)=0
      if(x( 17)>x( 15))then
         z(  2)=1
      elseif(x( 17)<x( 16))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq             ! compute status
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq
      eqtyp( 11)=0

!& swsign
      eqtyp( 12)=0
      if(x( 94)>=0.)then
         z(  3)=1
      else
         z(  3)=2
      endif

!& algeq                     ! current computation
      eqtyp( 13)=0

!& tf1p2lim
      if(prm(  4)< 0.001)then
         prm(  4)=0.d0
         prm( 89)=-huge(0.d0)
         prm( 88)= huge(0.d0)
         prm( 89)=-huge(0.d0)
         prm( 96)= huge(0.d0)
      endif
      if(1*x( 11)-x( 31)>prm( 96)*prm(  4))then
         z(  4)=1
      elseif(1*x( 11)-x( 31)<prm( 89)*prm(  4))then
         z(  4)=-1
      else
         z(  4)=0
      endif
      eqtyp( 14)=0
      if(x( 31)>prm( 88))then
         z(  5)=1
         eqtyp( 15)=0
      elseif(x( 31)<prm( 89))then
         z(  5)=-1
         eqtyp( 15)=0
      else
         z(  5)=0
         eqtyp( 15)= 31
      endif
      tc( 15)=prm(  4)

!& algeq                     ! include reactive current priority
      eqtyp( 16)=0

!& algeq
      eqtyp( 17)=0

!& algeq												! here a Q control could be added instead of Iqext
      eqtyp( 18)=0

!& limvb
      eqtyp( 19)=0
      if(x( 30)>x( 13))then
         z(  6)=1
      elseif(x( 30)<x( 14))then
         z(  6)=-1
      else
         z(  6)=0
      endif

!& algeq
      eqtyp( 20)=0

!& tf1p
      eqtyp( 21)= 32
      tc( 21)=prm(  4)

!& algeq
      eqtyp( 22)=0

!& db
      eqtyp( 23)=0
      if(x(  7)>prm( 29))then
         z(  7)=1
      elseif(x(  7)<prm( 28))then
         z(  7)=-1
      else
         z(  7)=0
      endif

!& algeq               !LVRT
      eqtyp( 24)=0

!& timer5
      eqtyp( 25)=0
      eqtyp( 26)=0
      z(  8)=-1
      x(173)=0.

!& algeq
      eqtyp( 27)=0

!& hyst
      eqtyp( 28)=0
      if(x( 26)>1.1)then
         z(  9)=1
      elseif(x( 26)<0.9)then
         z(  9)=-1
      else
         if(1.>= 0.)then
            z(  9)=1
         else
            z(  9)=-1
         endif
      endif

!& algeq
      eqtyp( 29)=0

!& inlim
      if (1>= 0.005)then
         tc( 30)=1
      endif
      if (x( 65)>5)then
         z( 10)=1
         eqtyp( 30)=0
      elseif (x( 65)<0.) then
         z( 10)=-1
         eqtyp( 30)=0
      else
         z( 10)=0
         if (1>= 0.005)then
            eqtyp( 30)= 65
         else
            eqtyp( 30)=0
         endif
      endif

!& algeq
      eqtyp( 31)=0

!& algeq
      eqtyp( 32)=0

!& algeq
      eqtyp( 33)=0

!& swsign
      eqtyp( 34)=0
      if(x( 70)>=0.)then
         z( 11)=1
      else
         z( 11)=2
      endif

!& tf1p2lim
      if(prm( 93)< 0.001)then
         prm( 93)=0.d0
         prm( 90)=-huge(0.d0)
         prm( 88)= huge(0.d0)
         prm( 89)=-huge(0.d0)
         prm( 97)= huge(0.d0)
      endif
      if(1*x( 67)-x( 66)>prm( 97)*prm( 93))then
         z( 12)=1
      elseif(1*x( 67)-x( 66)<prm( 89)*prm( 93))then
         z( 12)=-1
      else
         z( 12)=0
      endif
      eqtyp( 35)=0
      if(x( 66)>prm( 88))then
         z( 13)=1
         eqtyp( 36)=0
      elseif(x( 66)<prm( 90))then
         z( 13)=-1
         eqtyp( 36)=0
      else
         z( 13)=0
         eqtyp( 36)= 66
      endif
      tc( 36)=prm( 93)

!& algeq             ! overvoltage protection
      eqtyp( 37)=0

!& pwlin4
      eqtyp( 38)=0
      if(x( 34)<(-999))then
         z( 14)=1
      elseif(x( 34)>=999)then
         z( 14)=   3
      elseif((-999)<=x( 34) .and. x( 34)<0.)then
         z( 14)=  1
      elseif(0.<=x( 34) .and. x( 34)<0.)then
         z( 14)=  2
      elseif(0.<=x( 34) .and. x( 34)<999)then
         z( 14)=  3
      endif

!& algeq
      eqtyp( 39)=0

!& hyst
      eqtyp( 40)=0
      if(x( 27)>1.1)then
         z( 15)=1
      elseif(x( 27)<0.9)then
         z( 15)=-1
      else
         if(1.>= 0.)then
            z( 15)=1
         else
            z( 15)=-1
         endif
      endif

!& algeq                     ! reactive current priority during undervoltage
      eqtyp( 41)=0

!& algeq
      eqtyp( 42)=0

!& algeq
      eqtyp( 43)=0

!& swsign
      eqtyp( 44)=0
      if(x( 36)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& algeq             ! switch support on and off
      eqtyp( 45)=0

!& algeq
      eqtyp( 46)=0

!& swsign            ! switch support off
      eqtyp( 47)=0
      if(x( 97)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& algeq         ! PLL
      eqtyp( 48)=0

!& algeq
      eqtyp( 49)=0

!& algeq
      eqtyp( 50)=0

!& swsign
      eqtyp( 51)=0
      if(x( 62)>=0.)then
         z( 18)=1
      else
         z( 18)=2
      endif

!& int
      if (1.< 0.005)then
         eqtyp( 52)=0
      else
         eqtyp( 52)=  6
         tc( 52)=1.
      endif

!& pictl
      eqtyp( 53)=175
      x(175)=x( 63)
      eqtyp( 54)=0

!& algeq
      eqtyp( 55)=0

!& algeq
      eqtyp( 56)=0

!& tf1p
      eqtyp( 57)= 41
      tc( 57)=prm(  5)

!& tf1p
      eqtyp( 58)= 42
      tc( 58)=prm(  5)

!& algeq             ! frequency measurement
      eqtyp( 59)=0

!& algeq
      eqtyp( 60)=0

!& tf1p
      eqtyp( 61)= 45
      tc( 61)=1.

!& algeq             ! frequency protection
      eqtyp( 62)=0

!& algeq
      eqtyp( 63)=0

!& algeq
      eqtyp( 64)=0

!& swsign
      eqtyp( 65)=0
      if(x( 53)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& algeq
      eqtyp( 66)=0

!& algeq
      eqtyp( 67)=0

!& algeq
      eqtyp( 68)=0

!& swsign
      eqtyp( 69)=0
      if(x( 56)>=0.)then
         z( 20)=1
      else
         z( 20)=2
      endif

!& hyst
      eqtyp( 70)=0
      if(x( 46)>1.1)then
         z( 21)=1
      elseif(x( 46)<0.9)then
         z( 21)=-1
      else
         if(1.>= 0.)then
            z( 21)=1
         else
            z( 21)=-1
         endif
      endif

!& hyst
      eqtyp( 71)=0
      if(x( 48)>1.1)then
         z( 22)=1
      elseif(x( 48)<0.9)then
         z( 22)=-1
      else
         if(1.>= 0.)then
            z( 22)=1
         else
            z( 22)=-1
         endif
      endif

!& algeq             ! old frequency reaction
      eqtyp( 72)=0

!& lim
      eqtyp( 73)=0
      if(x( 50)>999)then
         z( 23)=1
      elseif(x( 50)<0)then
         z( 23)=-1
      else
         z( 23)=0
      endif

!& tf1p2lim
      if(prm( 93)< 0.001)then
         prm( 93)=0.d0
         prm( 92)=-huge(0.d0)
         prm( 91)= huge(0.d0)
         prm( 92)=-huge(0.d0)
         prm( 91)= huge(0.d0)
      endif
      if(1*x( 51)-x( 71)>prm( 91)*prm( 93))then
         z( 24)=1
      elseif(1*x( 51)-x( 71)<prm( 92)*prm( 93))then
         z( 24)=-1
      else
         z( 24)=0
      endif
      eqtyp( 74)=0
      if(x( 71)>prm( 91))then
         z( 25)=1
         eqtyp( 75)=0
      elseif(x( 71)<prm( 92))then
         z( 25)=-1
         eqtyp( 75)=0
      else
         z( 25)=0
         eqtyp( 75)= 71
      endif
      tc( 75)=prm( 93)

!& algeq
      eqtyp( 76)=0

!& algeq
      eqtyp( 77)=0

!& algeq
      eqtyp( 78)=0

!& swsign
      eqtyp( 79)=0
      if(x( 72)>=0.)then
         z( 26)=1
      else
         z( 26)=2
      endif

!& tf1p
      eqtyp( 80)= 76
      tc( 80)=1

!& algeq                 ! droop control
      eqtyp( 81)=0

!& db
      eqtyp( 82)=0
      if(x( 77)>prm( 35))then
         z( 27)=1
      elseif(x( 77)<prm( 34))then
         z( 27)=-1
      else
         z( 27)=0
      endif

!& algeq
      eqtyp( 83)=0

!& algeq
      eqtyp( 84)=0

!& lim
      eqtyp( 85)=0
      if(x( 79)>(-0.00001))then
         z( 28)=1
      elseif(x( 79)<(-99999.))then
         z( 28)=-1
      else
         z( 28)=0
      endif

!& lim
      eqtyp( 86)=0
      if(x( 80)>99999.)then
         z( 29)=1
      elseif(x( 80)<0.00001)then
         z( 29)=-1
      else
         z( 29)=0
      endif

!& algeq												! here the P control could be placed to give w7
      eqtyp( 87)=0

!& algeq
      eqtyp( 88)=0

!& swsign            ! switch support off
      eqtyp( 89)=0
      if(x( 97)>=0.)then
         z( 30)=1
      else
         z( 30)=2
      endif

!& algeq
      eqtyp( 90)=0

!& algeq             ! alignment
      eqtyp( 91)=0

!& algeq
      eqtyp( 92)=0

!& algeq
      eqtyp( 93)=0

!& algeq
      eqtyp( 94)=0

!& algeq
      eqtyp( 95)=0

!& algeq
      eqtyp( 96)=0

!& algeq                ! frequency deviation in Hz
      eqtyp( 97)=0

!& tfder1p               ! Rocof measurement in Hz/s
      x(177)=x( 85)
      eqtyp( 98)=177
      tc( 98)=prm( 38)
      eqtyp( 99)=0

!& abs
      eqtyp(100)=0
      if(x( 86)>0. )then
         z( 31)=1
      else
         z( 31)=-1
      endif

!& algeq                 ! Rocof protection
      eqtyp(101)=0

!& algeq
      eqtyp(102)=0

!& algeq
      eqtyp(103)=0

!& swsign
      eqtyp(104)=0
      if(x( 90)>=0.)then
         z( 32)=1
      else
         z( 32)=2
      endif

!& hyst
      eqtyp(105)=0
      if(x( 91)>1.1)then
         z( 33)=1
      elseif(x( 91)<0.9)then
         z( 33)=-1
      else
         if(1.>= 0.)then
            z( 33)=1
         else
            z( 33)=-1
         endif
      endif

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      eqtyp(106)=0

!& algeq
      eqtyp(107)=0

!& algeq												! NEIGHBOURHOOD CONTROL
      eqtyp(108)=0

!& algeq
      eqtyp(109)=0

!& swsign											! F_help_p_m is 1 if P_n > 0
      eqtyp(110)=0
      if(x(107)>=0.)then
         z( 34)=1
      else
         z( 34)=2
      endif

!& swsign											! F_help_p_p is 1 if P_n < 0
      eqtyp(111)=0
      if(x(106)>=0.)then
         z( 35)=1
      else
         z( 35)=2
      endif

!& hyst ! should it be V or Vm?
      eqtyp(112)=0
      if(x(  7)>prm( 46))then
         z( 36)=1
      elseif(x(  7)<prm( 47))then
         z( 36)=-1
      else
         if((-1.)>= 0.)then
            z( 36)=1
         else
            z( 36)=-1
         endif
      endif

!& hyst
      eqtyp(113)=0
      if(x(  7)>prm( 46))then
         z( 37)=1
      elseif(x(  7)<prm( 47))then
         z( 37)=-1
      else
         if(1.>= 0.)then
            z( 37)=1
         else
            z( 37)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      eqtyp(114)=0

!& abs
      eqtyp(115)=0
      if(x(108)>0. )then
         z( 38)=1
      else
         z( 38)=-1
      endif

!& algeq
      eqtyp(116)=0

!& swsign
      eqtyp(117)=0
      if(x(110)>=0.)then
         z( 39)=1
      else
         z( 39)=2
      endif

!& algeq												! priority of voltage over power
      eqtyp(118)=0

!& algeq
      eqtyp(119)=0

!& algeq												! reactive power computation
      eqtyp(120)=0

!& algeq
      eqtyp(121)=0

!& algeq
      eqtyp(122)=0

!& algeq												! active power computation
      eqtyp(123)=0

!& algeq
      eqtyp(124)=0

!& algeq
      eqtyp(125)=0

!& algeq
      eqtyp(126)=0

!& tf1p2lim
      if(prm( 55)< 0.001)then
         prm( 55)=0.d0
         prm( 89)=-huge(0.d0)
         prm( 88)= huge(0.d0)
         prm( 99)=-huge(0.d0)
         prm( 56)= huge(0.d0)
      endif
      if(1*x(102)-x(105)>prm( 56)*prm( 55))then
         z( 40)=1
      elseif(1*x(102)-x(105)<prm( 99)*prm( 55))then
         z( 40)=-1
      else
         z( 40)=0
      endif
      eqtyp(127)=0
      if(x(105)>prm( 88))then
         z( 41)=1
         eqtyp(128)=0
      elseif(x(105)<prm( 89))then
         z( 41)=-1
         eqtyp(128)=0
      else
         z( 41)=0
         eqtyp(128)=105
      endif
      tc(128)=prm( 55)

!& algeq
      eqtyp(129)=0

!& tf1p2lim
      if(prm( 55)< 0.001)then
         prm( 55)=0.d0
         prm( 89)=-huge(0.d0)
         prm( 88)= huge(0.d0)
         prm(100)=-huge(0.d0)
         prm( 57)= huge(0.d0)
      endif
      if(1*x(103)-x(104)>prm( 57)*prm( 55))then
         z( 42)=1
      elseif(1*x(103)-x(104)<prm(100)*prm( 55))then
         z( 42)=-1
      else
         z( 42)=0
      endif
      eqtyp(130)=0
      if(x(104)>prm( 88))then
         z( 43)=1
         eqtyp(131)=0
      elseif(x(104)<prm( 89))then
         z( 43)=-1
         eqtyp(131)=0
      else
         z( 43)=0
         eqtyp(131)=104
      endif
      tc(131)=prm( 55)

!& algeq
      eqtyp(132)=0

!& algeq												! CENTRAL CONTROL, positive and negative variation
      eqtyp(133)=0

!& algeq
      eqtyp(134)=0

!& algeq
      eqtyp(135)=0

!& algeq
      eqtyp(136)=0

!& algeq
      eqtyp(137)=0

!& algeq
      eqtyp(138)=0

!& algeq
      eqtyp(139)=0

!& swsign
      eqtyp(140)=0
      if(x(131)>=0.)then
         z( 44)=1
      else
         z( 44)=2
      endif

!& swsign
      eqtyp(141)=0
      if(x(131)>=0.)then
         z( 45)=1
      else
         z( 45)=2
      endif

!& swsign
      eqtyp(142)=0
      if(x(131)>=0.)then
         z( 46)=1
      else
         z( 46)=2
      endif

!& limvb												! limiting the variation to the max and min bounds
      eqtyp(143)=0
      if(x(132)>x(129))then
         z( 47)=1
      elseif(x(132)<x(130))then
         z( 47)=-1
      else
         z( 47)=0
      endif

!& limvb
      eqtyp(144)=0
      if(x(133)>x(129))then
         z( 48)=1
      elseif(x(133)<x(130))then
         z( 48)=-1
      else
         z( 48)=0
      endif

!& algeq !dPc_0 is equal to 0
      eqtyp(145)=0

!& algeq												! level 1 low negative
      eqtyp(146)=0

!& swsign
      eqtyp(147)=0
      if(x(138)>=0.)then
         z( 49)=1
      else
         z( 49)=2
      endif

!& algeq												! level 1 high negative
      eqtyp(148)=0

!& swsign
      eqtyp(149)=0
      if(x(140)>=0.)then
         z( 50)=1
      else
         z( 50)=2
      endif

!& algeq												! level 1 negative flag
      eqtyp(150)=0

!& algeq												! level 1 low positive
      eqtyp(151)=0

!& swsign
      eqtyp(152)=0
      if(x(143)>=0.)then
         z( 51)=1
      else
         z( 51)=2
      endif

!& algeq												! level 1 high positive
      eqtyp(153)=0

!& swsign
      eqtyp(154)=0
      if(x(145)>=0.)then
         z( 52)=1
      else
         z( 52)=2
      endif

!& algeq												! level 1 positive flag
      eqtyp(155)=0

!& swsign
      eqtyp(156)=0
      if(x(131)>=0.)then
         z( 53)=1
      else
         z( 53)=2
      endif

!& algeq
      eqtyp(157)=0

!& algeq												! level 2 low negative
      eqtyp(158)=0

!& swsign
      eqtyp(159)=0
      if(x(150)>=0.)then
         z( 54)=1
      else
         z( 54)=2
      endif

!& algeq												! level 2 high negative
      eqtyp(160)=0

!& swsign
      eqtyp(161)=0
      if(x(152)>=0.)then
         z( 55)=1
      else
         z( 55)=2
      endif

!& algeq												! level 2 negative flag
      eqtyp(162)=0

!& algeq												! level 2 low positive
      eqtyp(163)=0

!& swsign
      eqtyp(164)=0
      if(x(155)>=0.)then
         z( 56)=1
      else
         z( 56)=2
      endif

!& algeq												! level 2 high positive
      eqtyp(165)=0

!& swsign
      eqtyp(166)=0
      if(x(157)>=0.)then
         z( 57)=1
      else
         z( 57)=2
      endif

!& algeq												! level 2 positive flag
      eqtyp(167)=0

!& swsign
      eqtyp(168)=0
      if(x(131)>=0.)then
         z( 58)=1
      else
         z( 58)=2
      endif

!& algeq
      eqtyp(169)=0

!& abs												! absolute of the level
      eqtyp(170)=0
      if(x(131)>0. )then
         z( 59)=1
      else
         z( 59)=-1
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      eqtyp(171)=0

!& switch4											! choice between the levels
      eqtyp(172)=0
      z( 60)=max(1,min(  4,nint(x(163))))

!& algeq
      eqtyp(173)=0

!& algeq
      eqtyp(174)=0

!& switch4											! choice between the levels without bounds
      eqtyp(175)=0
      z( 61)=max(1,min(  4,nint(x(163))))

!& algeq													! checking if the variation should have been greater than the bounds
      eqtyp(176)=0

!& swsign
      eqtyp(177)=0
      if(x(168)>=0.)then
         z( 62)=1
      else
         z( 62)=2
      endif

!& algeq													! checking if the variation should have been greater than the bounds
      eqtyp(178)=0

!& swsign
      eqtyp(179)=0
      if(x(170)>=0.)then
         z( 63)=1
      else
         z( 63)=2
      endif

!........................................................................................
   case (evaluate_eqs)

!& algeq
      f(  1)=vx + prm( 21)*x(  1) - prm( 22)*x(  2) - x(  3)

!& algeq
      f(  2)=vy + prm( 21)*x(  2) + prm( 22)*x(  1) - x(  4)

!& algeq
      f(  3)=x(  5) - dsqrt(x(  3)**2 + x(  4)**2)

!& tf1p                          ! voltage measurement
      f(  4)=(-x(  7)+1.*x(  5))

!& max1v1c
      select case (z(  1))
         case(1)
            f(  5)=0.01-x(  8)
         case(2)
            f(  5)=x(  7)-x(  8)
      end select

!& algeq                         ! maximum current computation
      f(  6)=x( 52)/x(  8) - x( 17)

!& algeq
      f(  7)=x( 16) + 0.001

!& limvb
      select case (z(  2))
         case(0)
            f(  8)=x( 11)-x( 17)
         case(-1)
            f(  8)=x( 11)-x( 16)
         case(1)
            f(  8)=x( 11)-x( 15)
      end select

!& algeq             ! compute status
      f(  9)=x( 95) - x( 28)*x( 47)*x( 49)*x( 92)*x( 66)

!& algeq
      f( 10)=x( 94) - prm( 40)

!& algeq
      f( 11)=x( 96) - 1

!& swsign
      select case (z(  3))
         case(1)
            f( 12)=x( 93)-x( 95)
         case(2)
            f( 12)=x( 93)-x( 96)
      end select

!& algeq                     ! current computation
      f( 13)=x( 31)*x( 93) - x(  9)

!& tf1p2lim
      select case (z(  4))
         case(0)
            f( 14)=x(172)-1*x( 11)+x( 31)
         case(1)
            f( 14)=x(172)-prm( 96)*prm(  4)
         case(-1)
            f( 14)=x(172)-prm( 89)*prm(  4)
      end select
      select case (z(  5))
         case(0)
            f( 15)=x(172)
         case(1)
            f( 15)=x( 31)-prm( 88)
         case(-1)
            f( 15)=x( 31)-prm( 89)
      end select

!& algeq                     ! include reactive current priority
      f( 16)=x( 13) -x( 37)*dsqrt(max(0.d0,prm(  1)**2 - x( 11)**2)) - (1-x( 37))*(prm( 32)*prm(  1)*0.707 + (1-prm( 32))*prm(  1))

!& algeq
      f( 17)=x( 14) + x( 37)*dsqrt(max(0.d0,prm(  1)**2 - x( 11)**2)) + (1-x( 37))*prm(  1)

!& algeq												! here a Q control could be added instead of Iqext
      f( 18)= prm( 84) - x( 30) - x(124) - x( 24)*prm( 58)

!& limvb
      select case (z(  6))
         case(0)
            f( 19)=x( 12)-x( 30)
         case(-1)
            f( 19)=x( 12)-x( 14)
         case(1)
            f( 19)=x( 12)-x( 13)
      end select

!& algeq
      f( 20)=x( 32)*x( 93) - x( 10)

!& tf1p
      f( 21)=(-x( 32)+1.*x( 12))

!& algeq
      f( 22)=x( 15) - x( 37)*prm(  2) -  (1-x( 37))*dsqrt(max(0.d0,prm(  1)**2 - x( 12)**2))

!& db
      select case (z(  7))
         case(0)
            f( 23)=x( 24)
         case(-1)
            f( 23)=x( 24)-(-prm( 26)*prm(  1)*(prm( 23) + prm( 32)))-(prm( 24)*prm(  1)*(prm( 23) + prm( 32) ))*(x(  7)-prm( 28))
         case(1)
            f( 23)=x( 24)-prm( 27)*prm(  1)-prm( 25)*prm(  1)*prm( 30)*(x(  7)-prm( 29))
      end select

!& algeq               !LVRT
      f( 24)=x(  7) + x( 33)

!& timer5
      select case (z(  8))
         case (-1)
            f( 25)=x( 35)
            f( 26)=x(173)
         case (0)
            f( 25)=x( 35)
            f( 26)= 1.
         case (1)
            f( 25)=x( 35)-1.
            f( 26)= 0.
      end select

!& algeq
      f( 27)=x( 26) -1 + x( 35)

!& hyst
      if(z(  9) == 1)then
         f( 28)=x( 25)-1.-(1.-1.)*(x( 26)-1.1)/(1.1-0.9)
      else
         f( 28)=x( 25)-0.-(0.-0.)*(x( 26)-0.9)/(1.1-0.9)
      endif

!& algeq
      f( 29)=x( 64) - 1 + x( 25)

!& inlim
      if (1>= 0.005)then
         select case (z( 10))
            case(0)
               f( 30)=x( 64)
            case(1)
               f( 30)=x( 65)-5
            case(-1)
               f( 30)=x( 65)-0.
         end select
      else
         select case (z( 10))
            case(0)
               f( 30)=x( 64)-x( 65)
            case(1)
               f( 30)=x( 65)-5
            case(-1)
               f( 30)=x( 65)-0.
         end select
      endif

!& algeq
      f( 31)=x( 68) - 1

!& algeq
      f( 32)=x( 69) - 1 + x( 64)

!& algeq
      f( 33)=x( 70) + prm( 20) - x( 65)

!& swsign
      select case (z( 11))
         case(1)
            f( 34)=x( 67)-x( 68)
         case(2)
            f( 34)=x( 67)-x( 69)
      end select

!& tf1p2lim
      select case (z( 12))
         case(0)
            f( 35)=x(174)-1*x( 67)+x( 66)
         case(1)
            f( 35)=x(174)-prm( 97)*prm( 93)
         case(-1)
            f( 35)=x(174)-prm( 89)*prm( 93)
      end select
      select case (z( 13))
         case(0)
            f( 36)=x(174)
         case(1)
            f( 36)=x( 66)-prm( 88)
         case(-1)
            f( 36)=x( 66)-prm( 90)
      end select

!& algeq             ! overvoltage protection
      f( 37)=x( 34) - x(  7) + prm(  9)

!& pwlin4
      select case (z( 14))
         case (  1)
            f( 38)=0.+ ( (0.-0.)*(x( 34)-(-999))/(0.-(-999)) ) -x( 29)
         case (  2)
            f( 38)=0.+ ( (1.-0.)*(x( 34)-0.)/(0.-0.) ) -x( 29)
         case (  3)
            f( 38)=1.+ ( (1.-1.)*(x( 34)-0.)/(999-0.) ) -x( 29)
      end select

!& algeq
      f( 39)=x( 27) -1 + x( 29)

!& hyst
      if(z( 15) == 1)then
         f( 40)=x( 28)-1.-(1.-1.)*(x( 27)-1.1)/(1.1-0.9)
      else
         f( 40)=x( 28)-0.-(0.-0.)*(x( 27)-0.9)/(1.1-0.9)
      endif

!& algeq                     ! reactive current priority during undervoltage
      f( 41)=x( 36) - x(  7) + prm( 28)

!& algeq
      f( 42)=x( 39) - 1

!& algeq
      f( 43)=x( 40)

!& swsign
      select case (z( 16))
         case(1)
            f( 44)=x( 38)-x( 39)
         case(2)
            f( 44)=x( 38)-x( 40)
      end select

!& algeq             ! switch support on and off
      f( 45)=x( 97) - prm( 41)

!& algeq
      f( 46)=x( 98) - 1

!& swsign            ! switch support off
      select case (z( 17))
         case(1)
            f( 47)=x( 37)-x( 38)
         case(2)
            f( 47)=x( 37)-x( 98)
      end select

!& algeq         ! PLL
      f( 48)=x( 59) - 1

!& algeq
      f( 49)=x( 60)

!& algeq
      f( 50)=x( 62) - x(  7) + prm( 11)

!& swsign
      select case (z( 18))
         case(1)
            f( 51)=x( 61)-x( 59)
         case(2)
            f( 51)=x( 61)-x( 60)
      end select

!& int
      if (1.< 0.005)then
         f( 52)=x( 19)-x(  6)
      else
         f( 52)=x( 19)
      endif

!& pictl
      f( 53)=0.1/(prm( 10)*0.001)**2                                                                                                                                                                                                                                                                                     *x( 20)
      f( 54)=0.5/(prm( 10)*0.001)                                                                                                                                                                                                                                                                                        *x( 20)+x(175)-x( 63)

!& algeq
      f( 55)=x( 18) - x( 63) + omega*2*pi*50

!& algeq
      f( 56)=x( 19) - x( 18)*x( 61)

!& tf1p
      f( 57)=(-x( 41)+1.*x(  3))

!& tf1p
      f( 58)=(-x( 42)+1.*x(  4))

!& algeq             ! frequency measurement
      f( 59)=x( 43) - x( 63)/(2*pi*50)

!& algeq
      f( 60)=x( 44) - 50*x( 43)

!& tf1p
      f( 61)=(-x( 45)+1.*x( 44))

!& algeq             ! frequency protection
      f( 62)=x( 54) - 1

!& algeq
      f( 63)=x( 55)

!& algeq
      f( 64)=x( 53) - x( 45) + prm( 15)

!& swsign
      select case (z( 19))
         case(1)
            f( 65)=x( 46)-x( 54)
         case(2)
            f( 65)=x( 46)-x( 55)
      end select

!& algeq
      f( 66)=x( 57) - 1

!& algeq
      f( 67)=x( 58)

!& algeq
      f( 68)=x( 56) - prm( 16) + x( 45)

!& swsign
      select case (z( 20))
         case(1)
            f( 69)=x( 48)-x( 57)
         case(2)
            f( 69)=x( 48)-x( 58)
      end select

!& hyst
      if(z( 21) == 1)then
         f( 70)=x( 47)-1.-(1.-1.)*(x( 46)-1.1)/(1.1-0.9)
      else
         f( 70)=x( 47)-0.-(0.-0.)*(x( 46)-0.9)/(1.1-0.9)
      endif

!& hyst
      if(z( 22) == 1)then
         f( 71)=x( 49)-1.-(1.-1.)*(x( 48)-1.1)/(1.1-0.9)
      else
         f( 71)=x( 49)-0.-(0.-0.)*(x( 48)-0.9)/(1.1-0.9)
      endif

!& algeq             ! old frequency reaction
      f( 72)=x( 50) + prm( 18)*prm( 82)*(x( 45) - prm( 17))/50

!& lim
      select case (z( 23))
         case(0)
            f( 73)=x( 51)-x( 50)
         case(-1)
            f( 73)=x( 51)-0
         case(1)
            f( 73)=x( 51)-999
      end select

!& tf1p2lim
      select case (z( 24))
         case(0)
            f( 74)=x(176)-1*x( 51)+x( 71)
         case(1)
            f( 74)=x(176)-prm( 91)*prm( 93)
         case(-1)
            f( 74)=x(176)-prm( 92)*prm( 93)
      end select
      select case (z( 25))
         case(0)
            f( 75)=x(176)
         case(1)
            f( 75)=x( 71)-prm( 91)
         case(-1)
            f( 75)=x( 71)-prm( 92)
      end select

!& algeq
      f( 76)=x( 74) - 1

!& algeq
      f( 77)=x( 75)

!& algeq
      f( 78)=x( 72) - x( 45) + prm( 19)

!& swsign
      select case (z( 26))
         case(1)
            f( 79)=x( 73)-x( 74)
         case(2)
            f( 79)=x( 73)-x( 75)
      end select

!& tf1p
      f( 80)=(-x( 76)+1.*x( 73))

!& algeq                 ! droop control
      f( 81)=x( 77) + (x( 45)/50) - prm( 98)

!& db
      select case (z( 27))
         case(0)
            f( 82)=x( 78)
         case(-1)
            f( 82)=x( 78)-0.-1.*(x( 77)-prm( 34))
         case(1)
            f( 82)=x( 78)-0.-1.*(x( 77)-prm( 35))
      end select

!& algeq
      f( 83)=x( 79) - x( 78)*prm( 37)

!& algeq
      f( 84)=x( 80) - x( 78)*prm( 36)

!& lim
      select case (z( 28))
         case(0)
            f( 85)=x( 81)-x( 79)
         case(-1)
            f( 85)=x( 81)-(-99999.)
         case(1)
            f( 85)=x( 81)-(-0.00001)
      end select

!& lim
      select case (z( 29))
         case(0)
            f( 86)=x( 82)-x( 80)
         case(-1)
            f( 86)=x( 82)-0.00001
         case(1)
            f( 86)=x( 82)-99999.
      end select

!& algeq												! here the P control could be placed to give w7
      f( 87)=-x( 83) + x( 82) + x( 81)

!& algeq
      f( 88)=x( 99)

!& swsign            ! switch support off
      select case (z( 30))
         case(1)
            f( 89)=x( 84)-x( 83)
         case(2)
            f( 89)=x( 84)-x( 99)
      end select

!& algeq
      f( 90)=x( 52)+prm( 82)-x( 84) + x(105)

!& algeq             ! alignment
      f( 91)=x( 21) - x( 41)*cos(x(  6)) - x( 42)*sin(x(  6))

!& algeq
      f( 92)=x( 20) + x( 41)*sin(x(  6)) - x( 42)*cos(x(  6))

!& algeq
      f( 93)=x( 22) - x( 21)*x(  9)

!& algeq
      f( 94)=x( 23) - x( 21)*x( 10)

!& algeq
      f( 95)=x(  1) - x(  9)*cos(x(  6)) - x( 10)*sin(x(  6))

!& algeq
      f( 96)=x(  2) - x(  9)*sin(x(  6)) + x( 10)*cos(x(  6))

!& algeq                ! frequency deviation in Hz
      f( 97)=-x( 85) + x( 45)-prm( 98)*50

!& tfder1p               ! Rocof measurement in Hz/s
      f( 98)=-x(177)+x( 85)
      if (prm( 38)< 0.005)then
         f( 99)=1/prm( 38)*x( 85)-x( 86)
      else
         f( 99)=1/prm( 38)*(x( 85)-x(177))-x( 86)
      endif

!& abs
      if(z( 31) == 1 )then
         f(100)=x( 87)-x( 86)
      else
         f(100)=x( 87)+x( 86)
      endif

!& algeq                 ! Rocof protection
      f(101)=x( 88) - 1

!& algeq
      f(102)=x( 89)

!& algeq
      f(103)=-x( 90) +prm( 39) -x( 87)

!& swsign
      select case (z( 32))
         case(1)
            f(104)=x( 91)-x( 88)
         case(2)
            f(104)=x( 91)-x( 89)
      end select

!& hyst
      if(z( 33) == 1)then
         f(105)=x( 92)-1.-(1.-1.)*(x( 91)-1.1)/(1.1-0.9)
      else
         f(105)=x( 92)-0.-(0.-0.)*(x( 91)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      f(106)=x(101)

!& algeq
      f(107)=x(100) - 1

!& algeq												! NEIGHBOURHOOD CONTROL
      f(108)=-x(106) + prm( 45)

!& algeq
      f(109)=-x(107) - prm( 45)

!& swsign											! F_help_p_m is 1 if P_n > 0
      select case (z( 34))
         case(1)
            f(110)=x(113)-x(101)
         case(2)
            f(110)=x(113)-x(100)
      end select

!& swsign											! F_help_p_p is 1 if P_n < 0
      select case (z( 35))
         case(1)
            f(111)=x(112)-x(101)
         case(2)
            f(111)=x(112)-x(100)
      end select

!& hyst ! should it be V or Vm?
      if(z( 36) == 1)then
         f(112)=x(114)-1.-(1.-1.)*(x(  7)-prm( 46))/(prm( 46)-prm( 47))
      else
         f(112)=x(114)-0.-(0.-0.)*(x(  7)-prm( 47))/(prm( 46)-prm( 47))
      endif

!& hyst
      if(z( 37) == 1)then
         f(113)=x(115)-0.-(0.-0.)*(x(  7)-prm( 46))/(prm( 46)-prm( 47))
      else
         f(113)=x(115)-1.-(1.-1.)*(x(  7)-prm( 47))/(prm( 46)-prm( 47))
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      f(114)=-x(108) + prm( 44)

!& abs
      if(z( 38) == 1 )then
         f(115)=x(109)-x(108)
      else
         f(115)=x(109)+x(108)
      endif

!& algeq
      f(116)=-x(110) + x(109) - 1

!& swsign
      select case (z( 39))
         case(1)
            f(117)=x(111)-x(100)
         case(2)
            f(117)=x(111)-x(101)
      end select

!& algeq												! priority of voltage over power
      f(118)=-x(116) + x(112)*(1-x(111))

!& algeq
      f(119)=-x(117) + x(113)*(1-x(111))

!& algeq												! reactive power computation
      f(120)=-x(118) + prm( 44)*prm( 48)*x( 13)*x( 13)*x(114)/(x( 13)-x( 14)+prm( 49)*x( 13)*x(109))

!& algeq
      f(121)=-x(119) + prm( 44)*prm( 48)*x( 13)*x( 14)*x(115)/(x( 13)-x( 14)+prm( 49)*x( 13)*x(109))

!& algeq
      f(122)=-x(120) + x(118) + x(119)

!& algeq												! active power computation
      f(123)=-x(121) + prm( 45)*prm( 50)*(x( 15)-prm( 85))*x(116)

!& algeq
      f(124)=-x(122) + prm( 45)*prm( 50)*(prm( 85))*x(117)

!& algeq
      f(125)=-x(123) + x(121) + x(122)

!& algeq
      f(126)=-x(102) + x(123) + x(164)

!& tf1p2lim
      select case (z( 40))
         case(0)
            f(127)=x(178)-1*x(102)+x(105)
         case(1)
            f(127)=x(178)-prm( 56)*prm( 55)
         case(-1)
            f(127)=x(178)-prm( 99)*prm( 55)
      end select
      select case (z( 41))
         case(0)
            f(128)=x(178)
         case(1)
            f(128)=x(105)-prm( 88)
         case(-1)
            f(128)=x(105)-prm( 89)
      end select

!& algeq
      f(129)=-x(103) + x(120)

!& tf1p2lim
      select case (z( 42))
         case(0)
            f(130)=x(179)-1*x(103)+x(104)
         case(1)
            f(130)=x(179)-prm( 57)*prm( 55)
         case(-1)
            f(130)=x(179)-prm(100)*prm( 55)
      end select
      select case (z( 43))
         case(0)
            f(131)=x(179)
         case(1)
            f(131)=x(104)-prm( 88)
         case(-1)
            f(131)=x(104)-prm( 89)
      end select

!& algeq
      f(132)=-x(124) - x(104)

!& algeq												! CENTRAL CONTROL, positive and negative variation
      f(133)=-x(126) + prm( 59) * prm( 82)

!& algeq
      f(134)=-x(127) + prm( 60) * prm( 82)

!& algeq
      f(135)=x(125) + x(126)

!& algeq
      f(136)=x(128) + x(127)

!& algeq
      f(137)=-x(129) + x( 15)/prm( 61) - prm( 85)

!& algeq
      f(138)=-x(130) - prm( 85)

!& algeq
      f(139)=-x(131) + prm( 62)

!& swsign
      select case (z( 44))
         case(1)
            f(140)=x(132)-x(126)
         case(2)
            f(140)=x(132)-x(125)
      end select

!& swsign
      select case (z( 45))
         case(1)
            f(141)=x(133)-x(127)
         case(2)
            f(141)=x(133)-x(128)
      end select

!& swsign
      select case (z( 46))
         case(1)
            f(142)=x(136)-x(129)
         case(2)
            f(142)=x(136)-x(130)
      end select

!& limvb												! limiting the variation to the max and min bounds
      select case (z( 47))
         case(0)
            f(143)=x(134)-x(132)
         case(-1)
            f(143)=x(134)-x(130)
         case(1)
            f(143)=x(134)-x(129)
      end select

!& limvb
      select case (z( 48))
         case(0)
            f(144)=x(135)-x(133)
         case(-1)
            f(144)=x(135)-x(130)
         case(1)
            f(144)=x(135)-x(129)
      end select

!& algeq !dPc_0 is equal to 0
      f(145)=x(137)

!& algeq												! level 1 low negative
      f(146)=-x(138) + prm( 85) - prm( 63) * x( 15)

!& swsign
      select case (z( 49))
         case(1)
            f(147)=x(139)-x(100)
         case(2)
            f(147)=x(139)-x(101)
      end select

!& algeq												! level 1 high negative
      f(148)=-x(140) + prm( 64) * x( 15) - prm( 85)

!& swsign
      select case (z( 50))
         case(1)
            f(149)=x(141)-x(100)
         case(2)
            f(149)=x(141)-x(101)
      end select

!& algeq												! level 1 negative flag
      f(150)=-x(142) + (x(139) * x(141))

!& algeq												! level 1 low positive
      f(151)=-x(143) + prm( 85) - prm( 65) * x( 15)

!& swsign
      select case (z( 51))
         case(1)
            f(152)=x(144)-x(100)
         case(2)
            f(152)=x(144)-x(101)
      end select

!& algeq												! level 1 high positive
      f(153)=-x(145) + prm( 66) * x( 15) - prm( 85)

!& swsign
      select case (z( 52))
         case(1)
            f(154)=x(146)-x(100)
         case(2)
            f(154)=x(146)-x(101)
      end select

!& algeq												! level 1 positive flag
      f(155)=-x(147) + (x(144) * x(146))

!& swsign
      select case (z( 53))
         case(1)
            f(156)=x(148)-x(147)
         case(2)
            f(156)=x(148)-x(142)
      end select

!& algeq
      f(157)=-x(149) + x(134) * x(148)

!& algeq												! level 2 low negative
      f(158)=-x(150) + prm( 85) - prm( 67) * x( 15)

!& swsign
      select case (z( 54))
         case(1)
            f(159)=x(151)-x(100)
         case(2)
            f(159)=x(151)-x(101)
      end select

!& algeq												! level 2 high negative
      f(160)=-x(152) + prm( 68) * x( 15) - prm( 85)

!& swsign
      select case (z( 55))
         case(1)
            f(161)=x(153)-x(100)
         case(2)
            f(161)=x(153)-x(101)
      end select

!& algeq												! level 2 negative flag
      f(162)=-x(154) + (x(151) * x(153))

!& algeq												! level 2 low positive
      f(163)=-x(155) + prm( 85) - prm( 69) * x( 15)

!& swsign
      select case (z( 56))
         case(1)
            f(164)=x(156)-x(100)
         case(2)
            f(164)=x(156)-x(101)
      end select

!& algeq												! level 2 high positive
      f(165)=-x(157) + prm( 70) * x( 15) - prm( 85)

!& swsign
      select case (z( 57))
         case(1)
            f(166)=x(158)-x(100)
         case(2)
            f(166)=x(158)-x(101)
      end select

!& algeq												! level 2 positive flag
      f(167)=-x(159) + (x(156) * x(158))

!& swsign
      select case (z( 58))
         case(1)
            f(168)=x(160)-x(159)
         case(2)
            f(168)=x(160)-x(154)
      end select

!& algeq
      f(169)=-x(161) + x(135) * x(160)

!& abs												! absolute of the level
      if(z( 59) == 1 )then
         f(170)=x(162)-x(131)
      else
         f(170)=x(162)+x(131)
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      f(171)=-x(163) + x(162) + 1

!& switch4											! choice between the levels
      select case (z( 60))
         case(  1)
            f(172)=x(164)-x(137)
         case(  2)
            f(172)=x(164)-x(149)
         case(  3)
            f(172)=x(164)-x(161)
         case(  4)
            f(172)=x(164)-x(136)
      end select

!& algeq
      f(173)=-x(165) + x(132) * x(148)

!& algeq
      f(174)=-x(166) + x(133) * x(160)

!& switch4											! choice between the levels without bounds
      select case (z( 61))
         case(  1)
            f(175)=x(167)-x(137)
         case(  2)
            f(175)=x(167)-x(165)
         case(  3)
            f(175)=x(167)-x(166)
         case(  4)
            f(175)=x(167)-x(136)
      end select

!& algeq													! checking if the variation should have been greater than the bounds
      f(176)=-x(168) + x(167)+0.0001 - x(130)

!& swsign
      select case (z( 62))
         case(1)
            f(177)=x(169)-x(101)
         case(2)
            f(177)=x(169)-x(100)
      end select

!& algeq													! checking if the variation should have been greater than the bounds
      f(178)=-x(170) - x(167)-0.0001 + x(129)

!& swsign
      select case (z( 63))
         case(1)
            f(179)=x(171)-x(101)
         case(2)
            f(179)=x(171)-x(100)
      end select

!........................................................................................
   case (update_disc)

!& algeq

!& algeq

!& algeq

!& tf1p                          ! voltage measurement

!& max1v1c
      select case (z(  1))
         case(1)
            if(x(  7)>0.01)then
               z(  1)=2
            endif
         case(2)
            if(0.01>x(  7))then
               z(  1)=1
            endif
      end select

!& algeq                         ! maximum current computation

!& algeq

!& limvb
      select case (z(  2))
         case(0)
            if(x( 17)>x( 15))then
               z(  2)=1
            elseif(x( 17)<x( 16))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 17)>x( 16))then
               z(  2)=0
            endif
         case(1)
            if(x( 17)<x( 15))then
               z(  2)=0
            endif
      end select

!& algeq             ! compute status

!& algeq

!& algeq

!& swsign
      select case (z(  3))
         case(1)
            if(x( 94)<0.)then
               z(  3)=2
            endif
         case(2)
            if(x( 94)>=0.)then
               z(  3)=1
            endif
      end select

!& algeq                     ! current computation

!& tf1p2lim
      select case (z(  4))
         case(0)
            if(x(172)>prm( 96)*prm(  4))then
               z(  4)=1
            elseif(x(172)<prm( 89)*prm(  4))then
               z(  4)=-1
            endif
         case(1)
            if(1*x( 11)-x( 31)<prm( 96)*prm(  4))then
               z(  4)= 0
            endif
         case(-1)
            if(1*x( 11)-x( 31)>prm( 89)*prm(  4))then
               z(  4)= 0
            endif
      end select
      select case (z(  5))
         case(0)
            if(x( 31)>prm( 88))then
               z(  5)=1
               eqtyp( 15)=0
            elseif(x( 31)<prm( 89))then
               z(  5)=-1
               eqtyp( 15)=0
            endif
         case(1)
            if (x(172)<0.)then
               z(  5)= 0
               eqtyp( 15)= 31
            endif
         case(-1)
            if(x(172)>0.)then
               z(  5)= 0
               eqtyp( 15)= 31
            endif
      end select

!& algeq                     ! include reactive current priority

!& algeq

!& algeq												! here a Q control could be added instead of Iqext

!& limvb
      select case (z(  6))
         case(0)
            if(x( 30)>x( 13))then
               z(  6)=1
            elseif(x( 30)<x( 14))then
               z(  6)=-1
            endif
         case(-1)
            if(x( 30)>x( 14))then
               z(  6)=0
            endif
         case(1)
            if(x( 30)<x( 13))then
               z(  6)=0
            endif
      end select

!& algeq

!& tf1p

!& algeq

!& db
      select case (z(  7))
         case(0)
            if(x(  7)>prm( 29))then
               z(  7)=1
            elseif(x(  7)<prm( 28))then
               z(  7)=-1
            endif
         case(-1)
            if(x(  7)>prm( 28))then
               z(  7)=0
            endif
         case(1)
            if(x(  7)<prm( 29))then
               z(  7)=0
            endif
      end select

!& algeq               !LVRT

!& timer5
      if(z(  8) == -1)then
         if(x( 33) >= (-prm( 12)))then
            z(  8)=0
            eqtyp( 26)=173
         endif
      else
         if(x( 33) < (-prm( 12)))then
            z(  8)=-1
            eqtyp( 26)=0
         endif
      endif
      if(z(  8) == 0)then
         if(x( 33) > (-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))then
            if(x(173) > 0.)then
               z(  8)=1
            endif
         elseif(x( 33) > (-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))then
            if(x(173) > prm(  6)+(0.-prm(  6))*(x( 33)-(-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))))then
               z(  8)=1
            endif
         elseif(x( 33) > (-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))then
            if(x(173) > prm(  6)+(prm(  6)-prm(  6))*(x( 33)-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))))then
               z(  8)=1
            endif
         elseif(x( 33) > (-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))then
            if(x(173) > prm(  8)+(prm(  6)-prm(  8))*(x( 33)-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))))then
               z(  8)=1
            endif
         elseif(x( 33) > (-prm( 12)))then
            if(x(173) > prm(  7)+(prm(  8)-prm(  7))*(x( 33)-(-prm( 12)))/((-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))-(-prm( 12))))then
               z(  8)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  9) == -1)then
         if(x( 26)>1.1)then
            z(  9)=1
         endif
      else
         if(x( 26)<0.9)then
            z(  9)=-1
         endif
      endif

!& algeq

!& inlim
      if (1>= 0.005)then
         select case (z( 10))
            case(0)
               if(x( 65)<0.)then
                  z( 10)=-1
                  eqtyp( 30)=0
               elseif(x( 65)>5)then
                  z( 10)= 1
                  eqtyp( 30)=0
               endif
            case(1)
               if(x( 64)<0.)then
                  z( 10)=0
                  eqtyp( 30)= 65
               endif
            case(-1)
               if(x( 64)>0.)then
                  z( 10)=0
                  eqtyp( 30)= 65
               endif
         end select
      else
         select case (z( 10))
            case(0)
               if(x( 65)<0.)then
                  z( 10)=-1
               elseif(x( 65)>5)then
                  z( 10)= 1
               endif
            case(1)
               if(x( 64)<5)then
                  z( 10)=0
               endif
            case(-1)
               if(x( 64)>0.)then
                  z( 10)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 11))
         case(1)
            if(x( 70)<0.)then
               z( 11)=2
            endif
         case(2)
            if(x( 70)>=0.)then
               z( 11)=1
            endif
      end select

!& tf1p2lim
      select case (z( 12))
         case(0)
            if(x(174)>prm( 97)*prm( 93))then
               z( 12)=1
            elseif(x(174)<prm( 89)*prm( 93))then
               z( 12)=-1
            endif
         case(1)
            if(1*x( 67)-x( 66)<prm( 97)*prm( 93))then
               z( 12)= 0
            endif
         case(-1)
            if(1*x( 67)-x( 66)>prm( 89)*prm( 93))then
               z( 12)= 0
            endif
      end select
      select case (z( 13))
         case(0)
            if(x( 66)>prm( 88))then
               z( 13)=1
               eqtyp( 36)=0
            elseif(x( 66)<prm( 90))then
               z( 13)=-1
               eqtyp( 36)=0
            endif
         case(1)
            if (x(174)<0.)then
               z( 13)= 0
               eqtyp( 36)= 66
            endif
         case(-1)
            if(x(174)>0.)then
               z( 13)= 0
               eqtyp( 36)= 66
            endif
      end select

!& algeq             ! overvoltage protection

!& pwlin4
      if(x( 34)<(-999))then
         z( 14)=1
      elseif(x( 34)>=999)then
         z( 14)=  3
      elseif((-999)<=x( 34) .and. x( 34)<0.)then
         z( 14)=  1
      elseif(0.<=x( 34) .and. x( 34)<0.)then
         z( 14)=  2
      elseif(0.<=x( 34) .and. x( 34)<999)then
         z( 14)=  3
      endif

!& algeq

!& hyst
      if (z( 15) == -1)then
         if(x( 27)>1.1)then
            z( 15)=1
         endif
      else
         if(x( 27)<0.9)then
            z( 15)=-1
         endif
      endif

!& algeq                     ! reactive current priority during undervoltage

!& algeq

!& algeq

!& swsign
      select case (z( 16))
         case(1)
            if(x( 36)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x( 36)>=0.)then
               z( 16)=1
            endif
      end select

!& algeq             ! switch support on and off

!& algeq

!& swsign            ! switch support off
      select case (z( 17))
         case(1)
            if(x( 97)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x( 97)>=0.)then
               z( 17)=1
            endif
      end select

!& algeq         ! PLL

!& algeq

!& algeq

!& swsign
      select case (z( 18))
         case(1)
            if(x( 62)<0.)then
               z( 18)=2
            endif
         case(2)
            if(x( 62)>=0.)then
               z( 18)=1
            endif
      end select

!& int

!& pictl

!& algeq

!& algeq

!& tf1p

!& tf1p

!& algeq             ! frequency measurement

!& algeq

!& tf1p

!& algeq             ! frequency protection

!& algeq

!& algeq

!& swsign
      select case (z( 19))
         case(1)
            if(x( 53)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x( 53)>=0.)then
               z( 19)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 20))
         case(1)
            if(x( 56)<0.)then
               z( 20)=2
            endif
         case(2)
            if(x( 56)>=0.)then
               z( 20)=1
            endif
      end select

!& hyst
      if (z( 21) == -1)then
         if(x( 46)>1.1)then
            z( 21)=1
         endif
      else
         if(x( 46)<0.9)then
            z( 21)=-1
         endif
      endif

!& hyst
      if (z( 22) == -1)then
         if(x( 48)>1.1)then
            z( 22)=1
         endif
      else
         if(x( 48)<0.9)then
            z( 22)=-1
         endif
      endif

!& algeq             ! old frequency reaction

!& lim
      select case (z( 23))
         case(0)
            if(x( 50)>999)then
               z( 23)=1
            elseif(x( 50)<0)then
               z( 23)=-1
            endif
         case(-1)
            if(x( 50)>0)then
               z( 23)=0
            endif
         case(1)
            if(x( 50)<999)then
               z( 23)=0
            endif
      end select

!& tf1p2lim
      select case (z( 24))
         case(0)
            if(x(176)>prm( 91)*prm( 93))then
               z( 24)=1
            elseif(x(176)<prm( 92)*prm( 93))then
               z( 24)=-1
            endif
         case(1)
            if(1*x( 51)-x( 71)<prm( 91)*prm( 93))then
               z( 24)= 0
            endif
         case(-1)
            if(1*x( 51)-x( 71)>prm( 92)*prm( 93))then
               z( 24)= 0
            endif
      end select
      select case (z( 25))
         case(0)
            if(x( 71)>prm( 91))then
               z( 25)=1
               eqtyp( 75)=0
            elseif(x( 71)<prm( 92))then
               z( 25)=-1
               eqtyp( 75)=0
            endif
         case(1)
            if (x(176)<0.)then
               z( 25)= 0
               eqtyp( 75)= 71
            endif
         case(-1)
            if(x(176)>0.)then
               z( 25)= 0
               eqtyp( 75)= 71
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 26))
         case(1)
            if(x( 72)<0.)then
               z( 26)=2
            endif
         case(2)
            if(x( 72)>=0.)then
               z( 26)=1
            endif
      end select

!& tf1p

!& algeq                 ! droop control

!& db
      select case (z( 27))
         case(0)
            if(x( 77)>prm( 35))then
               z( 27)=1
            elseif(x( 77)<prm( 34))then
               z( 27)=-1
            endif
         case(-1)
            if(x( 77)>prm( 34))then
               z( 27)=0
            endif
         case(1)
            if(x( 77)<prm( 35))then
               z( 27)=0
            endif
      end select

!& algeq

!& algeq

!& lim
      select case (z( 28))
         case(0)
            if(x( 79)>(-0.00001))then
               z( 28)=1
            elseif(x( 79)<(-99999.))then
               z( 28)=-1
            endif
         case(-1)
            if(x( 79)>(-99999.))then
               z( 28)=0
            endif
         case(1)
            if(x( 79)<(-0.00001))then
               z( 28)=0
            endif
      end select

!& lim
      select case (z( 29))
         case(0)
            if(x( 80)>99999.)then
               z( 29)=1
            elseif(x( 80)<0.00001)then
               z( 29)=-1
            endif
         case(-1)
            if(x( 80)>0.00001)then
               z( 29)=0
            endif
         case(1)
            if(x( 80)<99999.)then
               z( 29)=0
            endif
      end select

!& algeq												! here the P control could be placed to give w7

!& algeq

!& swsign            ! switch support off
      select case (z( 30))
         case(1)
            if(x( 97)<0.)then
               z( 30)=2
            endif
         case(2)
            if(x( 97)>=0.)then
               z( 30)=1
            endif
      end select

!& algeq

!& algeq             ! alignment

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq                ! frequency deviation in Hz

!& tfder1p               ! Rocof measurement in Hz/s

!& abs
      if (z( 31) == -1 )then
         if(x( 86)> blocktol1 )then
            z( 31)=1
         endif
      else
         if(x( 86)< - blocktol1 )then
            z( 31)=-1
         endif
      endif

!& algeq                 ! Rocof protection

!& algeq

!& algeq

!& swsign
      select case (z( 32))
         case(1)
            if(x( 90)<0.)then
               z( 32)=2
            endif
         case(2)
            if(x( 90)>=0.)then
               z( 32)=1
            endif
      end select

!& hyst
      if (z( 33) == -1)then
         if(x( 91)>1.1)then
            z( 33)=1
         endif
      else
         if(x( 91)<0.9)then
            z( 33)=-1
         endif
      endif

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation

!& algeq

!& algeq												! NEIGHBOURHOOD CONTROL

!& algeq

!& swsign											! F_help_p_m is 1 if P_n > 0
      select case (z( 34))
         case(1)
            if(x(107)<0.)then
               z( 34)=2
            endif
         case(2)
            if(x(107)>=0.)then
               z( 34)=1
            endif
      end select

!& swsign											! F_help_p_p is 1 if P_n < 0
      select case (z( 35))
         case(1)
            if(x(106)<0.)then
               z( 35)=2
            endif
         case(2)
            if(x(106)>=0.)then
               z( 35)=1
            endif
      end select

!& hyst ! should it be V or Vm?
      if (z( 36) == -1)then
         if(x(  7)>prm( 46))then
            z( 36)=1
         endif
      else
         if(x(  7)<prm( 47))then
            z( 36)=-1
         endif
      endif

!& hyst
      if (z( 37) == -1)then
         if(x(  7)>prm( 46))then
            z( 37)=1
         endif
      else
         if(x(  7)<prm( 47))then
            z( 37)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0

!& abs
      if (z( 38) == -1 )then
         if(x(108)> blocktol1 )then
            z( 38)=1
         endif
      else
         if(x(108)< - blocktol1 )then
            z( 38)=-1
         endif
      endif

!& algeq

!& swsign
      select case (z( 39))
         case(1)
            if(x(110)<0.)then
               z( 39)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 39)=1
            endif
      end select

!& algeq												! priority of voltage over power

!& algeq

!& algeq												! reactive power computation

!& algeq

!& algeq

!& algeq												! active power computation

!& algeq

!& algeq

!& algeq

!& tf1p2lim
      select case (z( 40))
         case(0)
            if(x(178)>prm( 56)*prm( 55))then
               z( 40)=1
            elseif(x(178)<prm( 99)*prm( 55))then
               z( 40)=-1
            endif
         case(1)
            if(1*x(102)-x(105)<prm( 56)*prm( 55))then
               z( 40)= 0
            endif
         case(-1)
            if(1*x(102)-x(105)>prm( 99)*prm( 55))then
               z( 40)= 0
            endif
      end select
      select case (z( 41))
         case(0)
            if(x(105)>prm( 88))then
               z( 41)=1
               eqtyp(128)=0
            elseif(x(105)<prm( 89))then
               z( 41)=-1
               eqtyp(128)=0
            endif
         case(1)
            if (x(178)<0.)then
               z( 41)= 0
               eqtyp(128)=105
            endif
         case(-1)
            if(x(178)>0.)then
               z( 41)= 0
               eqtyp(128)=105
            endif
      end select

!& algeq

!& tf1p2lim
      select case (z( 42))
         case(0)
            if(x(179)>prm( 57)*prm( 55))then
               z( 42)=1
            elseif(x(179)<prm(100)*prm( 55))then
               z( 42)=-1
            endif
         case(1)
            if(1*x(103)-x(104)<prm( 57)*prm( 55))then
               z( 42)= 0
            endif
         case(-1)
            if(1*x(103)-x(104)>prm(100)*prm( 55))then
               z( 42)= 0
            endif
      end select
      select case (z( 43))
         case(0)
            if(x(104)>prm( 88))then
               z( 43)=1
               eqtyp(131)=0
            elseif(x(104)<prm( 89))then
               z( 43)=-1
               eqtyp(131)=0
            endif
         case(1)
            if (x(179)<0.)then
               z( 43)= 0
               eqtyp(131)=104
            endif
         case(-1)
            if(x(179)>0.)then
               z( 43)= 0
               eqtyp(131)=104
            endif
      end select

!& algeq

!& algeq												! CENTRAL CONTROL, positive and negative variation

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 44))
         case(1)
            if(x(131)<0.)then
               z( 44)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 44)=1
            endif
      end select

!& swsign
      select case (z( 45))
         case(1)
            if(x(131)<0.)then
               z( 45)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 45)=1
            endif
      end select

!& swsign
      select case (z( 46))
         case(1)
            if(x(131)<0.)then
               z( 46)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 46)=1
            endif
      end select

!& limvb												! limiting the variation to the max and min bounds
      select case (z( 47))
         case(0)
            if(x(132)>x(129))then
               z( 47)=1
            elseif(x(132)<x(130))then
               z( 47)=-1
            endif
         case(-1)
            if(x(132)>x(130))then
               z( 47)=0
            endif
         case(1)
            if(x(132)<x(129))then
               z( 47)=0
            endif
      end select

!& limvb
      select case (z( 48))
         case(0)
            if(x(133)>x(129))then
               z( 48)=1
            elseif(x(133)<x(130))then
               z( 48)=-1
            endif
         case(-1)
            if(x(133)>x(130))then
               z( 48)=0
            endif
         case(1)
            if(x(133)<x(129))then
               z( 48)=0
            endif
      end select

!& algeq !dPc_0 is equal to 0

!& algeq												! level 1 low negative

!& swsign
      select case (z( 49))
         case(1)
            if(x(138)<0.)then
               z( 49)=2
            endif
         case(2)
            if(x(138)>=0.)then
               z( 49)=1
            endif
      end select

!& algeq												! level 1 high negative

!& swsign
      select case (z( 50))
         case(1)
            if(x(140)<0.)then
               z( 50)=2
            endif
         case(2)
            if(x(140)>=0.)then
               z( 50)=1
            endif
      end select

!& algeq												! level 1 negative flag

!& algeq												! level 1 low positive

!& swsign
      select case (z( 51))
         case(1)
            if(x(143)<0.)then
               z( 51)=2
            endif
         case(2)
            if(x(143)>=0.)then
               z( 51)=1
            endif
      end select

!& algeq												! level 1 high positive

!& swsign
      select case (z( 52))
         case(1)
            if(x(145)<0.)then
               z( 52)=2
            endif
         case(2)
            if(x(145)>=0.)then
               z( 52)=1
            endif
      end select

!& algeq												! level 1 positive flag

!& swsign
      select case (z( 53))
         case(1)
            if(x(131)<0.)then
               z( 53)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 53)=1
            endif
      end select

!& algeq

!& algeq												! level 2 low negative

!& swsign
      select case (z( 54))
         case(1)
            if(x(150)<0.)then
               z( 54)=2
            endif
         case(2)
            if(x(150)>=0.)then
               z( 54)=1
            endif
      end select

!& algeq												! level 2 high negative

!& swsign
      select case (z( 55))
         case(1)
            if(x(152)<0.)then
               z( 55)=2
            endif
         case(2)
            if(x(152)>=0.)then
               z( 55)=1
            endif
      end select

!& algeq												! level 2 negative flag

!& algeq												! level 2 low positive

!& swsign
      select case (z( 56))
         case(1)
            if(x(155)<0.)then
               z( 56)=2
            endif
         case(2)
            if(x(155)>=0.)then
               z( 56)=1
            endif
      end select

!& algeq												! level 2 high positive

!& swsign
      select case (z( 57))
         case(1)
            if(x(157)<0.)then
               z( 57)=2
            endif
         case(2)
            if(x(157)>=0.)then
               z( 57)=1
            endif
      end select

!& algeq												! level 2 positive flag

!& swsign
      select case (z( 58))
         case(1)
            if(x(131)<0.)then
               z( 58)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 58)=1
            endif
      end select

!& algeq

!& abs												! absolute of the level
      if (z( 59) == -1 )then
         if(x(131)> blocktol1 )then
            z( 59)=1
         endif
      else
         if(x(131)< - blocktol1 )then
            z( 59)=-1
         endif
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1

!& switch4											! choice between the levels
      z( 60)=max(1,min(  4,nint(x(163))))

!& algeq

!& algeq

!& switch4											! choice between the levels without bounds
      z( 61)=max(1,min(  4,nint(x(163))))

!& algeq													! checking if the variation should have been greater than the bounds

!& swsign
      select case (z( 62))
         case(1)
            if(x(168)<0.)then
               z( 62)=2
            endif
         case(2)
            if(x(168)>=0.)then
               z( 62)=1
            endif
      end select

!& algeq													! checking if the variation should have been greater than the bounds

!& swsign
      select case (z( 63))
         case(1)
            if(x(170)<0.)then
               z( 63)=2
            endif
         case(2)
            if(x(170)>=0.)then
               z( 63)=1
            endif
      end select
   end select

end subroutine inj_IBGtesting2
