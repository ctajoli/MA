!  MODEL NAME : inj_ATLtestNB           
!  MODEL DESCRIPTION FILE : ATLtestNB.txt
!  Data :
!       prm(  1)=  Sb                              ! base power of the unit
!       prm(  2)=  Sbs                             ! base power of the system
!       prm(  3)=  vdc_star                        ! DC link voltage reference
!       prm(  4)=  pf                              ! setpoint for power factor at the terminal
!       prm(  5)=  tau                             ! Pll time constant
!       prm(  6)=  Vminpll                         ! PLL freezing voltage, PLL freezes below this value
!       prm(  7)=  tau_f                           ! filter constant for frequency
!       prm(  8)=  kp_v                            ! DC link voltage control parameters
!       prm(  9)=  ki_v
!       prm( 10)=  kp_c                            ! rectifier current control parameters
!       prm( 11)=  ki_c
!       prm( 12)=  kp_p                            ! power control parameters
!       prm( 13)=  ki_p
!       prm( 14)=  kp_w                            ! motor speed control parameters
!       prm( 15)=  ki_w
!       prm( 16)=  w_cc                            ! motor current control bandwidth
!       prm( 17)=  rt                              ! terminal impedance
!       prm( 18)=  lt
!       prm( 19)=  cdc                             ! DC link capacitance
!       prm( 20)=  kw                              ! speed/torque control constant
!       prm( 21)=  kT                              ! motor torque constant
!       prm( 22)=  ra                              ! motor anchor/stator resistance
!       prm( 23)=  H                               ! motor inertia
!       prm( 24)=  b                               ! motor friction coefficient
!       prm( 25)=  Tnm                             ! compressor torque at nominal speed
!       prm( 26)=  iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
!       prm( 27)=  wm_min                          ! limits on rotational speed
!       prm( 28)=  wm_max
!       prm( 29)=  Vmax                            ! voltage range during which unit needs to stay connected
!       prm( 30)=  Vmin
!       prm( 31)=  Vint
!       prm( 32)=  Vr
!       prm( 33)=  tLVRT1
!       prm( 34)=  tLVRT2
!       prm( 35)=  tLVRTint
!       prm( 36)=  Vtrip
!       prm( 37)=  fmin                            ! frequency control regime
!       prm( 38)=  fmax
!       prm( 39)=  Trocof                          ! delay for ROCOF measurement
!       prm( 40)=  dfmax                           ! maximum permissable ROCOF
!       prm( 41)=  VPmax                           ! power limit above 0.93 pu voltage
!       prm( 42)=  LVRT                            ! enable or disable LVRT
!       prm( 43)=  Tm                              ! measurement delay
!       prm( 44)=  protection                      ! Flag for protection, -1 for off, 1 for on
!       prm( 45)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 46)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 47)=  V_n                             ! number of neighbours at voltage limit
!       prm( 48)=  P_n                             ! number of neighbours at power limit
!       prm( 49)=  Vdb_p                           ! higher voltage deadband value
!       prm( 50)=  Vdb_m                           ! lower voltage deadband value
!       prm( 51)=  ro_v                            ! neighbourhood factor for voltage
!       prm( 52)=  ro_p                            ! neighbourhood factor for power
!       prm( 53)=  Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
!       prm( 54)=  Q_min                           ! min reactive power of the unit in pu, IT WIL
!       prm( 55)=  s_f                             ! security factor for unit limit control
!       prm( 56)=  lvl                             ! level of emergency of the central controller
!       prm( 57)=  pl1_3_0                        ! flags that varies for the different units
!       prm( 58)=  pl1_4_0
!       prm( 59)=  F_help_high_0
!       prm( 60)=  F_help_low_0
!  Parameters :
!       prm( 61)=  w0  
!       prm( 62)=  wb  
!       prm( 63)=  t2   Torque polynomial parameters, set to constant torque
!       prm( 64)=  t1  
!       prm( 65)=  t0  
!       prm( 66)=  Downlim  
!       prm( 67)=  Downlimdisc  
!       prm( 68)=  Uplimdis  
!       prm( 69)=  downlimdis  
!       prm( 70)=  theta0  
!       prm( 71)=  P0   initial active power
!       prm( 72)=  Q0   initial reactive power
!       prm( 73)=  P0_unit  
!       prm( 74)=  Q0_unit  
!       prm( 75)=  V0   initial voltage magnitude at bus
!       prm( 76)=  iP0   current alignment
!       prm( 77)=  iQ0  
!       prm( 78)=  vd0   voltage alignment
!       prm( 79)=  vq0  
!       prm( 80)=  md0   modulation indices
!       prm( 81)=  mq0  
!       prm( 82)=  idc0   DC link current
!       prm( 83)=  iT0  
!       prm( 84)=  wm0  
!       prm( 85)=  Te0  
!       prm( 86)=  Tc0  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vd                    
!       x(  4)=  vq                    
!       x(  5)=  theta                 
!       x(  6)=  vdc_ref               
!       x(  7)=  V                     
!       x(  8)=  vxm                   
!       x(  9)=  vym                   
!       x( 10)=  Vm                    
!       x( 11)=  iP                    
!       x( 12)=  diP                   
!       x( 13)=  iQ                    
!       x( 14)=  diQ                   
!       x( 15)=  P                     
!       x( 16)=  dp                    
!       x( 17)=  Punit                 
!       x( 18)=  Q                     
!       x( 19)=  Qunit                 
!       x( 20)=  Pref_lim               limited P reference
!       x( 21)=  dw_pll                
!       x( 22)=  dw_pllf               
!       x( 23)=  deltaf                
!       x( 24)=  w_pll                 
!       x( 25)=  f                     
!       x( 26)=  fi                    
!       x( 27)=  vdc                   
!       x( 28)=  dvdc                  
!       x( 29)=  iP_ref                
!       x( 30)=  Plim                  
!       x( 31)=  iQ_ref                
!       x( 32)=  mdr                   
!       x( 33)=  mqr                   
!       x( 34)=  md                    
!       x( 35)=  mq                    
!       x( 36)=  dvd                   
!       x( 37)=  dvq                   
!       x( 38)=  idc                   
!       x( 39)=  didc                  
!       x( 40)=  wm_ref                
!       x( 41)=  wm_ref_lim            
!       x( 42)=  iT_ref                
!       x( 43)=  iT                    
!       x( 44)=  dwm                   
!       x( 45)=  wm                    
!       x( 46)=  Te                    
!       x( 47)=  dT                    
!       x( 48)=  F_pll                 
!       x( 49)=  dv_pll                
!       x( 50)=  PLLmulta              
!       x( 51)=  PLLmultb              
!       x( 52)=  deltafvh              
!       x( 53)=  z1                    
!       x( 54)=  Fvhi                  
!       x( 55)=  Fvh                   
!       x( 56)=  iPs                   
!       x( 57)=  iQs                   
!       x( 58)=  Pref                  
!       x( 59)=  x10                   
!       x( 60)=  z                     
!       x( 61)=  Fvl                   
!       x( 62)=  Fvli                  
!       x( 63)=  deltafl               
!       x( 64)=  flagla                
!       x( 65)=  flaglb                
!       x( 66)=  Ffl                   
!       x( 67)=  Ffli                  
!       x( 68)=  deltafh               
!       x( 69)=  flagha                
!       x( 70)=  flaghb                
!       x( 71)=  Ffh                   
!       x( 72)=  Ffhi                  
!       x( 73)=  rocof                 
!       x( 74)=  abrocof               
!       x( 75)=  flagra                
!       x( 76)=  flagrb                
!       x( 77)=  deltarocof            
!       x( 78)=  Ffri                  
!       x( 79)=  Ffr                   
!       x( 80)=  Plim_min              
!       x( 81)=  status                 status of the device, 1 for on
!       x( 82)=  p1                    
!       x( 83)=  p2                    
!       x( 84)=  p3                    
!       x( 85)=  Fpl1                   placeholder for value 1 in switch block
!       x( 86)=  Fpl0                   placeholder for value 0 in switch block
!       x( 87)=  V_lim_min              unit controller states
!       x( 88)=  V_lim_max             
!       x( 89)=  P_lim_min             
!       x( 90)=  P_lim_max             
!       x( 91)=  pl1                   
!       x( 92)=  F_v_min_in            
!       x( 93)=  pl2                   
!       x( 94)=  F_v_max_in            
!       x( 95)=  F_v_opp               
!       x( 96)=  pl3                   
!       x( 97)=  F_p_min_in            
!       x( 98)=  pl4                   
!       x( 99)=  F_p_max_in            
!       x(100)=  F_v_min               
!       x(101)=  F_v_max               
!       x(102)=  F_p_min               
!       x(103)=  F_p_max               
!       x(104)=  F_lim                 
!       x(105)=  dP_lim                
!       x(106)=  dQ_lim                
!       x(107)=  dQ_sum                
!       x(108)=  dQ                    
!       x(109)=  dP_sum                
!       x(110)=  P_n_var                neighbourhood controller states
!       x(111)=  V_n_var               
!       x(112)=  V_n_abs               
!       x(113)=  pl5                   
!       x(114)=  F_help_v              
!       x(115)=  F_help_p_p            
!       x(116)=  F_help_p_m            
!       x(117)=  F_hlp_high            
!       x(118)=  F_hlp_low             
!       x(119)=  F_hlp_p_p2            
!       x(120)=  F_hlp_p_m2            
!       x(121)=  dQp                   
!       x(122)=  dQm                   
!       x(123)=  dQnb                  
!       x(124)=  dPp                   
!       x(125)=  dPm                   
!       x(126)=  dPnb                  

!.........................................................................................................

subroutine inj_ATLtestNB(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 60
      nbaddpar= 26
      parname(  1)='Sb'
      parname(  2)='Sbs'
      parname(  3)='vdc_star'
      parname(  4)='pf'
      parname(  5)='tau'
      parname(  6)='Vminpll'
      parname(  7)='tau_f'
      parname(  8)='kp_v'
      parname(  9)='ki_v'
      parname( 10)='kp_c'
      parname( 11)='ki_c'
      parname( 12)='kp_p'
      parname( 13)='ki_p'
      parname( 14)='kp_w'
      parname( 15)='ki_w'
      parname( 16)='w_cc'
      parname( 17)='rt'
      parname( 18)='lt'
      parname( 19)='cdc'
      parname( 20)='kw'
      parname( 21)='kT'
      parname( 22)='ra'
      parname( 23)='H'
      parname( 24)='b'
      parname( 25)='Tnm'
      parname( 26)='iF'
      parname( 27)='wm_min'
      parname( 28)='wm_max'
      parname( 29)='Vmax'
      parname( 30)='Vmin'
      parname( 31)='Vint'
      parname( 32)='Vr'
      parname( 33)='tLVRT1'
      parname( 34)='tLVRT2'
      parname( 35)='tLVRTint'
      parname( 36)='Vtrip'
      parname( 37)='fmin'
      parname( 38)='fmax'
      parname( 39)='Trocof'
      parname( 40)='dfmax'
      parname( 41)='VPmax'
      parname( 42)='LVRT'
      parname( 43)='Tm'
      parname( 44)='protection'
      parname( 45)='P_max'
      parname( 46)='P_min'
      parname( 47)='V_n'
      parname( 48)='P_n'
      parname( 49)='Vdb_p'
      parname( 50)='Vdb_m'
      parname( 51)='ro_v'
      parname( 52)='ro_p'
      parname( 53)='Q_max'
      parname( 54)='Q_min'
      parname( 55)='s_f'
      parname( 56)='lvl'
      parname( 57)='pl1_3_0'
      parname( 58)='pl1_4_0'
      parname( 59)='F_help_high_0'
      parname( 60)='F_help_low_0'
      parname( 61)='w0'
      parname( 62)='wb'
      parname( 63)='t2'
      parname( 64)='t1'
      parname( 65)='t0'
      parname( 66)='Downlim'
      parname( 67)='Downlimdisc'
      parname( 68)='Uplimdis'
      parname( 69)='downlimdis'
      parname( 70)='theta0'
      parname( 71)='P0'
      parname( 72)='Q0'
      parname( 73)='P0_unit'
      parname( 74)='Q0_unit'
      parname( 75)='V0'
      parname( 76)='iP0'
      parname( 77)='iQ0'
      parname( 78)='vd0'
      parname( 79)='vq0'
      parname( 80)='md0'
      parname( 81)='mq0'
      parname( 82)='idc0'
      parname( 83)='iT0'
      parname( 84)='wm0'
      parname( 85)='Te0'
      parname( 86)='Tc0'
      adix=  1
      adiy=  2
      nbxvar=134
      nbzvar= 23

!........................................................................................
   case (define_obs)
      nbobs= 25
      obsname(  1)='iP'
      obsname(  2)='iQ'
      obsname(  3)='vd'
      obsname(  4)='vq'
      obsname(  5)='P'
      obsname(  6)='Punit'
      obsname(  7)='Pref'
      obsname(  8)='Q'
      obsname(  9)='f'
      obsname( 10)='wm'
      obsname( 11)='Vm'
      obsname( 12)='Pref_lim'
      obsname( 13)='wm_ref_lim'
      obsname( 14)='status'
      obsname( 15)='rocof'
      obsname( 16)='F_v_min'
      obsname( 17)='F_v_max'
      obsname( 18)='F_p_min'
      obsname( 19)='F_p_max'
      obsname( 20)='F_hlp_high'
      obsname( 21)='F_hlp_low'
      obsname( 22)='F_hlp_p_p2'
      obsname( 23)='F_hlp_p_m2'
      obsname( 24)='dPnb'
      obsname( 25)='dQnb'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x( 11)              
      obs(  2)=x( 13)              
      obs(  3)=x(  3)              
      obs(  4)=x(  4)              
      obs(  5)=x( 15)              
      obs(  6)=x( 17)              
      obs(  7)=x( 58)              
      obs(  8)=x( 18)              
      obs(  9)=x( 25)              
      obs( 10)=x( 45)              
      obs( 11)=x( 10)              
      obs( 12)=x( 20)              
      obs( 13)=x( 41)              
      obs( 14)=x( 81)              
      obs( 15)=x( 73)              
      obs( 16)=x(100)              
      obs( 17)=x(101)              
      obs( 18)=x(102)              
      obs( 19)=x(103)              
      obs( 20)=x(117)              
      obs( 21)=x(118)              
      obs( 22)=x(119)              
      obs( 23)=x(120)              
      obs( 24)=x(126)              
      obs( 25)=x(123)              

!........................................................................................
   case (initialize)

!w0 = 1
      prm( 61)= 1

!wb = 2*pi*fnom
      prm( 62)= 2*pi*fnom

!t2 = 0
      prm( 63)= 0

!t1 = 0
      prm( 64)= 0

!t0 = 1
      prm( 65)= 1

!Downlim = -9999
      prm( 66)= -9999

!Downlimdisc = 0
      prm( 67)= 0

!Uplimdis = 0
      prm( 68)= 0

!downlimdis = -9999
      prm( 69)= -9999

!theta0 = atan([vy]/[vx])
      prm( 70)= atan(vy/vx)

!P0 = ([vx]*[ix]+[vy]*[iy])
      prm( 71)= (vx*ix+vy*iy)

!Q0 = [vy]*[ix]-[vx]*[iy]
      prm( 72)= vy*ix-vx*iy

!P0_unit = -{P0}*{Sbs}/{Sb}
      prm( 73)= -prm( 71)*prm(  2)/prm(  1)

!Q0_unit = -{Q0}*{Sbs}/{Sb}
      prm( 74)= -prm( 72)*prm(  2)/prm(  1)

!V0 = dsqrt([vx]**2+[vy]**2)
      prm( 75)= dsqrt(vx**2+vy**2)

!iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}
      prm( 76)=  (-ix*cos(prm( 70))-iy*sin(prm( 70)))*prm(  2)/prm(  1)

!iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
      prm( 77)=  (-ix*sin(prm( 70))+iy*cos(prm( 70)))*prm(  2)/prm(  1)

!vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})
      prm( 78)= vx*cos(prm( 70))+vy*sin(prm( 70))

!vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
      prm( 79)= vx*sin(prm( 70))-vy*cos(prm( 70))

!md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})
      prm( 80)= 1/prm(  3)*(prm( 78)+prm( 18)*prm( 61)*prm( 77)-prm( 17)*prm( 76))

!mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
      prm( 81)= 1/prm(  3)*(prm( 79)-prm( 18)*prm( 61)*prm( 76)-prm( 17)*prm( 77))

!idc0 = {md0}*{iP0}+{mq0}*{iQ0}
      prm( 82)= prm( 80)*prm( 76)+prm( 81)*prm( 77)

!iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
      prm( 83)= prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)) + dsqrt((prm( 25)*prm( 21)/(2*prm( 21)**2+2*prm( 22)*prm( 24)))**2-(prm( 22)*prm( 24)*prm( 26)**2-prm( 24)*prm( 82)*prm(  3))/(prm( 21)**2+prm( 22)*prm( 24)))

!wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
      prm( 84)= 1/prm( 24)*(prm( 21)*prm( 83)-prm( 25))

!Te0 = {iT0}*{kT}
      prm( 85)= prm( 83)*prm( 21)

!Tc0 = {Tnm}
      prm( 86)= prm( 25)

!vd =  {vd0}
      x(  3)= prm( 78)

!vq =  {vq0}
      x(  4)= prm( 79)

!theta =  {theta0}
      x(  5)= prm( 70)

!vdc_ref = {vdc_star}
      x(  6)=prm(  3)

!V =  {V0}
      x(  7)= prm( 75)

!vxm =  [vx]
      x(  8)= vx

!vym =  [vy]
      x(  9)= vy

!Vm =  {V0}
      x( 10)= prm( 75)

!iP =   {iP0}
      x( 11)=  prm( 76)

!diP =  0
      x( 12)= 0

!iQ =   {iQ0}
      x( 13)=  prm( 77)

!diQ =  0
      x( 14)= 0

!P =  {P0}
      x( 15)= prm( 71)

!dp =  0
      x( 16)= 0

!Punit =  {P0_unit}
      x( 17)= prm( 73)

!Q =  {Q0}
      x( 18)= prm( 72)

!Qunit =  {Q0_unit}
      x( 19)= prm( 74)

!Pref_lim =  {P0_unit}
      x( 20)= prm( 73)

!dw_pll =  0
      x( 21)= 0

!dw_pllf =  0
      x( 22)= 0

!deltaf =  0
      x( 23)= 0

!w_pll =  {wb}
      x( 24)= prm( 62)

!f =  fnom
      x( 25)= fnom

!fi =  fnom
      x( 26)= fnom

!vdc =  {vdc_star}
      x( 27)= prm(  3)

!dvdc =  0
      x( 28)= 0

!iP_ref =  {iP0}
      x( 29)= prm( 76)

!Plim =  {VPmax}
      x( 30)= prm( 41)

!iQ_ref =  {iQ0}
      x( 31)= prm( 77)

!mdr =  {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
      x( 32)= prm( 18)/prm(  3)*prm( 77)*prm( 61)-prm( 80)

!mqr =  -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
      x( 33)= -prm( 18)/prm(  3)*prm( 76)*prm( 61)-prm( 81)

!md =  {md0}
      x( 34)= prm( 80)

!mq =  {mq0}
      x( 35)= prm( 81)

!dvd =  {iP0}*{rt}
      x( 36)= prm( 76)*prm( 17)

!dvq =  {iQ0}*{rt}
      x( 37)= prm( 77)*prm( 17)

!idc =  {idc0}
      x( 38)= prm( 82)

!didc =  {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
      x( 39)= prm( 80)*prm( 76)+prm( 81)*prm( 77)-prm( 82)

!wm_ref =  {wm0}
      x( 40)= prm( 84)

!wm_ref_lim =  {wm0}
      x( 41)= prm( 84)

!iT_ref =  {iT0}
      x( 42)= prm( 83)

!iT =  {iT0}
      x( 43)= prm( 83)

!dwm =  0
      x( 44)= 0

!wm =  {wm0}
      x( 45)= prm( 84)

!Te =  {kT}*{iT0}
      x( 46)= prm( 21)*prm( 83)

!dT =  {kT}*{iT0}-{Tc0}
      x( 47)= prm( 21)*prm( 83)-prm( 86)

!F_pll =  1
      x( 48)= 1

!dv_pll =  {V0}-{Vminpll}
      x( 49)= prm( 75)-prm(  6)

!PLLmulta =  1
      x( 50)= 1

!PLLmultb =  0
      x( 51)= 0

!deltafvh =   {V0}-{Vmax}
      x( 52)=  prm( 75)-prm( 29)

!z1 =  0
      x( 53)= 0

!Fvhi =  1
      x( 54)= 1

!Fvh =  1
      x( 55)= 1

!iPs =  {iP0}
      x( 56)= prm( 76)

!iQs =  {iQ0}
      x( 57)= prm( 77)

!Pref =  {P0_unit}
      x( 58)= prm( 73)

!x10 =  -{V0}
      x( 59)= -prm( 75)

!z = 0
      x( 60)=0

!Fvl =  1
      x( 61)= 1

!Fvli =  1
      x( 62)= 1

!deltafl =  [f] - {fmin}
      x( 63)= x( 25) - prm( 37)

!flagla =  1
      x( 64)= 1

!flaglb =  0
      x( 65)= 0

!Ffl =  1
      x( 66)= 1

!Ffli =  1
      x( 67)= 1

!deltafh =  {fmax} - [f]
      x( 68)= prm( 38) - x( 25)

!flagha =  1
      x( 69)= 1

!flaghb =  0
      x( 70)= 0

!Ffh =  1
      x( 71)= 1

!Ffhi =  1
      x( 72)= 1

!rocof =  0
      x( 73)= 0

!abrocof =  0
      x( 74)= 0

!flagra =  1
      x( 75)= 1

!flagrb =  0
      x( 76)= 0

!deltarocof =  {dfmax} - 0
      x( 77)= prm( 40) - 0

!Ffri =  1
      x( 78)= 1

!Ffr =  1
      x( 79)= 1

!Plim_min =  0
      x( 80)= 0

!status =  1
      x( 81)= 1

!p1 =  {protection}
      x( 82)= prm( 44)

!p2 =  1
      x( 83)= 1

!p3 =  1
      x( 84)= 1

!Fpl1 =  1
      x( 85)= 1

!Fpl0 =  0
      x( 86)= 0

!V_lim_min =  {Vmin} * {s_f}
      x( 87)= prm( 30) * prm( 55)

!V_lim_max =  {Vmax} / {s_f}
      x( 88)= prm( 29) / prm( 55)

!P_lim_min =  {P_min} * {s_f}
      x( 89)= prm( 46) * prm( 55)

!P_lim_max =  {P_max} / {s_f}
      x( 90)= prm( 45) / prm( 55)

!pl1 =  {Vmin} * {s_f} - {V0}
      x( 91)= prm( 30) * prm( 55) - prm( 75)

!F_v_min_in =  0
      x( 92)= 0

!pl2 =  {V0} - {Vmax} / {s_f}
      x( 93)= prm( 75) - prm( 29) / prm( 55)

!F_v_max_in =  0
      x( 94)= 0

!F_v_opp =  1
      x( 95)= 1

!pl3 =  {P_min} * {s_f} - {P0_unit}
      x( 96)= prm( 46) * prm( 55) - prm( 73)

!F_p_min_in =  0
      x( 97)= 0

!pl4 =  {P0_unit} - {P_max} / {s_f}
      x( 98)= prm( 73) - prm( 45) / prm( 55)

!F_p_max_in =  0
      x( 99)= 0

!F_v_min =  0
      x(100)= 0

!F_v_max =  0
      x(101)= 0

!F_p_min =  0
      x(102)= 0

!F_p_max =  0
      x(103)= 0

!F_lim =  0
      x(104)= 0

!dP_lim =  0
      x(105)= 0

!dQ_lim =  0
      x(106)= 0

!dQ_sum =  0
      x(107)= 0

!dQ =  -{Q0_unit}
      x(108)= -prm( 74)

!dP_sum =  0
      x(109)= 0

!P_n_var =  {P_n}
      x(110)= prm( 48)

!V_n_var =  {V_n}
      x(111)= prm( 47)

!V_n_abs =  {V_n}
      x(112)= prm( 47)

!pl5 =  {V_n} - 1
      x(113)= prm( 47) - 1

!F_help_v =  0
      x(114)= 0

!F_help_p_p =  1
      x(115)= 1

!F_help_p_m =  0
      x(116)= 0

!F_hlp_high =  {F_help_high_0}
      x(117)= prm( 59)

!F_hlp_low =  {F_help_low_0}
      x(118)= prm( 60)

!F_hlp_p_p2 =  [F_help_p_p] * (1-[F_help_v])
      x(119)= x(115) * (1-x(114))

!F_hlp_p_m2 =  [F_help_p_m] * (1-[F_help_v])
      x(120)= x(116) * (1-x(114))

!dQp =  0
      x(121)= 0

!dQm =  0
      x(122)= 0

!dQnb =  0
      x(123)= 0

!dPp =  0
      x(124)= 0

!dPm =  0
      x(125)= 0

!dPnb =  0
      x(126)= 0

!& algeq                                             ! voltage magnitude
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)= 10
      tc(  2)=prm( 43)

!& tf1p
      eqtyp(  3)=  8
      tc(  3)=prm( 43)

!& tf1p
      eqtyp(  4)=  9
      tc(  4)=prm( 43)

!& algeq                                             ! voltage alignment
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& algeq                                             ! compute ix
      eqtyp(  7)=0

!& algeq                                             ! compute iy
      eqtyp(  8)=0

!& algeq
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq                                             ! compute powers
      eqtyp( 11)=0

!& algeq
      eqtyp( 12)=0

!& algeq                                             ! compute powers
      eqtyp( 13)=0

!& algeq
      eqtyp( 14)=0

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         eqtyp( 15)=0
      else
         eqtyp( 15)=  5
         tc( 15)=1.d0
      endif

!& pictl                                             ! PLL
      eqtyp( 16)=127
      x(127)=x( 24)
      eqtyp( 17)=0

!& algeq                                             ! frequency deviation
      eqtyp( 18)=0

!& algeq
      eqtyp( 19)=0

!& algeq                                             ! compute and filter frequency
      eqtyp( 20)=0

!& tf1p
      eqtyp( 21)= 25
      tc( 21)=prm(  7)

!& algeq                                             ! PLL freezing
      eqtyp( 22)=0

!& algeq
      eqtyp( 23)=0

!& algeq
      eqtyp( 24)=0

!& swsign
      eqtyp( 25)=0
      if(x( 49)>=0.)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq
      eqtyp( 26)=0

!& algeq                                             ! DC voltage control
      eqtyp( 27)=0

!& pictl
      eqtyp( 28)=128
      x(128)=x( 29)
      eqtyp( 29)=0

!& algeq                                             ! p-axis current control
      eqtyp( 30)=0

!& pictl
      eqtyp( 31)=129
      x(129)=x( 32)
      eqtyp( 32)=0

!& algeq                                             ! q-axis current control
      eqtyp( 33)=0

!& algeq
      eqtyp( 34)=0

!& pictl
      eqtyp( 35)=130
      x(130)=x( 33)
      eqtyp( 36)=0

!& algeq                                             ! md
      eqtyp( 37)=0

!& algeq                                             ! mq
      eqtyp( 38)=0

!& algeq                                             ! voltage over d-axis terminal impedance
      eqtyp( 39)=0

!& algeq                                             ! voltage over q-axis terminal impedance
      eqtyp( 40)=0

!& tf1p                                              ! d-axis current
      eqtyp( 41)= 11
      tc( 41)=prm( 18)/(prm( 62)*prm( 17))

!& tf1p                                              ! q-axis current
      eqtyp( 42)= 13
      tc( 42)=prm( 18)/(prm( 62)*prm( 17))

!& algeq                                             ! DC link current
      eqtyp( 43)=0

!& algeq                                             ! DC link
      eqtyp( 44)=0

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 62)< 0.005)then
         eqtyp( 45)=0
      else
         eqtyp( 45)= 27
         tc( 45)=prm( 19)/prm( 62)
      endif

!& limvb                                              ! power limiter
      eqtyp( 46)=0
      if(x( 58)>x( 30))then
         z(  2)=1
      elseif(x( 58)<x( 80))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq                                             ! power mismatch
      eqtyp( 47)=0

!& pictl                                             ! power control
      eqtyp( 48)=131
      x(131)=x( 40)
      eqtyp( 49)=0

!& lim                                               ! limit speed control input
      eqtyp( 50)=0
      if(x( 40)>prm( 28))then
         z(  3)=1
      elseif(x( 40)<prm( 27))then
         z(  3)=-1
      else
         z(  3)=0
      endif

!& algeq                                             ! speed control input
      eqtyp( 51)=0

!& pictl                                             ! speed control
      eqtyp( 52)=132
      x(132)=x( 42)
      eqtyp( 53)=0

!& tf1p                                              ! motor current control
      eqtyp( 54)= 43
      tc( 54)=1/prm( 16)

!& algeq                                             ! torque equations
      eqtyp( 55)=0

!& algeq
      eqtyp( 56)=0

!& tf1p                                              ! motor inertia
      eqtyp( 57)= 45
      tc( 57)=2*prm( 23)/prm( 24)

!& algeq
      eqtyp( 58)=0

!& pwlin4                                            ! overvoltage Protection
      eqtyp( 59)=0
      if(x( 52)<(-999))then
         z(  4)=1
      elseif(x( 52)>=999)then
         z(  4)=   3
      elseif((-999)<=x( 52) .and. x( 52)<0.)then
         z(  4)=  1
      elseif(0.<=x( 52) .and. x( 52)<0.)then
         z(  4)=  2
      elseif(0.<=x( 52) .and. x( 52)<999)then
         z(  4)=  3
      endif

!& algeq
      eqtyp( 60)=0

!& hyst
      eqtyp( 61)=0
      if(x( 54)>1.1)then
         z(  5)=1
      elseif(x( 54)<0.9)then
         z(  5)=-1
      else
         if(1.>= 0.)then
            z(  5)=1
         else
            z(  5)=-1
         endif
      endif

!& algeq                                             ! LVRT
      eqtyp( 62)=0

!& timer5
      eqtyp( 63)=0
      eqtyp( 64)=0
      z(  6)=-1
      x(133)=0.

!& algeq
      eqtyp( 65)=0

!& hyst
      eqtyp( 66)=0
      if(x( 62)>1.1)then
         z(  7)=1
      elseif(x( 62)<0.9)then
         z(  7)=-1
      else
         if(1.>= 0.)then
            z(  7)=1
         else
            z(  7)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      eqtyp( 67)=0

!& algeq
      eqtyp( 68)=0

!& algeq
      eqtyp( 69)=0

!& swsign
      eqtyp( 70)=0
      if(x( 82)>=0.)then
         z(  8)=1
      else
         z(  8)=2
      endif

!& algeq                                             ! underfrequency protection
      eqtyp( 71)=0

!& algeq
      eqtyp( 72)=0

!& algeq
      eqtyp( 73)=0

!& swsign
      eqtyp( 74)=0
      if(x( 63)>=0.)then
         z(  9)=1
      else
         z(  9)=2
      endif

!& hyst
      eqtyp( 75)=0
      if(x( 67)>1.1)then
         z( 10)=1
      elseif(x( 67)<0.9)then
         z( 10)=-1
      else
         if(1.>= 0.)then
            z( 10)=1
         else
            z( 10)=-1
         endif
      endif

!& algeq                                             ! overfrequency protection
      eqtyp( 76)=0

!& algeq
      eqtyp( 77)=0

!& algeq
      eqtyp( 78)=0

!& swsign
      eqtyp( 79)=0
      if(x( 68)>=0.)then
         z( 11)=1
      else
         z( 11)=2
      endif

!& hyst
      eqtyp( 80)=0
      if(x( 72)>1.1)then
         z( 12)=1
      elseif(x( 72)<0.9)then
         z( 12)=-1
      else
         if(1.>= 0.)then
            z( 12)=1
         else
            z( 12)=-1
         endif
      endif

!& algeq                                             ! frequency deviation in Hz
      eqtyp( 81)=0

!& tfder1p                                           ! Rocof measurement in Hz/s
      x(134)=x( 23)
      eqtyp( 82)=134
      tc( 82)=prm( 39)
      eqtyp( 83)=0

!& abs
      eqtyp( 84)=0
      if(x( 73)>0. )then
         z( 13)=1
      else
         z( 13)=-1
      endif

!& algeq                                             ! Rocof protection
      eqtyp( 85)=0

!& algeq
      eqtyp( 86)=0

!& algeq
      eqtyp( 87)=0

!& swsign
      eqtyp( 88)=0
      if(x( 77)>=0.)then
         z( 14)=1
      else
         z( 14)=2
      endif

!& hyst
      eqtyp( 89)=0
      if(x( 78)>1.1)then
         z( 15)=1
      elseif(x( 78)<0.9)then
         z( 15)=-1
      else
         if(1.>= 0.)then
            z( 15)=1
         else
            z( 15)=-1
         endif
      endif

!& algeq                                             ! min and max power limiters
      eqtyp( 90)=0

!& algeq
      eqtyp( 91)=0

!& algeq                                             ! status check without droop
      eqtyp( 92)=0

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      eqtyp( 93)=0

!& algeq
      eqtyp( 94)=0

!& algeq                                             ! NEIGHBOURHOOD	! V_n is the sum of the voltage limit signals of the neighbours, with +1 for upper limit and -1 for lower limit	! P_n is the same fo
      eqtyp( 95)=0

!& swsign
      eqtyp( 96)=0
      if(x(110)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& algeq
      eqtyp( 97)=0

!& swsign
      eqtyp( 98)=0
      if(x(111)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& algeq
      eqtyp( 99)=0

!& algeq
      eqtyp(100)=0

!& abs
      eqtyp(101)=0
      if(x(111)>0. )then
         z( 18)=1
      else
         z( 18)=-1
      endif

!& algeq
      eqtyp(102)=0

!& swsign
      eqtyp(103)=0
      if(x(113)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& algeq												! priority of voltage over power
      eqtyp(104)=0

!& algeq
      eqtyp(105)=0

!& algeq												! reactive power computation
      eqtyp(106)=0

!& algeq
      eqtyp(107)=0

!& algeq
      eqtyp(108)=0

!& algeq												! active power computation
      eqtyp(109)=0

!& algeq
      eqtyp(110)=0

!& algeq
      eqtyp(111)=0

!& algeq												! UNIT LIMIT	!s_f = security factor
      eqtyp(112)=0

!& algeq
      eqtyp(113)=0

!& algeq
      eqtyp(114)=0

!& algeq
      eqtyp(115)=0

!& algeq 											!should it be V or Vm?	! voltage low limit flag
      eqtyp(116)=0

!& swsign
      eqtyp(117)=0
      if(x( 91)>=0.)then
         z( 20)=1
      else
         z( 20)=2
      endif

!& algeq !should it be V or Vm?						! voltage high limit flag
      eqtyp(118)=0

!& swsign
      eqtyp(119)=0
      if(x( 93)>=0.)then
         z( 21)=1
      else
         z( 21)=2
      endif

!& algeq 											! F_v_opp = 0 if the voltage is smaller than V_lim_min or bigger than V_lim_max
      eqtyp(120)=0

!& algeq												! power low limit flag
      eqtyp(121)=0

!& swsign
      eqtyp(122)=0
      if(x( 96)>=0.)then
         z( 22)=1
      else
         z( 22)=2
      endif

!& algeq												! power high limit flag
      eqtyp(123)=0

!& swsign
      eqtyp(124)=0
      if(x( 98)>=0.)then
         z( 23)=1
      else
         z( 23)=2
      endif

!& algeq
      eqtyp(125)=0

!& algeq
      eqtyp(126)=0

!& algeq
      eqtyp(127)=0

!& algeq
      eqtyp(128)=0

!& algeq												! final limit flag F_lim doesn't work, since when V_n and P_n are 0 the max flags are 1!
      eqtyp(129)=0

!& algeq
      eqtyp(130)=0

!& algeq
      eqtyp(131)=0

!& algeq
      eqtyp(132)=0

!& algeq
      eqtyp(133)=0

!& algeq												! dQ MUST BE LINKED TO THE MOTOR SPEED!
      eqtyp(134)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq                                             ! voltage magnitude
      f(  1)=dsqrt(vx**2+vy**2) - x(  7)

!& tf1p
      f(  2)=(-x( 10)+1.*x(  7))

!& tf1p
      f(  3)=(-x(  8)+1.*vx    )

!& tf1p
      f(  4)=(-x(  9)+1.*vy    )

!& algeq                                             ! voltage alignment
      f(  5)=-x(  3) + x(  8)*cos(x(  5))+x(  9)*sin(x(  5))

!& algeq
      f(  6)=-x(  4) - x(  8)*sin(x(  5))+x(  9)*cos(x(  5))

!& algeq                                             ! compute ix
      f(  7)=-x(  1) + (-x( 56)*cos(x(  5))-x( 57)*sin(x(  5)))*prm(  1)/prm(  2)

!& algeq                                             ! compute iy
      f(  8)=-x(  2) + (-x( 56)*sin(x(  5))+x( 57)*sin(x(  5)))*prm(  1)/prm(  2)

!& algeq
      f(  9)=-x( 56) + x( 11)*x( 81)

!& algeq
      f( 10)=-x( 57) + x( 13)*x( 81)

!& algeq                                             ! compute powers
      f( 11)=-x( 15)*prm(  2)/prm(  1) -x( 17)                            ! + x(  1)*vx+x(  2)*vy

!& algeq
      f( 12)=-x( 18)*prm(  2)/prm(  1) - x( 19)                           ! -vx*x(  2) + vy*x(  1)

!& algeq                                             ! compute powers
      f( 13)=-x( 17) + x(  3)*x( 56) + x(  4)*x( 57)                              ! - x( 15)*prm(  2)/prm(  1)

!& algeq
      f( 14)=-x( 19) + x(  3)*x( 57) - x(  4)*x( 56)                              ! - x( 18)*prm(  2)/prm(  1)

!& int                                               ! voltage alignment angle, PLL angle
      if (1.d0< 0.005)then
         f( 15)=x( 22)-x(  5)
      else
         f( 15)=x( 22)
      endif

!& pictl                                             ! PLL
      f( 16)=0.1/(prm(  5)*0.001)**2                                                                                                                                                                                                                                                                                     *x(  4)
      f( 17)=0.5/(prm(  5)*0.001)                                                                                                                                                                                                                                                                                        *x(  4)+x(127)-x( 24)

!& algeq                                             ! frequency deviation
      f( 18)=x( 21) - x( 24) + omega*prm( 62)

!& algeq
      f( 19)=x( 22) -x( 21)*x( 48)

!& algeq                                             ! compute and filter frequency
      f( 20)=-x( 26)+x( 24)/prm( 62)*fnom

!& tf1p
      f( 21)=(-x( 25)+1.*x( 26))

!& algeq                                             ! PLL freezing
      f( 22)=-x( 49) + x( 10)-prm(  6)

!& algeq
      f( 23)=x( 51)

!& algeq
      f( 24)=-x( 50) + 1.d0

!& swsign
      select case (z(  1))
         case(1)
            f( 25)=x( 48)-x( 50)
         case(2)
            f( 25)=x( 48)-x( 51)
      end select

!& algeq
      f( 26)=-x(  6) + prm(  3)*x( 81)

!& algeq                                             ! DC voltage control
      f( 27)=-x( 28) + x( 81)*x(  6) - x( 27)

!& pictl
      f( 28)=prm(  9)                                                                                                                                                                                                                                                                                                    *x( 28)
      f( 29)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 28)+x(128)-x( 29)

!& algeq                                             ! p-axis current control
      f( 30)=-x( 12) + x( 29) -x( 11)

!& pictl
      f( 31)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 12)
      f( 32)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 12)+x(129)-x( 32)

!& algeq                                             ! q-axis current control
      f( 33)=-x( 31) + x( 29)*dsqrt(1-prm(  4))

!& algeq
      f( 34)=-x( 14) + x( 31) -x( 13)

!& pictl
      f( 35)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 14)
      f( 36)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 14)+x(130)-x( 33)

!& algeq                                             ! md
      f( 37)=-x( 34) + x( 81)*(-x( 32) + prm( 18)/max(1.d-3,prm(  3))*x( 25)/fnom*x( 13))

!& algeq                                             ! mq
      f( 38)=-x( 35) + x( 81)*(-x( 33) - prm( 18)/max(1.d-3,prm(  3))*x( 25)/fnom*x( 11))

!& algeq                                             ! voltage over d-axis terminal impedance
      f( 39)=-x( 36) + x(  3) - x( 34) * x( 27) + prm( 18)*omega*x( 13)

!& algeq                                             ! voltage over q-axis terminal impedance
      f( 40)=-x( 37) + x(  4) - x( 35) * x( 27) - prm( 18)*omega*x( 11)

!& tf1p                                              ! d-axis current
      f( 41)=(-x( 11)+1/prm( 17)*x( 36))

!& tf1p                                              ! q-axis current
      f( 42)=(-x( 13)+1/prm( 17)*x( 37))

!& algeq                                             ! DC link current
      f( 43)=-x( 38) + x( 81)*(x( 45)*prm( 21)*x( 43) + prm( 22)*x( 43)**2) / max(1.d-3,x( 27))

!& algeq                                             ! DC link
      f( 44)=-x( 39)+x( 34)*x( 11)+x( 35)*x( 13)-x( 38)

!& int                                               ! DC link voltage
      if (prm( 19)/prm( 62)< 0.005)then
         f( 45)=x( 39)-x( 27)
      else
         f( 45)=x( 39)
      endif

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            f( 46)=x( 20)-x( 58)
         case(-1)
            f( 46)=x( 20)-x( 80)
         case(1)
            f( 46)=x( 20)-x( 30)
      end select

!& algeq                                             ! power mismatch
      f( 47)=-x( 16) + x( 20)-x( 17)

!& pictl                                             ! power control
      f( 48)=prm( 13)                                                                                                                                                                                                                                                                                                    *x( 16)
      f( 49)=prm( 12)                                                                                                                                                                                                                                                                                                    *x( 16)+x(131)-x( 40)

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            f( 50)=x( 41)-x( 40)
         case(-1)
            f( 50)=x( 41)-prm( 27)
         case(1)
            f( 50)=x( 41)-prm( 28)
      end select

!& algeq                                             ! speed control input
      f( 51)=-x( 44) + prm( 20)*(x( 81)*x( 41)-x( 45))

!& pictl                                             ! speed control
      f( 52)=prm( 15)                                                                                                                                                                                                                                                                                                    *x( 44)
      f( 53)=prm( 14)                                                                                                                                                                                                                                                                                                    *x( 44)+x(132)-x( 42)

!& tf1p                                              ! motor current control
      f( 54)=(-x( 43)+1*x( 42))

!& algeq                                             ! torque equations
      f( 55)=-x( 46) + prm( 21)*x( 43)

!& algeq
      f( 56)=-x( 47) + x( 46)-prm( 86)

!& tf1p                                              ! motor inertia
      f( 57)=(-x( 45)+1/prm( 24)*x( 47))

!& algeq
      f( 58)=x( 52) - x( 10) +prm( 29)

!& pwlin4                                            ! overvoltage Protection
      select case (z(  4))
         case (  1)
            f( 59)=0.+ ( (0.-0.)*(x( 52)-(-999))/(0.-(-999)) ) -x( 53)
         case (  2)
            f( 59)=0.+ ( (1.-0.)*(x( 52)-0.)/(0.-0.) ) -x( 53)
         case (  3)
            f( 59)=1.+ ( (1.-1.)*(x( 52)-0.)/(999-0.) ) -x( 53)
      end select

!& algeq
      f( 60)=x( 54) -1 + x( 53)

!& hyst
      if(z(  5) == 1)then
         f( 61)=x( 55)-1.-(1.-1.)*(x( 54)-1.1)/(1.1-0.9)
      else
         f( 61)=x( 55)-0.-(0.-0.)*(x( 54)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! LVRT
      f( 62)=x( 10) + x( 59)

!& timer5
      select case (z(  6))
         case (-1)
            f( 63)=x( 60)
            f( 64)=x(133)
         case (0)
            f( 63)=x( 60)
            f( 64)= 1.
         case (1)
            f( 63)=x( 60)-1.
            f( 64)= 0.
      end select

!& algeq
      f( 65)=x( 62) -1 + x( 60)

!& hyst
      if(z(  7) == 1)then
         f( 66)=x( 61)-1.-(1.-1.)*(x( 62)-1.1)/(1.1-0.9)
      else
         f( 66)=x( 61)-0.-(0.-0.)*(x( 62)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      f( 67)=x( 83) - x( 55)*x( 61)*x( 66)*x( 71)*x( 79)

!& algeq
      f( 68)=x( 82) - prm( 44)

!& algeq
      f( 69)=x( 84) - 1

!& swsign
      select case (z(  8))
         case(1)
            f( 70)=x( 81)-x( 83)
         case(2)
            f( 70)=x( 81)-x( 84)
      end select

!& algeq                                             ! underfrequency protection
      f( 71)=x( 64) - 1

!& algeq
      f( 72)=x( 65)

!& algeq
      f( 73)=x( 63) - x( 25) + prm( 37)

!& swsign
      select case (z(  9))
         case(1)
            f( 74)=x( 67)-x( 64)
         case(2)
            f( 74)=x( 67)-x( 65)
      end select

!& hyst
      if(z( 10) == 1)then
         f( 75)=x( 66)-1.-(1.-1.)*(x( 67)-1.1)/(1.1-0.9)
      else
         f( 75)=x( 66)-0.-(0.-0.)*(x( 67)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! overfrequency protection
      f( 76)=x( 69) - 1

!& algeq
      f( 77)=x( 70)

!& algeq
      f( 78)=x( 68) - prm( 38) + x( 25)

!& swsign
      select case (z( 11))
         case(1)
            f( 79)=x( 72)-x( 69)
         case(2)
            f( 79)=x( 72)-x( 70)
      end select

!& hyst
      if(z( 12) == 1)then
         f( 80)=x( 71)-1.-(1.-1.)*(x( 72)-1.1)/(1.1-0.9)
      else
         f( 80)=x( 71)-0.-(0.-0.)*(x( 72)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! frequency deviation in Hz
      f( 81)=-x( 23) + x( 25)-fnom

!& tfder1p                                           ! Rocof measurement in Hz/s
      f( 82)=-x(134)+x( 23)
      if (prm( 39)< 0.005)then
         f( 83)=1/prm( 39)*x( 23)-x( 73)
      else
         f( 83)=1/prm( 39)*(x( 23)-x(134))-x( 73)
      endif

!& abs
      if(z( 13) == 1 )then
         f( 84)=x( 74)-x( 73)
      else
         f( 84)=x( 74)+x( 73)
      endif

!& algeq                                             ! Rocof protection
      f( 85)=x( 75) - 1

!& algeq
      f( 86)=x( 76)

!& algeq
      f( 87)=-x( 77) +prm( 40) -x( 74)

!& swsign
      select case (z( 14))
         case(1)
            f( 88)=x( 78)-x( 75)
         case(2)
            f( 88)=x( 78)-x( 76)
      end select

!& hyst
      if(z( 15) == 1)then
         f( 89)=x( 79)-1.-(1.-1.)*(x( 78)-1.1)/(1.1-0.9)
      else
         f( 89)=x( 79)-0.-(0.-0.)*(x( 78)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! min and max power limiters
      f( 90)=x( 80)

!& algeq
      f( 91)=-x( 30) + prm( 41)

!& algeq                                             ! status check without droop
      f( 92)=- x( 58) +  x( 81)*prm( 73)

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      f( 93)=x( 86)

!& algeq
      f( 94)=x( 85) - 1

!& algeq                                             ! NEIGHBOURHOOD	! V_n is the sum of the voltage limit signals of the neighbours, with +1 for upper limit and -1 for lower limit	! P_n is the same fo
      f( 95)=-x(110) + prm( 48)

!& swsign
      select case (z( 16))
         case(1)
            f( 96)=x(115)-x( 85)
         case(2)
            f( 96)=x(115)-x( 86)
      end select

!& algeq
      f( 97)=-x(116) + 1 - x(115)

!& swsign
      select case (z( 17))
         case(1)
            f( 98)=x(117)-x( 85)
         case(2)
            f( 98)=x(117)-x( 86)
      end select

!& algeq
      f( 99)=-x(118) + 1 - x(117)

!& algeq
      f(100)=-x(111) + prm( 47)

!& abs
      if(z( 18) == 1 )then
         f(101)=x(112)-x(111)
      else
         f(101)=x(112)+x(111)
      endif

!& algeq
      f(102)=-x(113) + x(112) - 1

!& swsign
      select case (z( 19))
         case(1)
            f(103)=x(114)-x( 85)
         case(2)
            f(103)=x(114)-x( 86)
      end select

!& algeq												! priority of voltage over power
      f(104)=-x(119) + x(115)*(1-x(114))

!& algeq
      f(105)=-x(120) + x(116)*(1-x(114))

!& algeq												! reactive power computation
      f(106)=-x(121) + prm( 47)*prm( 51)*prm( 53)*(prm( 53)-x( 19))/(prm( 53)-prm( 54))*x(117)

!& algeq
      f(107)=-x(122) + prm( 47)*prm( 51)*prm( 53)*(x( 19)-prm( 54))/(prm( 53)-prm( 54))*x(118)

!& algeq
      f(108)=-x(123) + x(121) + x(122)

!& algeq												! active power computation
      f(109)=-x(124) + prm( 48)*prm( 52)*prm( 45)*(prm( 45)-x( 17))/(prm( 45)-prm( 46))*x(119)

!& algeq
      f(110)=-x(125) + prm( 48)*prm( 52)*prm( 45)*(x( 17)-prm( 46))/(prm( 45)-prm( 46))*x(120)

!& algeq
      f(111)=-x(126) + x(124) + x(125)

!& algeq												! UNIT LIMIT	!s_f = security factor
      f(112)=-x( 87) + prm( 30)*prm( 55)

!& algeq
      f(113)=-x( 88) + prm( 29)/prm( 55)

!& algeq
      f(114)=-x( 89) + prm( 46)*prm( 55)

!& algeq
      f(115)=-x( 90) + prm( 45)/prm( 55)

!& algeq 											!should it be V or Vm?	! voltage low limit flag
      f(116)=-x( 91) + x( 87) - x( 10)

!& swsign
      select case (z( 20))
         case(1)
            f(117)=x( 92)-x( 85)
         case(2)
            f(117)=x( 92)-x( 86)
      end select

!& algeq !should it be V or Vm?						! voltage high limit flag
      f(118)=-x( 93) + x( 10) - x( 88)

!& swsign
      select case (z( 21))
         case(1)
            f(119)=x( 94)-x( 85)
         case(2)
            f(119)=x( 94)-x( 86)
      end select

!& algeq 											! F_v_opp = 0 if the voltage is smaller than V_lim_min or bigger than V_lim_max
      f(120)=-x( 95) + (1-x( 92))*(1-x( 94))

!& algeq												! power low limit flag
      f(121)=-x( 96) + x( 89) - x( 17)

!& swsign
      select case (z( 22))
         case(1)
            f(122)=x( 97)-x( 95)
         case(2)
            f(122)=x( 97)-x( 86)
      end select

!& algeq												! power high limit flag
      f(123)=-x( 98) + x( 17) - x( 90)

!& swsign
      select case (z( 23))
         case(1)
            f(124)=x( 99)-x( 95)
         case(2)
            f(124)=x( 99)-x( 86)
      end select

!& algeq
      f(125)=-x(100) + x( 92)*x( 81)

!& algeq
      f(126)=-x(101) + x( 94)*x( 81)

!& algeq
      f(127)=-x(102) + x( 97)*x( 81)

!& algeq
      f(128)=-x(103) + x( 99)*x( 81)

!& algeq												! final limit flag F_lim doesn't work, since when V_n and P_n are 0 the max flags are 1!
      f(129)=-x(104) + 1-(1-x(100))*(1-x(101))*(1-x(102))*(1-x(103))

!& algeq
      f(130)=-x(105) + (x( 17) - x( 20) + x(126))*x(104)

!& algeq
      f(131)=-x(109) + x(105)

!& algeq
      f(132)=-x(106) + (x( 19) + x(123))*x(104)

!& algeq
      f(133)=-x(107) + x(106)

!& algeq												! dQ MUST BE LINKED TO THE MOTOR SPEED!
      f(134)=-x(108) - x( 19) + x(107)

!........................................................................................
   case (update_disc)

!& algeq                                             ! voltage magnitude

!& tf1p

!& tf1p

!& tf1p

!& algeq                                             ! voltage alignment

!& algeq

!& algeq                                             ! compute ix

!& algeq                                             ! compute iy

!& algeq

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& int                                               ! voltage alignment angle, PLL angle

!& pictl                                             ! PLL

!& algeq                                             ! frequency deviation

!& algeq

!& algeq                                             ! compute and filter frequency

!& tf1p

!& algeq                                             ! PLL freezing

!& algeq

!& algeq

!& swsign
      select case (z(  1))
         case(1)
            if(x( 49)<0.)then
               z(  1)=2
            endif
         case(2)
            if(x( 49)>=0.)then
               z(  1)=1
            endif
      end select

!& algeq

!& algeq                                             ! DC voltage control

!& pictl

!& algeq                                             ! p-axis current control

!& pictl

!& algeq                                             ! q-axis current control

!& algeq

!& pictl

!& algeq                                             ! md

!& algeq                                             ! mq

!& algeq                                             ! voltage over d-axis terminal impedance

!& algeq                                             ! voltage over q-axis terminal impedance

!& tf1p                                              ! d-axis current

!& tf1p                                              ! q-axis current

!& algeq                                             ! DC link current

!& algeq                                             ! DC link

!& int                                               ! DC link voltage

!& limvb                                              ! power limiter
      select case (z(  2))
         case(0)
            if(x( 58)>x( 30))then
               z(  2)=1
            elseif(x( 58)<x( 80))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 58)>x( 80))then
               z(  2)=0
            endif
         case(1)
            if(x( 58)<x( 30))then
               z(  2)=0
            endif
      end select

!& algeq                                             ! power mismatch

!& pictl                                             ! power control

!& lim                                               ! limit speed control input
      select case (z(  3))
         case(0)
            if(x( 40)>prm( 28))then
               z(  3)=1
            elseif(x( 40)<prm( 27))then
               z(  3)=-1
            endif
         case(-1)
            if(x( 40)>prm( 27))then
               z(  3)=0
            endif
         case(1)
            if(x( 40)<prm( 28))then
               z(  3)=0
            endif
      end select

!& algeq                                             ! speed control input

!& pictl                                             ! speed control

!& tf1p                                              ! motor current control

!& algeq                                             ! torque equations

!& algeq

!& tf1p                                              ! motor inertia

!& algeq

!& pwlin4                                            ! overvoltage Protection
      if(x( 52)<(-999))then
         z(  4)=1
      elseif(x( 52)>=999)then
         z(  4)=  3
      elseif((-999)<=x( 52) .and. x( 52)<0.)then
         z(  4)=  1
      elseif(0.<=x( 52) .and. x( 52)<0.)then
         z(  4)=  2
      elseif(0.<=x( 52) .and. x( 52)<999)then
         z(  4)=  3
      endif

!& algeq

!& hyst
      if (z(  5) == -1)then
         if(x( 54)>1.1)then
            z(  5)=1
         endif
      else
         if(x( 54)<0.9)then
            z(  5)=-1
         endif
      endif

!& algeq                                             ! LVRT

!& timer5
      if(z(  6) == -1)then
         if(x( 59) >= (-prm( 32)))then
            z(  6)=0
            eqtyp( 64)=133
         endif
      else
         if(x( 59) < (-prm( 32)))then
            z(  6)=-1
            eqtyp( 64)=0
         endif
      endif
      if(z(  6) == 0)then
         if(x( 59) > (-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))then
            if(x(133) > 0.)then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))then
            if(x(133) > prm( 33)+(0.-prm( 33))*(x( 59)-(-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))then
            if(x(133) > prm( 33)+(prm( 33)-prm( 33))*(x( 59)-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 30) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))then
            if(x(133) > prm( 35)+(prm( 33)-prm( 35))*(x( 59)-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36))))/((-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))-(-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))))then
               z(  6)=1
            endif
         elseif(x( 59) > (-prm( 32)))then
            if(x(133) > prm( 34)+(prm( 35)-prm( 34))*(x( 59)-(-prm( 32)))/((-(prm( 42)*prm( 31) + (1-prm( 42))*prm( 36)))-(-prm( 32))))then
               z(  6)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  7) == -1)then
         if(x( 62)>1.1)then
            z(  7)=1
         endif
      else
         if(x( 62)<0.9)then
            z(  7)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off

!& algeq

!& algeq

!& swsign
      select case (z(  8))
         case(1)
            if(x( 82)<0.)then
               z(  8)=2
            endif
         case(2)
            if(x( 82)>=0.)then
               z(  8)=1
            endif
      end select

!& algeq                                             ! underfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z(  9))
         case(1)
            if(x( 63)<0.)then
               z(  9)=2
            endif
         case(2)
            if(x( 63)>=0.)then
               z(  9)=1
            endif
      end select

!& hyst
      if (z( 10) == -1)then
         if(x( 67)>1.1)then
            z( 10)=1
         endif
      else
         if(x( 67)<0.9)then
            z( 10)=-1
         endif
      endif

!& algeq                                             ! overfrequency protection

!& algeq

!& algeq

!& swsign
      select case (z( 11))
         case(1)
            if(x( 68)<0.)then
               z( 11)=2
            endif
         case(2)
            if(x( 68)>=0.)then
               z( 11)=1
            endif
      end select

!& hyst
      if (z( 12) == -1)then
         if(x( 72)>1.1)then
            z( 12)=1
         endif
      else
         if(x( 72)<0.9)then
            z( 12)=-1
         endif
      endif

!& algeq                                             ! frequency deviation in Hz

!& tfder1p                                           ! Rocof measurement in Hz/s

!& abs
      if (z( 13) == -1 )then
         if(x( 73)> blocktol1 )then
            z( 13)=1
         endif
      else
         if(x( 73)< - blocktol1 )then
            z( 13)=-1
         endif
      endif

!& algeq                                             ! Rocof protection

!& algeq

!& algeq

!& swsign
      select case (z( 14))
         case(1)
            if(x( 77)<0.)then
               z( 14)=2
            endif
         case(2)
            if(x( 77)>=0.)then
               z( 14)=1
            endif
      end select

!& hyst
      if (z( 15) == -1)then
         if(x( 78)>1.1)then
            z( 15)=1
         endif
      else
         if(x( 78)<0.9)then
            z( 15)=-1
         endif
      endif

!& algeq                                             ! min and max power limiters

!& algeq

!& algeq                                             ! status check without droop

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation

!& algeq

!& algeq                                             ! NEIGHBOURHOOD	! V_n is the sum of the voltage limit signals of the neighbours, with +1 for upper limit and -1 for lower limit	! P_n is the same fo

!& swsign
      select case (z( 16))
         case(1)
            if(x(110)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 16)=1
            endif
      end select

!& algeq

!& swsign
      select case (z( 17))
         case(1)
            if(x(111)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x(111)>=0.)then
               z( 17)=1
            endif
      end select

!& algeq

!& algeq

!& abs
      if (z( 18) == -1 )then
         if(x(111)> blocktol1 )then
            z( 18)=1
         endif
      else
         if(x(111)< - blocktol1 )then
            z( 18)=-1
         endif
      endif

!& algeq

!& swsign
      select case (z( 19))
         case(1)
            if(x(113)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x(113)>=0.)then
               z( 19)=1
            endif
      end select

!& algeq												! priority of voltage over power

!& algeq

!& algeq												! reactive power computation

!& algeq

!& algeq

!& algeq												! active power computation

!& algeq

!& algeq

!& algeq												! UNIT LIMIT	!s_f = security factor

!& algeq

!& algeq

!& algeq

!& algeq 											!should it be V or Vm?	! voltage low limit flag

!& swsign
      select case (z( 20))
         case(1)
            if(x( 91)<0.)then
               z( 20)=2
            endif
         case(2)
            if(x( 91)>=0.)then
               z( 20)=1
            endif
      end select

!& algeq !should it be V or Vm?						! voltage high limit flag

!& swsign
      select case (z( 21))
         case(1)
            if(x( 93)<0.)then
               z( 21)=2
            endif
         case(2)
            if(x( 93)>=0.)then
               z( 21)=1
            endif
      end select

!& algeq 											! F_v_opp = 0 if the voltage is smaller than V_lim_min or bigger than V_lim_max

!& algeq												! power low limit flag

!& swsign
      select case (z( 22))
         case(1)
            if(x( 96)<0.)then
               z( 22)=2
            endif
         case(2)
            if(x( 96)>=0.)then
               z( 22)=1
            endif
      end select

!& algeq												! power high limit flag

!& swsign
      select case (z( 23))
         case(1)
            if(x( 98)<0.)then
               z( 23)=2
            endif
         case(2)
            if(x( 98)>=0.)then
               z( 23)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq												! final limit flag F_lim doesn't work, since when V_n and P_n are 0 the max flags are 1!

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq												! dQ MUST BE LINKED TO THE MOTOR SPEED!
   end select

end subroutine inj_ATLtestNB
