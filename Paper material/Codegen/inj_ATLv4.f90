!  MODEL NAME : inj_ATLv4               
!  MODEL DESCRIPTION FILE : ATLv4.txt
!  Data :
!       prm(  1)=  Sb                              ! base power of the unit
!       prm(  2)=  Sbs                             ! base power of the system
!       prm(  3)=  vdc_star                        ! DC link voltage reference
!       prm(  4)=  pf                              ! setpoint for power factor at the terminal
!       prm(  5)=  kp_v                            ! DC link voltage control parameters
!       prm(  6)=  ki_v
!       prm(  7)=  kp_c                            ! rectifier current control parameters
!       prm(  8)=  ki_c
!       prm(  9)=  kp_p                            ! power control parameters
!       prm( 10)=  ki_p
!       prm( 11)=  kp_w                            ! motor speed control parameters
!       prm( 12)=  ki_w
!       prm( 13)=  w_cc                            ! motor current control bandwidth
!       prm( 14)=  rt                              ! terminal impedance
!       prm( 15)=  lt
!       prm( 16)=  cdc                             ! DC link capacitance
!       prm( 17)=  kw                              ! speed/torque control constant
!       prm( 18)=  kT                              ! motor torque constant
!       prm( 19)=  ra                              ! motor anchor/stator resistance
!       prm( 20)=  H                               ! motor inertia
!       prm( 21)=  b                               ! motor friction coefficient
!       prm( 22)=  Tnm                             ! compressor torque at nominal speed
!       prm( 23)=  iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
!       prm( 24)=  wm_min                          ! limits on rotational speed
!       prm( 25)=  wm_max
!       prm( 26)=  Vmax                            ! voltage range during which unit needs to stay connected
!       prm( 27)=  Vmin
!       prm( 28)=  Vint
!       prm( 29)=  Vr
!       prm( 30)=  tLVRT1
!       prm( 31)=  tLVRT2
!       prm( 32)=  tLVRTint
!       prm( 33)=  Vtrip
!       prm( 34)=  VPmax                           ! power limit above 0.93 pu voltage
!       prm( 35)=  LVRT                            ! enable or disable LVRT
!       prm( 36)=  Tm                              ! measurement delay
!       prm( 37)=  protection                      ! Flag for protection, -1 for off, 1 for on
!       prm( 38)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 39)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 40)=  dPc_1_0                         ! start power for positive level 1 central controller
!       prm( 41)=  dPc_2_0                         ! start power for positive level 2 central controller
!       prm( 42)=  l_1_p_min_par                 ! power level for the activation of the central controller help in pu
!       prm( 43)=  l_1_p_max_par
!       prm( 44)=  l_1_n_min_par
!       prm( 45)=  l_1_n_max_par
!       prm( 46)=  l_2_p_min_par
!       prm( 47)=  l_2_p_max_par
!       prm( 48)=  l_2_n_min_par
!       prm( 49)=  l_2_n_max_par
!       prm( 50)=  V_n                             ! number of neighbours at voltage limit
!       prm( 51)=  P_n                             ! number of neighbours at power limit
!       prm( 52)=  Vdb_p                           ! higher voltage deadband value
!       prm( 53)=  Vdb_m                           ! lower voltage deadband value
!       prm( 54)=  ro_v1                            ! neighbourhood factor for voltage1
!       prm( 55)=  ro_v2                            ! neighbourhood factor for voltage2
!       prm( 56)=  ro_p                            ! neighbourhood factor for power
!       prm( 57)=  Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
!       prm( 58)=  Q_min                           ! min reactive power of the unit in pu, IT WIL
!       prm( 59)=  s_f1                             ! security factor for unit limit control
!       prm( 60)=  s_f2                             ! security factor for dPc_3
!       prm( 61)=  lvl                             ! level of emergency of the central controller
!       prm( 62)=  pl1_2_0                        ! flags that varies for the different units
!       prm( 63)=  pl1_4_0
!       prm( 64)=  pl1_6_0
!       prm( 65)=  pl1_8_0
!       prm( 66)=  pl2_2_0                        ! flags that varies for the different units
!       prm( 67)=  pl2_4_0
!       prm( 68)=  pl2_6_0
!       prm( 69)=  pl2_8_0
!       prm( 70)=  F_help_high_0
!       prm( 71)=  F_help_low_0
!       prm( 72)=  Tp1
!       prm( 73)=  V_max_nb
!       prm( 74)=  V_min_nb
!       prm( 75)=  dPs_rate_max
!       prm( 76)=  dQs_rate_max
!  Parameters :
!       prm( 77)=  w0  
!       prm( 78)=  wb  
!       prm( 79)=  t2   Torque polynomial parameters, set to constant torque
!       prm( 80)=  t1  
!       prm( 81)=  t0  
!       prm( 82)=  Downlim  
!       prm( 83)=  Uplim  
!       prm( 84)=  Downlimdisc  
!       prm( 85)=  Uplimdis  
!       prm( 86)=  downlimdis  
!       prm( 87)=  theta0  
!       prm( 88)=  P0   initial active power
!       prm( 89)=  Q0   initial reactive power
!       prm( 90)=  P0_unit  
!       prm( 91)=  Q0_unit  
!       prm( 92)=  V0   initial voltage magnitude at bus
!       prm( 93)=  iP0   current alignment
!       prm( 94)=  iQ0  
!       prm( 95)=  vd0   voltage alignment
!       prm( 96)=  vq0  
!       prm( 97)=  md0   modulation indices
!       prm( 98)=  mq0  
!       prm( 99)=  idc0   DC link current
!       prm(100)=  iT0  
!       prm(101)=  wm0  
!       prm(102)=  Te0  
!       prm(103)=  Tc0  
!       prm(104)=  {dPs_rate_min}  
!       prm(105)=  {dQs_rate_min}  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vd                    
!       x(  4)=  vq                    
!       x(  5)=  vdc_ref               
!       x(  6)=  V                     
!       x(  7)=  vxm                   
!       x(  8)=  vym                   
!       x(  9)=  Vm                    
!       x( 10)=  iP                    
!       x( 11)=  diP                   
!       x( 12)=  iQ                    
!       x( 13)=  diQ                   
!       x( 14)=  P                     
!       x( 15)=  dp                    
!       x( 16)=  Punit                 
!       x( 17)=  Q                     
!       x( 18)=  Qunit                 
!       x( 19)=  Pref_lim               limited P reference
!       x( 20)=  vdc                   
!       x( 21)=  dvdc                  
!       x( 22)=  iP_ref                
!       x( 23)=  Plim                  
!       x( 24)=  iQ_ref                
!       x( 25)=  mdr                   
!       x( 26)=  mqr                   
!       x( 27)=  md                    
!       x( 28)=  mq                    
!       x( 29)=  dvd                   
!       x( 30)=  dvq                   
!       x( 31)=  idc                   
!       x( 32)=  didc                  
!       x( 33)=  wm_ref                
!       x( 34)=  wm_ref_lim            
!       x( 35)=  iT_ref                
!       x( 36)=  iT                    
!       x( 37)=  dwm                   
!       x( 38)=  wm                    
!       x( 39)=  Te                    
!       x( 40)=  dT                    
!       x( 41)=  deltafvh              
!       x( 42)=  z1                    
!       x( 43)=  Fvhi                  
!       x( 44)=  Fvh                   
!       x( 45)=  iPs                   
!       x( 46)=  iQs                   
!       x( 47)=  Pref                  
!       x( 48)=  x10                   
!       x( 49)=  z                     
!       x( 50)=  Fvl                   
!       x( 51)=  Fvli                  
!       x( 52)=  Plim_min              
!       x( 53)=  status                 status of the device, 1 for on
!       x( 54)=  p1                    
!       x( 55)=  p2                    
!       x( 56)=  p3                    
!       x( 57)=  Fpl1                   placeholder for value 1 in switch block
!       x( 58)=  Fpl0                   placeholder for value 0 in switch block
!       x( 59)=  V_lim_min              unit controller states
!       x( 60)=  V_lim_max             
!       x( 61)=  P_lim_min             
!       x( 62)=  P_lim_max             
!       x( 63)=  pl1                   
!       x( 64)=  F_v_min_in            
!       x( 65)=  pl2                   
!       x( 66)=  F_v_max_in            
!       x( 67)=  F_v_opp               
!       x( 68)=  pl3                   
!       x( 69)=  F_p_min_in            
!       x( 70)=  pl4                   
!       x( 71)=  F_p_max_in            
!       x( 72)=  F_v_min               
!       x( 73)=  F_v_max               
!       x( 74)=  F_p_min               
!       x( 75)=  F_p_max               
!       x( 76)=  dP_lim                
!       x( 77)=  dQ_lim                
!       x( 78)=  dQ_sum                
!       x( 79)=  dP_sum                
!       x( 80)=  P_n_var                neighbourhood controller states
!       x( 81)=  P_n_var_neg           
!       x( 82)=  V_n_var               
!       x( 83)=  V_n_var_neg           
!       x( 84)=  V_n_abs               
!       x( 85)=  pl5                   
!       x( 86)=  F_help_v              
!       x( 87)=  F_help_v_p            
!       x( 88)=  F_help_v_m            
!       x( 89)=  F_help_p_p            
!       x( 90)=  F_help_p_m            
!       x( 91)=  F_hlp_high            
!       x( 92)=  F_hlp_low             
!       x( 93)=  F_hlp_v_p2            
!       x( 94)=  F_hlp_v_m2            
!       x( 95)=  F_hlp_p_p2            
!       x( 96)=  F_hlp_p_m2            
!       x( 97)=  dQp                   
!       x( 98)=  dQm                   
!       x( 99)=  dQnb                  
!       x(100)=  dPp                   
!       x(101)=  dPm                   
!       x(102)=  dPnb                  
!       x(103)=  iQnb                  
!       x(104)=  dPc_1_neg              central controller states
!       x(105)=  dPc_1_pos             
!       x(106)=  dPc_2_pos             
!       x(107)=  dPc_2_neg             
!       x(108)=  dPc_3_pos             
!       x(109)=  dPc_3_neg             
!       x(110)=  level                 
!       x(111)=  dPc_1_in              
!       x(112)=  dPc_2_in              
!       x(113)=  dPc_1                 
!       x(114)=  dPc_2                 
!       x(115)=  dPc_3                 
!       x(116)=  dPc_0                 
!       x(117)=  pl1_1                 
!       x(118)=  pl1_2                 
!       x(119)=  pl1_3                 
!       x(120)=  pl1_4                 
!       x(121)=  F_l_1_neg             
!       x(122)=  pl1_5                 
!       x(123)=  pl1_6                 
!       x(124)=  pl1_7                 
!       x(125)=  pl1_8                 
!       x(126)=  F_l_1_pos             
!       x(127)=  F_l_1                 
!       x(128)=  dPc_1_s_fl            
!       x(129)=  pl2_1                 
!       x(130)=  pl2_2                 
!       x(131)=  pl2_3                 
!       x(132)=  pl2_4                 
!       x(133)=  F_l_2_neg             
!       x(134)=  pl2_5                 
!       x(135)=  pl2_6                 
!       x(136)=  pl2_7                 
!       x(137)=  pl2_8                 
!       x(138)=  F_l_2_pos             
!       x(139)=  F_l_2                 
!       x(140)=  dPc_2_s_fl            
!       x(141)=  l_abs                 
!       x(142)=  l_switch              
!       x(143)=  dPc                   
!       x(144)=  dPc_1_in_f            
!       x(145)=  dPc_2_in_f            
!       x(146)=  dPc_no_bou            
!       x(147)=  under_min             
!       x(148)=  F_p_c_min             
!       x(149)=  over_max              
!       x(150)=  F_p_c_max             
!       x(151)=  theta_est             

!.........................................................................................................

subroutine inj_ATLv4(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 76
      nbaddpar= 29
      parname(  1)='Sb'
      parname(  2)='Sbs'
      parname(  3)='vdc_star'
      parname(  4)='pf'
      parname(  5)='kp_v'
      parname(  6)='ki_v'
      parname(  7)='kp_c'
      parname(  8)='ki_c'
      parname(  9)='kp_p'
      parname( 10)='ki_p'
      parname( 11)='kp_w'
      parname( 12)='ki_w'
      parname( 13)='w_cc'
      parname( 14)='rt'
      parname( 15)='lt'
      parname( 16)='cdc'
      parname( 17)='kw'
      parname( 18)='kT'
      parname( 19)='ra'
      parname( 20)='H'
      parname( 21)='b'
      parname( 22)='Tnm'
      parname( 23)='iF'
      parname( 24)='wm_min'
      parname( 25)='wm_max'
      parname( 26)='Vmax'
      parname( 27)='Vmin'
      parname( 28)='Vint'
      parname( 29)='Vr'
      parname( 30)='tLVRT1'
      parname( 31)='tLVRT2'
      parname( 32)='tLVRTint'
      parname( 33)='Vtrip'
      parname( 34)='VPmax'
      parname( 35)='LVRT'
      parname( 36)='Tm'
      parname( 37)='protection'
      parname( 38)='P_max'
      parname( 39)='P_min'
      parname( 40)='dPc_1_0'
      parname( 41)='dPc_2_0'
      parname( 42)='l_1_p_min_par'
      parname( 43)='l_1_p_max_par'
      parname( 44)='l_1_n_min_par'
      parname( 45)='l_1_n_max_par'
      parname( 46)='l_2_p_min_par'
      parname( 47)='l_2_p_max_par'
      parname( 48)='l_2_n_min_par'
      parname( 49)='l_2_n_max_par'
      parname( 50)='V_n'
      parname( 51)='P_n'
      parname( 52)='Vdb_p'
      parname( 53)='Vdb_m'
      parname( 54)='ro_v1'
      parname( 55)='ro_v2'
      parname( 56)='ro_p'
      parname( 57)='Q_max'
      parname( 58)='Q_min'
      parname( 59)='s_f1'
      parname( 60)='s_f2'
      parname( 61)='lvl'
      parname( 62)='pl1_2_0'
      parname( 63)='pl1_4_0'
      parname( 64)='pl1_6_0'
      parname( 65)='pl1_8_0'
      parname( 66)='pl2_2_0'
      parname( 67)='pl2_4_0'
      parname( 68)='pl2_6_0'
      parname( 69)='pl2_8_0'
      parname( 70)='F_help_high_0'
      parname( 71)='F_help_low_0'
      parname( 72)='Tp1'
      parname( 73)='V_max_nb'
      parname( 74)='V_min_nb'
      parname( 75)='dPs_rate_max'
      parname( 76)='dQs_rate_max'
      parname( 77)='w0'
      parname( 78)='wb'
      parname( 79)='t2'
      parname( 80)='t1'
      parname( 81)='t0'
      parname( 82)='Downlim'
      parname( 83)='Uplim'
      parname( 84)='Downlimdisc'
      parname( 85)='Uplimdis'
      parname( 86)='downlimdis'
      parname( 87)='theta0'
      parname( 88)='P0'
      parname( 89)='Q0'
      parname( 90)='P0_unit'
      parname( 91)='Q0_unit'
      parname( 92)='V0'
      parname( 93)='iP0'
      parname( 94)='iQ0'
      parname( 95)='vd0'
      parname( 96)='vq0'
      parname( 97)='md0'
      parname( 98)='mq0'
      parname( 99)='idc0'
      parname(100)='iT0'
      parname(101)='wm0'
      parname(102)='Te0'
      parname(103)='Tc0'
      parname(104)='{dPs_rate_min}'
      parname(105)='{dQs_rate_min}'
      adix=  1
      adiy=  2
      nbxvar=159
      nbzvar= 43

!........................................................................................
   case (define_obs)
      nbobs= 30
      obsname(  1)='iP'
      obsname(  2)='iQ'
      obsname(  3)='vd'
      obsname(  4)='vq'
      obsname(  5)='P'
      obsname(  6)='Punit'
      obsname(  7)='Pref'
      obsname(  8)='Q'
      obsname(  9)='wm'
      obsname( 10)='Vm'
      obsname( 11)='Pref_lim'
      obsname( 12)='wm_ref_lim'
      obsname( 13)='status'
      obsname( 14)='F_v_min'
      obsname( 15)='F_v_max'
      obsname( 16)='F_p_min'
      obsname( 17)='F_p_max'
      obsname( 18)='F_hlp_high'
      obsname( 19)='F_hlp_low'
      obsname( 20)='F_hlp_p_p2'
      obsname( 21)='F_hlp_p_m2'
      obsname( 22)='dPnb'
      obsname( 23)='dQnb'
      obsname( 24)='P_n_var'
      obsname( 25)='V_n_var'
      obsname( 26)='dP_sum'
      obsname( 27)='dP_lim'
      obsname( 28)='Qunit'
      obsname( 29)='ix'
      obsname( 30)='iy'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x( 10)              
      obs(  2)=x( 12)              
      obs(  3)=x(  3)              
      obs(  4)=x(  4)              
      obs(  5)=x( 14)              
      obs(  6)=x( 16)              
      obs(  7)=x( 47)              
      obs(  8)=x( 17)              
      obs(  9)=x( 38)              
      obs( 10)=x(  9)              
      obs( 11)=x( 19)              
      obs( 12)=x( 34)              
      obs( 13)=x( 53)              
      obs( 14)=x( 72)              
      obs( 15)=x( 73)              
      obs( 16)=x( 74)              
      obs( 17)=x( 75)              
      obs( 18)=x( 91)              
      obs( 19)=x( 92)              
      obs( 20)=x( 95)              
      obs( 21)=x( 96)              
      obs( 22)=x(102)              
      obs( 23)=x( 99)              
      obs( 24)=x( 80)              
      obs( 25)=x( 82)              
      obs( 26)=x( 79)              
      obs( 27)=x( 76)              
      obs( 28)=x( 18)              
      obs( 29)=x(  1)              
      obs( 30)=x(  2)              

!........................................................................................
   case (initialize)

!w0 = 1
      prm( 77)= 1

!wb = 2*pi*fnom
      prm( 78)= 2*pi*fnom

!t2 = 0
      prm( 79)= 0

!t1 = 0
      prm( 80)= 0

!t0 = 1
      prm( 81)= 1

!Downlim = -9999
      prm( 82)= -9999

!Uplim = 9999
      prm( 83)= 9999

!Downlimdisc = 0
      prm( 84)= 0

!Uplimdis = 0
      prm( 85)= 0

!downlimdis = -9999
      prm( 86)= -9999

!theta0 = atan([vy]/[vx])
      prm( 87)= atan(vy/vx)

!P0 = ([vx]*[ix]+[vy]*[iy])
      prm( 88)= (vx*ix+vy*iy)

!Q0 = [vy]*[ix]-[vx]*[iy]
      prm( 89)= vy*ix-vx*iy

!P0_unit = -{P0}*{Sbs}/{Sb}
      prm( 90)= -prm( 88)*prm(  2)/prm(  1)

!Q0_unit = -{Q0}*{Sbs}/{Sb}
      prm( 91)= -prm( 89)*prm(  2)/prm(  1)

!V0 = dsqrt([vx]**2+[vy]**2)
      prm( 92)= dsqrt(vx**2+vy**2)

!iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}
      prm( 93)=  (-ix*cos(prm( 87))-iy*sin(prm( 87)))*prm(  2)/prm(  1)

!iQ0 =  (+[ix]*sin({theta0})-[iy]*cos({theta0}))*{Sbs}/{Sb}
      prm( 94)=  (+ix*sin(prm( 87))-iy*cos(prm( 87)))*prm(  2)/prm(  1)

!vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})
      prm( 95)= vx*cos(prm( 87))+vy*sin(prm( 87))

!vq0 = -[vx]*sin({theta0})+[vy]*cos({theta0})
      prm( 96)= -vx*sin(prm( 87))+vy*cos(prm( 87))

!md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})
      prm( 97)= 1/prm(  3)*(prm( 95)+prm( 15)*prm( 77)*prm( 94)-prm( 14)*prm( 93))

!mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
      prm( 98)= 1/prm(  3)*(prm( 96)-prm( 15)*prm( 77)*prm( 93)-prm( 14)*prm( 94))

!idc0 = {md0}*{iP0}+{mq0}*{iQ0}
      prm( 99)= prm( 97)*prm( 93)+prm( 98)*prm( 94)

!iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
      prm(100)= prm( 22)*prm( 18)/(2*prm( 18)**2+2*prm( 19)*prm( 21)) + dsqrt((prm( 22)*prm( 18)/(2*prm( 18)**2+2*prm( 19)*prm( 21)))**2-(prm( 19)*prm( 21)*prm( 23)**2-prm( 21)*prm( 99)*prm(  3))/(prm( 18)**2+prm( 19)*prm( 21)))

!wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
      prm(101)= 1/prm( 21)*(prm( 18)*prm(100)-prm( 22))

!Te0 = {iT0}*{kT}
      prm(102)= prm(100)*prm( 18)

!Tc0 = {Tnm}
      prm(103)= prm( 22)

!{dPs_rate_min} = -{dPs_rate_max}
      prm(104)= -prm( 75)

!{dQs_rate_min} = -{dQs_rate_max}
      prm(105)= -prm( 76)

!vd =  {vd0}
      x(  3)= prm( 95)

!vq =  {vq0}
      x(  4)= prm( 96)

!vdc_ref = {vdc_star}
      x(  5)=prm(  3)

!V =  {V0}
      x(  6)= prm( 92)

!vxm =  [vx]
      x(  7)= vx

!vym =  [vy]
      x(  8)= vy

!Vm =  {V0}
      x(  9)= prm( 92)

!iP =   {iP0}
      x( 10)=  prm( 93)

!diP =  0
      x( 11)= 0

!iQ =   {iQ0}
      x( 12)=  prm( 94)

!diQ =  0
      x( 13)= 0

!P =  {P0}
      x( 14)= prm( 88)

!dp =  0
      x( 15)= 0

!Punit =  {P0_unit}
      x( 16)= prm( 90)

!Q =  {Q0}
      x( 17)= prm( 89)

!Qunit =  {Q0_unit}
      x( 18)= prm( 91)

!Pref_lim =  {P0_unit}
      x( 19)= prm( 90)

!vdc =  {vdc_star}
      x( 20)= prm(  3)

!dvdc =  0
      x( 21)= 0

!iP_ref =  {iP0}
      x( 22)= prm( 93)

!Plim =  {VPmax}
      x( 23)= prm( 34)

!iQ_ref =  {iQ0}
      x( 24)= prm( 94)

!mdr =  {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
      x( 25)= prm( 15)/prm(  3)*prm( 94)*prm( 77)-prm( 97)

!mqr =  -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
      x( 26)= -prm( 15)/prm(  3)*prm( 93)*prm( 77)-prm( 98)

!md =  {md0}
      x( 27)= prm( 97)

!mq =  {mq0}
      x( 28)= prm( 98)

!dvd =  {iP0}*{rt}
      x( 29)= prm( 93)*prm( 14)

!dvq =  {iQ0}*{rt}
      x( 30)= prm( 94)*prm( 14)

!idc =  {idc0}
      x( 31)= prm( 99)

!didc =  {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
      x( 32)= prm( 97)*prm( 93)+prm( 98)*prm( 94)-prm( 99)

!wm_ref =  {wm0}
      x( 33)= prm(101)

!wm_ref_lim =  {wm0}
      x( 34)= prm(101)

!iT_ref =  {iT0}
      x( 35)= prm(100)

!iT =  {iT0}
      x( 36)= prm(100)

!dwm =  0
      x( 37)= 0

!wm =  {wm0}
      x( 38)= prm(101)

!Te =  {kT}*{iT0}
      x( 39)= prm( 18)*prm(100)

!dT =  {kT}*{iT0}-{Tc0}
      x( 40)= prm( 18)*prm(100)-prm(103)

!deltafvh =   {V0}-{Vmax}
      x( 41)=  prm( 92)-prm( 26)

!z1 =  0
      x( 42)= 0

!Fvhi =  1
      x( 43)= 1

!Fvh =  1
      x( 44)= 1

!iPs =  {iP0}
      x( 45)= prm( 93)

!iQs =  {iQ0}
      x( 46)= prm( 94)

!Pref =  {P0_unit}
      x( 47)= prm( 90)

!x10 =  -{V0}
      x( 48)= -prm( 92)

!z = 0
      x( 49)=0

!Fvl =  1
      x( 50)= 1

!Fvli =  1
      x( 51)= 1

!Plim_min =  0
      x( 52)= 0

!status =  1
      x( 53)= 1

!p1 =  {protection}
      x( 54)= prm( 37)

!p2 =  1
      x( 55)= 1

!p3 =  1
      x( 56)= 1

!Fpl1 =  1
      x( 57)= 1

!Fpl0 =  0
      x( 58)= 0

!V_lim_min =  {V_min_nb} * {s_f1}
      x( 59)= prm( 74) * prm( 59)

!V_lim_max =  {V_max_nb} / {s_f1}
      x( 60)= prm( 73) / prm( 59)

!P_lim_min =  {P_min} * {s_f1}
      x( 61)= prm( 39) * prm( 59)

!P_lim_max =  {P_max} / {s_f1}
      x( 62)= prm( 38) / prm( 59)

!pl1 =  {V_min_nb} * {s_f1} - {V0}
      x( 63)= prm( 74) * prm( 59) - prm( 92)

!F_v_min_in =  0
      x( 64)= 0

!pl2 =  {V0} - {V_max_nb} / {s_f1}
      x( 65)= prm( 92) - prm( 73) / prm( 59)

!F_v_max_in =  0
      x( 66)= 0

!F_v_opp =  1
      x( 67)= 1

!pl3 =  {P_min} * {s_f1} - {P0_unit}
      x( 68)= prm( 39) * prm( 59) - prm( 90)

!F_p_min_in =  0
      x( 69)= 0

!pl4 =  {P0_unit} - {P_max} / {s_f1}
      x( 70)= prm( 90) - prm( 38) / prm( 59)

!F_p_max_in =  0
      x( 71)= 0

!F_v_min =  0
      x( 72)= 0

!F_v_max =  0
      x( 73)= 0

!F_p_min =  0
      x( 74)= 0

!F_p_max =  0
      x( 75)= 0

!dP_lim =  0
      x( 76)= 0

!dQ_lim =  0
      x( 77)= 0

!dQ_sum =  0
      x( 78)= 0

!dP_sum =  0
      x( 79)= 0

!P_n_var =  {P_n} + 0.0001
      x( 80)= prm( 51) + 0.0001

!P_n_var_neg =  -{P_n} + 0.0001
      x( 81)= -prm( 51) + 0.0001

!V_n_var =  {V_n}
      x( 82)= prm( 50)

!V_n_var_neg =  -{V_n}
      x( 83)= -prm( 50)

!V_n_abs =  {V_n}
      x( 84)= prm( 50)

!pl5 =  {V_n} - 1
      x( 85)= prm( 50) - 1

!F_help_v =  0
      x( 86)= 0

!F_help_v_p =  0
      x( 87)= 0

!F_help_v_m =  0
      x( 88)= 0

!F_help_p_p =  0
      x( 89)= 0

!F_help_p_m =  0
      x( 90)= 0

!F_hlp_high =  {F_help_high_0}
      x( 91)= prm( 70)

!F_hlp_low =  {F_help_low_0}
      x( 92)= prm( 71)

!F_hlp_v_p2 =  [F_help_v_p] * [F_hlp_high]
      x( 93)= x( 87) * x( 91)

!F_hlp_v_m2 =  [F_help_v_m ]* [F_hlp_low]
      x( 94)= x( 88)* x( 92)

!F_hlp_p_p2 =  [F_help_p_p] * (1-[F_help_v])
      x( 95)= x( 89) * (1-x( 86))

!F_hlp_p_m2 =  [F_help_p_m] * (1-[F_help_v])
      x( 96)= x( 90) * (1-x( 86))

!dQp =  0
      x( 97)= 0

!dQm =  0
      x( 98)= 0

!dQnb =  0
      x( 99)= 0

!dPp =  0
      x(100)= 0

!dPm =  0
      x(101)= 0

!dPnb =  0
      x(102)= 0

!iQnb =  0
      x(103)= 0

!dPc_1_neg =  - {dPc_1_0}
      x(104)= - prm( 40)

!dPc_1_pos =  {dPc_1_0}
      x(105)= prm( 40)

!dPc_2_pos =  {dPc_2_0}
      x(106)= prm( 41)

!dPc_2_neg =  - {dPc_2_0}
      x(107)= - prm( 41)

!dPc_3_pos =  {P_max}/{s_f2} - {P0_unit}
      x(108)= prm( 38)/prm( 60) - prm( 90)

!dPc_3_neg =  - {P0_unit} + {P_min}*{s_f2}
      x(109)= - prm( 90) + prm( 39)*prm( 60)

!level =  {lvl}
      x(110)= prm( 61)

!dPc_1_in =  {dPc_1_0}
      x(111)= prm( 40)

!dPc_2_in =  {dPc_2_0}
      x(112)= prm( 41)

!dPc_1 =  {dPc_1_0}
      x(113)= prm( 40)

!dPc_2 =  {dPc_2_0}
      x(114)= prm( 41)

!dPc_3 =  {P_max}/{s_f2} - {P0_unit}
      x(115)= prm( 38)/prm( 60) - prm( 90)

!dPc_0 =  0
      x(116)= 0

!pl1_1 =  {P0_unit} - ({l_1_n_min_par} * ({P_max}-{P_min}) + {P_min})
      x(117)= prm( 90) - (prm( 44) * (prm( 38)-prm( 39)) + prm( 39))

!pl1_2 =  {pl1_2_0}
      x(118)= prm( 62)

!pl1_3 =  ({l_1_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(119)= (prm( 45) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 90)

!pl1_4 =  {pl1_4_0}
      x(120)= prm( 63)

!F_l_1_neg =  {pl1_2_0} * {pl1_4_0}
      x(121)= prm( 62) * prm( 63)

!pl1_5 =  {P0_unit} - ({l_1_p_min_par} * ({P_max}-{P_min}) + {P_min})
      x(122)= prm( 90) - (prm( 42) * (prm( 38)-prm( 39)) + prm( 39))

!pl1_6 =  {pl1_6_0}
      x(123)= prm( 64)

!pl1_7 =  ({l_1_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(124)= (prm( 43) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 90)

!pl1_8 =  {pl1_8_0}
      x(125)= prm( 65)

!F_l_1_pos =  {pl1_6_0} * {pl1_8_0}
      x(126)= prm( 64) * prm( 65)

!F_l_1 =  [F_l_1_pos]
      x(127)= x(126)

!dPc_1_s_fl =  {dPc_1_0} * [F_l_1]
      x(128)= prm( 40) * x(127)

!pl2_1 =  {P0_unit} - ({l_2_n_min_par} * ({P_max}-{P_min}) + {P_min})
      x(129)= prm( 90) - (prm( 48) * (prm( 38)-prm( 39)) + prm( 39))

!pl2_2 =  {pl2_2_0}
      x(130)= prm( 66)

!pl2_3 =  ({l_2_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(131)= (prm( 49) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 90)

!pl2_4 =  {pl2_4_0}
      x(132)= prm( 67)

!F_l_2_neg =  {pl2_2_0} * {pl2_4_0}
      x(133)= prm( 66) * prm( 67)

!pl2_5 =  {P0_unit} - ({l_2_p_min_par} * ({P_max}-{P_min}) + {P_min})
      x(134)= prm( 90) - (prm( 46) * (prm( 38)-prm( 39)) + prm( 39))

!pl2_6 =  {pl2_6_0}
      x(135)= prm( 68)

!pl2_7 =  ({l_2_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(136)= (prm( 47) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 90)

!pl2_8 =  {pl2_8_0}
      x(137)= prm( 69)

!F_l_2_pos =  {pl2_6_0} * {pl2_8_0}
      x(138)= prm( 68) * prm( 69)

!F_l_2 =  [F_l_2_pos]
      x(139)= x(138)

!dPc_2_s_fl =  {dPc_2_0} * [F_l_2]
      x(140)= prm( 41) * x(139)

!l_abs =  0
      x(141)= 0

!l_switch =  1
      x(142)= 1

!dPc =  0
      x(143)= 0

!dPc_1_in_f =  {dPc_1_0} * [F_l_1]
      x(144)= prm( 40) * x(127)

!dPc_2_in_f =  {dPc_2_0} * [F_l_2]
      x(145)= prm( 41) * x(139)

!dPc_no_bou =  0
      x(146)= 0

!under_min =  [dPc] +0.0001 - [dPc_3_neg]
      x(147)= x(143) +0.0001 - x(109)

!F_p_c_min =  0
      x(148)= 0

!over_max =  - [dPc] -0.0001 + [dPc_3_pos]
      x(149)= - x(143) -0.0001 + x(108)

!F_p_c_max =  0
      x(150)= 0

!theta_est =  {theta0}
      x(151)= prm( 87)

!& algeq                                             ! voltage magnitude
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)=  9
      tc(  2)=prm( 36)

!& tf1p
      eqtyp(  3)=  7
      tc(  3)=prm( 36)

!& tf1p
      eqtyp(  4)=  8
      tc(  4)=prm( 36)

!& algeq                                             ! voltage alignment
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& algeq
      eqtyp(  7)=0

!& algeq                                             ! compute ix
      eqtyp(  8)=0

!& algeq                                             ! compute iy
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq
      eqtyp( 11)=0

!& algeq                                             ! compute powers
      eqtyp( 12)=0

!& algeq
      eqtyp( 13)=0

!& algeq                                             ! compute powers
      eqtyp( 14)=0

!& algeq
      eqtyp( 15)=0

!& algeq
      eqtyp( 16)=0

!& algeq                                             ! DC voltage control
      eqtyp( 17)=0

!& pictl
      eqtyp( 18)=152
      x(152)=x( 22)
      eqtyp( 19)=0

!& algeq                                             ! p-axis current control
      eqtyp( 20)=0

!& pictl
      eqtyp( 21)=153
      x(153)=x( 25)
      eqtyp( 22)=0

!& algeq                                             ! q-axis current control
      eqtyp( 23)=0

!& algeq
      eqtyp( 24)=0

!& pictl
      eqtyp( 25)=154
      x(154)=x( 26)
      eqtyp( 26)=0

!& algeq                                             ! md
      eqtyp( 27)=0

!& algeq                                             ! mq
      eqtyp( 28)=0

!& algeq                                             ! voltage over d-axis terminal impedance
      eqtyp( 29)=0

!& algeq                                             ! voltage over q-axis terminal impedance
      eqtyp( 30)=0

!& tf1p                                              ! d-axis current
      eqtyp( 31)= 10
      tc( 31)=prm( 15)/(prm( 78)*prm( 14))

!& tf1p                                              ! q-axis current
      eqtyp( 32)= 12
      tc( 32)=prm( 15)/(prm( 78)*prm( 14))

!& algeq                                             ! DC link current
      eqtyp( 33)=0

!& algeq                                             ! DC link
      eqtyp( 34)=0

!& int                                               ! DC link voltage
      if (prm( 16)/prm( 78)< 0.005)then
         eqtyp( 35)=0
      else
         eqtyp( 35)= 20
         tc( 35)=prm( 16)/prm( 78)
      endif

!& limvb                                              ! power limiter
      eqtyp( 36)=0
      if(x( 47)>x( 23))then
         z(  1)=1
      elseif(x( 47)<x( 52))then
         z(  1)=-1
      else
         z(  1)=0
      endif

!& algeq                                             ! power mismatch
      eqtyp( 37)=0

!& pictl                                             ! power control
      eqtyp( 38)=155
      x(155)=x( 33)
      eqtyp( 39)=0

!& lim                                               ! limit speed control input
      eqtyp( 40)=0
      if(x( 33)>prm( 25))then
         z(  2)=1
      elseif(x( 33)<prm( 24))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq                                             ! speed control input
      eqtyp( 41)=0

!& pictl                                             ! speed control
      eqtyp( 42)=156
      x(156)=x( 35)
      eqtyp( 43)=0

!& tf1p                                              ! motor current control
      eqtyp( 44)= 36
      tc( 44)=1/prm( 13)

!& algeq                                             ! torque equations
      eqtyp( 45)=0

!& algeq
      eqtyp( 46)=0

!& tf1p                                              ! motor inertia
      eqtyp( 47)= 38
      tc( 47)=2*prm( 20)/prm( 21)

!& algeq
      eqtyp( 48)=0

!& pwlin4                                            ! overvoltage Protection
      eqtyp( 49)=0
      if(x( 41)<(-999))then
         z(  3)=1
      elseif(x( 41)>=999)then
         z(  3)=   3
      elseif((-999)<=x( 41) .and. x( 41)<0.)then
         z(  3)=  1
      elseif(0.<=x( 41) .and. x( 41)<0.)then
         z(  3)=  2
      elseif(0.<=x( 41) .and. x( 41)<999)then
         z(  3)=  3
      endif

!& algeq
      eqtyp( 50)=0

!& hyst
      eqtyp( 51)=0
      if(x( 43)>1.1)then
         z(  4)=1
      elseif(x( 43)<0.9)then
         z(  4)=-1
      else
         if(1.>= 0.)then
            z(  4)=1
         else
            z(  4)=-1
         endif
      endif

!& algeq                                             ! LVRT
      eqtyp( 52)=0

!& timer5
      eqtyp( 53)=0
      eqtyp( 54)=0
      z(  5)=-1
      x(157)=0.

!& algeq
      eqtyp( 55)=0

!& hyst
      eqtyp( 56)=0
      if(x( 51)>1.1)then
         z(  6)=1
      elseif(x( 51)<0.9)then
         z(  6)=-1
      else
         if(1.>= 0.)then
            z(  6)=1
         else
            z(  6)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      eqtyp( 57)=0

!& algeq
      eqtyp( 58)=0

!& algeq
      eqtyp( 59)=0

!& swsign
      eqtyp( 60)=0
      if(x( 54)>=0.)then
         z(  7)=1
      else
         z(  7)=2
      endif

!& algeq                                             ! min and max power limiters
      eqtyp( 61)=0

!& algeq
      eqtyp( 62)=0

!& algeq                                             ! status check without droop
      eqtyp( 63)=0

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      eqtyp( 64)=0

!& algeq
      eqtyp( 65)=0

!& algeq												! NEIGHBOURHOOD CONTROL
      eqtyp( 66)=0

!& algeq
      eqtyp( 67)=0

!& swsign											! F_help_p_p is 1 if P_n > 0
      eqtyp( 68)=0
      if(x( 81)>=0.)then
         z(  8)=1
      else
         z(  8)=2
      endif

!& swsign											! F_help_p_m is 1 if P_n < 0
      eqtyp( 69)=0
      if(x( 80)>=0.)then
         z(  9)=1
      else
         z(  9)=2
      endif

!& hyst ! should it be V or Vm?
      eqtyp( 70)=0
      if(x(  9)>prm( 52))then
         z( 10)=1
      elseif(x(  9)<prm( 53))then
         z( 10)=-1
      else
         if((-1.)>= 0.)then
            z( 10)=1
         else
            z( 10)=-1
         endif
      endif

!& hyst
      eqtyp( 71)=0
      if(x(  9)>prm( 52))then
         z( 11)=1
      elseif(x(  9)<prm( 53))then
         z( 11)=-1
      else
         if(1.>= 0.)then
            z( 11)=1
         else
            z( 11)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      eqtyp( 72)=0

!& abs
      eqtyp( 73)=0
      if(x( 82)>0. )then
         z( 12)=1
      else
         z( 12)=-1
      endif

!& algeq
      eqtyp( 74)=0

!& swsign
      eqtyp( 75)=0
      if(x( 85)>=0.)then
         z( 13)=1
      else
         z( 13)=2
      endif

!& algeq												! voltage help required
      eqtyp( 76)=0

!& swsign											! F_help_v_p is 1 if V_n > 0
      eqtyp( 77)=0
      if(x( 83)>=0.)then
         z( 14)=1
      else
         z( 14)=2
      endif

!& swsign											! F_help_v_m is 1 if V_n < 0
      eqtyp( 78)=0
      if(x( 82)>=0.)then
         z( 15)=1
      else
         z( 15)=2
      endif

!& algeq
      eqtyp( 79)=0

!& algeq
      eqtyp( 80)=0

!& algeq												! priority of voltage over power
      eqtyp( 81)=0

!& algeq
      eqtyp( 82)=0

!& algeq												! reactive power computation
      eqtyp( 83)=0

!& algeq
      eqtyp( 84)=0

!& algeq
      eqtyp( 85)=0

!& algeq												! active power computation
      eqtyp( 86)=0

!& algeq
      eqtyp( 87)=0

!& algeq
      eqtyp( 88)=0

!& algeq												! UNIT LIMIT	!s_f1 = security factor
      eqtyp( 89)=0

!& algeq
      eqtyp( 90)=0

!& algeq
      eqtyp( 91)=0

!& algeq
      eqtyp( 92)=0

!& algeq 											! voltage low limit flag
      eqtyp( 93)=0

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      eqtyp( 94)=0
      if(x( 63)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& algeq												! voltage high limit flag
      eqtyp( 95)=0

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      eqtyp( 96)=0
      if(x( 65)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      eqtyp( 97)=0

!& algeq												! power low limit flag
      eqtyp( 98)=0

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      eqtyp( 99)=0
      if(x( 68)>=0.)then
         z( 18)=1
      else
         z( 18)=2
      endif

!& algeq												! power high limit flag
      eqtyp(100)=0

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      eqtyp(101)=0
      if(x( 70)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& algeq												! if the units are off the limit flags go to 0
      eqtyp(102)=0

!& algeq
      eqtyp(103)=0

!& algeq
      eqtyp(104)=0

!& algeq
      eqtyp(105)=0

!& algeq
      eqtyp(106)=0

!& tf1p2lim
      if(prm( 72)< 0.001)then
         prm( 72)=0.d0
         prm( 82)=-huge(0.d0)
         prm( 83)= huge(0.d0)
         prm(104)=-huge(0.d0)
         prm( 75)= huge(0.d0)
      endif
      if(1*x( 76)-x( 79)>prm( 75)*prm( 72))then
         z( 20)=1
      elseif(1*x( 76)-x( 79)<prm(104)*prm( 72))then
         z( 20)=-1
      else
         z( 20)=0
      endif
      eqtyp(107)=0
      if(x( 79)>prm( 83))then
         z( 21)=1
         eqtyp(108)=0
      elseif(x( 79)<prm( 82))then
         z( 21)=-1
         eqtyp(108)=0
      else
         z( 21)=0
         eqtyp(108)= 79
      endif
      tc(108)=prm( 72)

!& algeq
      eqtyp(109)=0

!& tf1p2lim
      if(prm( 72)< 0.001)then
         prm( 72)=0.d0
         prm( 82)=-huge(0.d0)
         prm( 83)= huge(0.d0)
         prm(105)=-huge(0.d0)
         prm( 76)= huge(0.d0)
      endif
      if(1*x( 77)-x( 78)>prm( 76)*prm( 72))then
         z( 22)=1
      elseif(1*x( 77)-x( 78)<prm(105)*prm( 72))then
         z( 22)=-1
      else
         z( 22)=0
      endif
      eqtyp(110)=0
      if(x( 78)>prm( 83))then
         z( 23)=1
         eqtyp(111)=0
      elseif(x( 78)<prm( 82))then
         z( 23)=-1
         eqtyp(111)=0
      else
         z( 23)=0
         eqtyp(111)= 78
      endif
      tc(111)=prm( 72)

!& algeq
      eqtyp(112)=0

!& algeq												! CENTRAL CONTROL, positive and negative variation
      eqtyp(113)=0

!& algeq
      eqtyp(114)=0

!& algeq
      eqtyp(115)=0

!& algeq
      eqtyp(116)=0

!& algeq
      eqtyp(117)=0

!& algeq
      eqtyp(118)=0

!& algeq
      eqtyp(119)=0

!& swsign
      eqtyp(120)=0
      if(x(110)>=0.)then
         z( 24)=1
      else
         z( 24)=2
      endif

!& swsign
      eqtyp(121)=0
      if(x(110)>=0.)then
         z( 25)=1
      else
         z( 25)=2
      endif

!& swsign
      eqtyp(122)=0
      if(x(110)>=0.)then
         z( 26)=1
      else
         z( 26)=2
      endif

!& limvb												! limiting the variation to the max and min bounds
      eqtyp(123)=0
      if(x(111)>x(108))then
         z( 27)=1
      elseif(x(111)<x(109))then
         z( 27)=-1
      else
         z( 27)=0
      endif

!& limvb
      eqtyp(124)=0
      if(x(112)>x(108))then
         z( 28)=1
      elseif(x(112)<x(109))then
         z( 28)=-1
      else
         z( 28)=0
      endif

!& algeq !dPc_0 is equal to 0
      eqtyp(125)=0

!& algeq												! level 1 low negative
      eqtyp(126)=0

!& swsign
      eqtyp(127)=0
      if(x(117)>=0.)then
         z( 29)=1
      else
         z( 29)=2
      endif

!& algeq												! level 1 high negative
      eqtyp(128)=0

!& swsign
      eqtyp(129)=0
      if(x(119)>=0.)then
         z( 30)=1
      else
         z( 30)=2
      endif

!& algeq												! level 1 negative flag
      eqtyp(130)=0

!& algeq												! level 1 low positive
      eqtyp(131)=0

!& swsign
      eqtyp(132)=0
      if(x(122)>=0.)then
         z( 31)=1
      else
         z( 31)=2
      endif

!& algeq												! level 1 high positive
      eqtyp(133)=0

!& swsign
      eqtyp(134)=0
      if(x(124)>=0.)then
         z( 32)=1
      else
         z( 32)=2
      endif

!& algeq												! level 1 positive flag
      eqtyp(135)=0

!& swsign
      eqtyp(136)=0
      if(x(110)>=0.)then
         z( 33)=1
      else
         z( 33)=2
      endif

!& algeq
      eqtyp(137)=0

!& algeq												! level 2 low negative
      eqtyp(138)=0

!& swsign
      eqtyp(139)=0
      if(x(129)>=0.)then
         z( 34)=1
      else
         z( 34)=2
      endif

!& algeq												! level 2 high negative
      eqtyp(140)=0

!& swsign
      eqtyp(141)=0
      if(x(131)>=0.)then
         z( 35)=1
      else
         z( 35)=2
      endif

!& algeq												! level 2 negative flag
      eqtyp(142)=0

!& algeq												! level 2 low positive
      eqtyp(143)=0

!& swsign
      eqtyp(144)=0
      if(x(134)>=0.)then
         z( 36)=1
      else
         z( 36)=2
      endif

!& algeq												! level 2 high positive
      eqtyp(145)=0

!& swsign
      eqtyp(146)=0
      if(x(136)>=0.)then
         z( 37)=1
      else
         z( 37)=2
      endif

!& algeq												! level 2 positive flag
      eqtyp(147)=0

!& swsign
      eqtyp(148)=0
      if(x(110)>=0.)then
         z( 38)=1
      else
         z( 38)=2
      endif

!& algeq
      eqtyp(149)=0

!& abs												! absolute of the level
      eqtyp(150)=0
      if(x(110)>0. )then
         z( 39)=1
      else
         z( 39)=-1
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      eqtyp(151)=0

!& switch4											! choice between the levels
      eqtyp(152)=0
      z( 40)=max(1,min(  4,nint(x(142))))

!& algeq
      eqtyp(153)=0

!& algeq
      eqtyp(154)=0

!& switch4											! choice between the levels without bounds
      eqtyp(155)=0
      z( 41)=max(1,min(  4,nint(x(142))))

!& algeq													! checking if the variation should have been greater than the bounds
      eqtyp(156)=0

!& swsign
      eqtyp(157)=0
      if(x(147)>=0.)then
         z( 42)=1
      else
         z( 42)=2
      endif

!& algeq													! checking if the variation should have been greater than the bounds
      eqtyp(158)=0

!& swsign
      eqtyp(159)=0
      if(x(149)>=0.)then
         z( 43)=1
      else
         z( 43)=2
      endif

!........................................................................................
   case (evaluate_eqs)

!& algeq                                             ! voltage magnitude
      f(  1)=dsqrt(vx**2+vy**2) - x(  6)

!& tf1p
      f(  2)=(-x(  9)+1.*x(  6))

!& tf1p
      f(  3)=(-x(  7)+1.*vx    )

!& tf1p
      f(  4)=(-x(  8)+1.*vy    )

!& algeq                                             ! voltage alignment
      f(  5)=-x(  3) + x(  7)*cos(x(151))+x(  8)*sin(x(151))

!& algeq
      f(  6)=-x(  4) - x(  7)*sin(x(151))+x(  8)*cos(x(151))

!& algeq
      f(  7)=-x(151) + atan(x(  8)/x(  7))

!& algeq                                             ! compute ix
      f(  8)=-x(  1) + (-x( 45)*cos(x(151))+x( 46)*sin(x(151)))*prm(  1)/prm(  2)

!& algeq                                             ! compute iy
      f(  9)=-x(  2) + (-x( 45)*sin(x(151))-x( 46)*cos(x(151)))*prm(  1)/prm(  2)

!& algeq
      f( 10)=-x( 45) + x( 10)*x( 53)

!& algeq
      f( 11)=-x( 46) + x( 12)*x( 53)

!& algeq                                             ! compute powers
      f( 12)=-x( 14)*prm(  2)/prm(  1) -x( 16)                            ! + x(  1)*vx+x(  2)*vy

!& algeq
      f( 13)=-x( 17)*prm(  2)/prm(  1) - x( 18)                           ! -vx*x(  2) + vy*x(  1)

!& algeq                                             ! compute powers
      f( 14)=-x( 16) + x(  3)*x( 45) + x(  4)*x( 46)                              ! - x( 14)*prm(  2)/prm(  1)

!& algeq
      f( 15)=-x( 18) - x(  3)*x( 46) + x(  4)*x( 45)                              ! - x( 17)*prm(  2)/prm(  1)

!& algeq
      f( 16)=-x(  5) + prm(  3)*x( 53)

!& algeq                                             ! DC voltage control
      f( 17)=-x( 21) + x( 53)*x(  5) - x( 20)

!& pictl
      f( 18)=prm(  6)                                                                                                                                                                                                                                                                                                    *x( 21)
      f( 19)=prm(  5)                                                                                                                                                                                                                                                                                                    *x( 21)+x(152)-x( 22)

!& algeq                                             ! p-axis current control
      f( 20)=-x( 11) + x( 22) -x( 10)

!& pictl
      f( 21)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 11)
      f( 22)=prm(  7)                                                                                                                                                                                                                                                                                                    *x( 11)+x(153)-x( 25)

!& algeq                                             ! q-axis current control
      f( 23)=-x( 24) + (x( 22)/prm(  4))*dsqrt(1-prm(  4)**2)

!& algeq
      f( 24)=-x( 13) + x( 24) -x( 12) + x(103)

!& pictl
      f( 25)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 13)
      f( 26)=prm(  7)                                                                                                                                                                                                                                                                                                    *x( 13)+x(154)-x( 26)

!& algeq                                             ! md
      f( 27)=-x( 27) + x( 53)*(-x( 25) + prm( 15)/max(1.d-3,prm(  3))*omega*x( 12))

!& algeq                                             ! mq
      f( 28)=-x( 28) + x( 53)*(-x( 26) - prm( 15)/max(1.d-3,prm(  3))*omega*x( 10))

!& algeq                                             ! voltage over d-axis terminal impedance
      f( 29)=-x( 29) + x(  3) - x( 27) * x( 20) + prm( 15)*omega*x( 12)

!& algeq                                             ! voltage over q-axis terminal impedance
      f( 30)=-x( 30) + x(  4) - x( 28) * x( 20) - prm( 15)*omega*x( 10)

!& tf1p                                              ! d-axis current
      f( 31)=(-x( 10)+1/prm( 14)*x( 29))

!& tf1p                                              ! q-axis current
      f( 32)=(-x( 12)+1/prm( 14)*x( 30))

!& algeq                                             ! DC link current
      f( 33)=-x( 31) + x( 53)*(x( 38)*prm( 18)*x( 36) + prm( 19)*x( 36)**2) / max(1.d-3,x( 20))

!& algeq                                             ! DC link
      f( 34)=-x( 32)+x( 27)*x( 10)+x( 28)*x( 12)-x( 31)

!& int                                               ! DC link voltage
      if (prm( 16)/prm( 78)< 0.005)then
         f( 35)=x( 32)-x( 20)
      else
         f( 35)=x( 32)
      endif

!& limvb                                              ! power limiter
      select case (z(  1))
         case(0)
            f( 36)=x( 19)-x( 47)
         case(-1)
            f( 36)=x( 19)-x( 52)
         case(1)
            f( 36)=x( 19)-x( 23)
      end select

!& algeq                                             ! power mismatch
      f( 37)=-x( 15) + x( 19)-x( 16) + x( 79)

!& pictl                                             ! power control
      f( 38)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 15)
      f( 39)=prm(  9)                                                                                                                                                                                                                                                                                                    *x( 15)+x(155)-x( 33)

!& lim                                               ! limit speed control input
      select case (z(  2))
         case(0)
            f( 40)=x( 34)-x( 33)
         case(-1)
            f( 40)=x( 34)-prm( 24)
         case(1)
            f( 40)=x( 34)-prm( 25)
      end select

!& algeq                                             ! speed control input
      f( 41)=-x( 37) + prm( 17)*(x( 53)*x( 34)-x( 38))

!& pictl                                             ! speed control
      f( 42)=prm( 12)                                                                                                                                                                                                                                                                                                    *x( 37)
      f( 43)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 37)+x(156)-x( 35)

!& tf1p                                              ! motor current control
      f( 44)=(-x( 36)+1*x( 35))

!& algeq                                             ! torque equations
      f( 45)=-x( 39) + prm( 18)*x( 36)

!& algeq
      f( 46)=-x( 40) + x( 39)-prm(103)

!& tf1p                                              ! motor inertia
      f( 47)=(-x( 38)+1/prm( 21)*x( 40))

!& algeq
      f( 48)=x( 41) - x(  9) +prm( 26)

!& pwlin4                                            ! overvoltage Protection
      select case (z(  3))
         case (  1)
            f( 49)=0.+ ( (0.-0.)*(x( 41)-(-999))/(0.-(-999)) ) -x( 42)
         case (  2)
            f( 49)=0.+ ( (1.-0.)*(x( 41)-0.)/(0.-0.) ) -x( 42)
         case (  3)
            f( 49)=1.+ ( (1.-1.)*(x( 41)-0.)/(999-0.) ) -x( 42)
      end select

!& algeq
      f( 50)=x( 43) -1 + x( 42)

!& hyst
      if(z(  4) == 1)then
         f( 51)=x( 44)-1.-(1.-1.)*(x( 43)-1.1)/(1.1-0.9)
      else
         f( 51)=x( 44)-0.-(0.-0.)*(x( 43)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! LVRT
      f( 52)=x(  9) + x( 48)

!& timer5
      select case (z(  5))
         case (-1)
            f( 53)=x( 49)
            f( 54)=x(157)
         case (0)
            f( 53)=x( 49)
            f( 54)= 1.
         case (1)
            f( 53)=x( 49)-1.
            f( 54)= 0.
      end select

!& algeq
      f( 55)=x( 51) -1 + x( 49)

!& hyst
      if(z(  6) == 1)then
         f( 56)=x( 50)-1.-(1.-1.)*(x( 51)-1.1)/(1.1-0.9)
      else
         f( 56)=x( 50)-0.-(0.-0.)*(x( 51)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      f( 57)=x( 55) - x( 44)*x( 50)

!& algeq
      f( 58)=x( 54) - prm( 37)

!& algeq
      f( 59)=x( 56) - 1

!& swsign
      select case (z(  7))
         case(1)
            f( 60)=x( 53)-x( 55)
         case(2)
            f( 60)=x( 53)-x( 56)
      end select

!& algeq                                             ! min and max power limiters
      f( 61)=x( 52)

!& algeq
      f( 62)=-x( 23) + prm( 34)

!& algeq                                             ! status check without droop
      f( 63)=- x( 47) +  x( 53)*prm( 90)

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      f( 64)=x( 58)

!& algeq
      f( 65)=x( 57) - 1

!& algeq												! NEIGHBOURHOOD CONTROL
      f( 66)=-x( 80) + prm( 51) + 0.0001

!& algeq
      f( 67)=-x( 81) - prm( 51) + 0.0001

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z(  8))
         case(1)
            f( 68)=x( 89)-x( 58)
         case(2)
            f( 68)=x( 89)-x( 57)
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z(  9))
         case(1)
            f( 69)=x( 90)-x( 58)
         case(2)
            f( 69)=x( 90)-x( 57)
      end select

!& hyst ! should it be V or Vm?
      if(z( 10) == 1)then
         f( 70)=x( 91)-1.-(1.-1.)*(x(  9)-prm( 52))/(prm( 52)-prm( 53))
      else
         f( 70)=x( 91)-0.-(0.-0.)*(x(  9)-prm( 53))/(prm( 52)-prm( 53))
      endif

!& hyst
      if(z( 11) == 1)then
         f( 71)=x( 92)-0.-(0.-0.)*(x(  9)-prm( 52))/(prm( 52)-prm( 53))
      else
         f( 71)=x( 92)-1.-(1.-1.)*(x(  9)-prm( 53))/(prm( 52)-prm( 53))
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      f( 72)=-x( 82) + prm( 50)

!& abs
      if(z( 12) == 1 )then
         f( 73)=x( 84)-x( 82)
      else
         f( 73)=x( 84)+x( 82)
      endif

!& algeq
      f( 74)=-x( 85) + x( 84) - 1

!& swsign
      select case (z( 13))
         case(1)
            f( 75)=x( 86)-x( 57)
         case(2)
            f( 75)=x( 86)-x( 58)
      end select

!& algeq												! voltage help required
      f( 76)=-x( 83) - prm( 50)

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 14))
         case(1)
            f( 77)=x( 87)-x( 58)
         case(2)
            f( 77)=x( 87)-x( 57)
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 15))
         case(1)
            f( 78)=x( 88)-x( 58)
         case(2)
            f( 78)=x( 88)-x( 57)
      end select

!& algeq
      f( 79)=-x( 93) + x( 87)*x( 91)

!& algeq
      f( 80)=-x( 94) + x( 88)*x( 92)

!& algeq												! priority of voltage over power
      f( 81)=-x( 95) + x( 89)*(1-x( 86))

!& algeq
      f( 82)=-x( 96) + x( 90)*(1-x( 86))

!& algeq												! reactive power computation
      f( 83)=-x( 97) - prm( 50)*prm( 54)*prm( 57)*prm( 57)*x( 93)/(prm( 57)-prm( 58)+prm( 55)*prm( 57)*prm( 50))

!& algeq
      f( 84)=-x( 98) + prm( 50)*prm( 54)*prm( 57)*prm( 58)*x( 94)/(prm( 57)-prm( 58)-prm( 55)*prm( 57)*prm( 50))

!& algeq
      f( 85)=-x( 99) + x( 97) + x( 98)

!& algeq												! active power computation
      f( 86)=-x(100) + prm( 51)*prm( 56)*prm( 38)*(prm( 38)-x( 16))/(prm( 38)-prm( 39))*x( 95)

!& algeq
      f( 87)=-x(101) + prm( 51)*prm( 56)*prm( 38)*(x( 16)-prm( 39))/(prm( 38)-prm( 39))*x( 96)

!& algeq
      f( 88)=-x(102) + x(100) + x(101)

!& algeq												! UNIT LIMIT	!s_f1 = security factor
      f( 89)=-x( 59) + prm( 74)*prm( 59)

!& algeq
      f( 90)=-x( 60) + prm( 73)/prm( 59)

!& algeq
      f( 91)=-x( 61) + prm( 39)*prm( 59)

!& algeq
      f( 92)=-x( 62) + prm( 38)/prm( 59)

!& algeq 											! voltage low limit flag
      f( 93)=-x( 63) + x( 59) - x(  9)

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 16))
         case(1)
            f( 94)=x( 64)-x( 57)
         case(2)
            f( 94)=x( 64)-x( 58)
      end select

!& algeq												! voltage high limit flag
      f( 95)=-x( 65) + x(  9) - x( 60)

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 17))
         case(1)
            f( 96)=x( 66)-x( 57)
         case(2)
            f( 96)=x( 66)-x( 58)
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      f( 97)=-x( 67) + (1-x( 64))*(1-x( 66))

!& algeq												! power low limit flag
      f( 98)=-x( 68) + x( 61) - x( 16)

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 18))
         case(1)
            f( 99)=x( 69)-x( 67)
         case(2)
            f( 99)=x( 69)-x( 58)
      end select

!& algeq												! power high limit flag
      f(100)=-x( 70) + x( 16) - x( 62)

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 19))
         case(1)
            f(101)=x( 71)-x( 67)
         case(2)
            f(101)=x( 71)-x( 58)
      end select

!& algeq												! if the units are off the limit flags go to 0
      f(102)=-x( 72) + x( 64)*x( 53)

!& algeq
      f(103)=-x( 73) + x( 66)*x( 53)

!& algeq
      f(104)=-x( 74) + (1 - (1-x( 69)) * (1-x(148))) * x( 53)

!& algeq
      f(105)=-x( 75) + (1 - (1-x( 71)) * (1-x(150))) * x( 53)

!& algeq
      f(106)=-x( 76) + (x(102) + x(143))

!& tf1p2lim
      select case (z( 20))
         case(0)
            f(107)=x(158)-1*x( 76)+x( 79)
         case(1)
            f(107)=x(158)-prm( 75)*prm( 72)
         case(-1)
            f(107)=x(158)-prm(104)*prm( 72)
      end select
      select case (z( 21))
         case(0)
            f(108)=x(158)
         case(1)
            f(108)=x( 79)-prm( 83)
         case(-1)
            f(108)=x( 79)-prm( 82)
      end select

!& algeq
      f(109)=-x( 77) + x( 99)

!& tf1p2lim
      select case (z( 22))
         case(0)
            f(110)=x(159)-1*x( 77)+x( 78)
         case(1)
            f(110)=x(159)-prm( 76)*prm( 72)
         case(-1)
            f(110)=x(159)-prm(105)*prm( 72)
      end select
      select case (z( 23))
         case(0)
            f(111)=x(159)
         case(1)
            f(111)=x( 78)-prm( 83)
         case(-1)
            f(111)=x( 78)-prm( 82)
      end select

!& algeq
      f(112)=-x(103) + x( 78)

!& algeq												! CENTRAL CONTROL, positive and negative variation
      f(113)=-x(105) + prm( 40)

!& algeq
      f(114)=-x(106) + prm( 41)

!& algeq
      f(115)=x(104) + x(105)

!& algeq
      f(116)=x(107) + x(106)

!& algeq
      f(117)=-x(108) + prm( 38)/prm( 60) - x( 19)

!& algeq
      f(118)=-x(109) - x( 19) + prm( 39)*prm( 60)

!& algeq
      f(119)=-x(110) + prm( 61)

!& swsign
      select case (z( 24))
         case(1)
            f(120)=x(111)-x(105)
         case(2)
            f(120)=x(111)-x(104)
      end select

!& swsign
      select case (z( 25))
         case(1)
            f(121)=x(112)-x(106)
         case(2)
            f(121)=x(112)-x(107)
      end select

!& swsign
      select case (z( 26))
         case(1)
            f(122)=x(115)-x(108)
         case(2)
            f(122)=x(115)-x(109)
      end select

!& limvb												! limiting the variation to the max and min bounds
      select case (z( 27))
         case(0)
            f(123)=x(113)-x(111)
         case(-1)
            f(123)=x(113)-x(109)
         case(1)
            f(123)=x(113)-x(108)
      end select

!& limvb
      select case (z( 28))
         case(0)
            f(124)=x(114)-x(112)
         case(-1)
            f(124)=x(114)-x(109)
         case(1)
            f(124)=x(114)-x(108)
      end select

!& algeq !dPc_0 is equal to 0
      f(125)=x(116)

!& algeq												! level 1 low negative
      f(126)=-x(117) + x( 19) - (prm( 44) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 29))
         case(1)
            f(127)=x(118)-x( 57)
         case(2)
            f(127)=x(118)-x( 58)
      end select

!& algeq												! level 1 high negative
      f(128)=-x(119) + (prm( 45) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 30))
         case(1)
            f(129)=x(120)-x( 57)
         case(2)
            f(129)=x(120)-x( 58)
      end select

!& algeq												! level 1 negative flag
      f(130)=-x(121) + (x(118) * x(120))

!& algeq												! level 1 low positive
      f(131)=-x(122) + x( 19) - (prm( 42) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 31))
         case(1)
            f(132)=x(123)-x( 57)
         case(2)
            f(132)=x(123)-x( 58)
      end select

!& algeq												! level 1 high positive
      f(133)=-x(124) + (prm( 43) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 32))
         case(1)
            f(134)=x(125)-x( 57)
         case(2)
            f(134)=x(125)-x( 58)
      end select

!& algeq												! level 1 positive flag
      f(135)=-x(126) + (x(123) * x(125))

!& swsign
      select case (z( 33))
         case(1)
            f(136)=x(127)-x(126)
         case(2)
            f(136)=x(127)-x(121)
      end select

!& algeq
      f(137)=-x(128) + x(113) * x(127)

!& algeq												! level 2 low negative
      f(138)=-x(129) + x( 19) - (prm( 48) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 34))
         case(1)
            f(139)=x(130)-x( 57)
         case(2)
            f(139)=x(130)-x( 58)
      end select

!& algeq												! level 2 high negative
      f(140)=-x(131) + (prm( 49) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 35))
         case(1)
            f(141)=x(132)-x( 57)
         case(2)
            f(141)=x(132)-x( 58)
      end select

!& algeq												! level 2 negative flag
      f(142)=-x(133) + (x(130) * x(132))

!& algeq												! level 2 low positive
      f(143)=-x(134) + x( 19) - (prm( 46) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 36))
         case(1)
            f(144)=x(135)-x( 57)
         case(2)
            f(144)=x(135)-x( 58)
      end select

!& algeq												! level 2 high positive
      f(145)=-x(136) + (prm( 47) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 37))
         case(1)
            f(146)=x(137)-x( 57)
         case(2)
            f(146)=x(137)-x( 58)
      end select

!& algeq												! level 2 positive flag
      f(147)=-x(138) + (x(135) * x(137))

!& swsign
      select case (z( 38))
         case(1)
            f(148)=x(139)-x(138)
         case(2)
            f(148)=x(139)-x(133)
      end select

!& algeq
      f(149)=-x(140) + x(114) * x(139)

!& abs												! absolute of the level
      if(z( 39) == 1 )then
         f(150)=x(141)-x(110)
      else
         f(150)=x(141)+x(110)
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      f(151)=-x(142) + x(141) + 1

!& switch4											! choice between the levels
      select case (z( 40))
         case(  1)
            f(152)=x(143)-x(116)
         case(  2)
            f(152)=x(143)-x(128)
         case(  3)
            f(152)=x(143)-x(140)
         case(  4)
            f(152)=x(143)-x(115)
      end select

!& algeq
      f(153)=-x(144) + x(111) * x(127)

!& algeq
      f(154)=-x(145) + x(112) * x(139)

!& switch4											! choice between the levels without bounds
      select case (z( 41))
         case(  1)
            f(155)=x(146)-x(116)
         case(  2)
            f(155)=x(146)-x(144)
         case(  3)
            f(155)=x(146)-x(145)
         case(  4)
            f(155)=x(146)-x(115)
      end select

!& algeq													! checking if the variation should have been greater than the bounds
      f(156)=-x(147) + x(146)+0.0001 - x(109)

!& swsign
      select case (z( 42))
         case(1)
            f(157)=x(148)-x( 58)
         case(2)
            f(157)=x(148)-x( 57)
      end select

!& algeq													! checking if the variation should have been greater than the bounds
      f(158)=-x(149) - x(146)-0.0001 + x(108)

!& swsign
      select case (z( 43))
         case(1)
            f(159)=x(150)-x( 58)
         case(2)
            f(159)=x(150)-x( 57)
      end select

!........................................................................................
   case (update_disc)

!& algeq                                             ! voltage magnitude

!& tf1p

!& tf1p

!& tf1p

!& algeq                                             ! voltage alignment

!& algeq

!& algeq

!& algeq                                             ! compute ix

!& algeq                                             ! compute iy

!& algeq

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq

!& algeq                                             ! DC voltage control

!& pictl

!& algeq                                             ! p-axis current control

!& pictl

!& algeq                                             ! q-axis current control

!& algeq

!& pictl

!& algeq                                             ! md

!& algeq                                             ! mq

!& algeq                                             ! voltage over d-axis terminal impedance

!& algeq                                             ! voltage over q-axis terminal impedance

!& tf1p                                              ! d-axis current

!& tf1p                                              ! q-axis current

!& algeq                                             ! DC link current

!& algeq                                             ! DC link

!& int                                               ! DC link voltage

!& limvb                                              ! power limiter
      select case (z(  1))
         case(0)
            if(x( 47)>x( 23))then
               z(  1)=1
            elseif(x( 47)<x( 52))then
               z(  1)=-1
            endif
         case(-1)
            if(x( 47)>x( 52))then
               z(  1)=0
            endif
         case(1)
            if(x( 47)<x( 23))then
               z(  1)=0
            endif
      end select

!& algeq                                             ! power mismatch

!& pictl                                             ! power control

!& lim                                               ! limit speed control input
      select case (z(  2))
         case(0)
            if(x( 33)>prm( 25))then
               z(  2)=1
            elseif(x( 33)<prm( 24))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 33)>prm( 24))then
               z(  2)=0
            endif
         case(1)
            if(x( 33)<prm( 25))then
               z(  2)=0
            endif
      end select

!& algeq                                             ! speed control input

!& pictl                                             ! speed control

!& tf1p                                              ! motor current control

!& algeq                                             ! torque equations

!& algeq

!& tf1p                                              ! motor inertia

!& algeq

!& pwlin4                                            ! overvoltage Protection
      if(x( 41)<(-999))then
         z(  3)=1
      elseif(x( 41)>=999)then
         z(  3)=  3
      elseif((-999)<=x( 41) .and. x( 41)<0.)then
         z(  3)=  1
      elseif(0.<=x( 41) .and. x( 41)<0.)then
         z(  3)=  2
      elseif(0.<=x( 41) .and. x( 41)<999)then
         z(  3)=  3
      endif

!& algeq

!& hyst
      if (z(  4) == -1)then
         if(x( 43)>1.1)then
            z(  4)=1
         endif
      else
         if(x( 43)<0.9)then
            z(  4)=-1
         endif
      endif

!& algeq                                             ! LVRT

!& timer5
      if(z(  5) == -1)then
         if(x( 48) >= (-prm( 29)))then
            z(  5)=0
            eqtyp( 54)=157
         endif
      else
         if(x( 48) < (-prm( 29)))then
            z(  5)=-1
            eqtyp( 54)=0
         endif
      endif
      if(z(  5) == 0)then
         if(x( 48) > (-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))then
            if(x(157) > 0.)then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))then
            if(x(157) > prm( 30)+(0.-prm( 30))*(x( 48)-(-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))then
            if(x(157) > prm( 30)+(prm( 30)-prm( 30))*(x( 48)-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))then
            if(x(157) > prm( 32)+(prm( 30)-prm( 32))*(x( 48)-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-prm( 29)))then
            if(x(157) > prm( 31)+(prm( 32)-prm( 31))*(x( 48)-(-prm( 29)))/((-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))-(-prm( 29))))then
               z(  5)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  6) == -1)then
         if(x( 51)>1.1)then
            z(  6)=1
         endif
      else
         if(x( 51)<0.9)then
            z(  6)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off

!& algeq

!& algeq

!& swsign
      select case (z(  7))
         case(1)
            if(x( 54)<0.)then
               z(  7)=2
            endif
         case(2)
            if(x( 54)>=0.)then
               z(  7)=1
            endif
      end select

!& algeq                                             ! min and max power limiters

!& algeq

!& algeq                                             ! status check without droop

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation

!& algeq

!& algeq												! NEIGHBOURHOOD CONTROL

!& algeq

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z(  8))
         case(1)
            if(x( 81)<0.)then
               z(  8)=2
            endif
         case(2)
            if(x( 81)>=0.)then
               z(  8)=1
            endif
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z(  9))
         case(1)
            if(x( 80)<0.)then
               z(  9)=2
            endif
         case(2)
            if(x( 80)>=0.)then
               z(  9)=1
            endif
      end select

!& hyst ! should it be V or Vm?
      if (z( 10) == -1)then
         if(x(  9)>prm( 52))then
            z( 10)=1
         endif
      else
         if(x(  9)<prm( 53))then
            z( 10)=-1
         endif
      endif

!& hyst
      if (z( 11) == -1)then
         if(x(  9)>prm( 52))then
            z( 11)=1
         endif
      else
         if(x(  9)<prm( 53))then
            z( 11)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0

!& abs
      if (z( 12) == -1 )then
         if(x( 82)> blocktol1 )then
            z( 12)=1
         endif
      else
         if(x( 82)< - blocktol1 )then
            z( 12)=-1
         endif
      endif

!& algeq

!& swsign
      select case (z( 13))
         case(1)
            if(x( 85)<0.)then
               z( 13)=2
            endif
         case(2)
            if(x( 85)>=0.)then
               z( 13)=1
            endif
      end select

!& algeq												! voltage help required

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 14))
         case(1)
            if(x( 83)<0.)then
               z( 14)=2
            endif
         case(2)
            if(x( 83)>=0.)then
               z( 14)=1
            endif
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 15))
         case(1)
            if(x( 82)<0.)then
               z( 15)=2
            endif
         case(2)
            if(x( 82)>=0.)then
               z( 15)=1
            endif
      end select

!& algeq

!& algeq

!& algeq												! priority of voltage over power

!& algeq

!& algeq												! reactive power computation

!& algeq

!& algeq

!& algeq												! active power computation

!& algeq

!& algeq

!& algeq												! UNIT LIMIT	!s_f1 = security factor

!& algeq

!& algeq

!& algeq

!& algeq 											! voltage low limit flag

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 16))
         case(1)
            if(x( 63)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x( 63)>=0.)then
               z( 16)=1
            endif
      end select

!& algeq												! voltage high limit flag

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 17))
         case(1)
            if(x( 65)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x( 65)>=0.)then
               z( 17)=1
            endif
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max

!& algeq												! power low limit flag

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 18))
         case(1)
            if(x( 68)<0.)then
               z( 18)=2
            endif
         case(2)
            if(x( 68)>=0.)then
               z( 18)=1
            endif
      end select

!& algeq												! power high limit flag

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 19))
         case(1)
            if(x( 70)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x( 70)>=0.)then
               z( 19)=1
            endif
      end select

!& algeq												! if the units are off the limit flags go to 0

!& algeq

!& algeq

!& algeq

!& algeq

!& tf1p2lim
      select case (z( 20))
         case(0)
            if(x(158)>prm( 75)*prm( 72))then
               z( 20)=1
            elseif(x(158)<prm(104)*prm( 72))then
               z( 20)=-1
            endif
         case(1)
            if(1*x( 76)-x( 79)<prm( 75)*prm( 72))then
               z( 20)= 0
            endif
         case(-1)
            if(1*x( 76)-x( 79)>prm(104)*prm( 72))then
               z( 20)= 0
            endif
      end select
      select case (z( 21))
         case(0)
            if(x( 79)>prm( 83))then
               z( 21)=1
               eqtyp(108)=0
            elseif(x( 79)<prm( 82))then
               z( 21)=-1
               eqtyp(108)=0
            endif
         case(1)
            if (x(158)<0.)then
               z( 21)= 0
               eqtyp(108)= 79
            endif
         case(-1)
            if(x(158)>0.)then
               z( 21)= 0
               eqtyp(108)= 79
            endif
      end select

!& algeq

!& tf1p2lim
      select case (z( 22))
         case(0)
            if(x(159)>prm( 76)*prm( 72))then
               z( 22)=1
            elseif(x(159)<prm(105)*prm( 72))then
               z( 22)=-1
            endif
         case(1)
            if(1*x( 77)-x( 78)<prm( 76)*prm( 72))then
               z( 22)= 0
            endif
         case(-1)
            if(1*x( 77)-x( 78)>prm(105)*prm( 72))then
               z( 22)= 0
            endif
      end select
      select case (z( 23))
         case(0)
            if(x( 78)>prm( 83))then
               z( 23)=1
               eqtyp(111)=0
            elseif(x( 78)<prm( 82))then
               z( 23)=-1
               eqtyp(111)=0
            endif
         case(1)
            if (x(159)<0.)then
               z( 23)= 0
               eqtyp(111)= 78
            endif
         case(-1)
            if(x(159)>0.)then
               z( 23)= 0
               eqtyp(111)= 78
            endif
      end select

!& algeq

!& algeq												! CENTRAL CONTROL, positive and negative variation

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 24))
         case(1)
            if(x(110)<0.)then
               z( 24)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 24)=1
            endif
      end select

!& swsign
      select case (z( 25))
         case(1)
            if(x(110)<0.)then
               z( 25)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 25)=1
            endif
      end select

!& swsign
      select case (z( 26))
         case(1)
            if(x(110)<0.)then
               z( 26)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 26)=1
            endif
      end select

!& limvb												! limiting the variation to the max and min bounds
      select case (z( 27))
         case(0)
            if(x(111)>x(108))then
               z( 27)=1
            elseif(x(111)<x(109))then
               z( 27)=-1
            endif
         case(-1)
            if(x(111)>x(109))then
               z( 27)=0
            endif
         case(1)
            if(x(111)<x(108))then
               z( 27)=0
            endif
      end select

!& limvb
      select case (z( 28))
         case(0)
            if(x(112)>x(108))then
               z( 28)=1
            elseif(x(112)<x(109))then
               z( 28)=-1
            endif
         case(-1)
            if(x(112)>x(109))then
               z( 28)=0
            endif
         case(1)
            if(x(112)<x(108))then
               z( 28)=0
            endif
      end select

!& algeq !dPc_0 is equal to 0

!& algeq												! level 1 low negative

!& swsign
      select case (z( 29))
         case(1)
            if(x(117)<0.)then
               z( 29)=2
            endif
         case(2)
            if(x(117)>=0.)then
               z( 29)=1
            endif
      end select

!& algeq												! level 1 high negative

!& swsign
      select case (z( 30))
         case(1)
            if(x(119)<0.)then
               z( 30)=2
            endif
         case(2)
            if(x(119)>=0.)then
               z( 30)=1
            endif
      end select

!& algeq												! level 1 negative flag

!& algeq												! level 1 low positive

!& swsign
      select case (z( 31))
         case(1)
            if(x(122)<0.)then
               z( 31)=2
            endif
         case(2)
            if(x(122)>=0.)then
               z( 31)=1
            endif
      end select

!& algeq												! level 1 high positive

!& swsign
      select case (z( 32))
         case(1)
            if(x(124)<0.)then
               z( 32)=2
            endif
         case(2)
            if(x(124)>=0.)then
               z( 32)=1
            endif
      end select

!& algeq												! level 1 positive flag

!& swsign
      select case (z( 33))
         case(1)
            if(x(110)<0.)then
               z( 33)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 33)=1
            endif
      end select

!& algeq

!& algeq												! level 2 low negative

!& swsign
      select case (z( 34))
         case(1)
            if(x(129)<0.)then
               z( 34)=2
            endif
         case(2)
            if(x(129)>=0.)then
               z( 34)=1
            endif
      end select

!& algeq												! level 2 high negative

!& swsign
      select case (z( 35))
         case(1)
            if(x(131)<0.)then
               z( 35)=2
            endif
         case(2)
            if(x(131)>=0.)then
               z( 35)=1
            endif
      end select

!& algeq												! level 2 negative flag

!& algeq												! level 2 low positive

!& swsign
      select case (z( 36))
         case(1)
            if(x(134)<0.)then
               z( 36)=2
            endif
         case(2)
            if(x(134)>=0.)then
               z( 36)=1
            endif
      end select

!& algeq												! level 2 high positive

!& swsign
      select case (z( 37))
         case(1)
            if(x(136)<0.)then
               z( 37)=2
            endif
         case(2)
            if(x(136)>=0.)then
               z( 37)=1
            endif
      end select

!& algeq												! level 2 positive flag

!& swsign
      select case (z( 38))
         case(1)
            if(x(110)<0.)then
               z( 38)=2
            endif
         case(2)
            if(x(110)>=0.)then
               z( 38)=1
            endif
      end select

!& algeq

!& abs												! absolute of the level
      if (z( 39) == -1 )then
         if(x(110)> blocktol1 )then
            z( 39)=1
         endif
      else
         if(x(110)< - blocktol1 )then
            z( 39)=-1
         endif
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1

!& switch4											! choice between the levels
      z( 40)=max(1,min(  4,nint(x(142))))

!& algeq

!& algeq

!& switch4											! choice between the levels without bounds
      z( 41)=max(1,min(  4,nint(x(142))))

!& algeq													! checking if the variation should have been greater than the bounds

!& swsign
      select case (z( 42))
         case(1)
            if(x(147)<0.)then
               z( 42)=2
            endif
         case(2)
            if(x(147)>=0.)then
               z( 42)=1
            endif
      end select

!& algeq													! checking if the variation should have been greater than the bounds

!& swsign
      select case (z( 43))
         case(1)
            if(x(149)<0.)then
               z( 43)=2
            endif
         case(2)
            if(x(149)>=0.)then
               z( 43)=1
            endif
      end select
   end select

end subroutine inj_ATLv4
