inj
ATLv8

%data
Sb                              ! base power of the unit
Sbs                             ! base power of the system
vdc_star                        ! DC link voltage reference
pf                              ! setpoint for power factor at the terminal

kp_v                            ! DC link voltage control parameters
ki_v
kp_c                            ! rectifier current control parameters
ki_c
kp_p                            ! power control parameters
ki_p
kp_w                            ! motor speed control parameters
ki_w
w_cc                            ! motor current control bandwidth
rt                              ! terminal impedance
lt
cdc                             ! DC link capacitance
kw                              ! speed/torque control constant
kT                              ! motor torque constant
ra                              ! motor anchor/stator resistance
H                               ! motor inertia
b                               ! motor friction coefficient
Tnm                             ! compressor torque at nominal speed
iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
wm_min                          ! limits on rotational speed
wm_max
Vmax                            ! voltage range during which unit needs to stay connected
Vmin
Vint
Vr
tLVRT1
tLVRT2
tLVRTint
Vtrip

VPmax                           ! power limit above 0.93 pu voltage
LVRT                            ! enable or disable LVRT
Tm                              ! measurement delay
protection                      ! Flag for protection, -1 for off, 1 for on

P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
dPc_1_0                         ! start power for positive level 1 central controller
dPc_2_0                         ! start power for positive level 2 central controller
l_1_p_min_par                 ! power level for the activation of the central controller help in pu
l_1_p_max_par
l_1_n_min_par
l_1_n_max_par
l_2_p_min_par
l_2_p_max_par
l_2_n_min_par
l_2_n_max_par

V_n                             ! number of neighbours at voltage limit
P_n                             ! number of neighbours at power limit
Vdb_p                           ! higher voltage deadband value
Vdb_m                           ! lower voltage deadband value
ro_v1                            ! neighbourhood factor for voltage1
ro_v2                            ! neighbourhood factor for voltage2
ro_p                            ! neighbourhood factor for power
Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
Q_min                           ! min reactive power of the unit in pu, IT WIL

s_f1                             ! security factor for unit limit control
s_f2                             ! security factor for dPc_3
lvl                             ! level of emergency of the central controller
pl1_2_0                        ! flags that varies for the different units
pl1_4_0
pl1_6_0
pl1_8_0
pl2_2_0                        ! flags that varies for the different units
pl2_4_0
pl2_6_0
pl2_8_0
F_help_high_0
F_help_low_0
Tp1


V_max_nb
V_min_nb
dPs_rate_max
dQs_rate_max
T_hrt

%parameters

w0 = 1
wb = 2*pi*fnom
t2 = 0                          ! Torque polynomial parameters, set to constant torque
t1 = 0
t0 = 1

Downlim = -9999
Uplim = 9999
Downlimdisc = 0
Uplimdis = 0
downlimdis = -9999


theta0 = atan([vy]/[vx])
P0 = ([vx]*[ix]+[vy]*[iy])                                                      ! initial active power 
Q0 = [vy]*[ix]-[vx]*[iy]                                                        ! initial reactive power
P0_unit = -{P0}*{Sbs}/{Sb}
Q0_unit = -{Q0}*{Sbs}/{Sb}
V0 = dsqrt([vx]**2+[vy]**2)                                                     ! initial voltage magnitude at bus
iP0 = - ([vx]*[ix] + [vy]*[iy])*{Sbs} / ({Sb}*{V0})                      ! current alignment
iQ0 = - (+[vy]*[ix] - [vx]*[iy]) * {Sbs} / ({Sb}*{V0})
vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})                                     ! voltage alignment
vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})                            ! modulation indices
mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
idc0 = {md0}*{iP0}+{mq0}*{iQ0}                                                  ! DC link current
iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
Te0 = {iT0}*{kT}
Tc0 = {Tnm}

dPs_rate_min = -{dPs_rate_max}
dQs_rate_min = -{dQs_rate_max}

%states
vd = {vd0}
vq = {vq0}
vdc_ref ={vdc_star}
V = {V0}
vxm = [vx]
vym = [vy]
Vm = {V0}
iP =  {iP0}
diP = 0
iQ =  {iQ0}
diQ = 0
P = {P0}
dp = 0
Punit = {P0_unit}
Q = {Q0}
Qunit = {Q0_unit}
Pref_lim = {P0_unit}          ! limited P reference
vdc = {vdc_star}
dvdc = 0
iP_ref = {iP0}
Plim = {VPmax}
iQ_ref = {iQ0}
mdr = {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
mqr = -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
md = {md0}
mq = {mq0}
dvd = {iP0}*{rt}
dvq = {iQ0}*{rt}
idc = {idc0}
didc = {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
wm_ref = {wm0}
wm_ref_lim = {wm0}
iT_ref = {iT0}
iT = {iT0}
dwm = 0
wm = {wm0}
Te = {kT}*{iT0}
dT = {kT}*{iT0}-{Tc0}
deltafvh =  {V0}-{Vmax}
z1 = 0
Fvhi = 1
Fvh = 1
iPs = {iP0}
iQs = {iQ0}
Pref = {P0_unit}
x10 = -{V0}
z=0
Fvl = 1
Fvli = 1


Plim_min = 0

status  = 1             ! status of the device, 1 for on
p1 = {protection}
p2 = 1
p3 = 1



Fpl1 = 1                                ! placeholder for value 1 in switch block
Fpl0 = 0                                ! placeholder for value 0 in switch block


V_lim_min = {V_min_nb} * {s_f1}              ! unit controller states
V_lim_max = {V_max_nb} / {s_f1}
P_lim_min = {P_min} * {s_f1}
P_lim_max = {P_max} / {s_f1}
pl1 = {V_min_nb} * {s_f1} - {V0}
F_v_min_in = 0
pl2 = {V0} - {V_max_nb} / {s_f1}
F_v_max_in = 0
F_v_opp = 1
pl3 = {P_min} * {s_f1} - {P0_unit}
F_p_min_in = 0
pl4 = {P0_unit} - {P_max} / {s_f1}
F_p_max_in = 0
F_v_min = 0
F_v_max = 0
F_p_min = 0
F_p_max = 0
dP_lim = 0 
dQ_lim = 0
dQ_sum = 0
dP_sum = 0

P_n_var = {P_n} + 0.0001                         ! neighbourhood controller states
P_n_var_neg = -{P_n} + 0.0001
P_n_abs = [P_n_var]
V_n_var = {V_n}
V_n_var_neg = -{V_n}
V_n_abs = {V_n}
pl5 = {V_n} - 1
F_help_v = 0
F_help_v_p = 0
F_help_v_m = 0
F_help_p_p = 0
F_help_p_m = 0
F_hlp_high = {F_help_high_0}
F_hlp_low = {F_help_low_0}
F_hlp_v_p2 = [F_help_v_p] * [F_hlp_high]
F_hlp_v_m2 = [F_help_v_m]* [F_hlp_low]
F_hlp_p_p2 = [F_help_p_p] * (1-[F_help_v])
F_hlp_p_m2 = [F_help_p_m] * (1-[F_help_v])
dQp = 0
dQm = 0
dQnb = 0
dPp = 0
dPm = 0
dPnb = 0
iQnb = 0

dPc_1_neg = - {dPc_1_0}                 ! central controller states
dPc_1_pos = {dPc_1_0}
dPc_2_pos = {dPc_2_0}
dPc_2_neg = - {dPc_2_0}
dPc_3_pos = {P_max}/{s_f2} - {P0_unit}
dPc_3_neg = - {P0_unit} + {P_min}*{s_f2}
level = {lvl}
dPc_1_in = {dPc_1_0}
dPc_2_in = {dPc_2_0}
dPc_1 = {dPc_1_0}
dPc_2 = {dPc_2_0}
dPc_3 = {P_max}/{s_f2} - {P0_unit}
dPc_0 = 0

pl1_1 = {P0_unit} - ({l_1_n_min_par} * ({P_max}-{P_min}) + {P_min})
pl1_2 = {pl1_2_0}
pl1_3 = ({l_1_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
pl1_4 = {pl1_4_0}
F_l_1_neg = {pl1_2_0} * {pl1_4_0}
pl1_5 = {P0_unit} - ({l_1_p_min_par} * ({P_max}-{P_min}) + {P_min})
pl1_6 = {pl1_6_0}
pl1_7 = ({l_1_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
pl1_8 = {pl1_8_0}
F_l_1_pos = {pl1_6_0} * {pl1_8_0}
F_l_1 = [F_l_1_pos]
dPc_1_s_fl = {dPc_1_0} * [F_l_1]

pl2_1 = {P0_unit} - ({l_2_n_min_par} * ({P_max}-{P_min}) + {P_min})
pl2_2 = {pl2_2_0}
pl2_3 = ({l_2_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
pl2_4 = {pl2_4_0}
F_l_2_neg = {pl2_2_0} * {pl2_4_0}
pl2_5 = {P0_unit} - ({l_2_p_min_par} * ({P_max}-{P_min}) + {P_min})
pl2_6 = {pl2_6_0}
pl2_7 = ({l_2_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
pl2_8 = {pl2_8_0}
F_l_2_pos = {pl2_6_0} * {pl2_8_0}
F_l_2 = [F_l_2_pos]
dPc_2_s_fl = {dPc_2_0} * [F_l_2]

l_abs = 0
l_switch = 1
dPc = 0
dPc_1_in_f = {dPc_1_0} * [F_l_1]
dPc_2_in_f = {dPc_2_0} * [F_l_2]
dPc_no_bou = 0
under_min = [dPc] +0.0001 - [dPc_3_neg]
F_p_c_min = 0
over_max = - [dPc] -0.0001 + [dPc_3_pos]
F_p_c_max = 0

theta_est = {theta0}

%observables
iP
iQ
vd
vq
P
Punit
Pref
Q
wm
Vm

Pref_lim
wm_ref_lim
status
F_v_min
F_v_max
F_p_min
F_p_max
F_hlp_high
F_hlp_low
F_hlp_p_p2

F_hlp_p_m2
dPnb
dQnb
P_n_var
V_n_var
dP_sum
dP_lim
Qunit
ix
iy

%models

& algeq                                             ! voltage magnitude
dsqrt([vx]**2+[vy]**2) - [V] 

& tf1p
V
Vm
1.
{Tm}
& tf1p
vx
vxm
1.
{Tm}
& tf1p
vy
vym
1.
{Tm}

& algeq                                             ! voltage alignment         
-[vd] + [vxm]*cos([theta_est])+[vym]*sin([theta_est])
& algeq
-[vq] - [vxm]*sin([theta_est])+[vym]*cos([theta_est])

& algeq
-[theta_est] + atan([vym]/[vxm])

& algeq                                             ! compute ix
-[iPs] - ([vx]*[ix] + [vy]*[iy])*{Sbs}/({Sb}*[V]) ! -[ix] + (-[iPs]*cos([theta_est])-[iQs]*sin([theta_est]))*{Sb}/{Sbs}
& algeq                                             ! compute iy
-[iQs] - (+ [vy]*[ix] - [vx]*[iy])*{Sbs}/({Sb}*[V]) ! -[iy] + (-[iPs]*sin([theta_est])+[iQs]*cos([theta_est]))*{Sb}/{Sbs}

& algeq
-[iPs] + [iP]*[status]

& algeq
-[iQs] + [iQ]*[status]


& algeq                                             ! compute powers
-[P]*{Sbs}/{Sb} -[Punit]                            ! + [ix]*[vx]+[iy]*[vy]
& algeq             
-[Q]*{Sbs}/{Sb} - [Qunit]                           ! -[vx]*[iy] + [vy]*[ix]


& algeq                                             ! compute powers
-[Punit] + [vd]*[iPs] + [vq]*[iQs]                              ! - [P]*{Sbs}/{Sb}
& algeq             
-[Qunit] + [vd]*[iQs] - [vq]*[iPs]                              ! - [Q]*{Sbs}/{Sb}



& algeq 
-[vdc_ref] + {vdc_star}*[status]

& algeq                                             ! DC voltage control
-[dvdc] + [status]*[vdc_ref] - [vdc]

& pictl
dvdc
iP_ref
{ki_v}
{kp_v}

& algeq                                             ! p-axis current control
-[diP] + [iP_ref] -[iP]
& pictl
diP
mdr
{ki_c}
{kp_c}
& algeq                                             ! q-axis current control
-[iQ_ref] + {iQ0}
& algeq
-[diQ] + [iQ_ref] - [iQ] + [iQnb]
& pictl
diQ
mqr
{ki_c}
{kp_c}


& algeq                                             ! md
-[md] + [status]*(-[mdr] + {lt}/max(1.d-3,{vdc_star})*[omega]*[iQ])
& algeq                                             ! mq
-[mq] + [status]*(-[mqr] - {lt}/max(1.d-3,{vdc_star})*[omega]*[iP])


& algeq                                             ! voltage over d-axis terminal impedance
-[dvd] + [vd] - [md] * [vdc] + {lt}*[omega]*[iQ]
& algeq                                             ! voltage over q-axis terminal impedance
-[dvq] + [vq] - [mq] * [vdc] - {lt}*[omega]*[iP]


& tf1p                                              ! d-axis current
dvd
iP
1/{rt}
{lt}/({wb}*{rt})
& tf1p                                              ! q-axis current
dvq
iQ
1/{rt}
{lt}/({wb}*{rt})


& algeq                                             ! DC link current
-[idc] + [status]*([wm]*{kT}*[iT] + {ra}*[iT]**2) / max(1.d-3,[vdc])


& algeq                                             ! DC link
-[didc]+[md]*[iP]+[mq]*[iQ]-[idc]


& int                                               ! DC link voltage
didc
vdc
{cdc}/{wb}


& limvb                                              ! power limiter
Pref
Plim_min
Plim
Pref_lim


& algeq                                             ! power mismatch
-[dp] + [Pref_lim]-[Punit] + [dP_sum]


& pictl                                             ! power control
dp
wm_ref
{ki_p}
{kp_p}


& lim                                               ! limit speed control input
wm_ref
wm_ref_lim
{wm_min}
{wm_max}


& algeq                                             ! speed control input
-[dwm] + {kw}*([status]*[wm_ref_lim]-[wm])


& pictl                                             ! speed control
dwm
iT_ref
ki_w
kp_w


& tf1p                                              ! motor current control
iT_ref
iT
1
1/{w_cc}


& algeq                                             ! torque equations
-[Te] + {kT}*[iT]
& algeq
-[dT] + [Te]-{Tc0}


& tf1p                                              ! motor inertia
dT
wm
1/{b}
2*{H}/{b}


& algeq
[deltafvh] - [Vm] +{Vmax}


& pwlin4                                            ! overvoltage Protection
deltafvh
z1
-999
0.
0.
0.
0.
1.
999
1.
& algeq 
[Fvhi] -1 + [z1] 
& hyst
Fvhi
Fvh
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! LVRT
[Vm] + [x10]
& timer5
x10
z
-{Vr}
{tLVRT2}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRTint}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
0.

& algeq 
[Fvli] -1 + [z] 

& hyst
Fvli
Fvl
1.1
0.
1.
0.9
1.
0.
1.


& algeq                                             ! status variable that can switch the entire unit on or off
[p2] - [Fvh]*[Fvl]

& algeq
[p1] - {protection}
& algeq
[p3] - 1

& swsign
p2
p3
p1
status








& algeq                                             ! min and max power limiters
[Plim_min]
& algeq
-[Plim] + {VPmax}


& algeq                                             ! status check without droop
- [Pref] +  [status]*{P0_unit}





& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
[Fpl0]
& algeq
[Fpl1] - 1


& algeq												! NEIGHBOURHOOD CONTROL
-[P_n_var] + {P_n} + 0.0001
& algeq
-[P_n_var_neg] - {P_n} + 0.0001
& abs
P_n_var
P_n_abs

& swsign											! F_help_p_p is 1 if P_n > 0 
Fpl0
Fpl1
P_n_var_neg
F_help_p_p

& swsign											! F_help_p_m is 1 if P_n < 0 
Fpl0
Fpl1
P_n_var
F_help_p_m



& hyst ! should it be V or Vm?
Vm
F_hlp_high
{Vdb_p}
0.
1.
{Vdb_m}
1.
0.
-1.

& hyst
Vm
F_hlp_low
{Vdb_p}
1.
0.
{Vdb_m}
0.
1.
1.



& algeq												! F_help_v is different from 0 only if V_n is !=0
-[V_n_var] + {V_n}
& abs
V_n_var
V_n_abs
& algeq
-[pl5] + [V_n_abs] - 1
& swsign
Fpl1
Fpl0
pl5
F_help_v


& algeq												! voltage help required
-[V_n_var_neg] - {V_n}

& swsign											! F_help_v_p is 1 if V_n > 0 
Fpl0
Fpl1
V_n_var_neg
F_help_v_p

& swsign											! F_help_v_m is 1 if V_n < 0 
Fpl0
Fpl1
V_n_var
F_help_v_m


& algeq
-[F_hlp_v_p2] + [F_help_v_p]*[F_hlp_high]
& algeq
-[F_hlp_v_m2] + [F_help_v_m]*[F_hlp_low]

& algeq												! priority of voltage over power
-[F_hlp_p_p2] + [F_help_p_p]*(1-[F_help_v])*(1-[F_p_max])
& algeq
-[F_hlp_p_m2] + [F_help_p_m]*(1-[F_help_v])*(1-[F_p_min])

& algeq												! reactive power computation
-[dQp] + {V_n}*{ro_v1}*{Q_max}*{Q_max}*[F_hlp_v_p2]/({Q_max}-{Q_min}+{ro_v2}*{Q_max}*[V_n_abs])
& algeq
-[dQm] + {V_n}*{ro_v1}*{Q_max}*{Q_min}*[F_hlp_v_m2]/({Q_max}-{Q_min}+{ro_v2}*{Q_max}*[V_n_abs])
& algeq
-[dQnb] + [dQp] + [dQm]

& algeq												! active power computation
-[dPp] + {P_n}*{ro_p}*{P_max}*({P_max}-[Punit])/({P_max}-{P_min})*[F_hlp_p_p2]
& algeq
-[dPm] + {P_n}*{ro_p}*{P_max}*([Punit]-{P_min})/({P_max}-{P_min})*[F_hlp_p_m2]
& algeq
-[dPnb] + [dPp] + [dPm]


& algeq												! UNIT LIMIT	!s_f1 = security factor
-[V_lim_min] + {V_min_nb}*{s_f1}
& algeq
-[V_lim_max] + {V_max_nb}/{s_f1}
& algeq
-[P_lim_min] + {P_min}*{s_f1}
& algeq
-[P_lim_max] + {P_max}/{s_f1}



& algeq 											! voltage low limit flag
-[pl1] + [V_lim_min] - [Vm]
& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
Fpl1
Fpl0
pl1
F_v_min_in

& algeq												! voltage high limit flag
-[pl2] + [Vm] - [V_lim_max]
& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
Fpl1
Fpl0
pl2
F_v_max_in

& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
-[F_v_opp] + (1-[F_v_min_in])*(1-[F_v_max_in])


& algeq												! power low limit flag
-[pl3] + [P_lim_min] - [Punit]
& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
F_v_opp
Fpl0
pl3
F_p_min_in

& algeq												! power high limit flag
-[pl4] + [Punit] - [P_lim_max]
& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
F_v_opp
Fpl0
pl4
F_p_max_in

& algeq												! if the units are off the limit flags go to 0
-[F_v_min] + [F_v_min_in]*[status]
& algeq
-[F_v_max] + [F_v_max_in]*[status]
& algeq
-[F_p_min] + (1 - (1-[F_p_min_in]) * (1-[F_p_c_min])) * [status]
& algeq
-[F_p_max] + (1 - (1-[F_p_max_in]) * (1-[F_p_c_max])) * [status]

& timer1
F_v_min
F_v_min_out
0.5
T_hrt

& timer1
F_v_max
F_v_max_out
0.5
T_hrt


& algeq
-[dP_lim] + ([dPnb] + [dPc])

& tf1p2lim
dP_lim
dP_sum
1
{Tp1}
{Downlim}
{Uplim}
{dPs_rate_min}
{dPs_rate_max}


& lim
dQnb
dQ_lim
Q_min
Q_max


& tf1p2lim
dQ_lim
dQ_sum
1
{Tp1}
{Downlim}
{Uplim}
{dQs_rate_min}
{dQs_rate_max}


& algeq
-[iQnb] - [dQ_sum]


& algeq												! CENTRAL CONTROL, positive and negative variation
-[dPc_1_pos] + {dPc_1_0}
& algeq
-[dPc_2_pos] + {dPc_2_0}
& algeq
[dPc_1_neg] + [dPc_1_pos]
& algeq
[dPc_2_neg] + [dPc_2_pos]
& algeq 
-[dPc_3_pos] + {P_max}/{s_f2} - [Pref_lim]
& algeq
-[dPc_3_neg] - [Pref_lim] + {P_min}*{s_f2}

& algeq 
-[level] + {lvl}

& swsign
dPc_1_pos
dPc_1_neg
level
dPc_1_in
& swsign
dPc_2_pos
dPc_2_neg
level
dPc_2_in
& swsign
dPc_3_pos
dPc_3_neg
level
dPc_3

& limvb												! limiting the variation to the max and min bounds
dPc_1_in
dPc_3_neg
dPc_3_pos
dPc_1
& limvb
dPc_2_in
dPc_3_neg
dPc_3_pos
dPc_2


& algeq !dPc_0 is equal to 0 
[dPc_0]


& algeq												! level 1 low negative 
-[pl1_1] + [Pref_lim] - ({l_1_n_min_par} * ({P_max}-{P_min}) + {P_min})
& swsign
Fpl1
Fpl0
pl1_1
pl1_2

& algeq												! level 1 high negative
-[pl1_3] + ({l_1_n_max_par} * ({P_max}-{P_min}) + {P_min}) - [Pref_lim]
& swsign
Fpl1
Fpl0
pl1_3
pl1_4

& algeq												! level 1 negative flag
-[F_l_1_neg] + ([pl1_2] * [pl1_4])


& algeq												! level 1 low positive
-[pl1_5] + [Pref_lim] - ({l_1_p_min_par} * ({P_max}-{P_min}) + {P_min})
& swsign
Fpl1
Fpl0
pl1_5
pl1_6

& algeq												! level 1 high positive
-[pl1_7] + ({l_1_p_max_par} * ({P_max}-{P_min}) + {P_min}) - [Pref_lim]
& swsign
Fpl1
Fpl0
pl1_7
pl1_8

& algeq												! level 1 positive flag
-[F_l_1_pos] + ([pl1_6] * [pl1_8])


& swsign
F_l_1_pos
F_l_1_neg
level
F_l_1

& algeq
-[dPc_1_s_fl] + [dPc_1] * [F_l_1]



& algeq												! level 2 low negative 
-[pl2_1] + [Pref_lim] - ({l_2_n_min_par} * ({P_max}-{P_min}) + {P_min})
& swsign
Fpl1
Fpl0
pl2_1
pl2_2

& algeq												! level 2 high negative
-[pl2_3] + ({l_2_n_max_par} * ({P_max}-{P_min}) + {P_min}) - [Pref_lim]
& swsign
Fpl1
Fpl0
pl2_3
pl2_4

& algeq												! level 2 negative flag
-[F_l_2_neg] + ([pl2_2] * [pl2_4])


& algeq												! level 2 low positive
-[pl2_5] + [Pref_lim] - ({l_2_p_min_par} * ({P_max}-{P_min}) + {P_min})
& swsign
Fpl1
Fpl0
pl2_5
pl2_6

& algeq												! level 2 high positive
-[pl2_7] + ({l_2_p_max_par} * ({P_max}-{P_min}) + {P_min}) - [Pref_lim]
& swsign
Fpl1
Fpl0
pl2_7
pl2_8

& algeq												! level 2 positive flag
-[F_l_2_pos] + ([pl2_6] * [pl2_8])


& swsign
F_l_2_pos
F_l_2_neg
level
F_l_2

& algeq
-[dPc_2_s_fl] + [dPc_2] * [F_l_2]


& abs												! absolute of the level
level
l_abs

& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
-[l_switch] + [l_abs] + 1
& switch4											! choice between the levels
dPc_0
dPc_1_s_fl
dPc_2_s_fl
dPc_3											! the level 3 doesn't require a flag since every unit is activated
l_switch
dPc

& algeq
-[dPc_1_in_f] + [dPc_1_in] * [F_l_1]
& algeq
-[dPc_2_in_f] + [dPc_2_in] * [F_l_2]

& switch4											! choice between the levels without bounds
dPc_0
dPc_1_in_f
dPc_2_in_f
dPc_3
l_switch
dPc_no_bou


& algeq													! checking if the variation should have been greater than the bounds
-[under_min] + [dPc_no_bou]+0.0001 - [dPc_3_neg]
& swsign
Fpl0
Fpl1
under_min
F_p_c_min

& algeq													! checking if the variation should have been greater than the bounds
-[over_max] - [dPc_no_bou]-0.0001 + [dPc_3_pos]
& swsign
Fpl0
Fpl1
over_max
F_p_c_max



