!  MODEL NAME : tor_SFR                 
!  MODEL DESCRIPTION FILE : tor_SFR.txt
!  Data :
!       prm(  1)=  Tg
!       prm(  2)=  Km
!       prm(  3)=  Fh
!       prm(  4)=  Tr
!       prm(  5)=  R
!  Parameters :
!       prm(  6)=  Pref  
!  Output states :
!       x(  1)=  tm           mechanical torque
!  Internal states defined by user :
!       x(  2)=  d_Pg                  
!       x(  3)=  Pg                    
!       x(  4)=  Pm                    

!.........................................................................................................

subroutine tor_SFR(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adtm,eqtyp,tc,t,p,tm,omega,x,z,f,obs)

   use MODELING
   use SETTINGS, only : blocktol1
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,p,tm,omega
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,adtm,nbobs,eqtyp(*),z(*)
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata=  5
      nbaddpar=  1
      parname(  1)='Tg'
      parname(  2)='Km'
      parname(  3)='Fh'
      parname(  4)='Tr'
      parname(  5)='R'
      parname(  6)='Pref'
      adtm=  1
      nbxvar=  5
      nbzvar=  0

!........................................................................................
   case (define_obs)
      nbobs=  1
      obsname(  1)='Pm'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x(  4)              

!........................................................................................
   case (initialize)

!Pref = [tm]/{Km}
      prm(  6)= tm/prm(  2)

!d_Pg =  [tm]/{Km}
      x(  2)= x(  1)/prm(  2)

!Pg =  [tm]/{Km}
      x(  3)= x(  1)/prm(  2)

!Pm =  [tm]
      x(  4)= x(  1)

!& algeq
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)=  3
      tc(  2)=prm(  1)

!& tf1p1z
      x(  5)=x(  3)
      eqtyp(  3)=  5
      tc(  3)=prm(  4)
      eqtyp(  4)=0

!& algeq
      eqtyp(  5)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq
      f(  1)=x(  2) - prm(  6) + ((omega-1.d0)/prm(  5))

!& tf1p
      f(  2)=(-x(  3)+1.d0*x(  2))

!& tf1p1z
      f(  3)=-x(  5)+x(  3)
      if (prm(  4)< 0.005)then
         f(  4)=prm(  2)*x(  3)-x(  4)
      else
         f(  4)=prm(  2)*(prm(  3)*prm(  4)*x(  3)+(prm(  4)-prm(  3)*prm(  4))*x(  5))-prm(  4)*x(  4)
      endif

!& algeq
      f(  5)=x(  1)*omega-x(  4)

!........................................................................................
   case (update_disc)

!& algeq

!& tf1p

!& tf1p1z

!& algeq
   end select

end subroutine tor_SFR
