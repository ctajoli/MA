!> @file
!> @brief associates the name of the injector with the actual subroutine

!> @brief associates the name of the injector with the actual subroutine
!> @details .
subroutine assoc_inj_ptr(modelname,inj_ptr)

   use MODELING

   implicit none

   character(len=20), intent(in):: modelname
   procedure(injector), pointer, intent(out) :: inj_ptr
   external inj_vfault
   external inj_PQ
   external inj_IBG
   external inj_IBG3
   external inj_IBG22
   external inj_IBG22r
   external inj_IBGtesting
   external inj_IBGtesting2
   external inj_IBGtesting3

   external inj_ATL

   external inj_ATLv2
   external inj_ATLv3
   external inj_ATLv4
   external inj_ATLv5
   external inj_ATLv6
   external inj_ATLv7
   external inj_ATLv8
   external inj_ATLv9
  
   select case (modelname)

      case('VFAULT')
         inj_ptr=>inj_vfault
      case ('PQ')
          inj_ptr=>inj_PQ
      case('IBG')
          inj_ptr=>inj_IBG
      case('IBG3')
          inj_ptr=> inj_IBG3
      case('IBG22')
          inj_ptr=>inj_IBG22
      case('IBG22r')
          inj_ptr=>inj_IBG22r
      case('IBGtesting')
          inj_ptr=>inj_IBGtesting
      case('IBGtesting2')
          inj_ptr=>inj_IBGtesting2
      case('IBGtesting3')
          inj_ptr=>inj_IBGtesting3
      case('ATL')
          inj_ptr=>inj_ATL
    
       case('ATLv2')
          inj_ptr=>inj_ATLv2   
       case('ATLv3')
          inj_ptr=>inj_ATLv3       
       case('ATLv4')
          inj_ptr=>inj_ATLv4  
       case('ATLv5')
          inj_ptr=>inj_ATLv5 
       case('ATLv6')
          inj_ptr=>inj_ATLv6 
       case('ATLv7')
          inj_ptr=>inj_ATLv7 
       case('ATLv8')
          inj_ptr=>inj_ATLv8
       case('ATLv9')
          inj_ptr=>inj_ATLv9
       end select
end subroutine assoc_inj_ptr
