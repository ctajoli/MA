# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 14:21:45 2022

@author: ctajoli
"""
# %% Imports
import pyramses
import numpy as np
import os
import json
import codecs
import pandas as pd
import datetime
import time
from plyer import notification
from matplotlib import pyplot as plt
# import PyGnuplot as gp


# %% Functions
def get_measurements(f, rocof, old_level, nadir_reached, Tsample, hyst, ram):
    w = ram.getObs('SYN', 'G', 'Omega')
    f.append(w[0]*50)
    new_nadir = False

    lim32 = 48.5
    lim21 = 49.5
    lim10 = 49.9
    lim01 = 50.1
    lim12 = 50.5
    lim23 = 51.5
    if not hyst:
        if f[-1] <= lim32:
            f_level = -3
        elif f[-1] <= lim21:
            f_level = -2
        elif f[-1] <= lim10:
            f_level = -1
        elif f[-1] < lim01:
            f_level = 0
        elif f[-1] < lim12:
            f_level = 1
        elif f[-1] < lim23:
            f_level = 2
        else:
            f_level = 3

    else:
        f_1 = f[-1]
        db = 0.05

        if old_level == -3:
            if f_1 <= lim32 + db:
                f_level = -3
            if f_1 <= lim21:
                f_level = -2
            elif f_1 <= lim10:
                f_level = -1
            elif f_1 < lim01:
                f_level = 0
            elif f_1 < lim12:
                f_level = 1
            elif f_1 < lim23:
                f_level = 2
            else:
                f_level = 3

        if old_level == -2:
            if f_1 <= lim32 - db:
                f_level = -3
            if f_1 <= lim21 + db:
                f_level = -2
            elif f_1 <= lim10:
                f_level = -1
            elif f_1 < lim01:
                f_level = 0
            elif f_1 < lim12:
                f_level = 1
            elif f_1 < lim23:
                f_level = 2
            else:
                f_level = 3

        if old_level == -1:
            if f_1 <= lim32:
                f_level = -3
            if f_1 <= lim21 - db:
                f_level = -2
            elif f_1 <= lim10 + db:
                f_level = -1
            elif f_1 < lim01:
                f_level = 0
            elif f_1 < lim12:
                f_level = 1
            elif f_1 < lim23:
                f_level = 2
            else:
                f_level = 3

        if old_level == 0:
            if f_1 <= lim32:
                f_level = -3
            if f_1 <= lim21:
                f_level = -2
            elif f_1 <= lim10 - db:
                f_level = -1
            elif f_1 < lim01 + db:
                f_level = 0
            elif f_1 < lim12:
                f_level = 1
            elif f_1 < lim23:
                f_level = 2
            else:
                f_level = 3

        if old_level == 1:
            if f_1 <= lim32:
                f_level = -3
            if f_1 <= lim21:
                f_level = -2
            elif f_1 <= lim10:
                f_level = -1
            elif f_1 < lim01 - db:
                f_level = 0
            elif f_1 < lim12 + db:
                f_level = 1
            elif f_1 < lim23:
                f_level = 2
            else:
                f_level = 3

        if old_level == 2:
            if f_1 <= lim32:
                f_level = -3
            if f_1 <= lim21:
                f_level = -2
            elif f_1 <= lim10:
                f_level = -1
            elif f_1 < lim01:
                f_level = 0
            elif f_1 < lim12 - db:
                f_level = 1
            elif f_1 < lim23 + db:
                f_level = 2
            else:
                f_level = 3

        if old_level == 3:
            if f_1 <= lim32:
                f_level = -3
            if f_1 <= lim21:
                f_level = -2
            elif f_1 <= lim10:
                f_level = -1
            elif f_1 < lim01:
                f_level = 0
            elif f_1 < lim12:
                f_level = 1
            elif f_1 < lim23 - db:
                f_level = 2
            else:
                f_level = 3

    roc01 = 0.45
    roc12 = 2.3
    roc23 = 6.5
    rocof.append((f[-1] - f[-2]) / Tsample)
    if rocof[-1] <= -roc23:
        r_level = -3
    elif rocof[-1] <= -roc12:
        r_level = -2
    elif rocof[-1] <= -roc01:
        r_level = -1
    elif rocof[-1] < roc01:
        r_level = 0
    elif rocof[-1] < roc12:
        r_level = 1
    elif rocof[-1] < roc23:
        r_level = 2
    else:
        r_level = 3

    worst_level = f_level if abs(f_level) >= abs(r_level) else r_level
    if rocof[-1] * rocof[-2] < -0.0000001 or nadir_reached[-1] is True:
        new_nadir = True

    if new_nadir is True:
        lvl = f_level
    else:
        lvl = worst_level if abs(worst_level) >= abs(old_level) else old_level

    return lvl, new_nadir


def get_limits(ATL, ram):
    # having two functions we could use them at different frequencies.
    lim_v_min = ram.getObs('INJ', ATL, 'F_v_min_out')
    lim_v_max = ram.getObs('INJ', ATL, 'F_v_max_out')
    lim_p_min = ram.getObs('INJ', ATL, 'F_p_min')
    lim_p_max = ram.getObs('INJ', ATL, 'F_p_max')

    return (int(lim_v_min[0]), int(lim_v_max[0]),
            int(lim_p_min[0]), int(lim_p_max[0]))


def update_emergency_level(t, ATL, level_diff, ram):
    ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' lvl ' + str(level_diff) + ' 0')

# def update_nadir(t, ATL, nadir_diff):
#     ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' nadir '+str(2*nadir_diff)+' 0')


def update_neighbourhood_Phelp(t, ATL, p_diff, ram):
    ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' P_n ' + str(p_diff) + ' 0')


def update_neighbourhood_Vhelp(t, ATL, v_diff, ram):
    ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' V_n ' + str(v_diff) + ' 0')


def update_neighbourhood_help(t, ATL, v_diff, p_diff, ram):
    if p_diff != 0:
        ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' P_n ' + str(p_diff) + ' 0')
    if v_diff != 0:
        ram.addDisturb(t, 'CHGPRM INJ ' + ATL + ' V_n ' + str(v_diff) + ' 0')


def simulate(case, PV_model, out_dir, MV_buses_load, Tsample, Thorizon,
             event_time, LV_buses_load, LV_buses_PV, buses):
    ctrl = case['control']
    stren = case['strength']
    step_dir = case['direction']
    DP = case['step_change']
    print(ctrl, stren, step_dir, DP)

    if ctrl == 'active' or ctrl == 'inactive' or ctrl == 'mixed' or \
       ctrl == 'V-help':
        curr_ATL_vers = 'v9'
    else:
        curr_ATL_vers = 'dr'
    if step_dir == "up":
        delta_p = "+" + str(DP)
    else:
        delta_p = "-" + str(DP)

    cmd = 'cmd_' + curr_ATL_vers + '_' + PV_model + stren

    ram = pyramses.sim(os.getcwd()+r"\URAMSES-3.40c - copy\Release_intel_w64")
    c = pyramses.cfg(os.getcwd() + "\\cmd\\" + cmd + ".txt")
    c.addTrj(out_dir + stren + '_' + ctrl + "_" + step_dir
             + str(DP) + ".trj")
    c.addOut(out_dir + "output.trace")
    # c.addRunObs('MS G')
    ram.execSim(c, 0)

    level = [0]
    count_v = []
    count_p = []
    v_diff = []
    p_diff = []
    for i in range(len(MV_buses_load)):
        count_v.append([0])
    for i in range(len(MV_buses_load)):
        count_p.append([0])
    for i in range(len(MV_buses_load)):
        v_diff.append([0])
    for i in range(len(MV_buses_load)):
        p_diff.append([0])

    f = [50]
    rocof = [0]
    nadir_reached = [False]
    hyst = True

    for t in np.arange(Tsample, Thorizon, Tsample):
        print(round(t, 2))
        # Simulates until the next sampling point.
        # If there is an error, then exit the loop printing the error.
        try:
            ram.contSim(t)
        except Exception:
            print(ram.getLastErr())
            break
        if (event_time < t+Tsample) and (event_time >= t):
            ram.addDisturb(event_time, 'CHGPRM INJ L0 P0 ' + delta_p
                           + ' 0.1')
        #     ram.addDisturb(event_time, 'FAULT BUS 117501 0 0.1')
        # if (event_time2 < t+Tsample) and (event_time >= t):
        #     ram.addDisturb(event_time2, 'CLEAR BUS 117501')
        # units should disconnect, PVs reconnect after some time
        last_level, new_nadir = get_measurements(f, rocof, level[-1],
                                                 nadir_reached,
                                                 Tsample, hyst, ram)
        level.append(last_level)
        level_diff = level[-1] - level[-2]
        nadir_reached.append(new_nadir)

        if ctrl == 'active' or ctrl == 'mixed' or ctrl == 'V-help':
            for i, MV_bus in enumerate(MV_buses_load):
                count_v_instant = 0
                count_p_instant = 0
                for LV_bus_load in LV_buses_load:
                    # measure loop. LV_buses_load must be all the
                    # loads for the given LV system, so they have to
                    # have the correct name
                    ATL = ('ATL' + str(MV_bus) + str(LV_bus_load).
                           zfill(2))
                    lim_v_min, lim_v_max, lim_p_min, lim_p_max = \
                        get_limits(ATL, ram)
                    count_v_instant += lim_v_max - lim_v_min
                    count_p_instant += lim_p_max - lim_p_min

                count_v[i].append(count_v_instant)
                count_p[i].append(count_p_instant)

                v_diff[i].append(count_v[i][-1] - count_v[i][-2])
                p_diff[i].append(count_p[i][-1] - count_p[i][-2])

                for LV_bus_load in LV_buses_load:
                    # update loop. separate because this way the count
                    # goes through all the LV before updating
                    ATL = ('ATL' + str(MV_bus) + str(LV_bus_load).
                           zfill(2))
                    if level_diff != 0:
                        update_emergency_level(t+0.001, ATL,
                                               level_diff, ram)
                    if p_diff[i][-1] != 0:
                        update_neighbourhood_Phelp(t+0.001, ATL,
                                                   p_diff[i][-1], ram)
                    if v_diff[i][-1] != 0:
                        update_neighbourhood_Vhelp(t+0.001, ATL,
                                                   v_diff[i][-1], ram)
                if ctrl == 'active' or ctrl == "V-help":
                    for LV_bus_PV in LV_buses_PV:
                        IBG = ('PV' + str(MV_bus) + str(LV_bus_PV).
                               zfill(2))
                        if v_diff[i][-1] != 0:
                            update_neighbourhood_Vhelp(t+0.001, IBG,
                                                       v_diff[i][-1], ram)
                        if ctrl == "active":
                            if p_diff[i][-1] != 0:
                                update_neighbourhood_Phelp(t+0.0001, IBG,
                                                           -p_diff[i][-1], ram)
                            if level_diff != 0:
                                update_emergency_level(t+0.001, IBG,
                                                       -level_diff, ram)

    ram.endSim()
    # ext = pyramses.extractor(c.getTrj())
    # freq = ext.getSync("G").S.value * 50
    # v_min = []
    # v_max = []
    # if step_dir == 'up':
    #     f_nadir = max(freq)
    # else:
    #     f_nadir = min(freq)
    # for bus in buses:
    #     v_max.append(max(ext.getBus(bus).mag.value))
    #     v_min.append(min(ext.getBus(bus).mag.value))
    # v_min_abs = min(v_min)
    # v_max_abs = max(v_max)
    # # your path variable
    # file_path = out_dir + stren + '_' + \
    #     ctrl + "_" + step_dir + str(DP) + ".json"
    # data_dict = {
    #     "f nadir": f_nadir,
    #     "v_max": v_max_abs,
    #     "v_min": v_min_abs
    # }
    # json.dump(
    #     data_dict,
    #     codecs.open(file_path, "w", encoding="utf-8"),
    #     separators=(",", ":"),
    #     sort_keys=True,
    #     indent=4,
    # )
    return f, rocof, level


def notify(fail):
    if fail == 0:
        msg = "Successful"
    else:
        msg = str(fail) + " failure"
    while True:
        time.sleep(3)
        notification.notify(
            title="Finished executing " + os.path.basename(__file__),
            message=msg,
        )
        time.sleep(5)
        break


# %%
today = datetime.date.today()
out_dir = ".\\" + str(today) + "\\PVs\\"
# out_dir = ".\\" + str(today) + "\\"

event_time = 2
event_time2 = 2.5

file = 'Extended model.xlsx'
xl = pd.ExcelFile(file)
MV_buses_load = xl.parse('MV ATL')['busname']
LV_buses_load = xl.parse('LV ATL')['busname']
LV_buses_PV = xl.parse('LV PV')['busname']
ATLlist = []
MVbuses = [i for i in range(1101, 1176)]
LVbuses = [i for i in range(1, 19)]
buses = []
for MVbus in MVbuses:
    for LVbus in LVbuses:
        buses.append(str(MVbus) + str(LVbus).zfill(2))

Thorizon = 7.5
Tsample = 0.05

LVloadlist = ['TL117501', 'TL117511', 'TL117515', 'TL117516',
              'TL117517', 'TL117518']

# %% Simulation
# control = ['active', 'inactive', 'droop']
# control = ['active', 'droop']
# control = ['active', 'inactive']
# control = ['active']
control = ['inactive']
# control = ['droop']
# control = ['mixed']
# control = ['V-help']

strength = ['weak', 'strong']
# strength = ['weak']
# strength = ['strong']

step_direction = ['up', 'down']
# step_direction = ['up']
# step_direction = ['down']

step_change = [1, 1.5, 2]
# step_change = [2]

# PV_model = "IBG22r_"
PV_model = ""

skip_case = [
    {"control": 'active',
     "strength": 'weak',
     "direction": 'down',
     "step_change": 1.5
     },
    {"control": 'active',
     "strength": 'weak',
     "direction": 'down',
     "step_change": 2
     },
    ]
# skip_case = None
fail = 0
for ctrl in control:
    for stren in strength:
        for step_dir in step_direction:
            for DP in step_change:
                case = {
                    "control": ctrl,
                    "strength": stren,
                    "direction": step_dir,
                    "step_change": DP
                    }
                if case == any(skip_case):
                    continue
                # try:
                f, rocof, level = simulate(case, PV_model, out_dir,
                                           MV_buses_load, Tsample,
                                           Thorizon, event_time,
                                           LV_buses_load, LV_buses_PV,
                                           buses)
                # except Exception:
                #     fail += 1
                #     continue
                fig = plt.figure(None, (10, 6))
                (ax0, ax1, ax2) = fig.subplots(3, 1, sharex=True)
                ax0.set_title(stren + ' ' + ctrl + ' ' + step_dir +
                              ' ' + str(DP))
                ax0.set_title(' '.join((stren, ctrl, step_dir, str(DP))))
                ax0.plot(np.arange(0, Thorizon, Tsample), f, label='frequency')
                ax0.legend()
                ax1.plot(np.arange(0, Thorizon, Tsample), rocof, label='rocof')
                ax1.legend()
                # len1 = len(p_diff[0])
                ax2.plot(np.arange(0, Thorizon, Tsample), level, label='level')
                ax2.legend()
                plt.show()
notify(fail)
