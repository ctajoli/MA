# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 11:19:09 2022

@author: ctajoli
"""
import pyramses
import pandas as pd
import pickle

# %%
PVs = True
# strength = 'weak'
strength = 'strong'
date = "2022-08-18"
if PVs is True:
    # date = "2022-07-27"
    direc = ".\\" + str(date) + "\\PVs\\"
    control = ["inactive", "active", "droop", "mixed", "V-help"]
    legen = ["baseline", "scheme", "droop", "mixed", "V-help"]
    name = "PVs"
else:
    # date = "2022-07-21"
    direc = ".\\" + str(date) + "\\"
    control = ["inactive", "active", "droop"]
    legen = ["baseline", "scheme", "droop"]
    name = "no PVs"

atls = ['TL1', 'TL11', 'TL15', 'TL16', 'TL17', 'TL18']

MVbuses = [i for i in range(1101, 1176)]
LVbuses = [i for i in range(1, 19)]
buses = ['1100']

for MVbus in MVbuses:
    buses.append(str(MVbus).zfill(2))
    for LVbus in LVbuses:
        buses.append(str(MVbus) + str(LVbus).zfill(2))
file_name = []
step_direction = ['up', 'down']
step_change = [1, 1.5, 2]
neg_step_change = [-x for x in step_change]
cols = ["case"] + neg_step_change + step_change
df_fr = pd.DataFrame(columns=cols)
df_v = pd.DataFrame(columns=cols)

for ctrl in control:
    file_name.append(strength + "_" + ctrl + "_")
for i, f_name in enumerate(file_name):
    f_nadir = [legen[i]]
    v_nadir = [legen[i]]
    for di in step_direction:
        for ch in step_change:
            v_min = []
            v_max = []
            ext = pyramses.extractor(direc + f_name + di + str(ch) + ".trj")
            freq = ext.getSync("G").S.value * 50
            for bus in buses:
                v_max.append(max(ext.getBus(bus).mag.value))
                v_min.append(min(ext.getBus(bus).mag.value))
            v_max_abs = max(v_max)
            v_min_abs = min(v_min)

            if di == 'up':
                f_nadir.append(round(max(freq)-50, 3))
                v_nadir.append(round(v_max_abs, 3))
            else:
                f_nadir.append(round(min(freq)-50, 3))
                v_nadir.append(round(v_min_abs, 3))

    df_fr.loc[i] = f_nadir
    df_v.loc[i] = v_nadir
print('Frequency difference ' + strength + ' ' + name + ': \n', df_fr)
print()
print('Voltage nadir ' + strength + ' ' + name + ': \n', df_v)

with open(direc + "f_" + strength + '_' + name + ".obj", "wb") as input_file:
    pickle.dump(df_fr, input_file)
with open(direc + "v_" + strength + '_' + name + ".obj", "wb") as input_file:
    pickle.dump(df_v, input_file)
