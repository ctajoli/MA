#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

file = "Buses1.xlsx"


# In[2]:


xl = pd.ExcelFile(file)
P_loads_MV = xl.parse("Loads")["P"][0:75]
Q_loads_MV = xl.parse("Loads")["Q"][0:75]
P_ind_MV = xl.parse("Induction")["P"][0:75]
Q_ind_MV = xl.parse("Induction")["Q"][0:75]
P_PV_MV = xl.parse("IBGs")["P"][0:75]
Q_PV_MV = xl.parse("IBGs")["Q"][0:75]
P_ATL_MV = xl.parse("Thermal loads")["P"][0:75]
params_ATL_LV = xl.parse("LV atl").iloc[1][9:]


# In[3]:


MVbuses = [i for i in range(1101, 1176)]
LVbuses_loads = [1, 11, 15, 16, 17, 18]
LVbuses_PV = [11, 15, 16, 17, 18]
ATLperc = [0.574782, 0.051791, 0.125605, 0.101162, 0.060261, 0.086399]
LoadPperc = [
    0.331393656,
    0.061664964,
    0.178766207,
    0.144809512,
    0.138363662,
    0.145001999,
]
LoadQperc = [
    0.548100456,
    0.023457208,
    0.099664897,
    0.155540495,
    0.047159155,
    0.126077789,
]
IndPperc = [
    0.178404983,
    0.108715536,
    0.361078444,
    0.026569101,
    0.265516791,
    0.059715144,
]
IndQperc = [
    0.213343987,
    0.109268877,
    0.285257691,
    0.032261286,
    0.295645226,
    0.064222932,
]
PVPperc = [0.133333333, 0.333333333, 0.106666667, 0.266666667, 0.16]


# In[4]:


with open("ATL_list.dat", "w") as f:
    for i, MVbus in enumerate(MVbuses):
        for j, LVbus in enumerate(LVbuses_loads):
            P = round(P_ATL_MV[i] * ATLperc[j], 6)
            Sb = round(
                xl.parse("LV atl").iloc[j][8] * P /
                xl.parse("LV atl").iloc[j][6], 6
            )
            params_ATL_LV = xl.parse("LV atl").iloc[j][9:]
            parameters = str(" ")
            for param in params_ATL_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC ATL TL"
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " "
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " 0 0 "
                + str(P)
                + " 0 "
                + str(Sb)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[5]:


with open("IMP_list.dat", "w") as f:
    for i, MVbus in enumerate(MVbuses):
        for j, LVbus in enumerate(LVbuses_loads):
            P = round(P_loads_MV[i] * LoadPperc[j], 6)
            Q = round(Q_loads_MV[i] * LoadQperc[j], 6)
            params_IMP_LV = xl.parse("LV imp").iloc[j][8:]
            parameters = str(" ")
            for param in params_IMP_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC LOAD L"
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " "
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " 0 0 "
                + str(P)
                + " "
                + str(Q)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[6]:


with open("IND_list.dat", "w") as f:
    for i, MVbus in enumerate(MVbuses):
        for j, LVbus in enumerate(LVbuses_loads):
            P = round(P_ind_MV[i] * IndPperc[j], 6)
            Q = round(Q_ind_MV[i] * IndQperc[j], 6)
            params_IND_LV = xl.parse("LV ind").iloc[j][8:]
            parameters = str(" ")
            for param in params_IND_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC INDMACH1 IM"
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " "
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " 0 0 "
                + str(P)
                + " "
                + str(Q)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[7]:


with open("PV_list.dat", "w") as f:
    for i, MVbus in enumerate(MVbuses):
        for j, LVbus in enumerate(LVbuses_PV):
            P = round(P_PV_MV[i] * PVPperc[j], 6)
            #             Q = Q_PV_MV[i]*PVQperc[j]
            params_IND_LV = xl.parse("LV PV").iloc[j][8:]
            parameters = str(" ")
            for param in params_IND_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC IBG22r PV"
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " "
                + str(MVbus)
                + str(LVbus).zfill(2)
                + " 0 0 "
                + str(P)
                + " 0 "
                + parameters
                + "\n"
            )
        f.write("\n")
