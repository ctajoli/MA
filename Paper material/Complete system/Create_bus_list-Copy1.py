#!/usr/bin/env python
# coding: utf-8

# In[1]:


Type = ["IND", "IMP", "ATL"]
type_dict_P = {"IND": ["0"], "IMP": ["0"], "ATL": ["0"]}
type_dict_Q = {"IND": ["0"], "IMP": ["0"], "ATL": ["0"]}
for typ in type_dict_P:
    a = 1
    with open("artere-" + typ + "_list_test_out.dat") as f:
        lines = f.readlines()
        # new_list = [lines[index] for index in filter(lambda x: x % 2 == 1, range(len(lines)))]
        # print(new_list)
        for line in lines:
            # print(line)
            # # print(line.split())
            if len(line.split()) > 1:
                type_dict_P[typ].append(line.split()[6])
                type_dict_Q[typ].append(line.split()[7])
            else:
                type_dict_P[typ].append("0")
                type_dict_Q[typ].append("0")
    for i in range(len(type_dict_P[typ])):
        type_dict_P[typ][i] = -float(type_dict_P[typ][i])
        type_dict_Q[typ][i] = -float(type_dict_Q[typ][i])

total_P = [
    abs(round(x + y + z, 5))
    for (x, y, z) in zip(type_dict_P["IND"], type_dict_P["IMP"], type_dict_P["ATL"])
]
total_Q = [
    round(x + y + z, 5)
    for (x, y, z) in zip(type_dict_Q["IND"], type_dict_Q["IMP"], type_dict_Q["ATL"])
]
# total_load = [0]*len(type_dict["IND"])
# for i in type_dict.values():
#     total_load += i
# total_load = sum(type_dict.values())


# In[2]:


MV_bus_list = [i for i in range(1101, 1176)]
# print(MV_bus_list)
LV_bus_list = [str(i).zfill(2) for i in range(1, 19)]
# print(LV_bus_list)
count = 0
Total_bus_list = [0] * (len(LV_bus_list) + 1) * len(MV_bus_list)

P = total_P
Q = total_Q

for i in MV_bus_list:
    #     print(i)
    Total_bus_list[count] = "BUS " + str(i) + " 11 0 0 0 0 ;"
    count += 1
    for k in LV_bus_list:
        #         print(str(i)+k)
        Total_bus_list[count] = (
            "BUS "
            + str(i)
            + k
            + " 0.4 "
            + str(P[count])
            + " "
            + str(Q[count])
            + " 0 0 ;"
        )
        count += 1
print(Total_bus_list)


# In[ ]:


# In[ ]:
