import os


def save_figure(directory, fig_name, fig):
    """Saves the figure in the desired directory with the given name,
    without overwriting in case it already exists."""
    str_i = ""
    i = 0
    while True:
        if not os.path.exists(directory + fig_name + str_i + '.png'):
            fig.savefig(directory + fig_name + str_i + '.png',
                        bbox_inches='tight', dpi=300)
            fig.savefig(directory + fig_name + str_i + '.pdf',
                        bbox_inches='tight')
            break
        else:
            i += 1
            str_i = " " + str(i)
