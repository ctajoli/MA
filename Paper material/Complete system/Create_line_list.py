#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

file = "Buses.xlsx"


# In[2]:


xl = pd.ExcelFile(file)
bus_from_LV = xl.parse("LV lines")["bus from"][0:17]
bus_to_LV = xl.parse("LV lines")["bus to"][0:17]


# In[3]:


MV_bus_list = [i for i in range(1101, 1176)]
# MV_bus_list = [i for i in range(1101,1116)]
# print(MV_bus_list)
LV_bus_list = [str(i).zfill(2) for i in range(1, 19)]
# print(LV_bus_list)
count = 0
Total_bus_list = [0] * (len(LV_bus_list) + 1) * len(MV_bus_list)

params_TR = xl.parse("LV lines").iloc[17][4:]
parameters_TR = str(" ")
for param in params_TR:
    parameters_TR = parameters_TR + str(param) + " "
with open("lines_list.txt", "w") as f:
    for i in MV_bus_list:
        for j in range(0, 17):
            #             LV_bus_list = [str(i).zfill(2) for i in range(1,19)]
            bus_from = str(i) + str(int(bus_from_LV[j])).zfill(2)
            bus_to = str(i) + str(int(bus_to_LV[j])).zfill(2)
            params_lines_LV = xl.parse("LV lines").iloc[j][4:10]
            parameters = str(" ")
            for param in params_lines_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "LINE '"
                + bus_from
                + "-"
                + bus_to
                + "' "
                + bus_from
                + " "
                + bus_to
                + parameters
                + "\n"
            )

        f.write(
            "TRANSFO TR"
            + str(i)
            + " "
            + str(i)
            + " "
            + str(i)
            + "01"
            + parameters_TR
            + "\n\n"
        )


# In[4]:


print(bus_to_LV)


# In[5]:


MV_bus_list = [i for i in range(1101, 1176)]
bus_to = str(MV_bus_list[0]) + str(int(bus_from_LV[1])).zfill(2)
print(bus_to)
