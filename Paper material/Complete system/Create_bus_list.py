#!/usr/bin/env python
# coding: utf-8

# The script only creates the bus list, that alternates the MV bus with all the
# lv buses connected.
# Since it was a one time use, copy-pasting the output to the file that already
# has the first buses of transmission network was the most convenient way.


# In[5]:


MV_bus_list = [i for i in range(1101, 1176)]
# print(MV_bus_list)
LV_bus_list = [str(i).zfill(2) for i in range(1, 19)]
# print(LV_bus_list)
count = 0
Total_bus_list = [0]*(len(LV_bus_list)+1)*len(MV_bus_list)


for i in MV_bus_list:
    # print(i)
    Total_bus_list[count] = 'BUS ' + str(i) + ' 11 0 0 0 0 ;'
    count += 1
    for k in LV_bus_list:
        # print(str(i)+k)
        Total_bus_list[count] = 'BUS ' + str(i)+k + ' 0.4 0 0 0 0 ;'
        count += 1
for line in Total_bus_list:
    print(line)
