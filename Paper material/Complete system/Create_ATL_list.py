import pandas as pd
import numpy as np


file = "Extended model.xlsx"
xl = pd.ExcelFile(file)
artere_check = False
artere = "artere-" if artere_check is True else ""


rand_check = True
rand = " rand" if (rand_check and not artere_check) else ""
# MV section
MV_buses_loads = xl.parse("MV imp")["busname"]
P_IMP_MV = xl.parse("MV imp")["P"]
Q_IMP_MV = xl.parse("MV imp")["Q"]
P_IND_MV = xl.parse("MV ind")["P"]
Q_IND_MV = xl.parse("MV ind")["Q"]
P_PV_MV = xl.parse("MV PV")["P"]
Q_PV_MV = xl.parse("MV PV")["Q"]
P_ATL_MV = xl.parse("MV ATL")["P"]
# LV section
LV_buses_loads = xl.parse(artere + "LV imp")["busname"]
LV_buses_PV = xl.parse(artere + "LV PV")["busname"]
P_IMP_LV = xl.parse(artere + "LV imp")["P"]
Q_IMP_LV = xl.parse(artere + "LV imp")["Q"]
P_IND_LV = xl.parse(artere + "LV ind")["P"]
Q_IND_LV = xl.parse(artere + "LV ind")["Q"]
P_PV_LV = xl.parse(artere + "LV PV")["P"]
Q_PV_LV = xl.parse(artere + "LV PV")["Q"]
P_ATL_LV = xl.parse(artere + "LV ATLv4")["P"]
Sb_old_ATL_LV = xl.parse(artere + "LV ATLv4")["Sb"]

params_ATL_LV = xl.parse(artere + "LV ATLv4" + rand).iloc[1][9:]


# Total MV load
P_load_MV = P_IMP_MV + P_IND_MV + P_ATL_MV
Q_load_MV = Q_IMP_MV + Q_IND_MV

# Total LV load
P_load_LV = P_IMP_LV + P_IND_LV + P_ATL_LV
Q_load_LV = Q_IMP_LV + Q_IND_LV


# Shares by bus
P_bus_LV_share = P_load_LV / sum(P_load_LV)
Q_bus_LV_share = Q_load_LV / sum(Q_load_LV)
P_bus_LV_share_PV = P_PV_LV / sum(P_PV_LV)

P_bus_tot = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
Q_bus_tot = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
bus_name = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
count = 0

for i, MVbus in enumerate(MV_buses_loads):
    for j, LVbus in enumerate(LV_buses_loads):
        bus_name[count] = int(100 * MVbus + LVbus)
        P_bus_tot[count] = P_load_MV[i] * P_bus_LV_share[j]
        Q_bus_tot[count] = Q_load_MV[i] * Q_bus_LV_share[j]
        count += 1

P_ind_share_min = 0
P_ind_share_max = 0.1
P_ind_share = (
    np.random.rand(len(P_bus_tot),) * (P_ind_share_max - P_ind_share_min)
    + P_ind_share_min
)
Q_ind_share_min = 0
Q_ind_share_max = 0.1
Q_ind_share = (
    np.random.rand(len(Q_bus_tot),) * (Q_ind_share_max - Q_ind_share_min)
    + Q_ind_share_min
)
P_ATL_share_min = 0.1
P_ATL_share_max = 0.3
P_ATL_share = (
    np.random.rand(len(P_bus_tot),) * (P_ATL_share_max - P_ATL_share_min)
    + P_ATL_share_min
)
pf_ATL_min = 1
pf_ATL_max = 1
pf_ATL = np.random.rand(len(P_bus_tot),) * (pf_ATL_max -
                                            pf_ATL_min) + pf_ATL_min
sb_ATL_factor_min = (
    1 / 1.25
)  # the active power is between 0.35 and 1.25 of the nominal power.
sb_ATL_factor_max = 1 / 0.35
sb_ATL_factor = (
    np.random.rand(len(P_bus_tot),) * (sb_ATL_factor_max - sb_ATL_factor_min)
    + sb_ATL_factor_min
)

P_ind = P_bus_tot * P_ind_share
Q_ind = Q_bus_tot * Q_ind_share
P_ATL = P_bus_tot * P_ATL_share
Q_ATL = (P_ATL / pf_ATL) * np.sqrt(1 - np.square(pf_ATL))
Sb_ATL = P_ATL * sb_ATL_factor
P_imp = P_bus_tot - (P_ind + P_ATL)
Q_imp = Q_bus_tot - (Q_ind + Q_ATL)


variable_parameters_ATL = {
    "ra": [0.01, 0.1],
    "H": [0.03, 0.5],
    "b": [0.0005, 0.002],
    "rt": [0.005, 0.05],
    "lt": [0.1, 0.9],
}


with open(artere + "ATL_list_test.dat", "w") as f:

    for i, bus in enumerate(bus_name):
        ATL_name = "TL" + str("{0:.0f}".format(bus))
        parameters = str(" ")
        for param in params_ATL_LV[
            :-1
        ]:  # skips the ';', since it would be recognized as string
            if rand_check and isinstance(
                param, str
            ):
                # if the parameter is a string the bus number is added to the
                # string to be later defined individually.
                parameters = parameters + param + "_" + ATL_name + " "
            else:
                parameters = parameters + str(param) + " "
        parameters = parameters + ";"  # the ';' is added again
        # f.write('INJEC ATLv4 ' + ATL_name + ' ' + str(bus) + ' 0 0 ' +
        #         str(P_ATL[i]) + ' ' + str(Q_ATL[i]) + ' ' + '0.006' +
        #         parameters + '\n')
        f.write(
            "INJEC ATLv4 "
            + ATL_name
            + " {:.0f} 0 0 {:.5f} {:.5f}".format(bus, P_ATL[i], Q_ATL[i])
            + " {}{}\n".format("0.006", parameters)
        )

with open(artere + "ATL_list_test.dat", "r") as file:
    filedata = file.read()
    # filedata = f.read()
    for i, bus in enumerate(bus_name):
        ATL_name = "TL" + str("{0:.0f}".format(bus))
        filedata = filedata.replace("pf_" + ATL_name,
                                    str("{:.3f}".format(pf_ATL[i])))

        for var_par in variable_parameters_ATL:
            filedata = filedata.replace(
                var_par + "_" + ATL_name,
                str(
                    "{:.3f}".format(
                        np.random.uniform(
                            variable_parameters_ATL[var_par][0],
                            variable_parameters_ATL[var_par][1],
                        )
                    )
                ),
            )
with open(artere + "ATL_list_test_out.dat", "w") as file:
    file.write(filedata)

    # f.write('\n')
    # for i, MVbus in enumerate(MV_buses_loads):
    #     for j, LVbus in enumerate(LV_buses_load):
    #         bus_number = str(MVbus) + str(LVbus).zfill(2)
    #         ATL_name = 'TL' + bus_number
    #         P_name = 'P_' + ATL_name
    #         Q_name = 'Q_' + ATL_name
    #         Sb_name = 'Sb_' + ATL_name
    #         parameters = str(' ')
    #         for param in params_ATL_LV[:-1]:
    #             # skips the ';', since it would be recognized as string
    #             if rand_check and isinstance(param, str):
    #                 # if the parameter is a string the bus number is added
    #                 # to the string to be later defined individually.
    #                 parameters = parameters + str(param) + '_' + ATL_name + ' '
    #             else:
    #                 parameters = parameters + str(param) + ' '
    #         parameters = parameters + ';'  # the ';' is added again
    #         f.write('INJEC ATLv4 ' + ATL_name + ' ' + bus_number + ' 0 0 ' +
    #                 P_name + ' ' + Q_name + ' ' + Sb_name + parameters + '\n')
    #     f.write('\n')


# Shares by type
# P_IND_LV_share = [0]*len(LV_buses_load)
# Q_IND_LV_share = [0]*len(LV_buses_load)
# for i, P_load in enumerate(P_load_LV):
#     if P_load_LV[i] == 0:
#         P_IND_LV_share[i] = 0
#         Q_IND_LV_share[i] = 0
#     else:
#         P_IND_LV_share[i] = P_IND_LV[i]/P_load_LV[i]
#         Q_IND_LV_share[i] = Q_IND_LV[i]/Q_load_LV[i]


# pf_start = 0.95
# pf_end = 1
# variable_parameters_ATL = {
#     'ra': [0.01, 0.1],
#     'H': [0.03, 0.5],
#     'b': [0.0005, 0.002],
#     'rt': [0.005, 0.05],
#     'lt': [0.1, 0.9]
# }


# rand_check = True
# rand = ' rand' if (rand_check and not artere_check) else ''
# #Total MV load
# P_load_MV = P_IMP_MV + P_IND_MV + P_ATL_MV
# Q_load_MV = Q_IMP_MV + Q_IND_MV

# #Total LV load
# P_load_LV = P_IMP_LV + P_IND_LV + P_ATL_LV
# Q_load_LV = Q_IMP_LV + Q_IND_LV

# # Shares by type
# P_IND_LV_share = [0]*len(LV_buses_load)
# Q_IND_LV_share = [0]*len(LV_buses_load)
# for i, P_load in enumerate(P_load_LV):
#     if P_load_LV[i] == 0:
#         P_IND_LV_share[i] = 0
#         Q_IND_LV_share[i] = 0
#     else:
#         P_IND_LV_share[i] = P_IND_LV[i]/P_load_LV[i]
#         Q_IND_LV_share[i] = Q_IND_LV[i]/Q_load_LV[i]

# P_ATL_LV_share_min = 0.1
# P_ATL_LV_share_max = 0.3
# P_ATL_LV_share_fix = [0.2]*len(LV_buses_load)
# # seed = 1
# # np.random.seed(seed)

# P_ATL_LV_share_rand = np.random.rand(len(LV_buses_load),) * \
#     (P_ATL_LV_share_max - P_ATL_LV_share_min) + P_ATL_LV_share_min
# if rand_check:
#     P_ATL_LV_share = P_ATL_LV_share_rand
# else:
#     P_ATL_LV_share = P_ATL_LV_share_fix

# P_IMP_LV_share = 1 - (np.array(P_IND_LV_share) + np.array(P_ATL_LV_share))
# Q_IMP_LV_share = 1 - np.array(Q_IND_LV_share)

# # Shares by bus
# P_bus_LV_share = P_load_LV/sum(P_load_LV)
# Q_bus_LV_share = Q_load_LV/sum(Q_load_LV)
# P_bus_LV_share_PV = P_PV_LV/sum(P_PV_LV)


# with open(artere + 'ATL_list_in.dat', 'w') as f:
#     for i, MVbus in enumerate(MV_buses_loads):
#         for j, LVbus in enumerate(LV_buses_load):
#             bus_number = str(MVbus) + str(LVbus).zfill(2)
#             ATL_name = 'TL' + bus_number
#             P_name = 'P_' + ATL_name
#             Q_name = 'Q_' + ATL_name
#             Sb_name = 'Sb_' + ATL_name
#             # P = P_ATL_LV_share[j]*P_load_MV[i]*P_bus_LV_share[j]
#             # if P_ATL_LV[j] == 0:
#             #     Sb = 0
#             # else:
#             #     Sb = round(Sb_old_ATL_LV[j]*P/P_ATL_LV[j],6)
#             params_ATL_LV = xl.parse(artere+'LV ATLv4'+rand).iloc[j][9:]
#             parameters = str(' ')
#             for param in params_ATL_LV[:-1]:
#                 # skips the ';', since it would be recognized as string
#                 if rand_check and isinstance(param, str):
#                     # if the parameter is a string the bus number is added
#                     # to the string to be later defined individually.
#                     parameters = parameters + str(param) + '_' + ATL_name + ' '
#                 else:
#                     parameters = parameters + str(param) + ' '
#             parameters = parameters + ';'  # the ';' is added again
#             f.write('INJEC ATLv4 ' + ATL_name + ' ' + bus_number + ' 0 0 ' +
#                     P_name + ' ' + Q_name + ' ' + Sb_name + parameters + '\n')
#         f.write('\n')


# P_ATL_LV_new = [0]*len(LV_buses_load)
# Q_ATL_LV_new = [0]*len(LV_buses_load)
# np.random.seed()
# P_ATL_name_prefix = "P_TL"
# with open(artere + 'ATL_list_in.dat', 'r') as file:
#     filedata = file.read()

# for i, MV_bus in enumerate(MV_buses_loads):
#     for j, LV_bus in enumerate(LV_buses_load):
#         bus_number = str(MV_bus) + str(LV_bus).zfill(2)
#         ATL_name = 'TL' + bus_number
#         P_name = 'P_' + ATL_name
#         Q_name = 'Q_' + ATL_name
#         Sb_name = 'Sb_' + ATL_name
#         pf_name = 'pf_' + ATL_name
#         pf = np.random.uniform(pf_start, pf_end)
#         P_ATL_LV_new[j] = round(P_ATL_LV_share[j] * P_load_MV[i] *
#                                 P_bus_LV_share[j], 5)
#         Q_ATL_LV_new[j] = round(P_ATL_LV[j] * np.sqrt(1-pf ^ 2) / pf, 5)
#         Sb_ATL_LV_new = round(Sb_old_ATL_LV[j] * P_ATL_LV_new[j] /
#                               (P_ATL_LV[j] * pf), 6)
#         print(Sb_ATL_LV_new)
#         filedata = filedata.replace(P_name, str(P_ATL_LV_new[j]))
#         # filedata = filedata.replace(Q_name, str(Q_ATL_LV_new[j]))
#         filedata = filedata.replace(Q_name, str(0))
#         filedata = filedata.replace(Sb_name, str(Sb_ATL_LV_new))
#         filedata = filedata.replace(pf_name, str("{:.3f}".format(pf)))

#         for var_par in variable_parameters_ATL:
#             filedata = filedata.replace(var_par + '_' + ATL_name, str("{:.3f}".format(np.random.uniform(variable_parameters_ATL[var_par][0],variable_parameters_ATL[var_par][1]))))
# with open(artere + 'ATL_list_out.dat', 'w') as file:
#     file.write(filedata)
