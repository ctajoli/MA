#!/usr/bin/env python
# coding: utf-8

# In[1]:


import seaborn as sns
from matplotlib import rc
import pyramses
# import cmath
# import math
# import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1.inset_locator import (
#     inset_axes,
#     InsetPosition,
#     mark_inset,
#     zoomed_inset_axes,
# )
# import pandas as pd
import datetime
# import json, codecs
import os
# from save_figure import save_figure

today = datetime.date.today()
# date = today
date = '2022-04-06'
directory = ".\\" + str(date) + "\\"
fig_name = "strong-weak comparison"


sns.set_style("darkgrid", {"axes.facecolor": ".9"})
cm = 1 / 2.54
sns.set_context("paper", font_scale=0.7, rc={"grid.linewidth": 0.6})
rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
rc("font", **{"family": "serif", "serif": ["Palatino"]})
rc("text", usetex=True)
# colors_ blue, red, yellow, green. purple gray light blue dark blue dark green
palette1 = [
    "#1269b0",
    "#a8322d",
    "#edb120",
    "#72791c",
    "#91056a",
    "#6f6f64",
    "#007a96",
    "#1f407a",
    "#485a2c",
]
palette = [
    "#1269b0",
    '#1f407a',
    "#a8322d",
    "#edb120",
    '#72791c',
    '#91056a',
    '#6f6f64',
    '#1f407a',
    '#485a2c'
]

# palette_old = ["#1269b0", "#a8322d", "#91056a", '#edb120', '#72791c',
#                '#6f6f64', '#007a96', '#1f407a', '#485a2c']

MVbuses = [i for i in range(1101, 1176)]
sns.set_palette(palette)
LVbuses = [i for i in range(1, 19)]
LVbuses_loads = [1, 11, 15, 16, 17, 18]
LVbuses_PV = [11, 15, 16, 17, 18]
buses = []
loads = []
PVs = []
TLs = []
IMs = []
for MVbus in MVbuses:
    for LVbus in LVbuses:
        buses.append(str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        loads.append("IMP" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_PV:
        PVs.append("PV" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        TLs.append("ATL" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        IMs.append("IND" + str(MVbus) + str(LVbus).zfill(2))
pt = 1 / 72.27


parameters_left = "\n".join(
    (
        "$H = 1.5$ s",
        r"$\Delta P = 1.5$ MW",
        # "ATLs' load share = 20\%"
    )
)
parameters_right = "\n".join(
    (
        "$H = 6$ s",
        r"$\Delta P = 1.5$ MW",
        # "ATLs' load share = 20\%"
    )
)
ext0 = pyramses.extractor(directory + "weak_active" + ".trj")
ext1 = pyramses.extractor(directory + "strong_active" + ".trj")


# In[4]:


fig, axs = plt.subplots(
    3, 2, figsize=(360 * pt, 3 * 90 * pt), sharex="col", sharey="row"
)
fig.subplots_adjust(hspace=0.25, wspace=0.05)

axs = axs.ravel()
w0 = ext0.getSync('G').S
w1 = ext1.getSync('G').S

# sns.lineplot(ax=axs[0], y=w0.value*50, x=w0.time,linewidth=0.5,
#              label='f', legend = False)
# sns.lineplot(ax=axs[1], y=w1.value*50, x=w1.time,linewidth=0.5,
#              label='f', legend = False)

# axs[0].plot(w.time, w.value*50, label='f')
# axs[0].set_title('Frequency')
# axs[0].set_ylabel('$f$ [Hz]')
for bus in buses:
    v0 = ext0.getBus(bus).mag
    v1 = ext1.getBus(bus).mag
    axs[0].plot(v0.time, v0.value, linewidth=0.4)
    axs[1].plot(v1.time, v1.value, linewidth=0.4)
#     ax1.legend(loc="upper right")
# axs[2].set_title('Voltage Magnitude')
axs[0].set_ylabel("$v$ [p.u.]")
# axs[4].set_title('Active Power')
axs[2].set_ylabel(r"$p_\mathrm{ATL}$ [p.u.]")
# axs[6].set_title('Rective Power')
axs[4].set_ylabel(r"$q_\mathrm{ATL}$ [p.u.]")
# axs[8].set_ylabel('$P_\mathrm{sync}$ [MW]')
axs[4].set_xlabel("time [s]")
axs[5].set_xlabel("time [s]")
for unit in TLs:
    P0 = ext0.getInj(unit).P
    P1 = ext1.getInj(unit).P
    # Q = ext.getInj(unit).Q
    # wm = ext.getInj(unit).wm
    punit0 = ext0.getInj(unit).Punit
    punit1 = ext1.getInj(unit).Punit
    qunit0 = ext0.getInj(unit).Qunit
    qunit1 = ext1.getInj(unit).Qunit

    # status = ext.getInj(unit).status
    axs[2].plot(punit0.time, punit0.value, linewidth=0.4)
    axs[3].plot(punit1.time, punit1.value, linewidth=0.4)
    axs[4].plot(qunit0.time, qunit0.value, linewidth=0.4)
    axs[5].plot(qunit1.time, qunit1.value, linewidth=0.4)
    # axs[3].plot(P.time, P.value*1000, linewidth=0.4)
#     ax2.legend(loc="upper right")
# axs[3].set_title('Reactive Power in kVar')
# axs[3].plot(qunit.time, qunit.value, label=unit)
Psync0 = ext0.getSync('G').P
Psync1 = ext1.getSync('G').P
# sns.lineplot(ax=axs[8], y=Psync0.value, x=Psync0.time,linewidth=0.5,
#              label='P', legend = False)
# sns.lineplot(ax=axs[9], y=Psync1.value, x=Psync1.time,linewidth=0.5,
#              label='P', legend = False)
# axs[3].set_ylabel('$P_\mathrm{atl}$ [kW]')
axs[0].set_yticks([0.90, 0.95])
props = dict(boxstyle="square", facecolor="white", alpha=0.5)
axs[4].text(
    0.05,
    0.13,
    parameters_left,
    bbox=props,
    transform=axs[4].transAxes,
    ha="left",
    va="bottom",
)
axs[5].text(
    0.05,
    0.13,
    parameters_right,
    bbox=props,
    transform=axs[5].transAxes,
    ha="left",
    va="bottom",
)
fig.align_ylabels(axs[:])
# fig.savefig(out_dir + control + '_' + strength + '.png',
#             bbox_inches='tight', dpi = 300)


str_i = ""
i = 0
while True:
    if not os.path.exists(directory + fig_name + str_i + ".png"):
        fig.savefig(directory + fig_name + str_i + ".png",
                    bbox_inches="tight", dpi=300)

        break
    else:
        i += 1
        str_i = " " + str(i)


# In[3]:


fig_name = "direct_comparison"
fig, axs = plt.subplots(
    3, 1, figsize=(400 * pt, 3 * 90 * pt), sharex="col", sharey="row"
)
fig.subplots_adjust(hspace=0.25, wspace=0.05)
axs = axs.ravel()
sns.lineplot(
    ax=axs[0],
    y=w0.value * 50,
    x=w0.time,
    linewidth=0.5,
    label="weak grid",
    legend=False,
)
sns.lineplot(
    ax=axs[0],
    y=w1.value * 50,
    x=w1.time,
    linewidth=0.5,
    label="strong grid",
    legend=False,
)
sns.lineplot(
    ax=axs[1],
    y=ext0.getBus("114115").mag.value,
    x=Psync0.time,
    linewidth=0.5,
    label="weak grid",
    legend=False,
)
sns.lineplot(
    ax=axs[1],
    y=ext1.getBus("114115").mag.value,
    x=Psync1.time,
    linewidth=0.5,
    label="strong grid",
    legend=False,
)
sns.lineplot(
    ax=axs[2],
    y=Psync0.value,
    x=Psync0.time,
    linewidth=0.5,
    label="weak grid",
    legend=False,
)
sns.lineplot(
    ax=axs[2],
    y=Psync1.value,
    x=Psync1.time,
    linewidth=0.5,
    label="strong grid",
    legend=False,
)
axs[0].set_ylabel("$f$ [Hz]")
axs[1].set_ylabel("$v$ [p.u.]")
axs[2].set_ylabel(r"$P_\mathrm{sync}$ [MW]")
axs[2].set_xlabel("time [s]")
fig.align_ylabels(axs[:])
axs[2].legend(loc="upper right", bbox_to_anchor=(1, -0.25), ncol=2)

str_i = ""
i = 0
while True:
    if not os.path.exists(directory + fig_name + str_i + ".png"):
        fig.savefig(directory + fig_name + str_i + ".png",
                    bbox_inches="tight", dpi=300)

        break
    else:
        i += 1
        str_i = " " + str(i)

# plt.savefig(out_dir+'Vn.pdf')
