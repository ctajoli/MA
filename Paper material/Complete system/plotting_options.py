# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 13:49:17 2022

@author: ctajoli
"""
import seaborn as sns
from matplotlib import rc


def plotting_options():
    sns.set_style("darkgrid", {"axes.facecolor": ".9"})
    sns.set_context("paper", font_scale=0.7, rc={"grid.linewidth": 0.6})
    rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
    rc("font", **{"family": "serif", "serif": ["Palatino"]})
    rc("text", usetex=True)
    # palette1 = [
    #     "#1269b0",
    #     "#a8322d",
    #     "#edb120",
    #     "#72791c",
    #     "#91056a",
    #     "#6f6f64",
    #     "#007a96",
    #     "#1f407a",
    #     "#485a2c",
    # ]
    # palette = [
    #     "#1269b0",
    #     '#1f407a',
    #     "#a8322d",
    #     "#edb120",
    #     '#72791c',
    #     '#91056a',
    #     '#6f6f64',
    #     '#1f407a',
    #     '#485a2c'
    # ]
    palette_new = [
        '#000000',
        "#1269b0",
        "#a8322d",
        "#edb120",
        '#72791c',
        '#91056a',
        '#6f6f64',
        '#1f407a',
        '#485a2c'
    ]
    # palette_old = ["#1269b0", "#a8322d", "#91056a", '#edb120', '#72791c',
    #                '#6f6f64', '#007a96', '#1f407a', '#485a2c']
    sns.set_palette(palette_new)
