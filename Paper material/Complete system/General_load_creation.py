#!/usr/bin/env python
# coding: utf-8

# %% Imports
import pandas as pd
import numpy as np
import os

# %% Functions


def write_load_file(
    artere,
    model,
    typ,
    bus_name,
    params_LV,
    P,
    Q,
    variable_parameters,
    strength,
    constant_parameters=None,
    Sb=None,
    pf=None,
    init_values=None,
    I_max=None,
    IN=None,
):
    """
    artere = str, 'artere-' or ''
    typ = str, for example 'TL'
    bus_name = numpy array [len(MV)*len(LV),]
    params_LV = array from excel, starting after P and Q (column 8)
    P = numpy array [len(MV)*len(LV),]
    Q = numpy array [len(MV)*len(LV),]
    variable_parameters = dictionary with the ranges of min and max of the
    parameters to randomize
    constant_parameters = dictionary with the values of some parameters that
    remain constant in the simulation, but could be useful to change between
    simulations.
    Sb, pf = numpy array [len(MV)*len(LV),], only for ATLs
    init_values = dictionary of np array [len(MV)*len(LV),], only for ATLs
    """
    # writing a first file with strings instead of the variable
    # parameters and power.
    with open(dire + artere + typ + "_" + model + "_list_temp.dat", "w") as f:
        for i, bus in enumerate(bus_name):
            name = typ + str("{0:.0f}".format(bus))
            parameters = str(" ")
            for param in params_LV[
                :-1
            ]:  # skips the ';', since it would be recognized as string
                if isinstance(param, str):
                    # if the parameter is a string the bus number is added to
                    # be later defined individually.
                    parameters = parameters + param + "_" + name + " "
                else:
                    parameters = parameters + str(param) + " "
            parameters = parameters + ";"  # the ';' is added again
            f.write(
                "INJEC "
                + model
                + " "
                + name
                + " {:.0f} 0 0 {:.5f} {:.5f}".format(bus, P[i], Q[i])
                + parameters
                + "\n"
            )
    # writing a second file in which the correct values are wrote in.
    # This could be made as a different function and be the only one to be run.
    with open(dire + artere + typ + "_" + model +
              "_list_temp.dat", "r") as file:
        filedata = file.read()
        # filedata = f.read()
        for i, bus in enumerate(bus_name):
            name = typ + str("{0:.0f}".format(bus))
            if isinstance(Sb, np.ndarray):
                filedata = filedata.replace("Sb_" + name,
                                            str("{:.5f}".format(Sb[i])))
            if isinstance(pf, np.ndarray):
                filedata = filedata.replace("pf_" + name,
                                            str("{:.3f}".format(pf[i])))
            if isinstance(I_max, np.ndarray):
                filedata = filedata.replace("PV_I_max_" + name,
                                            str("{:.3f}".format(I_max[i])))
            if isinstance(IN, np.ndarray):
                filedata = filedata.replace("PV_IN_" + name,
                                            str("{:.3f}".format(IN[i])))
            if isinstance(init_values, dict):
                for init_val in init_values:
                    filedata = filedata.replace(
                        init_val + "_" + name,
                        str("{:.0f}".format(init_values[init_val][i])),
                    )
            if isinstance(constant_parameters, dict):
                for const_par in constant_parameters:
                    filedata = filedata.replace(
                        const_par + "_" + name,
                        str(constant_parameters[const_par])
                    )
            for var_par in variable_parameters:
                filedata = filedata.replace(
                    var_par + "_" + name,
                    str(
                        "{:.3f}".format(
                            np.random.uniform(
                                variable_parameters[var_par][0],
                                variable_parameters[var_par][1],
                            )
                        )
                    ),
                )
    with open(dire + artere + typ + "_" + model + "_list_"
              + strength + ".dat", "w") as file:
        file.write(filedata)


def convert_to_pu(par_perc, P_max, P_min):
    """It converts the power parameter given in
    percentage of max-min range to the per unit value"""
    par_pu = par_perc * (P_max - P_min) + P_min
    return par_pu


def check_sign(pl_check):
    pl = np.zeros(len(pl_check))
    for i in range(len(pl)):
        pl[i] = 1 if pl_check[i] >= 0 else 0
    return pl


# %% Selection

file = "Extended model.xlsx"
xl = pd.ExcelFile(file)
artere_check = False
artere = "artere-" if artere_check is True else ""

rand_check = True
rand = " rand" if (rand_check and not artere_check) else ""


# ATL_model = 'ATLv5'
# ATL_model = 'ATLv6'
# ATL_model = 'ATLv7'
# ATL_model = 'ATLv8'
ATL_model = 'ATLv9'

# ATL_model = "ATL"

PV_model = "IBGtesting2"
# PV_model = 'IBGtesting3' # not used
# PV_model = 'IBG22r'
strengths = ["strong", "weak"]
# strength = "strong"
# strength = 'weak'

PV_sheet_name = "PV" if PV_model == "IBG22r" else "PV testing2"

dire = os.getcwd() + "\\cmd\\"

# %% Data extraction


# MV section
MV_buses_loads = xl.parse("MV imp")["busname"]
P_IMP_MV = xl.parse("MV imp")["P"]
Q_IMP_MV = xl.parse("MV imp")["Q"]
P_IND_MV = xl.parse("MV ind")["P"]
Q_IND_MV = xl.parse("MV ind")["Q"]
P_PV_MV = xl.parse("MV PV")["P"]
Q_PV_MV = xl.parse("MV PV")["Q"]
P_ATL_MV = xl.parse("MV ATL")["P"]
# LV section
LV_buses_loads = xl.parse(artere + "LV imp")["busname"]
LV_buses_PV = xl.parse(artere + "LV PV")["busname"]
P_IMP_LV = xl.parse(artere + "LV imp")["P"]
Q_IMP_LV = xl.parse(artere + "LV imp")["Q"]
P_IND_LV = xl.parse(artere + "LV ind")["P"]
Q_IND_LV = xl.parse(artere + "LV ind")["Q"]
P_PV_LV = xl.parse(artere + "LV PV")["P"]
Q_PV_LV = xl.parse(artere + "LV PV")["Q"]
# P_ATL_LV = xl.parse(artere+'LV ATLv4') ['P']
P_ATL_LV = xl.parse(artere + "LV " + ATL_model)["P"]
# Sb_old_ATL_LV = xl.parse(artere+'LV ATLv4') ['Sb']
Sb_old_ATL_LV = xl.parse(artere + "LV " + ATL_model)["Sb"]

# params_ATL_LV = xl.parse(artere+'LV ATLv4'+rand).iloc[1][8:]
params_ATL_LV = xl.parse(artere + "LV " + ATL_model + rand).iloc[1][8:]
params_IMP_LV = xl.parse(artere + "LV imp" + rand).iloc[1][8:]
params_IND_LV = xl.parse(artere + "LV ind" + rand).iloc[1][8:]
params_PV_LV = xl.parse(artere + "LV " + PV_sheet_name + rand).iloc[1][8:]


# %% Power division by bus

# The lv system will always have the same share of load in the buses,
# as a fraction of the power in the MV bus.
# Total MV load
P_load_MV = P_IMP_MV + P_IND_MV + P_ATL_MV
Q_load_MV = Q_IMP_MV + Q_IND_MV

# Total LV load
P_load_LV = P_IMP_LV + P_IND_LV + P_ATL_LV
Q_load_LV = Q_IMP_LV + Q_IND_LV


# Shares by bus
P_bus_LV_share = P_load_LV / sum(P_load_LV)
Q_bus_LV_share = Q_load_LV / sum(Q_load_LV)
P_bus_LV_share_PV = P_PV_LV / sum(P_PV_LV)

P_bus_tot = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
Q_bus_tot = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
P_PV = np.zeros(len(MV_buses_loads) * len(LV_buses_PV),)
Q_PV = np.zeros(len(MV_buses_loads) * len(LV_buses_PV),)

bus_name = np.zeros(len(MV_buses_loads) * len(LV_buses_loads),)
bus_name_PV = np.zeros(len(MV_buses_loads) * len(LV_buses_PV),)
count = 0

for i, MVbus in enumerate(MV_buses_loads):
    for j, LVbus in enumerate(LV_buses_loads):
        bus_name[count] = int(100 * MVbus + LVbus)
        P_bus_tot[count] = P_load_MV[i] * P_bus_LV_share[j]
        Q_bus_tot[count] = Q_load_MV[i] * Q_bus_LV_share[j]
        count += 1
count = 0
for i, MVbus in enumerate(MV_buses_loads):
    for j, LVbus in enumerate(LV_buses_PV):
        bus_name_PV[count] = int(100 * MVbus + LVbus)
        P_PV[count] = P_PV_MV[i] * P_bus_LV_share_PV[j]
        Q_PV[count] = 0
        count += 1

# %% Power division by load

# The power of each LV bus is shared randomly between the exponential loads,
# the induction machines and the ATLs.
np.random.seed(1)
P_ind_share_min = 0.001
P_ind_share_max = 0.1
P_ind_share = (
    np.random.rand(len(P_bus_tot),) * (P_ind_share_max - P_ind_share_min)
    + P_ind_share_min
)
Q_ind_share_min = 0.001
Q_ind_share_max = 0.1
Q_ind_share = (
    np.random.rand(len(Q_bus_tot),) * (Q_ind_share_max - Q_ind_share_min)
    + Q_ind_share_min
)
P_ATL_share_min = 0.1
P_ATL_share_max = 0.3
P_ATL_share = (
    np.random.rand(len(P_bus_tot),) * (P_ATL_share_max - P_ATL_share_min)
    + P_ATL_share_min
)
pf_ATL_min = 1
pf_ATL_max = 1
pf_ATL = np.random.rand(len(P_bus_tot),) * (pf_ATL_max
                                            - pf_ATL_min) + pf_ATL_min
sb_ATL_factor_min = (
    1 / 1.25
)  # the active power is between 0.35 and 1.25 of the nominal power.
sb_ATL_factor_max = 1 / 0.35
sb_ATL_factor = (
    np.random.rand(len(P_bus_tot),) * (sb_ATL_factor_max - sb_ATL_factor_min)
    + sb_ATL_factor_min
)

PV_gen_min = 0.05
PV_gen_max = 0.8
PV_I_max = P_PV / (np.random.rand(len(P_PV),) * (PV_gen_max - PV_gen_min)
                   + PV_gen_min)
PV_IN = PV_I_max / 1.2

P_ind = P_bus_tot * P_ind_share
Q_ind = Q_bus_tot * Q_ind_share
P_ATL = P_bus_tot * P_ATL_share
Q_ATL = (P_ATL / pf_ATL) * np.sqrt(1 - np.square(pf_ATL))
Sb_ATL = -P_ATL * sb_ATL_factor
P_imp = P_bus_tot - (P_ind + P_ATL)
Q_imp = Q_bus_tot - (Q_ind + Q_ATL)


# %% Initial values

# Defining the parameters that will differ from unit
# to unit at the initialization
if ATL_model != "ATL":
    p_unit = -P_ATL / Sb_ATL  # equal to Pref_lim in codegen at t = 0
    P_max = params_ATL_LV["P_max"]
    P_min = params_ATL_LV["P_min"]
    pl1_2_check = p_unit - convert_to_pu(params_ATL_LV["l_1_neg_min_par"],
                                         P_max, P_min)
    pl1_4_check = convert_to_pu(params_ATL_LV["l_1_neg_max_par"], P_max,
                                P_min) - p_unit
    pl1_6_check = p_unit - convert_to_pu(params_ATL_LV["l_1_pos_min_par"],
                                         P_max, P_min)
    pl1_8_check = convert_to_pu(params_ATL_LV["l_1_pos_max_par"], P_max,
                                P_min) - p_unit
    pl1_2_0 = check_sign(pl1_2_check)
    pl1_4_0 = check_sign(pl1_4_check)
    pl1_6_0 = check_sign(pl1_6_check)
    pl1_8_0 = check_sign(pl1_8_check)

    pl2_2_check = p_unit - convert_to_pu(params_ATL_LV["l_2_neg_min_par"],
                                         P_max, P_min)
    pl2_4_check = convert_to_pu(params_ATL_LV["l_2_neg_max_par"], P_max,
                                P_min) - p_unit
    pl2_6_check = p_unit - convert_to_pu(params_ATL_LV["l_2_pos_min_par"],
                                         P_max, P_min)
    pl2_8_check = convert_to_pu(params_ATL_LV["l_2_pos_max_par"], P_max,
                                P_min) - p_unit
    pl2_2_0 = check_sign(pl2_2_check)
    pl2_4_0 = check_sign(pl2_4_check)
    pl2_6_0 = check_sign(pl2_6_check)
    pl2_8_0 = check_sign(pl2_8_check)

    Vdb_p = params_ATL_LV["Vdb_p"]
    Vdb_m = params_ATL_LV["Vdb_m"]
    lines = [[]]
    lines_in = []
    complete_bus_list = []
    V = np.zeros(len(bus_name))
    F_help_high_0 = np.zeros(len(bus_name))
    F_help_low_0 = np.zeros(len(bus_name))
    for strength in strengths:
        with open(dire + "base_" + strength + ".dat", "r") as f:
            for ln in f:
                line = ln
                lines.append(line.split())
    for i in range(len(lines)):
        if len(lines[i]) > 3:
            lines_in.append(lines[i])
    j = 0
    for i in range(len(lines_in)):
        if lines_in[j][0] != "LFRESV":
            lines_in.pop(j)
        else:
            j += 1
    for i in range(len(lines_in)):
        complete_bus_list.append(lines_in[i][1])
    for i, bus in enumerate(bus_name):
        position = complete_bus_list.index(str("{0:.0f}".format(bus)))
        V[i] = lines_in[position][2]

        F_help_high_0[i] = 1 if V[i] >= Vdb_p else 0
        F_help_low_0[i] = 1 if V[i] <= Vdb_m else 0

    init_values_ATL = {
        "pl1_2_0": pl1_2_0,
        "pl1_4_0": pl1_4_0,
        "pl1_6_0": pl1_6_0,
        "pl1_8_0": pl1_8_0,
        "pl2_2_0": pl2_2_0,
        "pl2_4_0": pl2_4_0,
        "pl2_6_0": pl2_6_0,
        "pl2_8_0": pl2_8_0,
        "F_help_high_0": F_help_high_0,
        "F_help_low_0": F_help_low_0,
    }

if PV_model != "IBG22r":
    Vdb_p_PV = params_PV_LV["Vdbp"]
    Vdb_m_PV = params_PV_LV["Vdbm"]
    lines = [[]]
    lines_in = []
    complete_bus_list = []
    V = np.zeros(len(bus_name_PV))
    F_help_high_0 = np.zeros(len(bus_name_PV))
    F_help_low_0 = np.zeros(len(bus_name_PV))
    for strength in strengths:
        with open(dire + "base_" + strength + ".dat", "r") as f:
            for ln in f:
                line = ln
                lines.append(line.split())
    for i in range(len(lines)):
        if len(lines[i]) > 3:
            lines_in.append(lines[i])
    j = 0
    for i in range(len(lines_in)):
        if lines_in[j][0] != "LFRESV":
            lines_in.pop(j)
        else:
            j += 1
    for i in range(len(lines_in)):
        complete_bus_list.append(lines_in[i][1])
    for i, bus in enumerate(bus_name_PV):
        position = complete_bus_list.index(str("{0:.0f}".format(bus)))
        V[i] = lines_in[position][2]

        F_help_high_0[i] = 1 if V[i] >= Vdb_p_PV else 0
        F_help_low_0[i] = 1 if V[i] <= Vdb_m_PV else 0

    init_values_PV = {"F_help_high_0": F_help_high_0,
                      "F_help_low_0": F_help_low_0}


# %% Parameter ranges

# Parameters that get changed from load to load are variable_parameters,
# while the constant_parameters are the same for each load.
variable_parameters_ATL = {
    "ra": [0.01, 0.1],
    "H": [0.03, 0.5],
    "b": [0.0005, 0.002],
    "rt": [0.005, 0.05],
    "lt": [0.1, 0.9],
}
constant_parameters_ATL = {
    "prot": -1,
    "ro_v1": 0.833,
    "ro_v2": 0.5,
    "ro_p": 0.1,
    "V_min_nb": 0.9,
    "dpc_1": 0.2,
    "dpc_2": 0.3,
}
variable_parameters_IMP = {
    "alpha1": [1, 2],
    "beta1": [1.5, 3]
}
variable_parameters_IND = {
    "Rs": [0.03, 0.13],
    "RR": [0.03, 0.13],
    "LSR": [2.5, 4],
    "LLS": [0.07, 0.15],
    "LLR": [0.06, 0.15],
    "H": [0.4, 0.6],
    "LF": [0.4, 0.6],
}
variable_parameters_PV = {
    "Iprate": [0.2, 0.5],
    "Tg": [0.1, 0.3],
    "tau": [50, 100]
}
constant_parameters_PV = {
    "prot": -1,
    "support": -1,
    "V_hlp": 0,
    "ro_v1": 0.5,
    "ro_v2": 0.5,
    "dPs_rate_max": 10,
    "dQs_rate_max": 10,
    "dpc_1": -0.2,
    "dpc_2": -0.3,
}

# %% Function execution

for strength in strengths:
    if ATL_model != "ATL":
        write_load_file(
            artere,
            ATL_model,
            "ATL",
            bus_name,
            params_ATL_LV,
            P_ATL,
            Q_ATL,
            variable_parameters_ATL,
            strength,
            constant_parameters_ATL,
            Sb_ATL,
            pf_ATL,
            init_values_ATL,
        )
    else:
        write_load_file(
            artere,
            ATL_model,
            "ATL",
            bus_name,
            params_ATL_LV,
            P_ATL,
            Q_ATL,
            variable_parameters_ATL,
            strength,
            constant_parameters_ATL,
            Sb=Sb_ATL,
            pf=pf_ATL,
        )
    write_load_file(
        artere,
        "LOAD",
        "IMP",
        bus_name,
        params_IMP_LV,
        P_imp,
        Q_imp,
        variable_parameters_IMP,
        strength,
    )
    write_load_file(
        artere,
        "INDMACH1",
        "IND",
        bus_name,
        params_IND_LV,
        P_ind,
        Q_ind,
        variable_parameters_IND,
        strength,
    )
    if PV_model != "IBG22r":
        write_load_file(
            artere,
            PV_model,
            "PV",
            bus_name_PV,
            params_PV_LV,
            P_PV,
            Q_PV,
            variable_parameters_PV,
            strength,
            constant_parameters_PV,
            init_values=init_values_PV,
            I_max=PV_I_max,
            IN=PV_IN,
        )
    else:
        write_load_file(
            artere,
            "IBG22r",
            "PV",
            bus_name_PV,
            params_PV_LV,
            P_PV,
            Q_PV,
            variable_parameters_PV,
            strength,
            constant_parameters_PV,
        )
