#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pyramses
# import cmath
# import math
# import numpy as np
import matplotlib.pyplot as plt
# from matplotlib import rc
import pandas as pd
import datetime
import seaborn as sns
# import json, codecs
import os
# from plot_strong_weak import plot_strong_weak
from save_figure import save_figure
from plotting_options import plotting_options

plotting_options()
# from importnb import Notebook
# with Notebook():
#     from CIGRE18_multisim import VAR
# print(VAR)

# Set plot options
# sns.set_style("darkgrid", {"axes.facecolor": ".9"})
# cm = 1/2.54
# pt = 1/72.27
# sns.set_context("paper", font_scale=0.7, rc={"grid.linewidth": 0.6})
# rc('font', **{'family': 'sans-serif', 'sans-serif': ['Helvetica']})
# rc('font', **{'family': 'serif', 'serif': ['Palatino']})
# rc('text', usetex=True)
# colors_ blue, red, yellow, green. purple gray light blue dark blue dark green
# palette1 = [
#     "#1269b0",
#     "#a8322d",
#     '#edb120',
#     '#72791c',
#     "#91056a",
#     '#6f6f64',
#     '#007a96',
#     '#1f407a',
#     '#485a2c'
# ]
# palette = [
#     "#1269b0",
#     '#1f407a',
#     "#a8322d",
#     "#edb120",
#     '#72791c',
#     '#91056a',
#     '#6f6f64',
#     '#1f407a',
#     '#485a2c'
# ]
# # palette_old = ["#1269b0", "#a8322d", "#91056a", '#edb120', '#72791c',
# #                '#6f6f64', '#007a96', '#1f407a', '#485a2c']
# sns.set_palette(palette)
# # sns.cubehelix_palette(start=.5, rot=-.5, as_cmap=True)


# In[2]:


# Current date

today = datetime.date.today()
# date = today
date = "2022-07-27"

directory = ".\\" + str(date) + "\\PVs\\"
# KP_P = np.linspace(0,2,11)
# Tp2 = [0, 1]
# Loop over the different loads
atls = ['TL1', 'TL11', 'TL15', 'TL16', 'TL17', 'TL18']
# Loop over the different files
names_weak = ['weak_on', 'weak_off', 'weak_droop']
names_strong = ['strong_on', 'strong_off', 'strong_droop']
# types = ['on', 'off', 'droop']
# types = ['active', 'inactive', 'droop']
# names = ['weak', 'strong']
# names = names_weak
# fig_name = 'strong-weak comparison'
# # names_hyst = []

# # VAR = KP_P
# # replace_str = "KP_P"
# var_path = ".\\" + str(today) + "\\var.json"
# obj_text = codecs.open(var_path, 'r', encoding='utf-8').read()
# VAR = json.loads(obj_text)
# var_str = list(VAR.keys())[0]

# out_dir = ".\\" + str(today) + "\\" + var_str + "\\"
# # Get the parameters
# with open(out_dir + 'parameters.json', 'r') as openfile:
#     parameters = json.load(openfile)


# fig_title = var_str + ' variation'
# fig_name = var_str + ' variation'
# for parameter in parameters:
#     fig_title += "; " + parameter + "= " + str(parameters[parameter])
#     fig_name += " " + parameter + str(parameters[parameter])
# # print(title)

# for i,var in enumerate(VAR[var_str]):
#     names.append('out_' + var_str + str(i))
#     # names_hyst.append('out_hyst' + str(i))

# In[3]:


# ext = {}
types = ['active', 'mixed', 'droop', 'inactive', "V-help"]
names = ['weak', 'strong']
variables = ['time in s', '$f$ in Hz', '$v$ in p.u.']
fig_name = 'PV comparison'
events_left = {
    '$^1$': 2,
    '$^2$': 2.5,
    '$^3$': 3.65,
    '$^4$': 5.1
}
events_right = {
    '$^5$': 2,
    '$^6$': 2.5,
}
# events_left = {
#     'Power drop': 2,
#     '$LoE = 1$':2.5,
#     '2 units shut down (centralized control)': 3.65,
#     '2 units shut down (droop control)': 5.1
# }
# events_right = {
#     'Power drop': 2,
#     '$LoE = 1$':2.5,
# }
direction = "down"

parameters_left = ', '.join(('weak grid',
                             r'$\Delta P = 1.5$ MW',
                             # "ATLs' load share = 20\%"
                             ))
parameters_right = ', '.join(('strong grid',
                              r'$\Delta P = 1.5$ MW',
                              # "ATLs' load share = 20\%"
                              ))


# In[4]:


A = []
B = []
C = variables*len(types)*2
D = []
for name in names:
    for var in variables:
        for typ in types:
            A += [name]
for name in names:
    for typ in types:
        for var in variables:
            B += [typ]

for name in names:
    for typ in types:
        # ext1 = pyramses.extractor(directory + name + '_' + typ + ".trj")
        ext = pyramses.extractor(directory + name + '_' + typ + "_" +
                                 direction + "1.5.trj")
        D.append(ext.getSync('G').S.time)
        D.append(ext.getSync('G').S.value*50)
        D.append(ext.getBus('114115').mag.value)

df = pd.DataFrame(zip(A, B, C, D), columns=['name', 'type', 'value', 'data'])
df.set_index(['name', 'type', 'value'], inplace=True)
df


# In[5]:

cm = 1/2.54

# fig,axs = plt.subplots(2,len(names), figsize = (360*pt,4*72*pt),
#                        sharex='col', sharey = 'row')
fig, axs = plt.subplots(2, len(names), figsize=(9*cm, 9*cm),
                        sharex='col', sharey='row')

# fig.suptitle("\detokenize{"+ fig_title +"}")
fig.subplots_adjust(hspace=0.1, wspace=0.05)
axs = axs.ravel()
for typ in types:
    sns.lineplot(ax=axs[0], y=C[1], x=C[0], data=df['data']['weak'][typ],
                 linewidth=0.5, label=typ, legend=False)
    sns.lineplot(ax=axs[1], y=C[1], x=C[0], data=df['data']['strong'][typ],
                 linewidth=0.5, label=typ, legend=False)
    sns.lineplot(ax=axs[2], y=C[2], x=C[0], data=df['data']['weak'][typ],
                 linewidth=0.5, label=typ, legend=False)
    sns.lineplot(ax=axs[3], y=C[2], x=C[0], data=df['data']['strong'][typ],
                 linewidth=0.5, label=typ, legend=False)

# for event, time in events_left.items():
#     for ax in axs[0],axs[2]:
#         ax.axvline(x = time, color = 'grey', linewidth=0.5)
#     axs[0].text(time-0.03, 50.01, event)
# to correct the height for the data present.
# for event, time in events_right.items():
#     for ax in axs[1],axs[3]:
#         ax.axvline(x = time, color = 'grey', linewidth=0.5)
#     axs[1].text(time-0.03, 50.01, event)
# to correct the height for the data present.
axs[2].set_xlim([1.75, 7.25])
axs[3].set_xlim([1.75, 7.25])
leg = axs[0].legend(
    loc='upper center',
    bbox_to_anchor=(1, 1.2),
    ncol=len(types),
    fancybox=False,
    shadow=False,
    facecolor='white',
    framealpha=0.1,
    handlelength=1
)
leg.get_frame().set_linewidth(0.1)
for el in leg.get_lines():
    el.set_linewidth(2)

fig.align_ylabels(axs[:])
# axs[0].axhline(y=49.69)
# axs[0].axhline(y=49.62)
props = dict(boxstyle='square', facecolor='white', alpha=0.8)

if direction == "down":
    axs[0].text(0.98, 0.9, parameters_left, bbox=props,
                transform=axs[0].transAxes, ha='right', va='top')
    axs[1].text(0.98, 0.9, parameters_right, bbox=props,
                transform=axs[1].transAxes, ha='right', va='top')
else:
    axs[0].text(0.98, 1-0.9, parameters_left, bbox=props,
                transform=axs[0].transAxes, ha='right', va='bottom')
    axs[1].text(0.98, 1-0.9, parameters_right, bbox=props,
                transform=axs[1].transAxes, ha='right', va='bottom')

axs[1].set_xlim([1.75, 7.25])
axs[0].set_ylabel('$f$ in Hz')
axs[1].set_ylabel('$v$ in p.u.')
axs[1].set_xlabel('time in s')


str_i = ""
i = 0
while True:
    if not os.path.exists(directory + fig_name + str_i + '.png'):
        fig.savefig(directory + fig_name + str_i + '.png', bbox_inches='tight',
                    dpi=300)
        fig.savefig(directory + fig_name + str_i + '.pdf', bbox_inches='tight')

        break
    else:
        i += 1
        str_i = " " + str(i)


# In[6]:


# %run plot_strong_weak.py
# plot_strong_weak(types, directory, 2, fig_name)


# In[7]:


fig_name = 'sum of loads comparison'
MVbuses = [i for i in range(1101, 1176)]
LVbuses = [i for i in range(1, 19)]
LVbuses_loads = [1, 11, 15, 16, 17, 18]
LVbuses_PV = [11, 15, 16, 17, 18]
buses = []
loads = []
PVs = []
TLs = []
IMs = []
for MVbus in MVbuses:
    for LVbus in LVbuses:
        buses.append(str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        loads.append("IMP" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_PV:
        PVs.append("PV" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        TLs.append("ATL" + str(MVbus) + str(LVbus).zfill(2))
    for LVbus in LVbuses_loads:
        IMs.append("IND" + str(MVbus) + str(LVbus).zfill(2))
# P_active = 0
# P_inactive = 0
# P_droop = 0
# P_nb = 0
# # events = {
# #     'Power drop': 2,
# #     'LoE = 1':2.5
# # }
# ext=pyramses.extractor(directory + "weak_active.trj")
# for IMP in TLs:
#     P_active += ext.getInj(IMP).P.value
# ext=pyramses.extractor(directory + "weak_droop.trj")
# for IMP in TLs:
#     P_droop += ext.getInj(IMP).P.value
# ext=pyramses.extractor(directory + "weak_inactive.trj")
# for IMP in TLs:
#     P_inactive += ext.getInj(IMP).P.value
# ext=pyramses.extractor(directory + "weak_neighbourhood.trj")
# for IMP in TLs:
#     P_nb += ext.getInj(IMP).P.value

# fig = plt.figure(None, figsize= (400*pt,200*pt))
# ax1 = fig.subplots(1,1)
# ext=pyramses.extractor(directory + "weak_active.trj")
# ax1.plot(ext.getInj('ATL111001').P.time, P_active, label = 'nb')
# ext=pyramses.extractor(directory + "weak_droop.trj")
# ax1.plot(ext.getInj('ATL117501').P.time, P_droop,  label = 'droop')
# ext=pyramses.extractor(directory + "weak_neighbourhood.trj")
# ax1.plot(ext.getInj('ATL117501').P.time, P_nb,  label = 'neighbourhood')
# ext=pyramses.extractor(directory + "weak_neighbourhood.trj")
# ax1.plot(ext.getInj('ATL117501').P.time, P_inactive,  label = 'off')
# # for event in events:
# #     ax1.axvline(x = events[event], color = 'grey')
# #     ax1.text(events[event]+0.05, 6.73, event)

# ax1.set_xlim([1.75,7.25])
# ax1.set_xlabel('time [s]')
# ax1.set_ylabel('$P_{ATL}$ [MW]')
# ax1.legend(ncol=len(types))
# save_figure(directory, fig_name, fig)


P_active = 0
P_inactive = 0
P_droop = 0
P_nb = 0
Q_active = 0
Q_inactive = 0
Q_droop = 0
Q_nb = 0

fig, axs = plt.subplots(1, 1, figsize=(9*cm, 9*cm),
                        sharex='col', sharey='row')
fig.subplots_adjust(hspace=0.1, wspace=0.05)
# axs = axs.ravel()

ext = pyramses.extractor(directory + "weak_active_down1.trj")
for IMP in PVs:
    P_active += ext.getInj(IMP).Pgen.value
    Q_active += ext.getInj(IMP).Qgen.value
axs.plot(ext.getInj('PV117511').Pgen.time, P_active, label='PV')
# axs[1].plot(ext.getInj('PV117511').Qgen.time, Q_active, label='active')
# ext = pyramses.extractor(directory + "strong_active_down1.trj")
# for IMP in PVs:
#     P_nb += ext.getInj(IMP).Pgen.value
#     Q_nb += ext.getInj(IMP).Qgen.value
# axs.plot(ext.getInj('PV117511').Pgen.time, P_nb,  label='down')
# axs[1].plot(ext.getInj('PV117511').Qgen.time, Q_nb,  label='neighbourhood')

# ext = pyramses.extractor(directory + "strong_active_up1.trj")
# for IMP in TLs:
#     P_active += ext.getInj(IMP).P.value
#     Q_active += ext.getInj(IMP).Q.value
# axs[0].plot(ext.getInj('ATL117511').P.time, P_active, label='up')
# axs[1].plot(ext.getInj('ATL117511').Q.time, Q_active, label='active')
ext = pyramses.extractor(directory + "weak_active_down1.trj")
for IMP in TLs:
    P_nb += ext.getInj(IMP).P.value
    Q_nb += ext.getInj(IMP).Q.value
axs.plot(ext.getInj('ATL117511').P.time, -P_nb*2,  label='ATLs')
# axs[1].plot(ext.getInj('ATL117511').Q.time, Q_nb,  label='neighbourhood')

# ext = pyramses.extractor(directory + "weak_droop.trj")
# for IMP in PVs:
#     P_droop += ext.getInj(IMP).Pgen.value
#     Q_droop += ext.getInj(IMP).Qgen.value
# axs[0].plot(ext.getInj('PV117511').Pgen.time, P_droop,  label='droop')
# axs[1].plot(ext.getInj('PV117511').Qgen.time, Q_droop,  label='droop')
# ext = pyramses.extractor(directory + "weak_inactive.trj")
# for IMP in PVs:
#     P_inactive += ext.getInj(IMP).Pgen.value
#     Q_inactive += ext.getInj(IMP).Qgen.value
# axs[0].plot(ext.getInj('PV117511').Pgen.time, P_inactive,  label='inactive')
# axs[1].plot(ext.getInj('PV117511').Qgen.time, Q_inactive,  label='inactive')


axs.set_ylabel('$P_{PV}$ [MW]')
axs.legend(loc='lower center', bbox_to_anchor=(0.5, 1.01), ncol=len(types))
axs.set_xlim([1.75, 3.25])
axs.set_xlabel('time [s]')
# axs.set_ylabel('$Q_{PV}$ [MW]')
save_figure(directory, fig_name, fig)


# In[8]:


Fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, figsize=(9*cm, 12.5*cm),
                                         sharex=True)
Fig.suptitle('ATL110111')
# Loop over some different simulation results stored in different trj files

for name in names:
    ext = pyramses.extractor(directory+name+".trj")
    data = pd.DataFrame(data=ext.getSync('G').S.time, columns=["time"])
#     data["pt"] = ext.getBranch('TxMAIN').PF.value*1000
    data["wm"] = ext.getInj('ATL110111').wm.value
    data["v"] = ext.getBus('117501').mag.value
    # data["F_help_high"] = ext.getInj('TL117511').F_hlp_high.value
    data["P"] = ext.getInj('ATL110111').P.value*1000
    data["f"] = ext.getSync('G').S.value*50
    # data["f"] = ext.getInj('TL11').f.value

    if name == names[1]:
        ax1 = sns.lineplot(ax=ax1, y="wm", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}", linestyle="--")
        ax2 = sns.lineplot(ax=ax2, y="v", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}", linestyle="--")
        ax3 = sns.lineplot(ax=ax3, y="P", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}", linestyle="--")
        ax4 = sns.lineplot(ax=ax4, y="f", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}", linestyle="--")

#     ax1 = sns.lineplot(ax=ax1, y="pt", x="time", data=data,linewidth=0.5)
    else:
        ax1 = sns.lineplot(ax=ax1, y="wm", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}")
        ax2 = sns.lineplot(ax=ax2, y="v", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}")
        ax3 = sns.lineplot(ax=ax3, y="P", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}")
        ax4 = sns.lineplot(ax=ax4, y="f", x="time", data=data, linewidth=0.5,
                           label=r"\detokenize{" + name + "}")

    # Legend

# Plot results for ATL Share
# Axis labels
ax1.set_ylabel(r'$\omega_m$ in p.u.')
ax2.set_ylabel(r'V')
ax3.set_ylabel(r'P in kW')
ax4.set_ylabel(r'$f$ in Hz')
ax4.set_xlabel(r"time in s")

ax1.legend(loc='right')
ax2.legend(loc='right')
ax3.legend(loc='right')
ax4.legend(loc='right')

# Axis limits
# ax1.set_xlim([1.25,10.25])
# ax2.set_xlim([1.25,10.25])
# ax3.set_xlim([1.25,10.25])
ax4.set_xlim([1.25, 10.25])
plt.savefig('.\\' + str(today) + '\\' + 'values comparison.pdf')


# In[ ]:


for i, atl in enumerate(atls):
    i, (ax2, ax3, ax4) = plt.subplots(3, 1, figsize=(9*cm, 12.5*cm),
                                      sharex=True)
    i.suptitle(atl)
    # Loop over some different simulation results stored in different trj files
    for name in names:
        ext = pyramses.extractor(directory+name+".trj")
        data = pd.DataFrame(data=ext.getSync('G').S.time, columns=["time"])
    #     data["pt"] = ext.getBranch('TxMAIN').PF.value*1000
        data["wm"] = ext.getInj(atl).wm.value
        data["patl"] = ext.getInj(atl).P.value*1000
        data["f"] = ext.getInj(atl).f.value
    #     ax1 = sns.lineplot(ax=ax1, y="pt", x="time", data=data,linewidth=0.5)
        ax2 = sns.lineplot(ax=ax2, y="wm", x="time", data=data,
                           linewidth=0.5, label=name)
        ax3 = sns.lineplot(ax=ax3, y="patl", x="time", data=data,
                           linewidth=0.5, label=name)
        ax4 = sns.lineplot(ax=ax4, y="f", x="time", data=data,
                           linewidth=0.5, label=name)

        # Legend
    ax2.legend()
    # Plot results for ATL Share
    # Axis labels
    # ax1.set_ylabel(r'$P_t$ in kW')
    ax2.set_ylabel(r'$\omega_m$ in p.u.')
    ax3.set_ylabel(r'$P_{atl}$ in kW')
    ax4.set_ylabel(r'$f$ in Hz')
    ax4.set_xlabel(r"time in s")

    # Axis limits
    # ax1.set_xlim([-0.25,5.25])
    ax2.set_xlim([-0.25, 4.25])
    ax3.set_xlim([-0.25, 4.25])
    ax4.set_xlim([-0.25, 4.25])

    plt.savefig('.\\' + str(today) + '\\' + atl + '.pdf')
