#!/usr/bin/env python
# coding: utf-8

# The goal of this script is to create a flexible system to assign the correct
# power to the loads of nested systems.
# The MV grid is expected to have a defined load (active and reactive) and
# PV production, that must be provided by the sub-elements of the LV grid.

# The impedance loads, inductions machines and ATLs are all loads and get
# summed up together in the MV loads.
# The share of induciton machines can be defined as constant for each load,
# while the share of impedance loads are dependent on the shares of TLs.
# The proportion of load between the different buses is kept constant.

# In the future, also some parameters should be kept flexible, so they should
# be all put at the end or the start of the parameter list

# In[1]:


import pandas as pd
import numpy as np


# In[2]:


file = "Extended model.xlsx"
xl = pd.ExcelFile(file)
artere_check = False
artere = "artere-" if artere_check is True else ""
# MV section
MV_buses_loads = xl.parse("MV imp")["busname"]
P_IMP_MV = xl.parse("MV imp")["P"]
Q_IMP_MV = xl.parse("MV imp")["Q"]
P_IND_MV = xl.parse("MV ind")["P"]
Q_IND_MV = xl.parse("MV ind")["Q"]
P_PV_MV = xl.parse("MV PV")["P"]
Q_PV_MV = xl.parse("MV PV")["Q"]
P_ATL_MV = xl.parse("MV ATL")["P"]
# LV section
LV_buses_load = xl.parse(artere + "LV imp")["busname"]
LV_buses_PV = xl.parse(artere + "LV PV")["busname"]
P_IMP_LV = xl.parse(artere + "LV imp")["P"]
Q_IMP_LV = xl.parse(artere + "LV imp")["Q"]
P_IND_LV = xl.parse(artere + "LV ind")["P"]
Q_IND_LV = xl.parse(artere + "LV ind")["Q"]
P_PV_LV = xl.parse(artere + "LV PV")["P"]
Q_PV_LV = xl.parse(artere + "LV PV")["Q"]
P_ATL_LV = xl.parse(artere + "LV ATLv4")["P"]
Sb_old_ATL_LV = xl.parse(artere + "LV ATLv4")["Sb"]


# In[3]:


rand_check = True
rand = " rand" if (rand_check and not artere_check) else ""
# Total MV load
P_load_MV = P_IMP_MV + P_IND_MV + P_ATL_MV
Q_load_MV = Q_IMP_MV + Q_IND_MV

# Total LV load
P_load_LV = P_IMP_LV + P_IND_LV + P_ATL_LV
Q_load_LV = Q_IMP_LV + Q_IND_LV

# Shares by bus
P_bus_LV_share = P_load_LV / sum(P_load_LV)
Q_bus_LV_share = Q_load_LV / sum(Q_load_LV)
P_bus_LV_share_PV = P_PV_LV / sum(P_PV_LV)


# Shares by type
P_IND_LV_share = [0] * len(LV_buses_load)
Q_IND_LV_share = [0] * len(LV_buses_load)
for i, P_load in enumerate(P_load_LV):
    if P_load_LV[i] == 0:
        P_IND_LV_share[i] = 0
        Q_IND_LV_share[i] = 0
    else:
        P_IND_LV_share[i] = P_IND_LV[i] / P_load_LV[i]
        Q_IND_LV_share[i] = Q_IND_LV[i] / Q_load_LV[i]
# P_IND_LV_share = P_IND_LV/P_load_LV
# Q_IND_LV_share = Q_IND_LV/Q_load_LV
P_ATL_LV_share_min = 0.1
P_ATL_LV_share_max = 0.3
P_ATL_LV_share_fix = [0.2] * len(LV_buses_load)
seed = 1
np.random.seed(seed)
P_ATL_LV_share_rand = (
    np.random.rand(len(LV_buses_load),) * (P_ATL_LV_share_max -
                                           P_ATL_LV_share_min)
    + P_ATL_LV_share_min
)
if rand_check:
    P_ATL_LV_share = P_ATL_LV_share_rand
else:
    P_ATL_LV_share = P_ATL_LV_share_fix

P_IMP_LV_share = 1 - (np.array(P_IND_LV_share) + np.array(P_ATL_LV_share))
Q_IMP_LV_share = 1 - np.array(Q_IND_LV_share)


# In[4]:


# a = P_bus_LV_share[0]*P_ATL_LV_share[0]*P_load_MV[0]
# b = P_bus_LV_share[0]*P_IND_LV_share[0]*P_load_MV[0]
# c = P_bus_LV_share[0]*P_IMP_LV_share[0]*P_load_MV[0]
# print(a+b+c)
# print(np.array(P_IND_LV_share) + np.array(P_ATL_LV_share))
# print(P_IND_LV_share)
# print(P_IMP_LV_share)


# In[5]:


# MVbuses = [i for i in range(1101,1176)]
# LVbuses_loads = [1,11,15,16,17,18]
# LVbuses_PV = [11,15,16,17,18]
# ATLperc = [0.574782, 0.051791, 0.125605, 0.101162, 0.060261, 0.086399]
# LoadPperc = [0.331393656,0.061664964,0.178766207,0.144809512,0.138363662,
#              0.145001999]
# LoadQperc = [0.548100456,0.023457208,0.099664897,0.155540495,0.047159155,
#              0.126077789]
# IndPperc = [0.178404983,0.108715536,0.361078444,0.026569101,0.265516791,
#             0.059715144]
# IndQperc = [0.213343987,0.109268877,0.285257691,0.032261286,0.295645226,
#             0.064222932]
# PVPperc = [0.133333333,0.333333333,0.106666667,0.266666667,0.16]


# In[6]:


with open(artere + "ATL_list_in.dat", "w") as f:
    for i, MVbus in enumerate(MV_buses_loads):
        for j, LVbus in enumerate(LV_buses_load):
            bus_number = str(MVbus) + str(LVbus).zfill(2)
            P = P_ATL_LV_share[j] * P_load_MV[i] * P_bus_LV_share[j]
            if P_ATL_LV[j] == 0:
                Sb = 0
            else:
                Sb = round(Sb_old_ATL_LV[j] * P / P_ATL_LV[j], 6)
            params_ATL_LV = xl.parse(artere + "LV ATLv4" + rand).iloc[j][9:]
            parameters = str(" ")
            for param in params_ATL_LV[
                :-1
            ]:  # skips the ';', since it would be recognized as string
                if rand_check and isinstance(param, str):
                    parameters = parameters + bus_number + str(param) + " "
                else:
                    parameters = parameters + str(param) + " "
            parameters = parameters + ";"  # the ';' is added again
            f.write(
                "INJEC ATLv4 TL"
                + bus_number
                + " "
                + bus_number
                + " 0 0 P_ATL"
                + bus_number
                + " 0 "
                + str(Sb)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[7]:


with open(artere + "IMP_list_in.dat", "w") as f:
    for i, MVbus in enumerate(MV_buses_loads):
        for j, LVbus in enumerate(LV_buses_load):
            bus_number = str(MVbus) + str(LVbus).zfill(2)
            P = round(P_IMP_LV_share[j] * P_bus_LV_share[j] * P_load_MV[i], 6)
            Q = round(Q_IMP_LV_share[j] * Q_bus_LV_share[j] * Q_load_MV[i], 6)
            params_IMP_LV = xl.parse(artere + "LV imp" + rand).iloc[j][8:]
            parameters = str(" ")
            for param in params_IMP_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC LOAD L"
                + bus_number
                + " "
                + bus_number
                + " 0 0 P_IMP"
                + bus_number
                + " "
                + str(Q)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[8]:


with open(artere + "IND_list_in.dat", "w") as f:
    for i, MVbus in enumerate(MV_buses_loads):
        for j, LVbus in enumerate(LV_buses_load):
            bus_number = str(MVbus) + str(LVbus).zfill(2)
            P = round(P_IND_LV_share[j] * P_bus_LV_share[j] * P_load_MV[i], 6)
            Q = round(Q_IND_LV_share[j] * Q_bus_LV_share[j] * Q_load_MV[i], 6)
            params_IND_LV = xl.parse(artere + "LV ind" + rand).iloc[j][8:]
            parameters = str(" ")
            for param in params_IND_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC INDMACH1 IM"
                + bus_number
                + " "
                + bus_number
                + " 0 0 P_IND"
                + bus_number
                + " "
                + str(Q)
                + parameters
                + "\n"
            )
        f.write("\n")


# In[9]:


with open(artere + "PV_list_in.dat", "w") as f:
    for i, MVbus in enumerate(MV_buses_loads):
        for j, LVbus in enumerate(LV_buses_PV):
            bus_number = str(MVbus) + str(LVbus).zfill(2)
            # P = round(P_PV_MV[i]*PVPperc[j],6)
            #             Q = Q_PV_MV[i]*PVQperc[j]
            params_IND_LV = xl.parse(artere + "LV PV" + rand).iloc[j][8:]
            parameters = str(" ")
            for param in params_IND_LV:
                parameters = parameters + str(param) + " "
            f.write(
                "INJEC IBG22r PV"
                + bus_number
                + " "
                + bus_number
                + " 0 0 P_PV"
                + bus_number
                + " 0 "
                + parameters
                + "\n"
            )
        f.write("\n")


# In[10]:


# Type = ["IND", "IMP", "ATL"]
P_IND_LV = [0] * len(LV_buses_load)

P_IND_name_prefix = "P_IND"
with open(artere + "IND_list_in.dat", "r") as file:
    filedata = file.read()

for i, MV_bus in enumerate(MV_buses_loads):
    for j, LV_bus in enumerate(LV_buses_load):
        P_IND_LV[j] = round(P_IND_LV_share[j] * P_load_MV[i] *
                            P_bus_LV_share[j], 5)
        P_IND_name = P_IND_name_prefix + str(MV_bus) + str(LV_bus).zfill(2)
        filedata = filedata.replace(P_IND_name, str(P_IND_LV[j]))
with open(artere + "IND_list_out.dat", "w") as file:
    file.write(filedata)


# In[11]:


P_IMP_LV = [0] * len(LV_buses_load)
IMP_name = "IMP"
P_IMP_name_prefix = "P_IMP"
with open(artere + "IMP_list_in.dat", "r") as file:
    filedata = file.read()

for i, MV_bus in enumerate(MV_buses_loads):
    for j, LV_bus in enumerate(LV_buses_load):
        P_IMP_LV[j] = round(P_IMP_LV_share[j] * P_load_MV[i] *
                            P_bus_LV_share[j], 5)
        P_IMP_name = P_IMP_name_prefix + str(MV_bus) + str(LV_bus).zfill(2)
        filedata = filedata.replace(P_IMP_name, str(P_IMP_LV[j]))
        # filedata = filedata.replace(load+'a', str("{:10.2f}".format(np.random.uniform(1,2,1)[0])))
        # filedata = filedata.replace(load+'b', str("{:10.2f}".format(np.random.uniform(1.5,3,1)[0])))

with open(artere + "IMP_list_out.dat", "w") as file:
    file.write(filedata)


# In[12]:


variable_parameters_ATL = {
    "pf": [0.95, 1],
    "ra": [0.01, 0.1],
    "H": [0.03, 0.5],
    "b": [0.0005, 0.002],
    "rt": [0.005, 0.05],
    "lt": [0.1, 0.9],
}

P_ATL_LV = [0] * len(LV_buses_load)
np.random.seed()
P_ATL_name_prefix = "P_ATL"
with open(artere + "ATL_list_in.dat", "r") as file:
    filedata = file.read()

for i, MV_bus in enumerate(MV_buses_loads):
    for j, LV_bus in enumerate(LV_buses_load):
        bus_number = str(MV_bus) + str(LV_bus).zfill(2)
        P_ATL_LV[j] = round(P_ATL_LV_share[j] * P_load_MV[i] *
                            P_bus_LV_share[j], 5)
        P_ATL_name = P_ATL_name_prefix + bus_number
        filedata = filedata.replace(P_ATL_name, str(P_ATL_LV[j]))
        for var_par in variable_parameters_ATL:
            filedata = filedata.replace(
                bus_number + var_par,
                str(
                    "{:.3f}".format(
                        np.random.uniform(
                            variable_parameters_ATL[var_par][0],
                            variable_parameters_ATL[var_par][1],
                        )
                    )
                ),
            )
with open(artere + "ATL_list_out.dat", "w") as file:
    file.write(filedata)


# In[13]:


P_PV_LV = [0] * len(LV_buses_PV)
P_PV_name_prefix = "P_PV"
with open(artere + "PV_list_in.dat", "r") as file:
    filedata = file.read()

for i, MV_bus in enumerate(MV_buses_loads):
    for j, LV_bus in enumerate(LV_buses_PV):
        P_PV_LV[j] = round(P_PV_MV[i] * P_bus_LV_share_PV[j], 5)
        P_PV_name = P_PV_name_prefix + str(MV_bus) + str(LV_bus).zfill(2)
        filedata = filedata.replace(P_PV_name, str(P_PV_LV[j]))
with open(artere + "PV_list_out.dat", "w") as file:
    file.write(filedata)


# In[ ]:


# In[ ]:
