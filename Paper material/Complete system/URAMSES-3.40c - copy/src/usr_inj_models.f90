!> @file
!> @brief associates the name of the injector with the actual subroutine

!> @brief associates the name of the injector with the actual subroutine
!> @details .
subroutine assoc_inj_ptr(modelname,inj_ptr)

   use MODELING

   implicit none

   character(len=20), intent(in):: modelname
   procedure(injector), pointer, intent(out) :: inj_ptr
   external inj_vfault
   external inj_ATL
   external inj_ATLv9
   external inj_IBG22r
   external inj_IBGtesting2
   external inj_PQ

   
   select case (modelname)

      case('VFAULT')
         inj_ptr=>inj_vfault
      case('ATL')
         inj_ptr=>inj_ATL
      case('ATLv9')
         inj_ptr=>inj_ATLv9
      case('IBG22r')
         inj_ptr=>inj_IBG22r      
      case('IBGtesting2')
         inj_ptr=>inj_IBGtesting2      
      case('PQ')
         inj_ptr=>inj_PQ 
   end select

end subroutine assoc_inj_ptr
