inj
ATLtestNOnb

%data
Sb                              ! base power of the unit
Sbs                             ! base power of the system
vdc_star                        ! DC link voltage reference
pf                              ! setpoint for power factor at the terminal
tau                             ! Pll time constant
Vminpll                         ! PLL freezing voltage, PLL freezes below this value
tau_f                           ! filter constant for frequency
kp_v                            ! DC link voltage control parameters
ki_v
kp_c                            ! rectifier current control parameters
ki_c
kp_p                            ! power control parameters
ki_p
kp_w                            ! motor speed control parameters
ki_w
w_cc                            ! motor current control bandwidth
rt                              ! terminal impedance
lt
cdc                             ! DC link capacitance
kw                              ! speed/torque control constant
kT                              ! motor torque constant
ra                              ! motor anchor/stator resistance
H                               ! motor inertia
b                               ! motor friction coefficient
Tnm                             ! compressor torque at nominal speed
iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
wm_min                          ! limits on rotational speed
wm_max
Vmax                            ! voltage range during which unit needs to stay connected
Vmin
Vint
Vr
tLVRT1
tLVRT2
tLVRTint
Vtrip
fmin                            ! frequency control regime
fmax
Trocof                          ! delay for ROCOF measurement
dfmax                           ! maximum permissable ROCOF
VPmin                           ! power limit below 0.9 pu of Voltage
VPmax                           ! power limit above 0.93 pu voltage
LVRT                            ! enable or disable LVRT
Tm                              ! measurement delay
protection                      ! Flag for protection, -1 for off, 1 for on
support                         ! Flag for grid support, -1 for off, 1 for on

dPc_1_0                         ! start power for positive level 1 central controller
dPc_2_0                         ! start power for positive level 2 central controller
P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
l_1_pos_min_par                 ! power level for the activation of the central controller help in pu
l_1_pos_max_par
l_1_neg_min_par
l_1_neg_max_par
l_2_pos_min_par
l_2_pos_max_par
l_2_neg_min_par
l_2_neg_max_par

s_f                             ! security factor for unit limit control
lvl                             ! level of emergency of the central controller

pl1_3_0                        ! flags that varies for the different units
pl1_4_0
F_help_high_0
F_help_low_0


%parameters

w0 = 1
wb = 2*pi*fnom
t2 = 0                          ! Torque polynomial parameters, set to constant torque
t1 = 0
t0 = 1

Tlim = 0.01
Downlim = -9999
Downlimdisc = 0
UplimdeltaP = 9999
DownlimdeltaP = 0
Uplimdis = 0
downlimdis = -9999


theta0 = atan([vy]/[vx])
P0 = ([vx]*[ix]+[vy]*[iy])                                                      ! initial active power 
Q0 = [vy]*[ix]-[vx]*[iy]                                                        ! initial reactive power
P0_unit = -{P0}*{Sbs}/{Sb}
Q0_unit = -{Q0}*{Sbs}/{Sb}
V0 = dsqrt([vx]**2+[vy]**2)                                                     ! initial voltage magnitude at bus
iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}                      ! current alignment
iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})                                     ! voltage alignment
vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})                            ! modulation indices
mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
idc0 = {md0}*{iP0}+{mq0}*{iQ0}                                                  ! DC link current
iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
Te0 = {iT0}*{kT}
Tc0 = {Tnm}


%states
vd = {vd0}
vq = {vq0}
theta = {theta0}
vdc_ref ={vdc_star}
V = {V0}
vxm = [vx]
vym = [vy]
Vm = {V0}
iP =  {iP0}
diP = 0
iQ =  {iQ0}
diQ = 0
P = {P0}
dp = 0
Punit = {P0_unit}
Q = {Q0}
Qunit = {Q0_unit}
Pref_lim = {P0_unit}          ! limited P reference
dw_pll = 0
dw_pllf = 0
deltaf = 0
w_pll = {wb}
f = fnom
fi = fnom
vdc = {vdc_star}
dvdc = 0
iP_ref = {iP0}
Plim = {VPmax}
Plimin = {VPmax}
iQ_ref = {iQ0}
mdr = {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
mqr = -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
md = {md0}
mq = {mq0}
dvd = {iP0}*{rt}
dvq = {iQ0}*{rt}
idc = {idc0}
didc = {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
wm_ref = {wm0}
wm_ref_lim = {wm0}
wm_ref_lim_in = {wm0}
iT_ref = {iT0}
iT = {iT0}
dwm = 0
wm = {wm0}
Te = {kT}*{iT0}
dT = {kT}*{iT0}-{Tc0}
F_pll = 1
dv_pll = {V0}-{Vminpll}
PLLmulta = 1
PLLmultb = 0
deltafvh =  {V0}-{Vmax}
z1 = 0
Fvhi = 1
Fvh = 1
iPs = {iP0}
iQs = {iQ0}
Pref = {P0_unit}
x10 = -{V0}
z=0
Fvl = 1
Fvli = 1
deltafl = [f] - {fmin}
flagla = 1
flaglb = 0
Ffl = 1
Ffli = 1
deltafh = {fmax} - [f]
flagha = 1
flaghb = 0
Ffh = 1
Ffhi = 1
rocof = 0
abrocof = 0
flagra = 1
flagrb = 0
deltarocof  = {dfmax} - 0
Ffri = 1
Ffr = 1
Plim_min = 0
status  = 1             ! status of the device, 1 for on
p1 = {protection}
p2 = 1
p3 = 1
s1 = {support}
s2 = {wm0}


V_lim_min = {Vmin} * {s_f}              ! unit controller states
V_lim_max = {Vmax} / {s_f}
P_lim_min = {P_min} * {s_f}
P_lim_max = {P_max} / {s_f}
pl1 = {Vmin} * {s_f} - {V0}
F_v_min_in = 0
pl2 = {V0} - {Vmax} / {s_f}
F_v_max_in = 0
F_v_opp = 1
pl3 = {P_min} * {s_f} - {P0_unit}
F_p_min_in = 0
pl4 = {P0_unit} - {P_max} / {s_f}
F_p_max_in = 0
F_v_min = 0
F_v_max = 0
F_p_min = 0
F_p_max = 0
F_lim = 0
dP_lim = 0 

dQ_lim = 0
dQ_sum = 0
dQ = -{Q0_unit}

dP_sum = 0


dPc_1_neg = - {dPc_1_0}                 ! central controller states
dPc_1 = {dPc_1_0}
dPc_2 = {dPc_2_0}
dPc_2_neg = - {dPc_2_0}
dPc_3 = {P_max} - {P0_unit}
dPc_3_neg = {P0_unit} - {P_min}
level = {lvl}
dPc_1_sign = {dPc_1_0}
dPc_2_sign = {dPc_2_0}
dPc_3_sign = {P_max} - {P0_unit}
dPc_0 = 0
l_1_pos_min = {l_1_pos_min_par}
l_1_pos_max = {l_1_pos_max_par}
l_1_neg_min = {l_1_neg_min_par}
l_1_neg_max = {l_1_neg_max_par}
l_1_min = {l_1_pos_min_par}
l_1_max = {l_1_pos_max_par}
pl1_1 = {P0_unit} - {l_1_pos_min_par}
pl1_2 = {l_1_pos_max_par} - {P0_unit}
pl1_3 = {pl1_3_0}
pl1_4 = {pl1_4_0}
F_l_1 = {pl1_3_0} * {pl1_4_0}
dPc_1_sign_fl = {dPc_1_0} * [F_l_1]
l_2_pos_min = {l_2_pos_min_par}
l_2_pos_max = {l_2_pos_max_par}
l_2_neg_min = {l_2_neg_min_par}
l_2_neg_max = {l_2_neg_max_par}
Fpl1 = 1                                   ! placeholder for value 1 in switch block
Fpl0 = 0                                   ! placeholder for value 0 in switch block
l_2_min = {l_2_pos_min_par}
l_2_max = {l_2_pos_max_par}
pl2_1 = {P0_unit} - {l_2_pos_min_par}
pl2_2 = {l_2_pos_max_par} - {P0_unit}
pl2_3 = 1
pl2_4 = 1
F_l_2 = 1
dPc_2_sign_fl = {dPc_2_0}
l_abs = 0
dPc = 0






%observables
iP
iQ
vd
vq
P
Punit
Pref
Q
f
wm
Vm
Pref_lim
wm_ref_lim
status
rocof
Ffl
Ffh
Fvl
Fvh
Ffr



%models

& algeq                                             ! voltage magnitude
dsqrt([vx]**2+[vy]**2) - [V] 

& tf1p
V
Vm
1.
{Tm}
& tf1p
vx
vxm
1.
{Tm}
& tf1p
vy
vym
1.
{Tm}

& algeq                                             ! voltage alignment         
-[vd] + [vxm]*cos([theta])+[vym]*sin([theta])
& algeq
-[vq] - [vxm]*sin([theta])+[vym]*cos([theta])


& algeq                                             ! compute ix
-[ix] + (-[iPs]*cos([theta])-[iQs]*sin([theta]))*{Sb}/{Sbs}
& algeq                                             ! compute iy
-[iy] + (-[iPs]*sin([theta])+[iQs]*sin([theta]))*{Sb}/{Sbs}

& algeq
-[iPs] + [iP]*[status]

& algeq
-[iQs] + [iQ]*[status]


& algeq                                             ! compute powers
-[P]*{Sbs}/{Sb} -[Punit]                            ! + [ix]*[vx]+[iy]*[vy]
& algeq             
-[Q]*{Sbs}/{Sb} - [Qunit]                           ! -[vx]*[iy] + [vy]*[ix]


& algeq                                             ! compute powers
-[Punit] + [vd]*[iPs] + [vq]*[iQs]                              ! - [P]*{Sbs}/{Sb}
& algeq             
-[Qunit] + [vd]*[iQs] - [vq]*[iPs]                              ! - [Q]*{Sbs}/{Sb}



& int                                               ! voltage alignment angle, PLL angle
dw_pllf
theta
1.d0
& pictl                                             ! PLL
vq
w_pll
0.1/({tau}*0.001)**2
0.5/({tau}*0.001)

& algeq                                             ! frequency deviation
[dw_pll] - [w_pll] + [omega]*{wb}

& algeq
[dw_pllf] -[dw_pll]*[F_pll]

& algeq                                             ! compute and filter frequency
-[fi]+[w_pll]/{wb}*fnom
& tf1p
fi
f
1.
{tau_f}


& algeq                                             ! PLL freezing 
-[dv_pll] + [Vm]-{Vminpll}
& algeq
[PLLmultb] 
& algeq
-[PLLmulta] + 1.d0
& swsign
PLLmulta
PLLmultb
dv_pll
F_pll                   

& algeq 
-[vdc_ref] + {vdc_star}*[status]

& algeq                                             ! DC voltage control
-[dvdc] + [status]*[vdc_ref] - [vdc]

& pictl             
dvdc
iP_ref
{ki_v}
{kp_v}

& algeq                                             ! p-axis current control
-[diP] + [iP_ref] -[iP]
& pictl    
diP
mdr
{ki_c}
{kp_c}

& algeq                                             ! q-axis current control
-[iQ_ref] + [iP_ref]*dsqrt(1-{pf})
& algeq             
-[diQ] + [iQ_ref] -[iQ]
& pictl     
diQ
mqr
{ki_c}
{kp_c}

& algeq                                             ! md
-[md] + [status]*(-[mdr] + {lt}/max(1.d-3,{vdc_star})*[f]/fnom*[iQ])
& algeq                                             ! mq
-[mq] + [status]*(-[mqr] - {lt}/max(1.d-3,{vdc_star})*[f]/fnom*[iP])

& algeq                                             ! voltage over d-axis terminal impedance
-[dvd] + [vd] - [md] * [vdc] + {lt}*[omega]*[iQ]
& algeq                                             ! voltage over q-axis terminal impedance
-[dvq] + [vq] - [mq] * [vdc] - {lt}*[omega]*[iP]

& tf1p                                              ! d-axis current
dvd
iP
1/{rt}
{lt}/({wb}*{rt})
& tf1p                                              ! q-axis current
dvq
iQ
1/{rt}
{lt}/({wb}*{rt})

& algeq                                             ! DC link current
-[idc] + [status]*([wm]*{kT}*[iT] + {ra}*[iT]**2) / max(1.d-3,[vdc])

& algeq                                             ! DC link
-[didc]+[md]*[iP]+[mq]*[iQ]-[idc]

& int                                               ! DC link voltage
didc
vdc
{cdc}/{wb}


& limvb                                              ! power limiter
Pref
Plim_min
Plim
Pref_lim


& algeq                                             ! power mismatch
-[dp] + [Pref_lim]-[Punit] + [dP_sum]

& pictl                                             ! power control
dp
wm_ref
{ki_p}
{kp_p}


& lim                                               ! limit speed control input
wm_ref
wm_ref_lim_in
{wm_min}
{wm_max}


& algeq                                             ! speed control input
-[dwm] + {kw}*([status]*[wm_ref_lim]-[wm])

& pictl                                             ! speed control
dwm
iT_ref
ki_w
kp_w


& tf1p                                              ! motor current control
iT_ref
iT
1
1/{w_cc}


& algeq                                             ! torque equations
-[Te] + {kT}*[iT]
& algeq
-[dT] + [Te]-{Tc0}

& tf1p                                              ! motor inertia
dT
wm
1/{b}
2*{H}/{b}


& algeq
[deltafvh] - [Vm] +{Vmax}

& pwlin4            ! Overvoltage Protection                 
deltafvh
z1
-999
0.
0.
0.
0.
1.
999
1.
& algeq 
[Fvhi] -1 + [z1] 
& hyst
Fvhi
Fvh
1.1
0.
1.
0.9
1.
0.
1.

& algeq                     ! LVRT                               
[Vm] + [x10]   
& timer5                       
x10
z
-{Vr}
{tLVRT2}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRTint}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
0.

& algeq 
[Fvli] -1 + [z] 

& hyst                             
Fvli
Fvl
1.1
0.
1.
0.9
1.
0.
1.

& algeq                         ! status variable that can switch the entire unit on or off
[p2] - [Fvh]*[Fvl]*[Ffl]*[Ffh]*[Ffr]

& algeq
[p1] - {protection}
& algeq
[p3] - 1

& swsign
p2
p3
p1
status


& algeq                     ! switch support on and off
[s1] - {support}
& algeq
[s2] - {wm0}

& swsign
wm_ref_lim_in
s2
s1
wm_ref_lim


& algeq                 ! underfrequency protection
[flagla] - 1
& algeq 
[flaglb] 
& algeq 
[deltafl] - [f] + {fmin}
& swsign
flagla
flaglb
deltafl
Ffli
& hyst
Ffli
Ffl
1.1
0.
1.
0.9
1.
0.
1.
& algeq                 ! overfrequency protection
[flagha] - 1
& algeq 
[flaghb]
& algeq 
[deltafh] - {fmax} + [f]
& swsign
flagha
flaghb
deltafh
Ffhi
& hyst
Ffhi
Ffh
1.1
0.
1.
0.9
1.
0.
1.


& algeq                ! frequency deviation in Hz
-[deltaf] + [f]-fnom

& tfder1p               ! Rocof measurement in Hz/s
deltaf
rocof
1/{Trocof}
{Trocof}

& abs
rocof
abrocof

& algeq                 ! Rocof protection
[flagra] - 1
& algeq 
[flagrb]
& algeq 
-[deltarocof] +{dfmax} -[abrocof]
& swsign
flagra
flagrb
deltarocof
Ffri
& hyst
Ffri
Ffr
1.1
0.
1.
0.9
1.
0.
1.

& pwlin4            ! limit active power during undervoltage
Vm
Plimin
0
{VPmin}
0.9
{VPmin}
0.93
{VPmax}
1.5
{VPmax}

& algeq
[Plim_min]

& tf1p2lim
Plimin
Plim
1
{Tlim}
{DownlimdeltaP}
{UplimdeltaP}
{DownlimdeltaP}
{UplimdeltaP}

& algeq
- [Pref] + [status]*{P0_unit}




& algeq ! TESTING STARTS HERE	 central control, positive and negative variation
-[dPc_1] + {dPc_1_0}
& algeq
-[dPc_2] + {dPc_2_0}
& algeq
[dPc_1_neg] + [dPc_1]
& algeq
[dPc_2_neg] + [dPc_2]
& algeq 
-[dPc_3] + {P_max} - [Punit]
& algeq
-[dPc_3_neg] + [Punit] - {P_min}

& algeq 
-[level] + {lvl}

& swsign
dPc_1
dPc_1_neg
level
dPc_1_sign
& swsign
dPc_2
dPc_2_neg
level
dPc_2_sign
& swsign
dPc_3
dPc_3_neg
level
dPc_3_sign

& algeq !dPc_0 is equal to 0 
[dPc_0]

& algeq							! power level check level 1
- [l_1_pos_min] + {l_1_pos_min_par}
& algeq
- [l_1_neg_min] + {l_1_neg_min_par}
& swsign
l_1_pos_min
l_1_neg_min
level
l_1_min
& algeq
- [l_1_pos_max] + {l_1_pos_max_par}
& algeq
- [l_1_neg_max] + {l_1_neg_max_par}
& swsign
l_1_pos_max
l_1_neg_max
level
l_1_max

& algeq
-[pl1_1] + [Punit] - [l_1_min] 
& algeq
-[pl1_2] + [l_1_max] - [Punit]

& swsign
Fpl1
Fpl0
pl1_1
pl1_3
& swsign
Fpl1
Fpl0
pl1_2
pl1_4

& algeq
[F_l_1] - ([pl1_3]*[pl1_4])
& algeq
[dPc_1_sign_fl] - [dPc_1_sign]*[F_l_1]

& algeq							! power level check level 2
- [l_2_pos_min] + {l_2_pos_min_par}
& algeq
- [l_2_neg_min] + {l_2_neg_min_par}
& swsign
l_2_pos_min
l_2_neg_min
level
l_2_min
& algeq
- [l_2_pos_min] + {l_2_pos_min_par}
& algeq
- [l_2_neg_min] + {l_2_neg_min_par}
& swsign
l_2_pos_max
l_2_neg_max
level
l_2_max

& algeq
-[pl2_1] + [Punit] - [l_2_min]
& algeq
-[pl2_2] + [l_2_max] - [Punit]
& swsign
Fpl1
Fpl0
pl2_1
pl2_3
& swsign
Fpl1
Fpl0
pl2_2
pl2_4

& algeq
[F_l_2] - ([pl2_3]*[pl2_4])

& algeq
[dPc_2_sign_fl] - [dPc_2_sign]*[F_l_2]

& abs								! absolute of the level
level
l_abs

& switch4							! choice between the levels
dPc_0
dPc_1_sign_fl
dPc_2_sign_fl
dPc_3_sign							! the level 3 doesn't require a flag since every unit is activated
l_abs
dPc



& algeq		! UNIT LIMIT	!s_f = security factor
-[V_lim_min] + {Vmin}*{s_f}
& algeq
-[V_lim_max] + {Vmax}/{s_f}
& algeq
-[P_lim_min] + {P_min}*{s_f}
& algeq
-[P_lim_max] + {P_max}/{s_f}

& algeq !should it be V or Vm?	! voltage low limit flag
-[pl1] + [V_lim_min] - [Vm]
& swsign
Fpl1
Fpl0
pl1
F_v_min_in

& algeq !should it be V or Vm?	! voltage high limit flag
-[pl2] + [Vm] - [V_lim_max]
& swsign
Fpl1
Fpl0
pl2
F_v_max_in

& algeq ! F_v_opp = 0 if the voltage is smaller than F_v_min_in or bigger than F_v_max_in
-[F_v_opp] + (1-[F_v_min_in])*(1-[F_v_max_in])


& algeq							! power low limit flag
-[pl3] + [P_lim_min] - [Punit]
& swsign
F_v_opp
Fpl0
pl3
F_p_min_in

& algeq							! power high limit flag
-[pl4] + [Punit] - [P_lim_max]
& swsign
F_v_opp
Fpl0
pl4
F_p_max_in

& algeq
-[F_v_min] + [F_v_min_in]*[status]
& algeq
-[F_v_max] + [F_v_max_in]*[status]
& algeq
-[F_p_min] + [F_p_min_in]*[status]
& algeq
-[F_p_max] + [F_p_max_in]*[status]
& algeq							! final limit flag
-[F_lim] + 1-(1-[F_v_min])*(1-[F_v_max])*(1-[F_p_min])*(1-[F_p_max])

& algeq
-[dP_lim] + ([Punit] - [Pref_lim] - [dPc])*[F_lim]
& algeq
-[dP_sum] + [dPc] + [dP_lim]


& algeq
-[dQ_lim] + [Qunit]*[F_lim]
& algeq
-[dQ_sum] + [dQ_lim]

& algeq		! dQ MUST BE LINKED TO THE MOTOR SPEED!
-[dQ] - [Qunit] + [dQ_sum]




