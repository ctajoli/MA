!> @file
!! @brief DISCRETE CONTROLLER: Line overcurrent protection
!> @details
!! data read from file
!!
!! w(1)  : oversize allowed on line max power \n
!! w(2)  : The number of protected lines \n
!! w(2-)  : the branch names protected separated by space\n
!!
!! Example of using this controller:
!! DCTL dctl_line_prot name 1.05 2 'L1-L2' 'L3-L4' ;

subroutine dctl_line_prot(nb,modelname,mode,name,field,w,nbfields,nbwvar,parname,nbobs,obsname,t,obs)

    ! Include the models you want ---------------------------
    use BRANCH, only: smax_bra, brabr_orig, brabr_extr, origin, braname
    use observ_mod, only: pqbra
    use NET_TOPO, only: sbases, bussubnet
    use SETTINGS, only: write_msg
    use UNITS, only: disc, log
    use SETTINGS, only: disp_disc
    use SimTIME, only: t_h
    ! -------------------------------------------------------

    ! Do not change -----------------------------------------
    use MODELING
    use FUNCTIONS_IN_MODELS

    implicit none
    integer, intent(in) :: nb
    integer, intent(out):: nbwvar
    character(len=20), intent(in):: modelname,name,field(*)
    character(len=10), intent(out):: parname(*)
    integer, intent(in) :: nbfields, mode
    integer, intent(out):: nbobs
    character(len=10), intent(out):: obsname(*)
    double precision, intent(inout):: w(*)
    double precision, intent(in):: t
    double precision, intent(out):: obs(*)
    ! -------------------------------------------------------

    ! Declare the variables you might need ------------------
    integer i,branch_num
    double precision :: p_orig, q_orig, p_extr, q_extr, S_orig, S_extr
    character(len=64):: msg
    ! -------------------------------------------------------
 
    
    select case (mode)
    case (define_var_and_par)

        if(nbfields < 3)then
            write(log,"('DCTL_line_prot ',a20,' has less than 3 fields!! ',i3)")name,nbfields
            stop
         endif
       
        ! We have nbfield input values. The first is the oversize allowed (e.g., 1.05 for 5% oversize) and the rest are the protected line names.
        nbwvar = nbfields+1

        ! Get the oversize value
        read(field(1),*)w(1)

        ! The number of protected lines
        read(field(2),*)w(2)

        parname(1) = 'Oversize'
        parname(2) = 'ProtLines'

        ! Convert the names of the protected lines into their values and check for errors.
        do i=3, nbfields
            call searb(field(i),branch_num)
            if(branch_num == 0)then
                write(log,"('DCTL_line_prot ',a20,' monitors an unknown branch ',a20)")name,field(i)
                stop
            endif
            w(i)=dble(branch_num)
        enddo
 
    case (define_obs)

        nbobs=0
 
    case (evaluate_obs)

 
    case (initialize)

 
    case (update_disc)

        do i=3, 2+int(w(2))
            ! Compute the power on the sending and receiving ends in per-unit
            call pqbra(int(w(i)), p_orig, q_orig, p_extr, q_extr, .true.)
            
            ! Convert into apparent power in per-unit
            S_orig = hypot(p_orig,q_orig)
            S_extr = hypot(p_extr,q_extr)
            
            ! Apply the protection (the smax_bra is already in per-unit)
            if (S_orig > w(1)*smax_bra(int(w(i)))) then
                brabr_orig(int(w(i))) = 0
                if(disp_disc)then
                    write(msg,"('t = ',f8.3,' Tripped protection on Line ',17x,a20)")t_h(0),braname(i)
                    call write_msg('',trim(msg),disc)
                 endif
            endif

            if (S_extr> w(1)*smax_bra(int(w(i)))) then
                brabr_extr(int(w(i))) = 0
                if(disp_disc)then
                    write(msg,"('t = ',f8.3,' Tripped protection on Line ',17x,a20)")t_h(0),braname(i)
                    call write_msg('',trim(msg),disc)
                 endif
            endif

        enddo
 
 
    end select
 
 end subroutine dctl_line_prot
 