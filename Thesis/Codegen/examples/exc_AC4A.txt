exc
AC4A

%data

Kv
Rc
Xc
TR
VIMAX
VIMIN
TC
TB
VUEL
KA
TA
VRMAX
VRMIN
KC

%parameters

VREF      = vcomp([v],[p],[q],{Kv},{Rc},{Xc})+([vf]/{KA})

%states

Vc1       = vcomp([v],[p],[q],{Kv},{Rc},{Xc})
Vc        = [Vc1]
deltaV    = [vf]/{KA}
VI        = [deltaV]
V1        = [deltaV]
V2        = [deltaV]
uplim     = {VRMAX}-{KC}*[if]
lolim     = {VRMIN}

%observables

vf
uplim

%models

& algeq                    line drop compensation
[Vc1]-vcomp([v],[p],[q],{Kv},{Rc},{Xc})
& tf1p                     voltage measurement time constant
Vc1
Vc
1.d0
{TR}
& algeq                    summation point of AVR
{VREF}-[Vc]-[deltaV]
& lim                      input limiter
deltaV
VI
{VIMIN}
{VIMAX}
& tf1p1z                   lead-lag
VI
V1
1.d0
{TC}
{TB}
& max1v1c                  HV gate of UEL
V1
V2
{VUEL}
& algeq                    upper limit of output limiter
{VRMAX}-{KC}*[if]-[uplim]
& algeq                    lower limit of output limiter
{VRMIN}-[lolim]
& tf1pvlim                 output limiter
V2
vf
lolim
uplim
{KA}
{TA}
