!  MODEL NAME : inj_ATLnew              
!  MODEL DESCRIPTION FILE : ATLnew.txt
!  Data :
!       prm(  1)=  Sb                              ! base power of the unit
!       prm(  2)=  Sbs                             ! base power of the system
!       prm(  3)=  vdc_star                        ! DC link voltage reference
!       prm(  4)=  pf                              ! setpoint for power factor at the terminal
!       prm(  5)=  kp_v                            ! DC link voltage control parameters
!       prm(  6)=  ki_v
!       prm(  7)=  kp_c                            ! rectifier current control parameters
!       prm(  8)=  ki_c
!       prm(  9)=  kp_p                            ! power control parameters
!       prm( 10)=  ki_p
!       prm( 11)=  kp_w                            ! motor speed control parameters
!       prm( 12)=  ki_w
!       prm( 13)=  w_cc                            ! motor current control bandwidth
!       prm( 14)=  rt                              ! terminal impedance
!       prm( 15)=  lt
!       prm( 16)=  cdc                             ! DC link capacitance
!       prm( 17)=  kw                              ! speed/torque control constant
!       prm( 18)=  kT                              ! motor torque constant
!       prm( 19)=  ra                              ! motor anchor/stator resistance
!       prm( 20)=  H                               ! motor inertia
!       prm( 21)=  b                               ! motor friction coefficient
!       prm( 22)=  Tnm                             ! compressor torque at nominal speed
!       prm( 23)=  iF                              ! motor field current, equal to 0 in case of BLDC and PMSM
!       prm( 24)=  wm_min                          ! limits on rotational speed
!       prm( 25)=  wm_max
!       prm( 26)=  Vmax                            ! voltage range during which unit needs to stay connected
!       prm( 27)=  Vmin
!       prm( 28)=  Vint
!       prm( 29)=  Vr
!       prm( 30)=  tLVRT1
!       prm( 31)=  tLVRT2
!       prm( 32)=  tLVRTint
!       prm( 33)=  Vtrip
!       prm( 34)=  VPmax                           ! power limit above 0.93 pu voltage
!       prm( 35)=  LVRT                            ! enable or disable LVRT
!       prm( 36)=  Tm                              ! measurement delay
!       prm( 37)=  protection                      ! Flag for protection, -1 for off, 1 for on
!       prm( 38)=  P_max                           ! max power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 39)=  P_min                           ! min power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR LIMITS!
!       prm( 40)=  dPc_1_0                         ! start power for positive level 1 central controller
!       prm( 41)=  dPc_2_0                         ! start power for positive level 2 central controller
!       prm( 42)=  l_1_p_min_par                 ! power level for the activation of the central controller help in pu
!       prm( 43)=  l_1_p_max_par
!       prm( 44)=  l_1_n_min_par
!       prm( 45)=  l_1_n_max_par
!       prm( 46)=  l_2_p_min_par
!       prm( 47)=  l_2_p_max_par
!       prm( 48)=  l_2_n_min_par
!       prm( 49)=  l_2_n_max_par
!       prm( 50)=  V_n                             ! number of neighbours at voltage limit
!       prm( 51)=  P_n                             ! number of neighbours at power limit
!       prm( 52)=  Vdb_p                           ! higher voltage deadband value
!       prm( 53)=  Vdb_m                           ! lower voltage deadband value
!       prm( 54)=  ro_v                            ! neighbourhood factor for voltage
!       prm( 55)=  ro_p                            ! neighbourhood factor for power
!       prm( 56)=  Q_max                           ! max reactive power of the unit in pu, IT WILL HAVE TO BE CALCULATED FROM THE MOTOR AND THE POWER FACTOR!
!       prm( 57)=  Q_min                           ! min reactive power of the unit in pu, IT WIL
!       prm( 58)=  s_f1                             ! security factor for unit limit control
!       prm( 59)=  s_f2                             ! security factor for dPc_3
!       prm( 60)=  lvl                             ! level of emergency of the central controller
!       prm( 61)=  pl1_2_0                        ! flags that varies for the different units
!       prm( 62)=  pl1_4_0
!       prm( 63)=  pl1_6_0
!       prm( 64)=  pl1_8_0
!       prm( 65)=  pl2_2_0                        ! flags that varies for the different units
!       prm( 66)=  pl2_4_0
!       prm( 67)=  pl2_6_0
!       prm( 68)=  pl2_8_0
!       prm( 69)=  F_help_high_0
!       prm( 70)=  F_help_low_0
!       prm( 71)=  Tp1
!       prm( 72)=  Tp2
!       prm( 73)=  nadir
!       prm( 74)=  V_max_nb
!       prm( 75)=  V_min_nb
!       prm( 76)=  dPs_rate_max
!       prm( 77)=  dQs_rate_max
!  Parameters :
!       prm( 78)=  w0  
!       prm( 79)=  wb  
!       prm( 80)=  t2   Torque polynomial parameters, set to constant torque
!       prm( 81)=  t1  
!       prm( 82)=  t0  
!       prm( 83)=  Downlim  
!       prm( 84)=  Uplim  
!       prm( 85)=  Downlimdisc  
!       prm( 86)=  Uplimdis  
!       prm( 87)=  downlimdis  
!       prm( 88)=  theta0  
!       prm( 89)=  P0   initial active power
!       prm( 90)=  Q0   initial reactive power
!       prm( 91)=  P0_unit  
!       prm( 92)=  Q0_unit  
!       prm( 93)=  V0   initial voltage magnitude at bus
!       prm( 94)=  iP0   current alignment
!       prm( 95)=  iQ0  
!       prm( 96)=  vd0   voltage alignment
!       prm( 97)=  vq0  
!       prm( 98)=  md0   modulation indices
!       prm( 99)=  mq0  
!       prm(100)=  idc0   DC link current
!       prm(101)=  iT0  
!       prm(102)=  wm0  
!       prm(103)=  Te0  
!       prm(104)=  Tc0  
!       prm(105)=  {dPs_rate_min}  
!       prm(106)=  {dQs_rate_min}  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vd                    
!       x(  4)=  vq                    
!       x(  5)=  vdc_ref               
!       x(  6)=  V                     
!       x(  7)=  vxm                   
!       x(  8)=  vym                   
!       x(  9)=  Vm                    
!       x( 10)=  iP                    
!       x( 11)=  diP                   
!       x( 12)=  iQ                    
!       x( 13)=  diQ                   
!       x( 14)=  P                     
!       x( 15)=  dp                    
!       x( 16)=  Punit                 
!       x( 17)=  Q                     
!       x( 18)=  Qunit                 
!       x( 19)=  Pref_lim               limited P reference
!       x( 20)=  vdc                   
!       x( 21)=  dvdc                  
!       x( 22)=  iP_ref                
!       x( 23)=  Plim                  
!       x( 24)=  iQ_ref                
!       x( 25)=  mdr                   
!       x( 26)=  mqr                   
!       x( 27)=  md                    
!       x( 28)=  mq                    
!       x( 29)=  dvd                   
!       x( 30)=  dvq                   
!       x( 31)=  idc                   
!       x( 32)=  didc                  
!       x( 33)=  wm_ref                
!       x( 34)=  wm_ref_lim            
!       x( 35)=  iT_ref                
!       x( 36)=  iT                    
!       x( 37)=  dwm                   
!       x( 38)=  wm                    
!       x( 39)=  Te                    
!       x( 40)=  dT                    
!       x( 41)=  deltafvh              
!       x( 42)=  z1                    
!       x( 43)=  Fvhi                  
!       x( 44)=  Fvh                   
!       x( 45)=  iPs                   
!       x( 46)=  iQs                   
!       x( 47)=  Pref                  
!       x( 48)=  x10                   
!       x( 49)=  z                     
!       x( 50)=  Fvl                   
!       x( 51)=  Fvli                  
!       x( 52)=  Plim_min              
!       x( 53)=  status                 status of the device, 1 for on
!       x( 54)=  p1                    
!       x( 55)=  p2                    
!       x( 56)=  p3                    
!       x( 57)=  Fpl1                   placeholder for value 1 in switch block
!       x( 58)=  Fpl0                   placeholder for value 0 in switch block
!       x( 59)=  V_lim_min              unit controller states
!       x( 60)=  V_lim_max             
!       x( 61)=  P_lim_min             
!       x( 62)=  P_lim_max             
!       x( 63)=  pl1                   
!       x( 64)=  F_v_min_in            
!       x( 65)=  pl2                   
!       x( 66)=  F_v_max_in            
!       x( 67)=  F_v_opp               
!       x( 68)=  pl3                   
!       x( 69)=  F_p_min_in            
!       x( 70)=  pl4                   
!       x( 71)=  F_p_max_in            
!       x( 72)=  F_v_min               
!       x( 73)=  F_v_max               
!       x( 74)=  F_p_min               
!       x( 75)=  F_p_max               
!       x( 76)=  F_lim                 
!       x( 77)=  dP_lim                
!       x( 78)=  dQ_lim                
!       x( 79)=  dQ_sum                
!       x( 80)=  dP_sum1               
!       x( 81)=  dP_sum2               
!       x( 82)=  dP_sum                
!       x( 83)=  nadir_var             
!       x( 84)=  P_n_var                neighbourhood controller states
!       x( 85)=  P_n_var_neg           
!       x( 86)=  V_n_var               
!       x( 87)=  V_n_var_neg           
!       x( 88)=  V_n_abs               
!       x( 89)=  pl5                   
!       x( 90)=  F_help_v              
!       x( 91)=  F_help_v_p            
!       x( 92)=  F_help_v_m            
!       x( 93)=  F_help_p_p            
!       x( 94)=  F_help_p_m            
!       x( 95)=  F_hlp_high            
!       x( 96)=  F_hlp_low             
!       x( 97)=  F_hlp_v_p2            
!       x( 98)=  F_hlp_v_m2            
!       x( 99)=  F_hlp_p_p2            
!       x(100)=  F_hlp_p_m2            
!       x(101)=  dQp                   
!       x(102)=  dQm                   
!       x(103)=  dQnb                  
!       x(104)=  dPp                   
!       x(105)=  dPm                   
!       x(106)=  dPnb                  
!       x(107)=  iQnb                  
!       x(108)=  dPc_1_neg              central controller states
!       x(109)=  dPc_1_pos             
!       x(110)=  dPc_2_pos             
!       x(111)=  dPc_2_neg             
!       x(112)=  dPc_3_pos             
!       x(113)=  dPc_3_neg             
!       x(114)=  level                 
!       x(115)=  dPc_1_in              
!       x(116)=  dPc_2_in              
!       x(117)=  dPc_1                 
!       x(118)=  dPc_2                 
!       x(119)=  dPc_3                 
!       x(120)=  dPc_0                 
!       x(121)=  pl1_1                 
!       x(122)=  pl1_2                 
!       x(123)=  pl1_3                 
!       x(124)=  pl1_4                 
!       x(125)=  F_l_1_neg             
!       x(126)=  pl1_5                 
!       x(127)=  pl1_6                 
!       x(128)=  pl1_7                 
!       x(129)=  pl1_8                 
!       x(130)=  F_l_1_pos             
!       x(131)=  F_l_1                 
!       x(132)=  dPc_1_s_fl            
!       x(133)=  pl2_1                 
!       x(134)=  pl2_2                 
!       x(135)=  pl2_3                 
!       x(136)=  pl2_4                 
!       x(137)=  F_l_2_neg             
!       x(138)=  pl2_5                 
!       x(139)=  pl2_6                 
!       x(140)=  pl2_7                 
!       x(141)=  pl2_8                 
!       x(142)=  F_l_2_pos             
!       x(143)=  F_l_2                 
!       x(144)=  dPc_2_s_fl            
!       x(145)=  l_abs                 
!       x(146)=  l_switch              
!       x(147)=  dPc                   
!       x(148)=  theta_est             

!.........................................................................................................

subroutine inj_ATLnew(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 77
      nbaddpar= 29
      parname(  1)='Sb'
      parname(  2)='Sbs'
      parname(  3)='vdc_star'
      parname(  4)='pf'
      parname(  5)='kp_v'
      parname(  6)='ki_v'
      parname(  7)='kp_c'
      parname(  8)='ki_c'
      parname(  9)='kp_p'
      parname( 10)='ki_p'
      parname( 11)='kp_w'
      parname( 12)='ki_w'
      parname( 13)='w_cc'
      parname( 14)='rt'
      parname( 15)='lt'
      parname( 16)='cdc'
      parname( 17)='kw'
      parname( 18)='kT'
      parname( 19)='ra'
      parname( 20)='H'
      parname( 21)='b'
      parname( 22)='Tnm'
      parname( 23)='iF'
      parname( 24)='wm_min'
      parname( 25)='wm_max'
      parname( 26)='Vmax'
      parname( 27)='Vmin'
      parname( 28)='Vint'
      parname( 29)='Vr'
      parname( 30)='tLVRT1'
      parname( 31)='tLVRT2'
      parname( 32)='tLVRTint'
      parname( 33)='Vtrip'
      parname( 34)='VPmax'
      parname( 35)='LVRT'
      parname( 36)='Tm'
      parname( 37)='protection'
      parname( 38)='P_max'
      parname( 39)='P_min'
      parname( 40)='dPc_1_0'
      parname( 41)='dPc_2_0'
      parname( 42)='l_1_p_min_par'
      parname( 43)='l_1_p_max_par'
      parname( 44)='l_1_n_min_par'
      parname( 45)='l_1_n_max_par'
      parname( 46)='l_2_p_min_par'
      parname( 47)='l_2_p_max_par'
      parname( 48)='l_2_n_min_par'
      parname( 49)='l_2_n_max_par'
      parname( 50)='V_n'
      parname( 51)='P_n'
      parname( 52)='Vdb_p'
      parname( 53)='Vdb_m'
      parname( 54)='ro_v'
      parname( 55)='ro_p'
      parname( 56)='Q_max'
      parname( 57)='Q_min'
      parname( 58)='s_f1'
      parname( 59)='s_f2'
      parname( 60)='lvl'
      parname( 61)='pl1_2_0'
      parname( 62)='pl1_4_0'
      parname( 63)='pl1_6_0'
      parname( 64)='pl1_8_0'
      parname( 65)='pl2_2_0'
      parname( 66)='pl2_4_0'
      parname( 67)='pl2_6_0'
      parname( 68)='pl2_8_0'
      parname( 69)='F_help_high_0'
      parname( 70)='F_help_low_0'
      parname( 71)='Tp1'
      parname( 72)='Tp2'
      parname( 73)='nadir'
      parname( 74)='V_max_nb'
      parname( 75)='V_min_nb'
      parname( 76)='dPs_rate_max'
      parname( 77)='dQs_rate_max'
      parname( 78)='w0'
      parname( 79)='wb'
      parname( 80)='t2'
      parname( 81)='t1'
      parname( 82)='t0'
      parname( 83)='Downlim'
      parname( 84)='Uplim'
      parname( 85)='Downlimdisc'
      parname( 86)='Uplimdis'
      parname( 87)='downlimdis'
      parname( 88)='theta0'
      parname( 89)='P0'
      parname( 90)='Q0'
      parname( 91)='P0_unit'
      parname( 92)='Q0_unit'
      parname( 93)='V0'
      parname( 94)='iP0'
      parname( 95)='iQ0'
      parname( 96)='vd0'
      parname( 97)='vq0'
      parname( 98)='md0'
      parname( 99)='mq0'
      parname(100)='idc0'
      parname(101)='iT0'
      parname(102)='wm0'
      parname(103)='Te0'
      parname(104)='Tc0'
      parname(105)='{dPs_rate_min}'
      parname(106)='{dQs_rate_min}'
      adix=  1
      adiy=  2
      nbxvar=156
      nbzvar= 41

!........................................................................................
   case (define_obs)
      nbobs= 28
      obsname(  1)='iP'
      obsname(  2)='iQ'
      obsname(  3)='vd'
      obsname(  4)='vq'
      obsname(  5)='P'
      obsname(  6)='Punit'
      obsname(  7)='Pref'
      obsname(  8)='Q'
      obsname(  9)='wm'
      obsname( 10)='Vm'
      obsname( 11)='Pref_lim'
      obsname( 12)='wm_ref_lim'
      obsname( 13)='status'
      obsname( 14)='F_v_min'
      obsname( 15)='F_v_max'
      obsname( 16)='F_p_min'
      obsname( 17)='F_p_max'
      obsname( 18)='F_hlp_high'
      obsname( 19)='F_hlp_low'
      obsname( 20)='F_hlp_p_p2'
      obsname( 21)='F_hlp_p_m2'
      obsname( 22)='dPnb'
      obsname( 23)='dQnb'
      obsname( 24)='P_n_var'
      obsname( 25)='V_n_var'
      obsname( 26)='dP_sum'
      obsname( 27)='dP_lim'
      obsname( 28)='dQ_sum'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x( 10)              
      obs(  2)=x( 12)              
      obs(  3)=x(  3)              
      obs(  4)=x(  4)              
      obs(  5)=x( 14)              
      obs(  6)=x( 16)              
      obs(  7)=x( 47)              
      obs(  8)=x( 17)              
      obs(  9)=x( 38)              
      obs( 10)=x(  9)              
      obs( 11)=x( 19)              
      obs( 12)=x( 34)              
      obs( 13)=x( 53)              
      obs( 14)=x( 72)              
      obs( 15)=x( 73)              
      obs( 16)=x( 74)              
      obs( 17)=x( 75)              
      obs( 18)=x( 95)              
      obs( 19)=x( 96)              
      obs( 20)=x( 99)              
      obs( 21)=x(100)              
      obs( 22)=x(106)              
      obs( 23)=x(103)              
      obs( 24)=x( 84)              
      obs( 25)=x( 86)              
      obs( 26)=x( 82)              
      obs( 27)=x( 77)              
      obs( 28)=x( 79)              

!........................................................................................
   case (initialize)

!w0 = 1
      prm( 78)= 1

!wb = 2*pi*fnom
      prm( 79)= 2*pi*fnom

!t2 = 0
      prm( 80)= 0

!t1 = 0
      prm( 81)= 0

!t0 = 1
      prm( 82)= 1

!Downlim = -9999
      prm( 83)= -9999

!Uplim = 9999
      prm( 84)= 9999

!Downlimdisc = 0
      prm( 85)= 0

!Uplimdis = 0
      prm( 86)= 0

!downlimdis = -9999
      prm( 87)= -9999

!theta0 = atan([vy]/[vx])
      prm( 88)= atan(vy/vx)

!P0 = ([vx]*[ix]+[vy]*[iy])
      prm( 89)= (vx*ix+vy*iy)

!Q0 = [vy]*[ix]-[vx]*[iy]
      prm( 90)= vy*ix-vx*iy

!P0_unit = -{P0}*{Sbs}/{Sb}
      prm( 91)= -prm( 89)*prm(  2)/prm(  1)

!Q0_unit = -{Q0}*{Sbs}/{Sb}
      prm( 92)= -prm( 90)*prm(  2)/prm(  1)

!V0 = dsqrt([vx]**2+[vy]**2)
      prm( 93)= dsqrt(vx**2+vy**2)

!iP0 =  (-[ix]*cos({theta0})-[iy]*sin({theta0}))*{Sbs}/{Sb}
      prm( 94)=  (-ix*cos(prm( 88))-iy*sin(prm( 88)))*prm(  2)/prm(  1)

!iQ0 =  (-[ix]*sin({theta0})+[iy]*cos({theta0}))*{Sbs}/{Sb}
      prm( 95)=  (-ix*sin(prm( 88))+iy*cos(prm( 88)))*prm(  2)/prm(  1)

!vd0 = [vx]*cos({theta0})+[vy]*sin({theta0})
      prm( 96)= vx*cos(prm( 88))+vy*sin(prm( 88))

!vq0 = [vx]*sin({theta0})-[vy]*cos({theta0})
      prm( 97)= vx*sin(prm( 88))-vy*cos(prm( 88))

!md0 = 1/{vdc_star}*({vd0}+{lt}*{w0}*{iQ0}-{rt}*{iP0})
      prm( 98)= 1/prm(  3)*(prm( 96)+prm( 15)*prm( 78)*prm( 95)-prm( 14)*prm( 94))

!mq0 = 1/{vdc_star}*({vq0}-{lt}*{w0}*{iP0}-{rt}*{iQ0})
      prm( 99)= 1/prm(  3)*(prm( 97)-prm( 15)*prm( 78)*prm( 94)-prm( 14)*prm( 95))

!idc0 = {md0}*{iP0}+{mq0}*{iQ0}
      prm(100)= prm( 98)*prm( 94)+prm( 99)*prm( 95)

!iT0 = {Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}) + dsqrt(({Tnm}*{kT}/(2*{kT}**2+2*{ra}*{b}))**2-({ra}*{b}*{iF}**2-{b}*{idc0}*{vdc_star})/({kT}**2+{ra}*{b}))
      prm(101)= prm( 22)*prm( 18)/(2*prm( 18)**2+2*prm( 19)*prm( 21)) + dsqrt((prm( 22)*prm( 18)/(2*prm( 18)**2+2*prm( 19)*prm( 21)))**2-(prm( 19)*prm( 21)*prm( 23)**2-prm( 21)*prm(100)*prm(  3))/(prm( 18)**2+prm( 19)*prm( 21)))

!wm0 = 1/{b}*({kT}*{iT0}-{Tnm})
      prm(102)= 1/prm( 21)*(prm( 18)*prm(101)-prm( 22))

!Te0 = {iT0}*{kT}
      prm(103)= prm(101)*prm( 18)

!Tc0 = {Tnm}
      prm(104)= prm( 22)

!{dPs_rate_min} = -{dPs_rate_max}
      prm(105)= -prm( 76)

!{dQs_rate_min} = -{dQs_rate_max}
      prm(106)= -prm( 77)

!vd =  {vd0}
      x(  3)= prm( 96)

!vq =  {vq0}
      x(  4)= prm( 97)

!vdc_ref = {vdc_star}
      x(  5)=prm(  3)

!V =  {V0}
      x(  6)= prm( 93)

!vxm =  [vx]
      x(  7)= vx

!vym =  [vy]
      x(  8)= vy

!Vm =  {V0}
      x(  9)= prm( 93)

!iP =   {iP0}
      x( 10)=  prm( 94)

!diP =  0
      x( 11)= 0

!iQ =   {iQ0}
      x( 12)=  prm( 95)

!diQ =  0
      x( 13)= 0

!P =  {P0}
      x( 14)= prm( 89)

!dp =  0
      x( 15)= 0

!Punit =  {P0_unit}
      x( 16)= prm( 91)

!Q =  {Q0}
      x( 17)= prm( 90)

!Qunit =  {Q0_unit}
      x( 18)= prm( 92)

!Pref_lim =  {P0_unit}
      x( 19)= prm( 91)

!vdc =  {vdc_star}
      x( 20)= prm(  3)

!dvdc =  0
      x( 21)= 0

!iP_ref =  {iP0}
      x( 22)= prm( 94)

!Plim =  {VPmax}
      x( 23)= prm( 34)

!iQ_ref =  {iQ0}
      x( 24)= prm( 95)

!mdr =  {lt}/{vdc_star}*{iQ0}*{w0}-{md0}
      x( 25)= prm( 15)/prm(  3)*prm( 95)*prm( 78)-prm( 98)

!mqr =  -{lt}/{vdc_star}*{iP0}*{w0}-{mq0}
      x( 26)= -prm( 15)/prm(  3)*prm( 94)*prm( 78)-prm( 99)

!md =  {md0}
      x( 27)= prm( 98)

!mq =  {mq0}
      x( 28)= prm( 99)

!dvd =  {iP0}*{rt}
      x( 29)= prm( 94)*prm( 14)

!dvq =  {iQ0}*{rt}
      x( 30)= prm( 95)*prm( 14)

!idc =  {idc0}
      x( 31)= prm(100)

!didc =  {md0}*{iP0}+{mq0}*{iQ0}-{idc0}
      x( 32)= prm( 98)*prm( 94)+prm( 99)*prm( 95)-prm(100)

!wm_ref =  {wm0}
      x( 33)= prm(102)

!wm_ref_lim =  {wm0}
      x( 34)= prm(102)

!iT_ref =  {iT0}
      x( 35)= prm(101)

!iT =  {iT0}
      x( 36)= prm(101)

!dwm =  0
      x( 37)= 0

!wm =  {wm0}
      x( 38)= prm(102)

!Te =  {kT}*{iT0}
      x( 39)= prm( 18)*prm(101)

!dT =  {kT}*{iT0}-{Tc0}
      x( 40)= prm( 18)*prm(101)-prm(104)

!deltafvh =   {V0}-{Vmax}
      x( 41)=  prm( 93)-prm( 26)

!z1 =  0
      x( 42)= 0

!Fvhi =  1
      x( 43)= 1

!Fvh =  1
      x( 44)= 1

!iPs =  {iP0}
      x( 45)= prm( 94)

!iQs =  {iQ0}
      x( 46)= prm( 95)

!Pref =  {P0_unit}
      x( 47)= prm( 91)

!x10 =  -{V0}
      x( 48)= -prm( 93)

!z = 0
      x( 49)=0

!Fvl =  1
      x( 50)= 1

!Fvli =  1
      x( 51)= 1

!Plim_min =  0
      x( 52)= 0

!status =  1
      x( 53)= 1

!p1 =  {protection}
      x( 54)= prm( 37)

!p2 =  1
      x( 55)= 1

!p3 =  1
      x( 56)= 1

!Fpl1 =  1
      x( 57)= 1

!Fpl0 =  0
      x( 58)= 0

!V_lim_min =  {V_min_nb} * {s_f1}
      x( 59)= prm( 75) * prm( 58)

!V_lim_max =  {V_max_nb} / {s_f1}
      x( 60)= prm( 74) / prm( 58)

!P_lim_min =  {P_min} * {s_f1}
      x( 61)= prm( 39) * prm( 58)

!P_lim_max =  {P_max} / {s_f1}
      x( 62)= prm( 38) / prm( 58)

!pl1 =  {V_min_nb} * {s_f1} - {V0}
      x( 63)= prm( 75) * prm( 58) - prm( 93)

!F_v_min_in =  0
      x( 64)= 0

!pl2 =  {V0} - {V_max_nb} / {s_f1}
      x( 65)= prm( 93) - prm( 74) / prm( 58)

!F_v_max_in =  0
      x( 66)= 0

!F_v_opp =  1
      x( 67)= 1

!pl3 =  {P_min} * {s_f1} - {P0_unit}
      x( 68)= prm( 39) * prm( 58) - prm( 91)

!F_p_min_in =  0
      x( 69)= 0

!pl4 =  {P0_unit} - {P_max} / {s_f1}
      x( 70)= prm( 91) - prm( 38) / prm( 58)

!F_p_max_in =  0
      x( 71)= 0

!F_v_min =  0
      x( 72)= 0

!F_v_max =  0
      x( 73)= 0

!F_p_min =  0
      x( 74)= 0

!F_p_max =  0
      x( 75)= 0

!F_lim =  0
      x( 76)= 0

!dP_lim =  0
      x( 77)= 0

!dQ_lim =  0
      x( 78)= 0

!dQ_sum =  0
      x( 79)= 0

!dP_sum1 =  0
      x( 80)= 0

!dP_sum2 =  0
      x( 81)= 0

!dP_sum =  0
      x( 82)= 0

!nadir_var =  -1
      x( 83)= -1

!P_n_var =  {P_n}
      x( 84)= prm( 51)

!P_n_var_neg =  -{P_n}
      x( 85)= -prm( 51)

!V_n_var =  {V_n}
      x( 86)= prm( 50)

!V_n_var_neg =  -{V_n}
      x( 87)= -prm( 50)

!V_n_abs =  {V_n}
      x( 88)= prm( 50)

!pl5 =  {V_n} - 1
      x( 89)= prm( 50) - 1

!F_help_v =  0
      x( 90)= 0

!F_help_v_p =  0
      x( 91)= 0

!F_help_v_m =  0
      x( 92)= 0

!F_help_p_p =  0
      x( 93)= 0

!F_help_p_m =  0
      x( 94)= 0

!F_hlp_high =  {F_help_high_0}
      x( 95)= prm( 69)

!F_hlp_low =  {F_help_low_0}
      x( 96)= prm( 70)

!F_hlp_v_p2 =  [F_help_v_p] * [F_hlp_high]
      x( 97)= x( 91) * x( 95)

!F_hlp_v_m2 =  [F_help_v_m ]* [F_hlp_low]
      x( 98)= x( 92)* x( 96)

!F_hlp_p_p2 =  [F_help_p_p] * (1-[F_help_v])
      x( 99)= x( 93) * (1-x( 90))

!F_hlp_p_m2 =  [F_help_p_m] * (1-[F_help_v])
      x(100)= x( 94) * (1-x( 90))

!dQp =  0
      x(101)= 0

!dQm =  0
      x(102)= 0

!dQnb =  0
      x(103)= 0

!dPp =  0
      x(104)= 0

!dPm =  0
      x(105)= 0

!dPnb =  0
      x(106)= 0

!iQnb =  0
      x(107)= 0

!dPc_1_neg =  - {dPc_1_0}
      x(108)= - prm( 40)

!dPc_1_pos =  {dPc_1_0}
      x(109)= prm( 40)

!dPc_2_pos =  {dPc_2_0}
      x(110)= prm( 41)

!dPc_2_neg =  - {dPc_2_0}
      x(111)= - prm( 41)

!dPc_3_pos =  {P_max}/{s_f2} - {P0_unit}
      x(112)= prm( 38)/prm( 59) - prm( 91)

!dPc_3_neg =  - {P0_unit} + {P_min}*{s_f2}
      x(113)= - prm( 91) + prm( 39)*prm( 59)

!level =  {lvl}
      x(114)= prm( 60)

!dPc_1_in =  {dPc_1_0}
      x(115)= prm( 40)

!dPc_2_in =  {dPc_2_0}
      x(116)= prm( 41)

!dPc_1 =  {dPc_1_0}
      x(117)= prm( 40)

!dPc_2 =  {dPc_2_0}
      x(118)= prm( 41)

!dPc_3 =  {P_max}/{s_f2} - {P0_unit}
      x(119)= prm( 38)/prm( 59) - prm( 91)

!dPc_0 =  0
      x(120)= 0

!pl1_1 =  {P0_unit} - ({l_1_n_min_par} * ({P_max}-{P_min}) + {P_min})
      x(121)= prm( 91) - (prm( 44) * (prm( 38)-prm( 39)) + prm( 39))

!pl1_2 =  {pl1_2_0}
      x(122)= prm( 61)

!pl1_3 =  ({l_1_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(123)= (prm( 45) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 91)

!pl1_4 =  {pl1_4_0}
      x(124)= prm( 62)

!F_l_1_neg =  {pl1_2_0} * {pl1_4_0}
      x(125)= prm( 61) * prm( 62)

!pl1_5 =  {P0_unit} - ({l_1_p_min_par} * ({P_max}-{P_min}) + {P_min})
      x(126)= prm( 91) - (prm( 42) * (prm( 38)-prm( 39)) + prm( 39))

!pl1_6 =  {pl1_6_0}
      x(127)= prm( 63)

!pl1_7 =  ({l_1_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(128)= (prm( 43) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 91)

!pl1_8 =  {pl1_8_0}
      x(129)= prm( 64)

!F_l_1_pos =  {pl1_6_0} * {pl1_8_0}
      x(130)= prm( 63) * prm( 64)

!F_l_1 =  [F_l_1_pos]
      x(131)= x(130)

!dPc_1_s_fl =  {dPc_1_0} * [F_l_1]
      x(132)= prm( 40) * x(131)

!pl2_1 =  {P0_unit} - ({l_2_n_min_par} * ({P_max}-{P_min}) + {P_min})
      x(133)= prm( 91) - (prm( 48) * (prm( 38)-prm( 39)) + prm( 39))

!pl2_2 =  {pl2_2_0}
      x(134)= prm( 65)

!pl2_3 =  ({l_2_n_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(135)= (prm( 49) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 91)

!pl2_4 =  {pl2_4_0}
      x(136)= prm( 66)

!F_l_2_neg =  {pl2_2_0} * {pl2_4_0}
      x(137)= prm( 65) * prm( 66)

!pl2_5 =  {P0_unit} - ({l_2_p_min_par} * ({P_max}-{P_min}) + {P_min})
      x(138)= prm( 91) - (prm( 46) * (prm( 38)-prm( 39)) + prm( 39))

!pl2_6 =  {pl2_6_0}
      x(139)= prm( 67)

!pl2_7 =  ({l_2_p_max_par} * ({P_max}-{P_min}) + {P_min}) - {P0_unit}
      x(140)= (prm( 47) * (prm( 38)-prm( 39)) + prm( 39)) - prm( 91)

!pl2_8 =  {pl2_8_0}
      x(141)= prm( 68)

!F_l_2_pos =  {pl2_6_0} * {pl2_8_0}
      x(142)= prm( 67) * prm( 68)

!F_l_2 =  [F_l_2_pos]
      x(143)= x(142)

!dPc_2_s_fl =  {dPc_2_0} * [F_l_2]
      x(144)= prm( 41) * x(143)

!l_abs =  0
      x(145)= 0

!l_switch =  1
      x(146)= 1

!dPc =  0
      x(147)= 0

!theta_est =  {theta0}
      x(148)= prm( 88)

!& algeq                                             ! voltage magnitude
      eqtyp(  1)=0

!& tf1p
      eqtyp(  2)=  9
      tc(  2)=prm( 36)

!& tf1p
      eqtyp(  3)=  7
      tc(  3)=prm( 36)

!& tf1p
      eqtyp(  4)=  8
      tc(  4)=prm( 36)

!& algeq                                             ! voltage alignment
      eqtyp(  5)=0

!& algeq
      eqtyp(  6)=0

!& algeq
      eqtyp(  7)=0

!& algeq                                             ! compute ix
      eqtyp(  8)=0

!& algeq                                             ! compute iy
      eqtyp(  9)=0

!& algeq
      eqtyp( 10)=0

!& algeq
      eqtyp( 11)=0

!& algeq                                             ! compute powers
      eqtyp( 12)=0

!& algeq
      eqtyp( 13)=0

!& algeq                                             ! compute powers
      eqtyp( 14)=0

!& algeq
      eqtyp( 15)=0

!& algeq
      eqtyp( 16)=0

!& algeq                                             ! DC voltage control
      eqtyp( 17)=0

!& pictl
      eqtyp( 18)=149
      x(149)=x( 22)
      eqtyp( 19)=0

!& algeq                                             ! p-axis current control
      eqtyp( 20)=0

!& pictl
      eqtyp( 21)=150
      x(150)=x( 25)
      eqtyp( 22)=0

!& algeq                                             ! q-axis current control
      eqtyp( 23)=0

!& algeq
      eqtyp( 24)=0

!& pictl
      eqtyp( 25)=151
      x(151)=x( 26)
      eqtyp( 26)=0

!& algeq                                             ! md
      eqtyp( 27)=0

!& algeq                                             ! mq
      eqtyp( 28)=0

!& algeq                                             ! voltage over d-axis terminal impedance
      eqtyp( 29)=0

!& algeq                                             ! voltage over q-axis terminal impedance
      eqtyp( 30)=0

!& tf1p                                              ! d-axis current
      eqtyp( 31)= 10
      tc( 31)=prm( 15)/(prm( 79)*prm( 14))

!& tf1p                                              ! q-axis current
      eqtyp( 32)= 12
      tc( 32)=prm( 15)/(prm( 79)*prm( 14))

!& algeq                                             ! DC link current
      eqtyp( 33)=0

!& algeq                                             ! DC link
      eqtyp( 34)=0

!& int                                               ! DC link voltage
      if (prm( 16)/prm( 79)< 0.005)then
         eqtyp( 35)=0
      else
         eqtyp( 35)= 20
         tc( 35)=prm( 16)/prm( 79)
      endif

!& limvb                                              ! power limiter
      eqtyp( 36)=0
      if(x( 47)>x( 23))then
         z(  1)=1
      elseif(x( 47)<x( 52))then
         z(  1)=-1
      else
         z(  1)=0
      endif

!& algeq                                             ! power mismatch
      eqtyp( 37)=0

!& pictl                                             ! power control
      eqtyp( 38)=152
      x(152)=x( 33)
      eqtyp( 39)=0

!& lim                                               ! limit speed control input
      eqtyp( 40)=0
      if(x( 33)>prm( 25))then
         z(  2)=1
      elseif(x( 33)<prm( 24))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq                                             ! speed control input
      eqtyp( 41)=0

!& pictl                                             ! speed control
      eqtyp( 42)=153
      x(153)=x( 35)
      eqtyp( 43)=0

!& tf1p                                              ! motor current control
      eqtyp( 44)= 36
      tc( 44)=1/prm( 13)

!& algeq                                             ! torque equations
      eqtyp( 45)=0

!& algeq
      eqtyp( 46)=0

!& tf1p                                              ! motor inertia
      eqtyp( 47)= 38
      tc( 47)=2*prm( 20)/prm( 21)

!& algeq
      eqtyp( 48)=0

!& pwlin4                                            ! overvoltage Protection
      eqtyp( 49)=0
      if(x( 41)<(-999))then
         z(  3)=1
      elseif(x( 41)>=999)then
         z(  3)=   3
      elseif((-999)<=x( 41) .and. x( 41)<0.)then
         z(  3)=  1
      elseif(0.<=x( 41) .and. x( 41)<0.)then
         z(  3)=  2
      elseif(0.<=x( 41) .and. x( 41)<999)then
         z(  3)=  3
      endif

!& algeq
      eqtyp( 50)=0

!& hyst
      eqtyp( 51)=0
      if(x( 43)>1.1)then
         z(  4)=1
      elseif(x( 43)<0.9)then
         z(  4)=-1
      else
         if(1.>= 0.)then
            z(  4)=1
         else
            z(  4)=-1
         endif
      endif

!& algeq                                             ! LVRT
      eqtyp( 52)=0

!& timer5
      eqtyp( 53)=0
      eqtyp( 54)=0
      z(  5)=-1
      x(154)=0.

!& algeq
      eqtyp( 55)=0

!& hyst
      eqtyp( 56)=0
      if(x( 51)>1.1)then
         z(  6)=1
      elseif(x( 51)<0.9)then
         z(  6)=-1
      else
         if(1.>= 0.)then
            z(  6)=1
         else
            z(  6)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      eqtyp( 57)=0

!& algeq
      eqtyp( 58)=0

!& algeq
      eqtyp( 59)=0

!& swsign
      eqtyp( 60)=0
      if(x( 54)>=0.)then
         z(  7)=1
      else
         z(  7)=2
      endif

!& algeq                                             ! min and max power limiters
      eqtyp( 61)=0

!& algeq
      eqtyp( 62)=0

!& algeq                                             ! status check without droop
      eqtyp( 63)=0

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      eqtyp( 64)=0

!& algeq
      eqtyp( 65)=0

!& algeq												! NEIGHBOURHOOD CONTROL
      eqtyp( 66)=0

!& algeq
      eqtyp( 67)=0

!& swsign											! F_help_p_p is 1 if P_n > 0
      eqtyp( 68)=0
      if(x( 85)>=0.)then
         z(  8)=1
      else
         z(  8)=2
      endif

!& swsign											! F_help_p_m is 1 if P_n < 0
      eqtyp( 69)=0
      if(x( 84)>=0.)then
         z(  9)=1
      else
         z(  9)=2
      endif

!& hyst ! should it be V or Vm?
      eqtyp( 70)=0
      if(x(  9)>prm( 52))then
         z( 10)=1
      elseif(x(  9)<prm( 53))then
         z( 10)=-1
      else
         if((-1.)>= 0.)then
            z( 10)=1
         else
            z( 10)=-1
         endif
      endif

!& hyst
      eqtyp( 71)=0
      if(x(  9)>prm( 52))then
         z( 11)=1
      elseif(x(  9)<prm( 53))then
         z( 11)=-1
      else
         if(1.>= 0.)then
            z( 11)=1
         else
            z( 11)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      eqtyp( 72)=0

!& abs
      eqtyp( 73)=0
      if(x( 86)>0. )then
         z( 12)=1
      else
         z( 12)=-1
      endif

!& algeq
      eqtyp( 74)=0

!& swsign
      eqtyp( 75)=0
      if(x( 89)>=0.)then
         z( 13)=1
      else
         z( 13)=2
      endif

!& algeq												! voltage help required
      eqtyp( 76)=0

!& swsign											! F_help_v_p is 1 if V_n > 0
      eqtyp( 77)=0
      if(x( 87)>=0.)then
         z( 14)=1
      else
         z( 14)=2
      endif

!& swsign											! F_help_v_m is 1 if V_n < 0
      eqtyp( 78)=0
      if(x( 86)>=0.)then
         z( 15)=1
      else
         z( 15)=2
      endif

!& algeq
      eqtyp( 79)=0

!& algeq
      eqtyp( 80)=0

!& algeq												! priority of voltage over power
      eqtyp( 81)=0

!& algeq
      eqtyp( 82)=0

!& algeq												! reactive power computation
      eqtyp( 83)=0

!& algeq
      eqtyp( 84)=0

!& algeq
      eqtyp( 85)=0

!& algeq												! active power computation
      eqtyp( 86)=0

!& algeq
      eqtyp( 87)=0

!& algeq
      eqtyp( 88)=0

!& algeq												! UNIT LIMIT	!s_f1 = security factor
      eqtyp( 89)=0

!& algeq
      eqtyp( 90)=0

!& algeq
      eqtyp( 91)=0

!& algeq
      eqtyp( 92)=0

!& algeq 											! voltage low limit flag
      eqtyp( 93)=0

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      eqtyp( 94)=0
      if(x( 63)>=0.)then
         z( 16)=1
      else
         z( 16)=2
      endif

!& algeq												! voltage high limit flag
      eqtyp( 95)=0

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      eqtyp( 96)=0
      if(x( 65)>=0.)then
         z( 17)=1
      else
         z( 17)=2
      endif

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      eqtyp( 97)=0

!& algeq												! power low limit flag
      eqtyp( 98)=0

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      eqtyp( 99)=0
      if(x( 68)>=0.)then
         z( 18)=1
      else
         z( 18)=2
      endif

!& algeq												! power high limit flag
      eqtyp(100)=0

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      eqtyp(101)=0
      if(x( 70)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& algeq												! if the units are off the limit flags go to 0
      eqtyp(102)=0

!& algeq
      eqtyp(103)=0

!& algeq
      eqtyp(104)=0

!& algeq
      eqtyp(105)=0

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1
      eqtyp(106)=0

!& algeq
      eqtyp(107)=0

!& tf1p
      eqtyp(108)= 81
      tc(108)=prm( 72)

!& algeq
      eqtyp(109)=0

!& swsign
      eqtyp(110)=0
      if(x( 83)>=0.)then
         z( 20)=1
      else
         z( 20)=2
      endif

!& tf1p2lim
      if(prm( 71)< 0.001)then
         prm( 71)=0.d0
         prm( 83)=-huge(0.d0)
         prm( 84)= huge(0.d0)
         prm(105)=-huge(0.d0)
         prm( 76)= huge(0.d0)
      endif
      if(1*x( 80)-x( 82)>prm( 76)*prm( 71))then
         z( 21)=1
      elseif(1*x( 80)-x( 82)<prm(105)*prm( 71))then
         z( 21)=-1
      else
         z( 21)=0
      endif
      eqtyp(111)=0
      if(x( 82)>prm( 84))then
         z( 22)=1
         eqtyp(112)=0
      elseif(x( 82)<prm( 83))then
         z( 22)=-1
         eqtyp(112)=0
      else
         z( 22)=0
         eqtyp(112)= 82
      endif
      tc(112)=prm( 71)

!& algeq
      eqtyp(113)=0

!& tf1p2lim
      if(prm( 71)< 0.001)then
         prm( 71)=0.d0
         prm( 83)=-huge(0.d0)
         prm( 84)= huge(0.d0)
         prm(106)=-huge(0.d0)
         prm( 77)= huge(0.d0)
      endif
      if(1*x( 78)-x( 79)>prm( 77)*prm( 71))then
         z( 23)=1
      elseif(1*x( 78)-x( 79)<prm(106)*prm( 71))then
         z( 23)=-1
      else
         z( 23)=0
      endif
      eqtyp(114)=0
      if(x( 79)>prm( 84))then
         z( 24)=1
         eqtyp(115)=0
      elseif(x( 79)<prm( 83))then
         z( 24)=-1
         eqtyp(115)=0
      else
         z( 24)=0
         eqtyp(115)= 79
      endif
      tc(115)=prm( 71)

!& algeq
      eqtyp(116)=0

!& algeq												! CENTRAL CONTROL, positive and negative variation
      eqtyp(117)=0

!& algeq
      eqtyp(118)=0

!& algeq
      eqtyp(119)=0

!& algeq
      eqtyp(120)=0

!& algeq
      eqtyp(121)=0

!& algeq
      eqtyp(122)=0

!& algeq
      eqtyp(123)=0

!& swsign
      eqtyp(124)=0
      if(x(114)>=0.)then
         z( 25)=1
      else
         z( 25)=2
      endif

!& swsign
      eqtyp(125)=0
      if(x(114)>=0.)then
         z( 26)=1
      else
         z( 26)=2
      endif

!& swsign
      eqtyp(126)=0
      if(x(114)>=0.)then
         z( 27)=1
      else
         z( 27)=2
      endif

!& limvb
      eqtyp(127)=0
      if(x(115)>x(112))then
         z( 28)=1
      elseif(x(115)<x(113))then
         z( 28)=-1
      else
         z( 28)=0
      endif

!& limvb
      eqtyp(128)=0
      if(x(116)>x(112))then
         z( 29)=1
      elseif(x(116)<x(113))then
         z( 29)=-1
      else
         z( 29)=0
      endif

!& algeq !dPc_0 is equal to 0
      eqtyp(129)=0

!& algeq												! level 1 low negative
      eqtyp(130)=0

!& swsign
      eqtyp(131)=0
      if(x(121)>=0.)then
         z( 30)=1
      else
         z( 30)=2
      endif

!& algeq												! level 1 high negative
      eqtyp(132)=0

!& swsign
      eqtyp(133)=0
      if(x(123)>=0.)then
         z( 31)=1
      else
         z( 31)=2
      endif

!& algeq												! level 1 negative flag
      eqtyp(134)=0

!& algeq												! level 1 low positive
      eqtyp(135)=0

!& swsign
      eqtyp(136)=0
      if(x(126)>=0.)then
         z( 32)=1
      else
         z( 32)=2
      endif

!& algeq												! level 1 high positive
      eqtyp(137)=0

!& swsign
      eqtyp(138)=0
      if(x(128)>=0.)then
         z( 33)=1
      else
         z( 33)=2
      endif

!& algeq												! level 1 positive flag
      eqtyp(139)=0

!& swsign
      eqtyp(140)=0
      if(x(114)>=0.)then
         z( 34)=1
      else
         z( 34)=2
      endif

!& algeq
      eqtyp(141)=0

!& algeq												! level 2 low negative
      eqtyp(142)=0

!& swsign
      eqtyp(143)=0
      if(x(133)>=0.)then
         z( 35)=1
      else
         z( 35)=2
      endif

!& algeq												! level 2 high negative
      eqtyp(144)=0

!& swsign
      eqtyp(145)=0
      if(x(135)>=0.)then
         z( 36)=1
      else
         z( 36)=2
      endif

!& algeq												! level 2 negative flag
      eqtyp(146)=0

!& algeq												! level 2 low positive
      eqtyp(147)=0

!& swsign
      eqtyp(148)=0
      if(x(138)>=0.)then
         z( 37)=1
      else
         z( 37)=2
      endif

!& algeq												! level 2 high positive
      eqtyp(149)=0

!& swsign
      eqtyp(150)=0
      if(x(140)>=0.)then
         z( 38)=1
      else
         z( 38)=2
      endif

!& algeq												! level 2 positive flag
      eqtyp(151)=0

!& swsign
      eqtyp(152)=0
      if(x(114)>=0.)then
         z( 39)=1
      else
         z( 39)=2
      endif

!& algeq
      eqtyp(153)=0

!& abs												! absolute of the level
      eqtyp(154)=0
      if(x(114)>0. )then
         z( 40)=1
      else
         z( 40)=-1
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      eqtyp(155)=0

!& switch4											! choice between the levels
      eqtyp(156)=0
      z( 41)=max(1,min(  4,nint(x(146))))

!........................................................................................
   case (evaluate_eqs)

!& algeq                                             ! voltage magnitude
      f(  1)=dsqrt(vx**2+vy**2) - x(  6)

!& tf1p
      f(  2)=(-x(  9)+1.*x(  6))

!& tf1p
      f(  3)=(-x(  7)+1.*vx    )

!& tf1p
      f(  4)=(-x(  8)+1.*vy    )

!& algeq                                             ! voltage alignment
      f(  5)=-x(  3) + x(  7)*cos(x(148))+x(  8)*sin(x(148))

!& algeq
      f(  6)=-x(  4) - x(  7)*sin(x(148))+x(  8)*cos(x(148))

!& algeq
      f(  7)=-x(148) + atan(x(  8)/x(  7))

!& algeq                                             ! compute ix
      f(  8)=-x(  1) + (-x( 45)*cos(x(148))+x( 46)*sin(x(148)))*prm(  1)/prm(  2)

!& algeq                                             ! compute iy
      f(  9)=-x(  2) + (-x( 45)*sin(x(148))-x( 46)*cos(x(148)))*prm(  1)/prm(  2)

!& algeq
      f( 10)=-x( 45) + x( 10)*x( 53)

!& algeq
      f( 11)=-x( 46) + x( 12)*x( 53)

!& algeq                                             ! compute powers
      f( 12)=-x( 14)*prm(  2)/prm(  1) -x( 16)                            ! + x(  1)*vx+x(  2)*vy

!& algeq
      f( 13)=-x( 17)*prm(  2)/prm(  1) - x( 18)                           ! -vx*x(  2) + vy*x(  1)

!& algeq                                             ! compute powers
      f( 14)=-x( 16) + x(  3)*x( 45) + x(  4)*x( 46)                              ! - x( 14)*prm(  2)/prm(  1)

!& algeq
      f( 15)=-x( 18) + x(  3)*x( 46) - x(  4)*x( 45)                              ! - x( 17)*prm(  2)/prm(  1)

!& algeq
      f( 16)=-x(  5) + prm(  3)*x( 53)

!& algeq                                             ! DC voltage control
      f( 17)=-x( 21) + x( 53)*x(  5) - x( 20)

!& pictl
      f( 18)=prm(  6)                                                                                                                                                                                                                                                                                                    *x( 21)
      f( 19)=prm(  5)                                                                                                                                                                                                                                                                                                    *x( 21)+x(149)-x( 22)

!& algeq                                             ! p-axis current control
      f( 20)=-x( 11) + x( 22) -x( 10)

!& pictl
      f( 21)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 11)
      f( 22)=prm(  7)                                                                                                                                                                                                                                                                                                    *x( 11)+x(150)-x( 25)

!& algeq                                             ! q-axis current control
      f( 23)=-x( 24) ! + x( 22)*dsqrt(1-prm(  4))

!& algeq
      f( 24)=-x( 13) + x( 24) -x( 12) + x(107)

!& pictl
      f( 25)=prm(  8)                                                                                                                                                                                                                                                                                                    *x( 13)
      f( 26)=prm(  7)                                                                                                                                                                                                                                                                                                    *x( 13)+x(151)-x( 26)

!& algeq                                             ! md
      f( 27)=-x( 27) + x( 53)*(-x( 25) + prm( 15)/max(1.d-3,prm(  3))*omega*x( 12))

!& algeq                                             ! mq
      f( 28)=-x( 28) + x( 53)*(-x( 26) - prm( 15)/max(1.d-3,prm(  3))*omega*x( 10))

!& algeq                                             ! voltage over d-axis terminal impedance
      f( 29)=-x( 29) + x(  3) - x( 27) * x( 20) + prm( 15)*omega*x( 12)

!& algeq                                             ! voltage over q-axis terminal impedance
      f( 30)=-x( 30) + x(  4) - x( 28) * x( 20) - prm( 15)*omega*x( 10)

!& tf1p                                              ! d-axis current
      f( 31)=(-x( 10)+1/prm( 14)*x( 29))

!& tf1p                                              ! q-axis current
      f( 32)=(-x( 12)+1/prm( 14)*x( 30))

!& algeq                                             ! DC link current
      f( 33)=-x( 31) + x( 53)*(x( 38)*prm( 18)*x( 36) + prm( 19)*x( 36)**2) / max(1.d-3,x( 20))

!& algeq                                             ! DC link
      f( 34)=-x( 32)+x( 27)*x( 10)+x( 28)*x( 12)-x( 31)

!& int                                               ! DC link voltage
      if (prm( 16)/prm( 79)< 0.005)then
         f( 35)=x( 32)-x( 20)
      else
         f( 35)=x( 32)
      endif

!& limvb                                              ! power limiter
      select case (z(  1))
         case(0)
            f( 36)=x( 19)-x( 47)
         case(-1)
            f( 36)=x( 19)-x( 52)
         case(1)
            f( 36)=x( 19)-x( 23)
      end select

!& algeq                                             ! power mismatch
      f( 37)=-x( 15) + x( 19)-x( 16) + x( 82)

!& pictl                                             ! power control
      f( 38)=prm( 10)                                                                                                                                                                                                                                                                                                    *x( 15)
      f( 39)=prm(  9)                                                                                                                                                                                                                                                                                                    *x( 15)+x(152)-x( 33)

!& lim                                               ! limit speed control input
      select case (z(  2))
         case(0)
            f( 40)=x( 34)-x( 33)
         case(-1)
            f( 40)=x( 34)-prm( 24)
         case(1)
            f( 40)=x( 34)-prm( 25)
      end select

!& algeq                                             ! speed control input
      f( 41)=-x( 37) + prm( 17)*(x( 53)*x( 34)-x( 38))

!& pictl                                             ! speed control
      f( 42)=prm( 12)                                                                                                                                                                                                                                                                                                    *x( 37)
      f( 43)=prm( 11)                                                                                                                                                                                                                                                                                                    *x( 37)+x(153)-x( 35)

!& tf1p                                              ! motor current control
      f( 44)=(-x( 36)+1*x( 35))

!& algeq                                             ! torque equations
      f( 45)=-x( 39) + prm( 18)*x( 36)

!& algeq
      f( 46)=-x( 40) + x( 39)-prm(104)

!& tf1p                                              ! motor inertia
      f( 47)=(-x( 38)+1/prm( 21)*x( 40))

!& algeq
      f( 48)=x( 41) - x(  9) +prm( 26)

!& pwlin4                                            ! overvoltage Protection
      select case (z(  3))
         case (  1)
            f( 49)=0.+ ( (0.-0.)*(x( 41)-(-999))/(0.-(-999)) ) -x( 42)
         case (  2)
            f( 49)=0.+ ( (1.-0.)*(x( 41)-0.)/(0.-0.) ) -x( 42)
         case (  3)
            f( 49)=1.+ ( (1.-1.)*(x( 41)-0.)/(999-0.) ) -x( 42)
      end select

!& algeq
      f( 50)=x( 43) -1 + x( 42)

!& hyst
      if(z(  4) == 1)then
         f( 51)=x( 44)-1.-(1.-1.)*(x( 43)-1.1)/(1.1-0.9)
      else
         f( 51)=x( 44)-0.-(0.-0.)*(x( 43)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! LVRT
      f( 52)=x(  9) + x( 48)

!& timer5
      select case (z(  5))
         case (-1)
            f( 53)=x( 49)
            f( 54)=x(154)
         case (0)
            f( 53)=x( 49)
            f( 54)= 1.
         case (1)
            f( 53)=x( 49)-1.
            f( 54)= 0.
      end select

!& algeq
      f( 55)=x( 51) -1 + x( 49)

!& hyst
      if(z(  6) == 1)then
         f( 56)=x( 50)-1.-(1.-1.)*(x( 51)-1.1)/(1.1-0.9)
      else
         f( 56)=x( 50)-0.-(0.-0.)*(x( 51)-0.9)/(1.1-0.9)
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off
      f( 57)=x( 55) - x( 44)*x( 50)

!& algeq
      f( 58)=x( 54) - prm( 37)

!& algeq
      f( 59)=x( 56) - 1

!& swsign
      select case (z(  7))
         case(1)
            f( 60)=x( 53)-x( 55)
         case(2)
            f( 60)=x( 53)-x( 56)
      end select

!& algeq                                             ! min and max power limiters
      f( 61)=x( 52)

!& algeq
      f( 62)=-x( 23) + prm( 34)

!& algeq                                             ! status check without droop
      f( 63)=- x( 47) +  x( 53)*prm( 91)

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation
      f( 64)=x( 58)

!& algeq
      f( 65)=x( 57) - 1

!& algeq												! NEIGHBOURHOOD CONTROL
      f( 66)=-x( 84) + prm( 51)

!& algeq
      f( 67)=-x( 85) - prm( 51)

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z(  8))
         case(1)
            f( 68)=x( 93)-x( 58)
         case(2)
            f( 68)=x( 93)-x( 57)
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z(  9))
         case(1)
            f( 69)=x( 94)-x( 58)
         case(2)
            f( 69)=x( 94)-x( 57)
      end select

!& hyst ! should it be V or Vm?
      if(z( 10) == 1)then
         f( 70)=x( 95)-1.-(1.-1.)*(x(  9)-prm( 52))/(prm( 52)-prm( 53))
      else
         f( 70)=x( 95)-0.-(0.-0.)*(x(  9)-prm( 53))/(prm( 52)-prm( 53))
      endif

!& hyst
      if(z( 11) == 1)then
         f( 71)=x( 96)-0.-(0.-0.)*(x(  9)-prm( 52))/(prm( 52)-prm( 53))
      else
         f( 71)=x( 96)-1.-(1.-1.)*(x(  9)-prm( 53))/(prm( 52)-prm( 53))
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0
      f( 72)=-x( 86) + prm( 50)

!& abs
      if(z( 12) == 1 )then
         f( 73)=x( 88)-x( 86)
      else
         f( 73)=x( 88)+x( 86)
      endif

!& algeq
      f( 74)=-x( 89) + x( 88) - 1

!& swsign
      select case (z( 13))
         case(1)
            f( 75)=x( 90)-x( 57)
         case(2)
            f( 75)=x( 90)-x( 58)
      end select

!& algeq												! voltage help required
      f( 76)=-x( 87) - prm( 50)

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 14))
         case(1)
            f( 77)=x( 91)-x( 58)
         case(2)
            f( 77)=x( 91)-x( 57)
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 15))
         case(1)
            f( 78)=x( 92)-x( 58)
         case(2)
            f( 78)=x( 92)-x( 57)
      end select

!& algeq
      f( 79)=-x( 97) + x( 91)*x( 95)

!& algeq
      f( 80)=-x( 98) + x( 92)*x( 96)

!& algeq												! priority of voltage over power
      f( 81)=-x( 99) + x( 93)*(1-x( 90))

!& algeq
      f( 82)=-x(100) + x( 94)*(1-x( 90))

!& algeq												! reactive power computation
      f( 83)=-x(101) + prm( 50)*prm( 54)*prm( 56)*(prm( 56)-x( 18))/(prm( 56)-prm( 57))*x( 97)

!& algeq
      f( 84)=-x(102) + prm( 50)*prm( 54)*prm( 56)*(x( 18)-prm( 57))/(prm( 56)-prm( 57))*x( 98)

!& algeq
      f( 85)=-x(103) + x(101) + x(102)

!& algeq												! active power computation
      f( 86)=-x(104) + prm( 51)*prm( 55)*prm( 38)*(prm( 38)-x( 16))/(prm( 38)-prm( 39))*x( 99)

!& algeq
      f( 87)=-x(105) + prm( 51)*prm( 55)*prm( 38)*(x( 16)-prm( 39))/(prm( 38)-prm( 39))*x(100)

!& algeq
      f( 88)=-x(106) + x(104) + x(105)

!& algeq												! UNIT LIMIT	!s_f1 = security factor
      f( 89)=-x( 59) + prm( 75)*prm( 58)

!& algeq
      f( 90)=-x( 60) + prm( 74)/prm( 58)

!& algeq
      f( 91)=-x( 61) + prm( 39)*prm( 58)

!& algeq
      f( 92)=-x( 62) + prm( 38)/prm( 58)

!& algeq 											! voltage low limit flag
      f( 93)=-x( 63) + x( 59) - x(  9)

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 16))
         case(1)
            f( 94)=x( 64)-x( 57)
         case(2)
            f( 94)=x( 64)-x( 58)
      end select

!& algeq												! voltage high limit flag
      f( 95)=-x( 65) + x(  9) - x( 60)

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 17))
         case(1)
            f( 96)=x( 66)-x( 57)
         case(2)
            f( 96)=x( 66)-x( 58)
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max
      f( 97)=-x( 67) + (1-x( 64))*(1-x( 66))

!& algeq												! power low limit flag
      f( 98)=-x( 68) + x( 61) - x( 16)

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 18))
         case(1)
            f( 99)=x( 69)-x( 67)
         case(2)
            f( 99)=x( 69)-x( 58)
      end select

!& algeq												! power high limit flag
      f(100)=-x( 70) + x( 16) - x( 62)

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 19))
         case(1)
            f(101)=x( 71)-x( 67)
         case(2)
            f(101)=x( 71)-x( 58)
      end select

!& algeq												! if the units are off the limit flags go to 0
      f(102)=-x( 72) + x( 64)*x( 53)

!& algeq
      f(103)=-x( 73) + x( 66)*x( 53)

!& algeq
      f(104)=-x( 74) + x( 69)*x( 53)

!& algeq
      f(105)=-x( 75) + x( 71)*x( 53)

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1
      f(106)=-x( 76) + 1-(1-x( 72))*(1-x( 73))*(1-x( 74))*(1-x( 75))

!& algeq
      f(107)=-x( 77) + (x(106) + x(147)) ! * (1 - x( 76)) ! + (x( 16) - x( 19)) * x( 76)

!& tf1p
      f(108)=(-x( 81)+1*x( 77))

!& algeq
      f(109)=-x( 83) + prm( 73)

!& swsign
      select case (z( 20))
         case(1)
            f(110)=x( 80)-x( 81)
         case(2)
            f(110)=x( 80)-x( 77)
      end select

!& tf1p2lim
      select case (z( 21))
         case(0)
            f(111)=x(155)-1*x( 80)+x( 82)
         case(1)
            f(111)=x(155)-prm( 76)*prm( 71)
         case(-1)
            f(111)=x(155)-prm(105)*prm( 71)
      end select
      select case (z( 22))
         case(0)
            f(112)=x(155)
         case(1)
            f(112)=x( 82)-prm( 84)
         case(-1)
            f(112)=x( 82)-prm( 83)
      end select

!& algeq
      f(113)=-x( 78) + x(103) ! * (1 - x( 76))

!& tf1p2lim
      select case (z( 23))
         case(0)
            f(114)=x(156)-1*x( 78)+x( 79)
         case(1)
            f(114)=x(156)-prm( 77)*prm( 71)
         case(-1)
            f(114)=x(156)-prm(106)*prm( 71)
      end select
      select case (z( 24))
         case(0)
            f(115)=x(156)
         case(1)
            f(115)=x( 79)-prm( 84)
         case(-1)
            f(115)=x( 79)-prm( 83)
      end select

!& algeq
      f(116)=-x(107) + x( 79)

!& algeq												! CENTRAL CONTROL, positive and negative variation
      f(117)=-x(109) + prm( 40)

!& algeq
      f(118)=-x(110) + prm( 41)

!& algeq
      f(119)=x(108) + x(109)

!& algeq
      f(120)=x(111) + x(110)

!& algeq
      f(121)=-x(112) + prm( 38)/prm( 59) - x( 19)

!& algeq
      f(122)=-x(113) - x( 19) + prm( 39)*prm( 59)

!& algeq
      f(123)=-x(114) + prm( 60)

!& swsign
      select case (z( 25))
         case(1)
            f(124)=x(115)-x(109)
         case(2)
            f(124)=x(115)-x(108)
      end select

!& swsign
      select case (z( 26))
         case(1)
            f(125)=x(116)-x(110)
         case(2)
            f(125)=x(116)-x(111)
      end select

!& swsign
      select case (z( 27))
         case(1)
            f(126)=x(119)-x(112)
         case(2)
            f(126)=x(119)-x(113)
      end select

!& limvb
      select case (z( 28))
         case(0)
            f(127)=x(117)-x(115)
         case(-1)
            f(127)=x(117)-x(113)
         case(1)
            f(127)=x(117)-x(112)
      end select

!& limvb
      select case (z( 29))
         case(0)
            f(128)=x(118)-x(116)
         case(-1)
            f(128)=x(118)-x(113)
         case(1)
            f(128)=x(118)-x(112)
      end select

!& algeq !dPc_0 is equal to 0
      f(129)=x(120)

!& algeq												! level 1 low negative
      f(130)=-x(121) + x( 19) - (prm( 44) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 30))
         case(1)
            f(131)=x(122)-x( 57)
         case(2)
            f(131)=x(122)-x( 58)
      end select

!& algeq												! level 1 high negative
      f(132)=-x(123) + (prm( 45) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 31))
         case(1)
            f(133)=x(124)-x( 57)
         case(2)
            f(133)=x(124)-x( 58)
      end select

!& algeq												! level 1 negative flag
      f(134)=-x(125) + (x(122) * x(124))

!& algeq												! level 1 low positive
      f(135)=-x(126) + x( 19) - (prm( 42) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 32))
         case(1)
            f(136)=x(127)-x( 57)
         case(2)
            f(136)=x(127)-x( 58)
      end select

!& algeq												! level 1 high positive
      f(137)=-x(128) + (prm( 43) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 33))
         case(1)
            f(138)=x(129)-x( 57)
         case(2)
            f(138)=x(129)-x( 58)
      end select

!& algeq												! level 1 positive flag
      f(139)=-x(130) + (x(127) * x(129))

!& swsign
      select case (z( 34))
         case(1)
            f(140)=x(131)-x(130)
         case(2)
            f(140)=x(131)-x(125)
      end select

!& algeq
      f(141)=-x(132) + x(117) * x(131)

!& algeq												! level 2 low negative
      f(142)=-x(133) + x( 19) - (prm( 48) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 35))
         case(1)
            f(143)=x(134)-x( 57)
         case(2)
            f(143)=x(134)-x( 58)
      end select

!& algeq												! level 2 high negative
      f(144)=-x(135) + (prm( 49) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 36))
         case(1)
            f(145)=x(136)-x( 57)
         case(2)
            f(145)=x(136)-x( 58)
      end select

!& algeq												! level 2 negative flag
      f(146)=-x(137) + (x(134) * x(136))

!& algeq												! level 2 low positive
      f(147)=-x(138) + x( 19) - (prm( 46) * (prm( 38)-prm( 39)) + prm( 39))

!& swsign
      select case (z( 37))
         case(1)
            f(148)=x(139)-x( 57)
         case(2)
            f(148)=x(139)-x( 58)
      end select

!& algeq												! level 2 high positive
      f(149)=-x(140) + (prm( 47) * (prm( 38)-prm( 39)) + prm( 39)) - x( 19)

!& swsign
      select case (z( 38))
         case(1)
            f(150)=x(141)-x( 57)
         case(2)
            f(150)=x(141)-x( 58)
      end select

!& algeq												! level 2 positive flag
      f(151)=-x(142) + (x(139) * x(141))

!& swsign
      select case (z( 39))
         case(1)
            f(152)=x(143)-x(142)
         case(2)
            f(152)=x(143)-x(137)
      end select

!& algeq
      f(153)=-x(144) + x(118) * x(143)

!& abs												! absolute of the level
      if(z( 40) == 1 )then
         f(154)=x(145)-x(114)
      else
         f(154)=x(145)+x(114)
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1
      f(155)=-x(146) + x(145) + 1

!& switch4											! choice between the levels
      select case (z( 41))
         case(  1)
            f(156)=x(147)-x(120)
         case(  2)
            f(156)=x(147)-x(132)
         case(  3)
            f(156)=x(147)-x(144)
         case(  4)
            f(156)=x(147)-x(119)
      end select

!........................................................................................
   case (update_disc)

!& algeq                                             ! voltage magnitude

!& tf1p

!& tf1p

!& tf1p

!& algeq                                             ! voltage alignment

!& algeq

!& algeq

!& algeq                                             ! compute ix

!& algeq                                             ! compute iy

!& algeq

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq                                             ! compute powers

!& algeq

!& algeq

!& algeq                                             ! DC voltage control

!& pictl

!& algeq                                             ! p-axis current control

!& pictl

!& algeq                                             ! q-axis current control

!& algeq

!& pictl

!& algeq                                             ! md

!& algeq                                             ! mq

!& algeq                                             ! voltage over d-axis terminal impedance

!& algeq                                             ! voltage over q-axis terminal impedance

!& tf1p                                              ! d-axis current

!& tf1p                                              ! q-axis current

!& algeq                                             ! DC link current

!& algeq                                             ! DC link

!& int                                               ! DC link voltage

!& limvb                                              ! power limiter
      select case (z(  1))
         case(0)
            if(x( 47)>x( 23))then
               z(  1)=1
            elseif(x( 47)<x( 52))then
               z(  1)=-1
            endif
         case(-1)
            if(x( 47)>x( 52))then
               z(  1)=0
            endif
         case(1)
            if(x( 47)<x( 23))then
               z(  1)=0
            endif
      end select

!& algeq                                             ! power mismatch

!& pictl                                             ! power control

!& lim                                               ! limit speed control input
      select case (z(  2))
         case(0)
            if(x( 33)>prm( 25))then
               z(  2)=1
            elseif(x( 33)<prm( 24))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 33)>prm( 24))then
               z(  2)=0
            endif
         case(1)
            if(x( 33)<prm( 25))then
               z(  2)=0
            endif
      end select

!& algeq                                             ! speed control input

!& pictl                                             ! speed control

!& tf1p                                              ! motor current control

!& algeq                                             ! torque equations

!& algeq

!& tf1p                                              ! motor inertia

!& algeq

!& pwlin4                                            ! overvoltage Protection
      if(x( 41)<(-999))then
         z(  3)=1
      elseif(x( 41)>=999)then
         z(  3)=  3
      elseif((-999)<=x( 41) .and. x( 41)<0.)then
         z(  3)=  1
      elseif(0.<=x( 41) .and. x( 41)<0.)then
         z(  3)=  2
      elseif(0.<=x( 41) .and. x( 41)<999)then
         z(  3)=  3
      endif

!& algeq

!& hyst
      if (z(  4) == -1)then
         if(x( 43)>1.1)then
            z(  4)=1
         endif
      else
         if(x( 43)<0.9)then
            z(  4)=-1
         endif
      endif

!& algeq                                             ! LVRT

!& timer5
      if(z(  5) == -1)then
         if(x( 48) >= (-prm( 29)))then
            z(  5)=0
            eqtyp( 54)=154
         endif
      else
         if(x( 48) < (-prm( 29)))then
            z(  5)=-1
            eqtyp( 54)=0
         endif
      endif
      if(z(  5) == 0)then
         if(x( 48) > (-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))then
            if(x(154) > 0.)then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))then
            if(x(154) > prm( 30)+(0.-prm( 30))*(x( 48)-(-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))then
            if(x(154) > prm( 30)+(prm( 30)-prm( 30))*(x( 48)-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 27) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))then
            if(x(154) > prm( 32)+(prm( 30)-prm( 32))*(x( 48)-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33))))/((-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))-(-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))))then
               z(  5)=1
            endif
         elseif(x( 48) > (-prm( 29)))then
            if(x(154) > prm( 31)+(prm( 32)-prm( 31))*(x( 48)-(-prm( 29)))/((-(prm( 35)*prm( 28) + (1-prm( 35))*prm( 33)))-(-prm( 29))))then
               z(  5)=1
            endif
         endif
      endif

!& algeq

!& hyst
      if (z(  6) == -1)then
         if(x( 51)>1.1)then
            z(  6)=1
         endif
      else
         if(x( 51)<0.9)then
            z(  6)=-1
         endif
      endif

!& algeq                                             ! status variable that can switch the entire unit on or off

!& algeq

!& algeq

!& swsign
      select case (z(  7))
         case(1)
            if(x( 54)<0.)then
               z(  7)=2
            endif
         case(2)
            if(x( 54)>=0.)then
               z(  7)=1
            endif
      end select

!& algeq                                             ! min and max power limiters

!& algeq

!& algeq                                             ! status check without droop

!& algeq                                             ! TESTING STARTS HERE	 Positive and negative variation

!& algeq

!& algeq												! NEIGHBOURHOOD CONTROL

!& algeq

!& swsign											! F_help_p_p is 1 if P_n > 0
      select case (z(  8))
         case(1)
            if(x( 85)<0.)then
               z(  8)=2
            endif
         case(2)
            if(x( 85)>=0.)then
               z(  8)=1
            endif
      end select

!& swsign											! F_help_p_m is 1 if P_n < 0
      select case (z(  9))
         case(1)
            if(x( 84)<0.)then
               z(  9)=2
            endif
         case(2)
            if(x( 84)>=0.)then
               z(  9)=1
            endif
      end select

!& hyst ! should it be V or Vm?
      if (z( 10) == -1)then
         if(x(  9)>prm( 52))then
            z( 10)=1
         endif
      else
         if(x(  9)<prm( 53))then
            z( 10)=-1
         endif
      endif

!& hyst
      if (z( 11) == -1)then
         if(x(  9)>prm( 52))then
            z( 11)=1
         endif
      else
         if(x(  9)<prm( 53))then
            z( 11)=-1
         endif
      endif

!& algeq												! F_help_v is different from 0 only if V_n is !=0

!& abs
      if (z( 12) == -1 )then
         if(x( 86)> blocktol1 )then
            z( 12)=1
         endif
      else
         if(x( 86)< - blocktol1 )then
            z( 12)=-1
         endif
      endif

!& algeq

!& swsign
      select case (z( 13))
         case(1)
            if(x( 89)<0.)then
               z( 13)=2
            endif
         case(2)
            if(x( 89)>=0.)then
               z( 13)=1
            endif
      end select

!& algeq												! voltage help required

!& swsign											! F_help_v_p is 1 if V_n > 0
      select case (z( 14))
         case(1)
            if(x( 87)<0.)then
               z( 14)=2
            endif
         case(2)
            if(x( 87)>=0.)then
               z( 14)=1
            endif
      end select

!& swsign											! F_help_v_m is 1 if V_n < 0
      select case (z( 15))
         case(1)
            if(x( 86)<0.)then
               z( 15)=2
            endif
         case(2)
            if(x( 86)>=0.)then
               z( 15)=1
            endif
      end select

!& algeq

!& algeq

!& algeq												! priority of voltage over power

!& algeq

!& algeq												! reactive power computation

!& algeq

!& algeq

!& algeq												! active power computation

!& algeq

!& algeq

!& algeq												! UNIT LIMIT	!s_f1 = security factor

!& algeq

!& algeq

!& algeq

!& algeq 											! voltage low limit flag

!& swsign											! F_v_min_in = 1 only if Vm <= V_lim_min
      select case (z( 16))
         case(1)
            if(x( 63)<0.)then
               z( 16)=2
            endif
         case(2)
            if(x( 63)>=0.)then
               z( 16)=1
            endif
      end select

!& algeq												! voltage high limit flag

!& swsign											! F_v_max_in = 1 only if Vm >= V_lim_max
      select case (z( 17))
         case(1)
            if(x( 65)<0.)then
               z( 17)=2
            endif
         case(2)
            if(x( 65)>=0.)then
               z( 17)=1
            endif
      end select

!& algeq 											! F_v_opp = 0 if V_m <= V_lim_min or V_m >= V_lim_max

!& algeq												! power low limit flag

!& swsign											! F_p_min_in = 1 only if Punit <= P_lim_min and F_v_opp = 1
      select case (z( 18))
         case(1)
            if(x( 68)<0.)then
               z( 18)=2
            endif
         case(2)
            if(x( 68)>=0.)then
               z( 18)=1
            endif
      end select

!& algeq												! power high limit flag

!& swsign											! F_p_max_in = 1 only if Punit >= P_lim_max and F_v_opp = 1
      select case (z( 19))
         case(1)
            if(x( 70)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x( 70)>=0.)then
               z( 19)=1
            endif
      end select

!& algeq												! if the units are off the limit flags go to 0

!& algeq

!& algeq

!& algeq

!& algeq												! F_lim = 1 if any F_v/p_min/max = 1

!& algeq

!& tf1p

!& algeq

!& swsign
      select case (z( 20))
         case(1)
            if(x( 83)<0.)then
               z( 20)=2
            endif
         case(2)
            if(x( 83)>=0.)then
               z( 20)=1
            endif
      end select

!& tf1p2lim
      select case (z( 21))
         case(0)
            if(x(155)>prm( 76)*prm( 71))then
               z( 21)=1
            elseif(x(155)<prm(105)*prm( 71))then
               z( 21)=-1
            endif
         case(1)
            if(1*x( 80)-x( 82)<prm( 76)*prm( 71))then
               z( 21)= 0
            endif
         case(-1)
            if(1*x( 80)-x( 82)>prm(105)*prm( 71))then
               z( 21)= 0
            endif
      end select
      select case (z( 22))
         case(0)
            if(x( 82)>prm( 84))then
               z( 22)=1
               eqtyp(112)=0
            elseif(x( 82)<prm( 83))then
               z( 22)=-1
               eqtyp(112)=0
            endif
         case(1)
            if (x(155)<0.)then
               z( 22)= 0
               eqtyp(112)= 82
            endif
         case(-1)
            if(x(155)>0.)then
               z( 22)= 0
               eqtyp(112)= 82
            endif
      end select

!& algeq

!& tf1p2lim
      select case (z( 23))
         case(0)
            if(x(156)>prm( 77)*prm( 71))then
               z( 23)=1
            elseif(x(156)<prm(106)*prm( 71))then
               z( 23)=-1
            endif
         case(1)
            if(1*x( 78)-x( 79)<prm( 77)*prm( 71))then
               z( 23)= 0
            endif
         case(-1)
            if(1*x( 78)-x( 79)>prm(106)*prm( 71))then
               z( 23)= 0
            endif
      end select
      select case (z( 24))
         case(0)
            if(x( 79)>prm( 84))then
               z( 24)=1
               eqtyp(115)=0
            elseif(x( 79)<prm( 83))then
               z( 24)=-1
               eqtyp(115)=0
            endif
         case(1)
            if (x(156)<0.)then
               z( 24)= 0
               eqtyp(115)= 79
            endif
         case(-1)
            if(x(156)>0.)then
               z( 24)= 0
               eqtyp(115)= 79
            endif
      end select

!& algeq

!& algeq												! CENTRAL CONTROL, positive and negative variation

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 25))
         case(1)
            if(x(114)<0.)then
               z( 25)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 25)=1
            endif
      end select

!& swsign
      select case (z( 26))
         case(1)
            if(x(114)<0.)then
               z( 26)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 26)=1
            endif
      end select

!& swsign
      select case (z( 27))
         case(1)
            if(x(114)<0.)then
               z( 27)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 27)=1
            endif
      end select

!& limvb
      select case (z( 28))
         case(0)
            if(x(115)>x(112))then
               z( 28)=1
            elseif(x(115)<x(113))then
               z( 28)=-1
            endif
         case(-1)
            if(x(115)>x(113))then
               z( 28)=0
            endif
         case(1)
            if(x(115)<x(112))then
               z( 28)=0
            endif
      end select

!& limvb
      select case (z( 29))
         case(0)
            if(x(116)>x(112))then
               z( 29)=1
            elseif(x(116)<x(113))then
               z( 29)=-1
            endif
         case(-1)
            if(x(116)>x(113))then
               z( 29)=0
            endif
         case(1)
            if(x(116)<x(112))then
               z( 29)=0
            endif
      end select

!& algeq !dPc_0 is equal to 0

!& algeq												! level 1 low negative

!& swsign
      select case (z( 30))
         case(1)
            if(x(121)<0.)then
               z( 30)=2
            endif
         case(2)
            if(x(121)>=0.)then
               z( 30)=1
            endif
      end select

!& algeq												! level 1 high negative

!& swsign
      select case (z( 31))
         case(1)
            if(x(123)<0.)then
               z( 31)=2
            endif
         case(2)
            if(x(123)>=0.)then
               z( 31)=1
            endif
      end select

!& algeq												! level 1 negative flag

!& algeq												! level 1 low positive

!& swsign
      select case (z( 32))
         case(1)
            if(x(126)<0.)then
               z( 32)=2
            endif
         case(2)
            if(x(126)>=0.)then
               z( 32)=1
            endif
      end select

!& algeq												! level 1 high positive

!& swsign
      select case (z( 33))
         case(1)
            if(x(128)<0.)then
               z( 33)=2
            endif
         case(2)
            if(x(128)>=0.)then
               z( 33)=1
            endif
      end select

!& algeq												! level 1 positive flag

!& swsign
      select case (z( 34))
         case(1)
            if(x(114)<0.)then
               z( 34)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 34)=1
            endif
      end select

!& algeq

!& algeq												! level 2 low negative

!& swsign
      select case (z( 35))
         case(1)
            if(x(133)<0.)then
               z( 35)=2
            endif
         case(2)
            if(x(133)>=0.)then
               z( 35)=1
            endif
      end select

!& algeq												! level 2 high negative

!& swsign
      select case (z( 36))
         case(1)
            if(x(135)<0.)then
               z( 36)=2
            endif
         case(2)
            if(x(135)>=0.)then
               z( 36)=1
            endif
      end select

!& algeq												! level 2 negative flag

!& algeq												! level 2 low positive

!& swsign
      select case (z( 37))
         case(1)
            if(x(138)<0.)then
               z( 37)=2
            endif
         case(2)
            if(x(138)>=0.)then
               z( 37)=1
            endif
      end select

!& algeq												! level 2 high positive

!& swsign
      select case (z( 38))
         case(1)
            if(x(140)<0.)then
               z( 38)=2
            endif
         case(2)
            if(x(140)>=0.)then
               z( 38)=1
            endif
      end select

!& algeq												! level 2 positive flag

!& swsign
      select case (z( 39))
         case(1)
            if(x(114)<0.)then
               z( 39)=2
            endif
         case(2)
            if(x(114)>=0.)then
               z( 39)=1
            endif
      end select

!& algeq

!& abs												! absolute of the level
      if (z( 40) == -1 )then
         if(x(114)> blocktol1 )then
            z( 40)=1
         endif
      else
         if(x(114)< - blocktol1 )then
            z( 40)=-1
         endif
      endif

!& algeq												! the switch takes values from 1 to 4, we shift l_abs by 1

!& switch4											! choice between the levels
      z( 41)=max(1,min(  4,nint(x(146))))
   end select

end subroutine inj_ATLnew
