!  MODEL NAME : inj_IBG3                
!  MODEL DESCRIPTION FILE : IBG3_V2.txt
!  Data :
!       prm(  1)=  Imax
!       prm(  2)=  IN
!       prm(  3)=  Iprate
!       prm(  4)=  Tg
!       prm(  5)=  Tm
!       prm(  6)=  tLVRT1
!       prm(  7)=  tLVRT2
!       prm(  8)=  tLVRTint
!       prm(  9)=  Vmax
!       prm( 10)=  tau              ! response time of the PLL in milliseconds
!       prm( 11)=  Vminpll          ! Voltage magnitude under which the PLL is blocked
!       prm( 12)=  a
!       prm( 13)=  Vmin
!       prm( 14)=  Vint
!       prm( 15)=  fmin
!       prm( 16)=  fmax
!       prm( 17)=  fstart
!       prm( 18)=  b
!       prm( 19)=  fr
!       prm( 20)=  Tr      ! Time after which units are allowed to reconnect to the network
!       prm( 21)=  Re
!       prm( 22)=  Xe
!       prm( 23)=  CM1
!       prm( 24)=  kRCI
!       prm( 25)=  kRCA
!       prm( 26)=  m
!       prm( 27)=  n
!       prm( 28)=  dbmin
!       prm( 29)=  dbmax
!       prm( 30)=  HVRT
!       prm( 31)=  LVRT
!       prm( 32)=  CM2
!       prm( 33)=  Vtrip
!       prm( 34)=  fdbup
!       prm( 35)=  fdbdn
!       prm( 36)=  Rup
!       prm( 37)=  Rdn
!  Parameters :
!       prm( 38)=  vxlv  
!       prm( 39)=  vylv  
!       prm( 40)=  Vref  
!       prm( 41)=  Pext  
!       prm( 42)=  Qext  
!       prm( 43)=  Iqref  
!       prm( 44)=  Ipref  
!       prm( 45)=  kpll  
!       prm( 46)=  theta_PLL  
!       prm( 47)=  Uplim  
!       prm( 48)=  Downlim  
!       prm( 49)=  Downlimdisc  
!       prm( 50)=  UplimdeltaP  
!       prm( 51)=  DownlimdeltaP  
!       prm( 52)=  Tlim  
!       prm( 53)=  Uplimdis  
!       prm( 54)=  downlimdis  
!       prm( 55)=  ratemax  
!       prm( 56)=  rate  
!       prm( 57)=  rateh  
!       prm( 58)=  fref  
!  Output states :
!       x(  1)=  ix           real component of current
!       x(  2)=  iy           imaginary component of current
!  Internal states defined by user :
!       x(  3)=  vxl                   
!       x(  4)=  vyl                   
!       x(  5)=  Vt                    
!       x(  6)=  PLLPhaseAngle         
!       x(  7)=  Vm                    
!       x(  8)=  x2                    
!       x(  9)=  Ip                    
!       x( 10)=  Iq                    
!       x( 11)=  Ipcmd                 
!       x( 12)=  Iqcmd                 
!       x( 13)=  Iqmax                 
!       x( 14)=  Iqmin                 
!       x( 15)=  Ipmax                 
!       x( 16)=  Ipmin                 
!       x( 17)=  x4                    
!       x( 18)=  DeltaW                
!       x( 19)=  DeltaWf               
!       x( 20)=  vq                    
!       x( 21)=  vd                    
!       x( 22)=  Pgen                  
!       x( 23)=  Qgen                  
!       x( 24)=  Iqext                 
!       x( 25)=  Fvl                   
!       x( 26)=  Fvli                  
!       x( 27)=  z1                    
!       x( 28)=  x5                    
!       x( 29)=  Iptemp                
!       x( 30)=  Iqtemp                
!       x( 31)=  x10                   
!       x( 32)=  x11                   
!       x( 33)=  z                     
!       x( 34)=  deltaV                
!       x( 35)=  Pflag                 
!       x( 36)=  Pflaga                 Switch input a
!       x( 37)=  Pflagb                 Switch input b
!       x( 38)=  vxlm                  
!       x( 39)=  vylm                  
!       x( 40)=  omegam                
!       x( 41)=  fm                    
!       x( 42)=  fmfilt                
!       x( 43)=  Ffli                   Current multiplier (input of hysteresis)
!       x( 44)=  Ffl                   
!       x( 45)=  Ffhi                  
!       x( 46)=  Ffh                   
!       x( 47)=  deltaP                
!       x( 48)=  deltaPfin              Active power correction
!       x( 49)=  Ptot                  
!       x( 50)=  deltafl               
!       x( 51)=  flagla                
!       x( 52)=  flaglb                
!       x( 53)=  deltafh               
!       x( 54)=  flagha                
!       x( 55)=  flaghb                
!       x( 56)=  PLLmulta              
!       x( 57)=  PLLmultb              
!       x( 58)=  mult                  
!       x( 59)=  deltaVPLL             
!       x( 60)=  wpll                  
!       x( 61)=  g                     
!       x( 62)=  tr                    
!       x( 63)=  Fr                    
!       x( 64)=  Frtemp                
!       x( 65)=  fvla                  
!       x( 66)=  fvlb                  
!       x( 67)=  deltafvl              
!       x( 68)=  g1                    
!       x( 69)=  tr1                   
!       x( 70)=  Frvh                  
!       x( 71)=  Frvhtemp              
!       x( 72)=  fvha                  
!       x( 73)=  fvhb                  
!       x( 74)=  deltafvh              
!       x( 75)=  g2                    
!       x( 76)=  tr2                   
!       x( 77)=  Frfl                  
!       x( 78)=  Frfltemp              
!       x( 79)=  ffla                  
!       x( 80)=  fflb                  
!       x( 81)=  deltaffl              
!       x( 82)=  g3                    
!       x( 83)=  tr3                   
!       x( 84)=  Frfh                  
!       x( 85)=  Frfhtemp              
!       x( 86)=  ffha                  
!       x( 87)=  ffhb                  
!       x( 88)=  deltaffh              
!       x( 89)=  deltaPlim             
!       x( 90)=  deltafcomp            
!       x( 91)=  fcomp                 
!       x( 92)=  fa                    
!       x( 93)=  fb                    
!       x( 94)=  fcompf                
!       x( 95)=  w1                    
!       x( 96)=  w2                    
!       x( 97)=  w3                    
!       x( 98)=  w4                    
!       x( 99)=  w5                    
!       x(100)=  w6                    
!       x(101)=  w7                    
!       x(102)=  Frvhtemp1             

!.........................................................................................................

subroutine inj_IBG3(nb,name,mode,nbxvar,nbzvar,nbdata,nbaddpar,prm,parname,nbobs, &
   obsname,adix,adiy,eqtyp,tc,t,omega,sbase,bus,vx,vy,ix,iy,x,z,f,obs)

   use MODELING
   use FREQUENCY
   use ISLAND, only : isl
   use SETTINGS, only : blocktol1,omega_ref,pi
   use FUNCTIONS_IN_MODELS

   implicit none
   double precision, intent(in):: t,vx,vy,omega,sbase,ix,iy
   double precision, intent(out):: f(*)
   double precision :: obs(*)
   double precision, intent(inout):: x(*),prm(*),tc(*)
   integer, intent(in):: nb,mode,bus
   integer, intent(inout):: nbxvar,nbzvar,nbdata,nbaddpar,nbobs,eqtyp(*),z(*),adix,adiy
   character(len=20), intent(in):: name
   character(len=10) :: parname(*),obsname(*)

   select case (mode)
   case (define_var_and_par)
      nbdata= 37
      nbaddpar= 21
      parname(  1)='Imax'
      parname(  2)='IN'
      parname(  3)='Iprate'
      parname(  4)='Tg'
      parname(  5)='Tm'
      parname(  6)='tLVRT1'
      parname(  7)='tLVRT2'
      parname(  8)='tLVRTint'
      parname(  9)='Vmax'
      parname( 10)='tau'
      parname( 11)='Vminpll'
      parname( 12)='a'
      parname( 13)='Vmin'
      parname( 14)='Vint'
      parname( 15)='fmin'
      parname( 16)='fmax'
      parname( 17)='fstart'
      parname( 18)='b'
      parname( 19)='fr'
      parname( 20)='Tr'
      parname( 21)='Re'
      parname( 22)='Xe'
      parname( 23)='CM1'
      parname( 24)='kRCI'
      parname( 25)='kRCA'
      parname( 26)='m'
      parname( 27)='n'
      parname( 28)='dbmin'
      parname( 29)='dbmax'
      parname( 30)='HVRT'
      parname( 31)='LVRT'
      parname( 32)='CM2'
      parname( 33)='Vtrip'
      parname( 34)='fdbup'
      parname( 35)='fdbdn'
      parname( 36)='Rup'
      parname( 37)='Rdn'
      parname( 38)='vxlv'
      parname( 39)='vylv'
      parname( 40)='Vref'
      parname( 41)='Pext'
      parname( 42)='Qext'
      parname( 43)='Iqref'
      parname( 44)='Ipref'
      parname( 45)='kpll'
      parname( 46)='theta_PLL'
      parname( 47)='Uplim'
      parname( 48)='Downlim'
      parname( 49)='Downlimdisc'
      parname( 50)='UplimdeltaP'
      parname( 51)='DownlimdeltaP'
      parname( 52)='Tlim'
      parname( 53)='Uplimdis'
      parname( 54)='downlimdis'
      parname( 55)='ratemax'
      parname( 56)='rate'
      parname( 57)='rateh'
      parname( 58)='fref'
      adix=  1
      adiy=  2
      nbxvar=110
      nbzvar= 38

!........................................................................................
   case (define_obs)
      nbobs= 20
      obsname(  1)='Ip'
      obsname(  2)='Iq'
      obsname(  3)='Pgen'
      obsname(  4)='Qgen'
      obsname(  5)='Vm'
      obsname(  6)='PLLPhaseAngle'
      obsname(  7)='fm'
      obsname(  8)='fmfilt'
      obsname(  9)='Fr'
      obsname( 10)='Ffl'
      obsname( 11)='Ffh'
      obsname( 12)='Frtemp'
      obsname( 13)='Ipcmd'
      obsname( 14)='Vt'
      obsname( 15)='wpll'
      obsname( 16)='vq'
      obsname( 17)='z1'
      obsname( 18)='tr1'
      obsname( 19)='deltafvh'
      obsname( 20)='Frvh'

!........................................................................................
   case (evaluate_obs)
      obs(  1)=x(  9)              
      obs(  2)=x( 10)              
      obs(  3)=x( 22)              
      obs(  4)=x( 23)              
      obs(  5)=x(  7)              
      obs(  6)=x(  6)              
      obs(  7)=x( 41)              
      obs(  8)=x( 42)              
      obs(  9)=x( 63)              
      obs( 10)=x( 44)              
      obs( 11)=x( 46)              
      obs( 12)=x( 64)              
      obs( 13)=x( 11)              
      obs( 14)=x(  5)              
      obs( 15)=x( 60)              
      obs( 16)=x( 20)              
      obs( 17)=x( 27)              
      obs( 18)=x( 69)              
      obs( 19)=x( 74)              
      obs( 20)=x( 70)              

!........................................................................................
   case (initialize)

!vxlv = [vx] + {Re}*[ix] - {Xe}*[iy]
      prm( 38)= vx + prm( 21)*ix - prm( 22)*iy

!vylv = [vy] + {Re}*[iy] + {Xe}*[ix]
      prm( 39)= vy + prm( 21)*iy + prm( 22)*ix

!Vref = dsqrt({vxlv}**2 + {vylv}**2)
      prm( 40)= dsqrt(prm( 38)**2 + prm( 39)**2)

!Pext = -{vxlv}*[ix]-{vylv}*[iy]
      prm( 41)= -prm( 38)*ix-prm( 39)*iy

!Qext = +{vxlv}*[iy]-{vylv}*[ix]
      prm( 42)= +prm( 38)*iy-prm( 39)*ix

!Iqref = -{Qext}/{Vref}
      prm( 43)= -prm( 42)/prm( 40)

!Ipref = -{Pext}/{Vref}
      prm( 44)= -prm( 41)/prm( 40)

!kpll = 10/({tau}*0.001)
      prm( 45)= 10/(prm( 10)*0.001)

!theta_PLL = atan({vylv}/{vxlv})
      prm( 46)= atan(prm( 39)/prm( 38))

!Uplim = 9999
      prm( 47)= 9999

!Downlim = -9999
      prm( 48)= -9999

!Downlimdisc = 0
      prm( 49)= 0

!UplimdeltaP = 9999
      prm( 50)= 9999

!DownlimdeltaP = 0
      prm( 51)= 0

!Tlim = 0.01
      prm( 52)= 0.01

!Uplimdis = 0
      prm( 53)= 0

!downlimdis = -9999
      prm( 54)= -9999

!ratemax = {Iprate}*{IN}
      prm( 55)= prm(  3)*prm(  2)

!rate = 0.5
      prm( 56)= 0.5

!rateh = 0.5
      prm( 57)= 0.5

!fref = 1
      prm( 58)= 1

!vxl =  {vxlv}
      x(  3)= prm( 38)

!vyl =  {vylv}
      x(  4)= prm( 39)

!Vt =  {Vref}
      x(  5)= prm( 40)

!PLLPhaseAngle =  {theta_PLL}
      x(  6)= prm( 46)

!Vm =  {Vref}
      x(  7)= prm( 40)

!x2 =  {Vref}
      x(  8)= prm( 40)

!Ip =  {Ipref}
      x(  9)= prm( 44)

!Iq =  {Iqref}
      x( 10)= prm( 43)

!Ipcmd =  {Ipref}
      x( 11)= prm( 44)

!Iqcmd =  {Iqref}
      x( 12)= prm( 43)

!Iqmax =  dsqrt({Imax}**2 - {Ipref}**2 )
      x( 13)= dsqrt(prm(  1)**2 - prm( 44)**2 )

!Iqmin =  - dsqrt({Imax}**2 - {Ipref}**2 )
      x( 14)= - dsqrt(prm(  1)**2 - prm( 44)**2 )

!Ipmax =  {IN}
      x( 15)= prm(  2)

!Ipmin =  -0.001
      x( 16)= -0.001

!x4 =  {Ipref}
      x( 17)= prm( 44)

!DeltaW =  0
      x( 18)= 0

!DeltaWf =   0
      x( 19)=  0

!vq =  -{vxlv}*sin({theta_PLL}) + {vylv}*cos({theta_PLL})
      x( 20)= -prm( 38)*sin(prm( 46)) + prm( 39)*cos(prm( 46))

!vd =  {vxlv}*cos({theta_PLL}) + {vylv}*sin({theta_PLL})
      x( 21)= prm( 38)*cos(prm( 46)) + prm( 39)*sin(prm( 46))

!Pgen =  -{Pext}
      x( 22)= -prm( 41)

!Qgen =  -{Qext}
      x( 23)= -prm( 42)

!Iqext =  0
      x( 24)= 0

!Fvl =  1
      x( 25)= 1

!Fvli =  1
      x( 26)= 1

!z1 =  0.
      x( 27)= 0.

!x5 =  {Iqref}
      x( 28)= prm( 43)

!Iptemp =  {Ipref}
      x( 29)= prm( 44)

!Iqtemp =  {Iqref}
      x( 30)= prm( 43)

!x10 =  -{Vref}
      x( 31)= -prm( 40)

!x11 =  {Vref} - {Vmax}
      x( 32)= prm( 40) - prm(  9)

!z =  0.
      x( 33)= 0.

!deltaV =  {Vref} - {dbmin}
      x( 34)= prm( 40) - prm( 28)

!Pflag =  1
      x( 35)= 1

!Pflaga =  1
      x( 36)= 1

!Pflagb =  0.
      x( 37)= 0.

!vxlm =  {vxlv}
      x( 38)= prm( 38)

!vylm =  {vylv}
      x( 39)= prm( 39)

!omegam =  -0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1
      x( 40)= -0.5*(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46))) + 1

!fm =  50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)
      x( 41)= 50*(-0.5*(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46))) + 1)

!fmfilt =  50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)
      x( 42)= 50*(-0.5*(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46))) + 1)

!Ffli =  1.
      x( 43)= 1.

!Ffl =  1.
      x( 44)= 1.

!Ffhi =  1.
      x( 45)= 1.

!Ffh =  1.
      x( 46)= 1.

!deltaP =  {b}*-{Pext}*(50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)  - {fstart})/50
      x( 47)= prm( 18)*-prm( 41)*(50*(-(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46))) + 1)  - prm( 17))/50

!deltaPfin =  0
      x( 48)= 0

!Ptot =  -{Pext}
      x( 49)= -prm( 41)

!deltafl =  50*[omega] - {fmin}
      x( 50)= 50*omega - prm( 15)

!flagla =  1
      x( 51)= 1

!flaglb =  0
      x( 52)= 0

!deltafh =  {fmax} - 50*[omega]
      x( 53)= prm( 16) - 50*omega

!flagha =  1
      x( 54)= 1

!flaghb =  0
      x( 55)= 0

!PLLmulta =  1
      x( 56)= 1

!PLLmultb =  0
      x( 57)= 0

!mult =  1
      x( 58)= 1

!deltaVPLL =  {Vref} - {Vminpll}
      x( 59)= prm( 40) - prm( 11)

!wpll =  -({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL}))*{kpll} + 2*pi*50
      x( 60)= -(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46)))*prm( 45) + 2*pi*50

!g =  0
      x( 61)= 0

!tr =  0
      x( 62)= 0

!Fr =  1
      x( 63)= 1

!Frtemp =  1
      x( 64)= 1

!fvla =  1
      x( 65)= 1

!fvlb =  1
      x( 66)= 1

!deltafvl =  -{Tr}
      x( 67)= -prm( 20)

!g1 =  1
      x( 68)= 1

!tr1 = 0
      x( 69)=0

!Frvh =  1
      x( 70)= 1

!Frvhtemp =  1
      x( 71)= 1

!fvha =  1
      x( 72)= 1

!fvhb =  1
      x( 73)= 1

!deltafvh =  -{Tr}
      x( 74)= -prm( 20)

!g2 =  0
      x( 75)= 0

!tr2 =  0
      x( 76)= 0

!Frfl =  1
      x( 77)= 1

!Frfltemp =  1
      x( 78)= 1

!ffla =  1
      x( 79)= 1

!fflb =  1
      x( 80)= 1

!deltaffl =  -{Tr}
      x( 81)= -prm( 20)

!g3 =  0
      x( 82)= 0

!tr3 =  0
      x( 83)= 0

!Frfh =  1
      x( 84)= 1

!Frfhtemp =  1
      x( 85)= 1

!ffha =  1
      x( 86)= 1

!ffhb =  1
      x( 87)= 1

!deltaffh =  -{Tr}
      x( 88)= -prm( 20)

!deltaPlim =  0
      x( 89)= 0

!deltafcomp =  50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1) - {fr}
      x( 90)= 50*(-(prm( 38)*sin(prm( 46)) - prm( 39)*cos(prm( 46))) + 1) - prm( 19)

!fcomp =  0
      x( 91)= 0

!fa =  1
      x( 92)= 1

!fb =  0
      x( 93)= 0

!fcompf =  0
      x( 94)= 0

!w1 =  0
      x( 95)= 0

!w2 =  0
      x( 96)= 0

!w3 =  0
      x( 97)= 0

!w4 =  0
      x( 98)= 0

!w5 =  0
      x( 99)= 0

!w6 =  0
      x(100)= 0

!w7 =  0
      x(101)= 0

!Frvhtemp1 = 1
      x(102)=1

!& algeq
      eqtyp(  1)=0

!& algeq
      eqtyp(  2)=0

!& algeq
      eqtyp(  3)=0

!& tf1p
      eqtyp(  4)=  7
      tc(  4)=prm(  5)

!& max1v1c
      eqtyp(  5)=0
      if(x(  7)<0.01)then
         z(  1)=1
      else
         z(  1)=2
      endif

!& algeq
      eqtyp(  6)=0

!& algeq
      eqtyp(  7)=0

!& limvb
      eqtyp(  8)=0
      if(x( 17)>x( 15))then
         z(  2)=1
      elseif(x( 17)<x( 16))then
         z(  2)=-1
      else
         z(  2)=0
      endif

!& algeq
      eqtyp(  9)=0

!& tf1p2lim
      if(prm(  4)< 0.001)then
         prm(  4)=0.d0
         prm( 48)=-huge(0.d0)
         prm( 47)= huge(0.d0)
         prm( 48)=-huge(0.d0)
         prm( 55)= huge(0.d0)
      endif
      if(1*x( 11)-x( 29)>prm( 55)*prm(  4))then
         z(  3)=1
      elseif(1*x( 11)-x( 29)<prm( 48)*prm(  4))then
         z(  3)=-1
      else
         z(  3)=0
      endif
      eqtyp( 10)=0
      if(x( 29)>prm( 47))then
         z(  4)=1
         eqtyp( 11)=0
      elseif(x( 29)<prm( 48))then
         z(  4)=-1
         eqtyp( 11)=0
      else
         z(  4)=0
         eqtyp( 11)= 29
      endif
      tc( 11)=prm(  4)

!& algeq 				  ! Active - reactive power priority
      eqtyp( 12)=0

!& algeq
      eqtyp( 13)=0

!& algeq
      eqtyp( 14)=0

!& limvb
      eqtyp( 15)=0
      if(x( 28)>x( 13))then
         z(  5)=1
      elseif(x( 28)<x( 14))then
         z(  5)=-1
      else
         z(  5)=0
      endif

!& algeq						!  Over/Under voltage/frequency flags
      eqtyp( 16)=0

!& tf1p
      eqtyp( 17)= 30
      tc( 17)=prm(  4)

!& algeq 				  ! Active - reactive power priority
      eqtyp( 18)=0

!& db 						!Reactive current injection
      eqtyp( 19)=0
      if(x(  7)>prm( 29))then
         z(  6)=1
      elseif(x(  7)<prm( 28))then
         z(  6)=-1
      else
         z(  6)=0
      endif

!& algeq   						!  Low voltage ride-through and LV protection flag
      eqtyp( 20)=0

!& timer5
      eqtyp( 21)=0
      eqtyp( 22)=0
      z(  7)=-1
      x(104)=0.

!& algeq 		
      eqtyp( 23)=0

!& hyst
      eqtyp( 24)=0
      if(x( 26)>1.1)then
         z(  8)=1
      elseif(x( 26)<0.9)then
         z(  8)=-1
      else
         if(1.>= 0.)then
            z(  8)=1
         else
            z(  8)=-1
         endif
      endif

!& algeq
      eqtyp( 25)=0

!& inlim
      if (1>= 0.005)then
         tc( 26)=1
      endif
      if (x( 62)>5)then
         z(  9)=1
         eqtyp( 26)=0
      elseif (x( 62)<0.) then
         z(  9)=-1
         eqtyp( 26)=0
      else
         z(  9)=0
         if (1>= 0.005)then
            eqtyp( 26)= 62
         else
            eqtyp( 26)=0
         endif
      endif

!& algeq
      eqtyp( 27)=0

!& algeq
      eqtyp( 28)=0

!& algeq
      eqtyp( 29)=0

!& swsign
      eqtyp( 30)=0
      if(x( 67)>=0.)then
         z( 10)=1
      else
         z( 10)=2
      endif

!& tf1p2lim
      if(prm( 52)< 0.001)then
         prm( 52)=0.d0
         prm( 49)=-huge(0.d0)
         prm( 47)= huge(0.d0)
         prm( 48)=-huge(0.d0)
         prm( 56)= huge(0.d0)
      endif
      if(1*x( 64)-x( 63)>prm( 56)*prm( 52))then
         z( 11)=1
      elseif(1*x( 64)-x( 63)<prm( 48)*prm( 52))then
         z( 11)=-1
      else
         z( 11)=0
      endif
      eqtyp( 31)=0
      if(x( 63)>prm( 47))then
         z( 12)=1
         eqtyp( 32)=0
      elseif(x( 63)<prm( 49))then
         z( 12)=-1
         eqtyp( 32)=0
      else
         z( 12)=0
         eqtyp( 32)= 63
      endif
      tc( 32)=prm( 52)

!& algeq								! High voltage ride-through and HV protection flag
      eqtyp( 33)=0

!& pwlin4
      eqtyp( 34)=0
      if(x( 32)<(-999))then
         z( 13)=1
      elseif(x( 32)>=999)then
         z( 13)=   3
      elseif((-999)<=x( 32) .and. x( 32)<0.)then
         z( 13)=  1
      elseif(0.<=x( 32) .and. x( 32)<0.)then
         z( 13)=  2
      elseif(0.<=x( 32) .and. x( 32)<999)then
         z( 13)=  3
      endif

!& inlim
      if (1>= 0.005)then
         tc( 35)=1
      endif
      if (x( 69)>5)then
         z( 14)=1
         eqtyp( 35)=0
      elseif (x( 69)<0.) then
         z( 14)=-1
         eqtyp( 35)=0
      else
         z( 14)=0
         if (1>= 0.005)then
            eqtyp( 35)= 69
         else
            eqtyp( 35)=0
         endif
      endif

!& algeq
      eqtyp( 36)=0

!& algeq
      eqtyp( 37)=0

!& algeq
      eqtyp( 38)=0

!& algeq
      eqtyp( 39)=0

!& swsign
      eqtyp( 40)=0
      if(x( 74)>=0.)then
         z( 15)=1
      else
         z( 15)=2
      endif

!& algeq
      eqtyp( 41)=0

!& tf1p2lim
      if(prm( 52)< 0.001)then
         prm( 52)=0.d0
         prm( 49)=-huge(0.d0)
         prm( 47)= huge(0.d0)
         prm( 48)=-huge(0.d0)
         prm( 57)= huge(0.d0)
      endif
      if(1*x(102)-x( 70)>prm( 57)*prm( 52))then
         z( 16)=1
      elseif(1*x(102)-x( 70)<prm( 48)*prm( 52))then
         z( 16)=-1
      else
         z( 16)=0
      endif
      eqtyp( 42)=0
      if(x( 70)>prm( 47))then
         z( 17)=1
         eqtyp( 43)=0
      elseif(x( 70)<prm( 49))then
         z( 17)=-1
         eqtyp( 43)=0
      else
         z( 17)=0
         eqtyp( 43)= 70
      endif
      tc( 43)=prm( 52)

!& algeq 						! PLL control loop
      eqtyp( 44)=0

!& algeq
      eqtyp( 45)=0

!& algeq
      eqtyp( 46)=0

!& swsign
      eqtyp( 47)=0
      if(x( 34)>=0.)then
         z( 18)=1
      else
         z( 18)=2
      endif

!& algeq
      eqtyp( 48)=0

!& algeq
      eqtyp( 49)=0

!& algeq
      eqtyp( 50)=0

!& swsign
      eqtyp( 51)=0
      if(x( 59)>=0.)then
         z( 19)=1
      else
         z( 19)=2
      endif

!& int
      if (1.< 0.005)then
         eqtyp( 52)=0
      else
         eqtyp( 52)=  6
         tc( 52)=1.
      endif

!& pictl
      eqtyp( 53)=107
      x(107)=x( 60)
      eqtyp( 54)=0

!& algeq
      eqtyp( 55)=0

!& algeq
      eqtyp( 56)=0

!& tf1p
      eqtyp( 57)= 38
      tc( 57)=prm(  5)

!& tf1p
      eqtyp( 58)= 39
      tc( 58)=prm(  5)

!& algeq
      eqtyp( 59)=0

!& algeq
      eqtyp( 60)=0

!& tf1p
      eqtyp( 61)= 42
      tc( 61)=0.1

!& algeq
      eqtyp( 62)=0

!& algeq
      eqtyp( 63)=0

!& algeq
      eqtyp( 64)=0

!& swsign
      eqtyp( 65)=0
      if(x( 50)>=0.)then
         z( 20)=1
      else
         z( 20)=2
      endif

!& algeq
      eqtyp( 66)=0

!& algeq
      eqtyp( 67)=0

!& algeq
      eqtyp( 68)=0

!& swsign
      eqtyp( 69)=0
      if(x( 53)>=0.)then
         z( 21)=1
      else
         z( 21)=2
      endif

!& hyst
      eqtyp( 70)=0
      if(x( 43)>1.1)then
         z( 22)=1
      elseif(x( 43)<0.9)then
         z( 22)=-1
      else
         if(1.>= 0.)then
            z( 22)=1
         else
            z( 22)=-1
         endif
      endif

!& hyst
      eqtyp( 71)=0
      if(x( 45)>1.1)then
         z( 23)=1
      elseif(x( 45)<0.9)then
         z( 23)=-1
      else
         if(1.>= 0.)then
            z( 23)=1
         else
            z( 23)=-1
         endif
      endif

!& algeq						! Low frequency protection flag and reconnection
      eqtyp( 72)=0

!& inlim
      if (1>= 0.005)then
         tc( 73)=1
      endif
      if (x( 76)>5)then
         z( 24)=1
         eqtyp( 73)=0
      elseif (x( 76)<0.) then
         z( 24)=-1
         eqtyp( 73)=0
      else
         z( 24)=0
         if (1>= 0.005)then
            eqtyp( 73)= 76
         else
            eqtyp( 73)=0
         endif
      endif

!& algeq
      eqtyp( 74)=0

!& algeq
      eqtyp( 75)=0

!& algeq
      eqtyp( 76)=0

!& swsign
      eqtyp( 77)=0
      if(x( 81)>=0.)then
         z( 25)=1
      else
         z( 25)=2
      endif

!& tf1p2lim
      if(prm( 52)< 0.001)then
         prm( 52)=0.d0
         prm( 49)=-huge(0.d0)
         prm( 47)= huge(0.d0)
         prm( 48)=-huge(0.d0)
         prm( 57)= huge(0.d0)
      endif
      if(1*x( 78)-x( 77)>prm( 57)*prm( 52))then
         z( 26)=1
      elseif(1*x( 78)-x( 77)<prm( 48)*prm( 52))then
         z( 26)=-1
      else
         z( 26)=0
      endif
      eqtyp( 78)=0
      if(x( 77)>prm( 47))then
         z( 27)=1
         eqtyp( 79)=0
      elseif(x( 77)<prm( 49))then
         z( 27)=-1
         eqtyp( 79)=0
      else
         z( 27)=0
         eqtyp( 79)= 77
      endif
      tc( 79)=prm( 52)

!& algeq								! High frequency protection flag and reconnection
      eqtyp( 80)=0

!& inlim
      if (1>= 0.005)then
         tc( 81)=1
      endif
      if (x( 83)>5)then
         z( 28)=1
         eqtyp( 81)=0
      elseif (x( 83)<0.) then
         z( 28)=-1
         eqtyp( 81)=0
      else
         z( 28)=0
         if (1>= 0.005)then
            eqtyp( 81)= 83
         else
            eqtyp( 81)=0
         endif
      endif

!& algeq
      eqtyp( 82)=0

!& algeq
      eqtyp( 83)=0

!& algeq
      eqtyp( 84)=0

!& swsign
      eqtyp( 85)=0
      if(x( 88)>=0.)then
         z( 29)=1
      else
         z( 29)=2
      endif

!& tf1p2lim
      if(prm( 52)< 0.001)then
         prm( 52)=0.d0
         prm( 49)=-huge(0.d0)
         prm( 47)= huge(0.d0)
         prm( 48)=-huge(0.d0)
         prm( 57)= huge(0.d0)
      endif
      if(1*x( 78)-x( 84)>prm( 57)*prm( 52))then
         z( 30)=1
      elseif(1*x( 78)-x( 84)<prm( 48)*prm( 52))then
         z( 30)=-1
      else
         z( 30)=0
      endif
      eqtyp( 86)=0
      if(x( 84)>prm( 47))then
         z( 31)=1
         eqtyp( 87)=0
      elseif(x( 84)<prm( 49))then
         z( 31)=-1
         eqtyp( 87)=0
      else
         z( 31)=0
         eqtyp( 87)= 84
      endif
      tc( 87)=prm( 52)

!& algeq						!Previous frequency control
      eqtyp( 88)=0

!& lim
      eqtyp( 89)=0
      if(x( 47)>999)then
         z( 32)=1
      elseif(x( 47)<0)then
         z( 32)=-1
      else
         z( 32)=0
      endif

!& tf1p2lim
      if(prm( 52)< 0.001)then
         prm( 52)=0.d0
         prm( 51)=-huge(0.d0)
         prm( 50)= huge(0.d0)
         prm( 51)=-huge(0.d0)
         prm( 50)= huge(0.d0)
      endif
      if(1*x( 48)-x( 89)>prm( 50)*prm( 52))then
         z( 33)=1
      elseif(1*x( 48)-x( 89)<prm( 51)*prm( 52))then
         z( 33)=-1
      else
         z( 33)=0
      endif
      eqtyp( 90)=0
      if(x( 89)>prm( 50))then
         z( 34)=1
         eqtyp( 91)=0
      elseif(x( 89)<prm( 51))then
         z( 34)=-1
         eqtyp( 91)=0
      else
         z( 34)=0
         eqtyp( 91)= 89
      endif
      tc( 91)=prm( 52)

!& algeq
      eqtyp( 92)=0

!& algeq
      eqtyp( 93)=0

!& algeq
      eqtyp( 94)=0

!& swsign
      eqtyp( 95)=0
      if(x( 90)>=0.)then
         z( 35)=1
      else
         z( 35)=2
      endif

!& tf1p
      eqtyp( 96)= 94
      tc( 96)=1

!& algeq				! Frequency droop control
      eqtyp( 97)=0

!& db
      eqtyp( 98)=0
      if(x( 95)>prm( 35))then
         z( 36)=1
      elseif(x( 95)<prm( 34))then
         z( 36)=-1
      else
         z( 36)=0
      endif

!& algeq
      eqtyp( 99)=0

!& algeq
      eqtyp(100)=0

!& lim
      eqtyp(101)=0
      if(x( 97)>0.00001)then
         z( 37)=1
      elseif(x( 97)<(-99999.))then
         z( 37)=-1
      else
         z( 37)=0
      endif

!& lim
      eqtyp(102)=0
      if(x( 98)>99999.)then
         z( 38)=1
      elseif(x( 98)<0.0001)then
         z( 38)=-1
      else
         z( 38)=0
      endif

!& algeq
      eqtyp(103)=0

!& algeq
      eqtyp(104)=0

!& algeq
      eqtyp(105)=0

!& algeq
      eqtyp(106)=0

!& algeq
      eqtyp(107)=0

!& algeq
      eqtyp(108)=0

!& algeq
      eqtyp(109)=0

!& algeq
      eqtyp(110)=0

!........................................................................................
   case (evaluate_eqs)

!& algeq
      f(  1)=vx + prm( 21)*x(  1) - prm( 22)*x(  2) - x(  3)

!& algeq
      f(  2)=vy + prm( 21)*x(  2) + prm( 22)*x(  1) - x(  4)

!& algeq
      f(  3)=x(  5) - dsqrt(x(  3)**2 + x(  4)**2)

!& tf1p
      f(  4)=(-x(  7)+1.*x(  5))

!& max1v1c
      select case (z(  1))
         case(1)
            f(  5)=0.01-x(  8)
         case(2)
            f(  5)=x(  7)-x(  8)
      end select

!& algeq
      f(  6)=x( 49)/x(  8) - x( 17)

!& algeq
      f(  7)=x( 16) + 0.001

!& limvb
      select case (z(  2))
         case(0)
            f(  8)=x( 11)-x( 17)
         case(-1)
            f(  8)=x( 11)-x( 16)
         case(1)
            f(  8)=x( 11)-x( 15)
      end select

!& algeq
      f(  9)=x( 29)*x( 63)*x( 70)*x( 77)*x( 84) - x(  9)

!& tf1p2lim
      select case (z(  3))
         case(0)
            f( 10)=x(103)-1*x( 11)+x( 29)
         case(1)
            f( 10)=x(103)-prm( 55)*prm(  4)
         case(-1)
            f( 10)=x(103)-prm( 48)*prm(  4)
      end select
      select case (z(  4))
         case(0)
            f( 11)=x(103)
         case(1)
            f( 11)=x( 29)-prm( 47)
         case(-1)
            f( 11)=x( 29)-prm( 48)
      end select

!& algeq 				  ! Active - reactive power priority
      f( 12)=x( 13) -x( 35)*dsqrt(max(0.d0,prm(  1)**2 - x( 11)**2)) - (1-x( 35))*(prm( 32)*prm(  1)*0.707 + (1-prm( 32))*prm(  1))

!& algeq
      f( 13)=x( 14) + x( 35)*dsqrt(max(0.d0,prm(  1)**2 - x( 11)**2)) + (1-x( 35))*prm(  1)

!& algeq
      f( 14)=-x( 24) + prm( 43) - x( 28)

!& limvb
      select case (z(  5))
         case(0)
            f( 15)=x( 12)-x( 28)
         case(-1)
            f( 15)=x( 12)-x( 14)
         case(1)
            f( 15)=x( 12)-x( 13)
      end select

!& algeq						!  Over/Under voltage/frequency flags
      f( 16)=x( 30)*x( 63)*x( 70)*x( 77)*x( 84) - x( 10)

!& tf1p
      f( 17)=(-x( 30)+1.*x( 12))

!& algeq 				  ! Active - reactive power priority
      f( 18)=x( 15) - x( 35)*prm(  2) -  (1-x( 35))*dsqrt(max(0.d0,prm(  1)**2 - x( 12)**2))

!& db 						!Reactive current injection
      select case (z(  6))
         case(0)
            f( 19)=x( 24)
         case(-1)
            f( 19)=x( 24)-(-prm( 26)*prm(  1)*(prm( 23) + prm( 32)))-(prm( 24)*prm(  1)*(prm( 23) + prm( 32) ))*(x(  7)-prm( 28))
         case(1)
            f( 19)=x( 24)-prm( 27)*prm(  1)-prm( 25)*prm(  1)*prm( 30)*(x(  7)-prm( 29))
      end select

!& algeq   						!  Low voltage ride-through and LV protection flag
      f( 20)=x(  7) + x( 31)

!& timer5
      select case (z(  7))
         case (-1)
            f( 21)=x( 33)
            f( 22)=x(104)
         case (0)
            f( 21)=x( 33)
            f( 22)= 1.
         case (1)
            f( 21)=x( 33)-1.
            f( 22)= 0.
      end select

!& algeq 		
      f( 23)=x( 26) -1 + x( 33)

!& hyst
      if(z(  8) == 1)then
         f( 24)=x( 25)-1.-(1.-1.)*(x( 26)-1.1)/(1.1-0.9)
      else
         f( 24)=x( 25)-0.-(0.-0.)*(x( 26)-0.9)/(1.1-0.9)
      endif

!& algeq
      f( 25)=x( 61) - 1 + x( 25)

!& inlim
      if (1>= 0.005)then
         select case (z(  9))
            case(0)
               f( 26)=x( 61)
            case(1)
               f( 26)=x( 62)-5
            case(-1)
               f( 26)=x( 62)-0.
         end select
      else
         select case (z(  9))
            case(0)
               f( 26)=x( 61)-x( 62)
            case(1)
               f( 26)=x( 62)-5
            case(-1)
               f( 26)=x( 62)-0.
         end select
      endif

!& algeq
      f( 27)=x( 65) - 1

!& algeq
      f( 28)=x( 66) - 1 + x( 61)

!& algeq
      f( 29)=x( 67) + prm( 20) - x( 62)

!& swsign
      select case (z( 10))
         case(1)
            f( 30)=x( 64)-x( 65)
         case(2)
            f( 30)=x( 64)-x( 66)
      end select

!& tf1p2lim
      select case (z( 11))
         case(0)
            f( 31)=x(105)-1*x( 64)+x( 63)
         case(1)
            f( 31)=x(105)-prm( 56)*prm( 52)
         case(-1)
            f( 31)=x(105)-prm( 48)*prm( 52)
      end select
      select case (z( 12))
         case(0)
            f( 32)=x(105)
         case(1)
            f( 32)=x( 63)-prm( 47)
         case(-1)
            f( 32)=x( 63)-prm( 49)
      end select

!& algeq								! High voltage ride-through and HV protection flag
      f( 33)=x( 32) - x(  7) + prm(  9)

!& pwlin4
      select case (z( 13))
         case (  1)
            f( 34)=0.+ ( (0.-0.)*(x( 32)-(-999))/(0.-(-999)) ) -x( 27)
         case (  2)
            f( 34)=0.+ ( (1.-0.)*(x( 32)-0.)/(0.-0.) ) -x( 27)
         case (  3)
            f( 34)=1.+ ( (1.-1.)*(x( 32)-0.)/(999-0.) ) -x( 27)
      end select

!& inlim
      if (1>= 0.005)then
         select case (z( 14))
            case(0)
               f( 35)=x( 27)
            case(1)
               f( 35)=x( 69)-5
            case(-1)
               f( 35)=x( 69)-0.
         end select
      else
         select case (z( 14))
            case(0)
               f( 35)=x( 27)-x( 69)
            case(1)
               f( 35)=x( 69)-5
            case(-1)
               f( 35)=x( 69)-0.
         end select
      endif

!& algeq
      f( 36)=x( 68) - 1 + x( 27)

!& algeq
      f( 37)=x( 72) - 1

!& algeq
      f( 38)=x( 73) - 1 + x( 27)

!& algeq
      f( 39)=x( 74) + prm( 20) - x( 69)

!& swsign
      select case (z( 15))
         case(1)
            f( 40)=x( 71)-x( 72)
         case(2)
            f( 40)=x( 71)-x( 73)
      end select

!& algeq
      f( 41)=x(102) - x( 71)*x( 68)

!& tf1p2lim
      select case (z( 16))
         case(0)
            f( 42)=x(106)-1*x(102)+x( 70)
         case(1)
            f( 42)=x(106)-prm( 57)*prm( 52)
         case(-1)
            f( 42)=x(106)-prm( 48)*prm( 52)
      end select
      select case (z( 17))
         case(0)
            f( 43)=x(106)
         case(1)
            f( 43)=x( 70)-prm( 47)
         case(-1)
            f( 43)=x( 70)-prm( 49)
      end select

!& algeq 						! PLL control loop
      f( 44)=x( 34) - x(  7) + prm( 28)

!& algeq
      f( 45)=x( 36) - 1

!& algeq
      f( 46)=x( 37)

!& swsign
      select case (z( 18))
         case(1)
            f( 47)=x( 35)-x( 36)
         case(2)
            f( 47)=x( 35)-x( 37)
      end select

!& algeq
      f( 48)=x( 56) - 1

!& algeq
      f( 49)=x( 57)

!& algeq
      f( 50)=x( 59) - x(  7) + prm( 11)

!& swsign
      select case (z( 19))
         case(1)
            f( 51)=x( 58)-x( 56)
         case(2)
            f( 51)=x( 58)-x( 57)
      end select

!& int
      if (1.< 0.005)then
         f( 52)=x( 19)-x(  6)
      else
         f( 52)=x( 19)
      endif

!& pictl
      f( 53)=0.1/(prm( 10)*0.001)**2                                                                                                                                                                                                                                                                                     *x( 20)
      f( 54)=0.5/(prm( 10)*0.001)                                                                                                                                                                                                                                                                                        *x( 20)+x(107)-x( 60)

!& algeq
      f( 55)=x( 18) - x( 60) + omega*2*pi*50

!& algeq
      f( 56)=x( 19) - x( 18)*x( 58)

!& tf1p
      f( 57)=(-x( 38)+1.*x(  3))

!& tf1p
      f( 58)=(-x( 39)+1.*x(  4))

!& algeq
      f( 59)=x( 40) - x( 60)/(2*pi*50)

!& algeq
      f( 60)=x( 41) - 50*x( 40)

!& tf1p
      f( 61)=(-x( 42)+1*x( 41))

!& algeq
      f( 62)=x( 51) - 1

!& algeq
      f( 63)=x( 52)

!& algeq
      f( 64)=x( 50) - x( 42) + prm( 15)

!& swsign
      select case (z( 20))
         case(1)
            f( 65)=x( 43)-x( 51)
         case(2)
            f( 65)=x( 43)-x( 52)
      end select

!& algeq
      f( 66)=x( 54) - 1

!& algeq
      f( 67)=x( 55)

!& algeq
      f( 68)=x( 53) - prm( 16) + x( 42)

!& swsign
      select case (z( 21))
         case(1)
            f( 69)=x( 45)-x( 54)
         case(2)
            f( 69)=x( 45)-x( 55)
      end select

!& hyst
      if(z( 22) == 1)then
         f( 70)=x( 44)-1.-(1.-1.)*(x( 43)-1.1)/(1.1-0.9)
      else
         f( 70)=x( 44)-0.-(0.-0.)*(x( 43)-0.9)/(1.1-0.9)
      endif

!& hyst
      if(z( 23) == 1)then
         f( 71)=x( 46)-1.-(1.-1.)*(x( 45)-1.1)/(1.1-0.9)
      else
         f( 71)=x( 46)-0.-(0.-0.)*(x( 45)-0.9)/(1.1-0.9)
      endif

!& algeq						! Low frequency protection flag and reconnection
      f( 72)=x( 75) - 1 + x( 44)

!& inlim
      if (1>= 0.005)then
         select case (z( 24))
            case(0)
               f( 73)=x( 75)
            case(1)
               f( 73)=x( 76)-5
            case(-1)
               f( 73)=x( 76)-0.
         end select
      else
         select case (z( 24))
            case(0)
               f( 73)=x( 75)-x( 76)
            case(1)
               f( 73)=x( 76)-5
            case(-1)
               f( 73)=x( 76)-0.
         end select
      endif

!& algeq
      f( 74)=x( 79) - 1

!& algeq
      f( 75)=x( 80) - 1 + x( 75)

!& algeq
      f( 76)=x( 81) + prm( 20) - x( 76)

!& swsign
      select case (z( 25))
         case(1)
            f( 77)=x( 78)-x( 79)
         case(2)
            f( 77)=x( 78)-x( 80)
      end select

!& tf1p2lim
      select case (z( 26))
         case(0)
            f( 78)=x(108)-1*x( 78)+x( 77)
         case(1)
            f( 78)=x(108)-prm( 57)*prm( 52)
         case(-1)
            f( 78)=x(108)-prm( 48)*prm( 52)
      end select
      select case (z( 27))
         case(0)
            f( 79)=x(108)
         case(1)
            f( 79)=x( 77)-prm( 47)
         case(-1)
            f( 79)=x( 77)-prm( 49)
      end select

!& algeq								! High frequency protection flag and reconnection
      f( 80)=x( 82) - 1 + x( 46)

!& inlim
      if (1>= 0.005)then
         select case (z( 28))
            case(0)
               f( 81)=x( 82)
            case(1)
               f( 81)=x( 83)-5
            case(-1)
               f( 81)=x( 83)-0.
         end select
      else
         select case (z( 28))
            case(0)
               f( 81)=x( 82)-x( 83)
            case(1)
               f( 81)=x( 83)-5
            case(-1)
               f( 81)=x( 83)-0.
         end select
      endif

!& algeq
      f( 82)=x( 86) - 1

!& algeq
      f( 83)=x( 87) - 1 + x( 82)

!& algeq
      f( 84)=x( 88) + prm( 20) - x( 83)

!& swsign
      select case (z( 29))
         case(1)
            f( 85)=x( 85)-x( 86)
         case(2)
            f( 85)=x( 85)-x( 87)
      end select

!& tf1p2lim
      select case (z( 30))
         case(0)
            f( 86)=x(109)-1*x( 78)+x( 84)
         case(1)
            f( 86)=x(109)-prm( 57)*prm( 52)
         case(-1)
            f( 86)=x(109)-prm( 48)*prm( 52)
      end select
      select case (z( 31))
         case(0)
            f( 87)=x(109)
         case(1)
            f( 87)=x( 84)-prm( 47)
         case(-1)
            f( 87)=x( 84)-prm( 49)
      end select

!& algeq						!Previous frequency control
      f( 88)=x( 47) + prm( 18)*prm( 41)*(x( 42) - prm( 17))/50

!& lim
      select case (z( 32))
         case(0)
            f( 89)=x( 48)-x( 47)
         case(-1)
            f( 89)=x( 48)-0
         case(1)
            f( 89)=x( 48)-999
      end select

!& tf1p2lim
      select case (z( 33))
         case(0)
            f( 90)=x(110)-1*x( 48)+x( 89)
         case(1)
            f( 90)=x(110)-prm( 50)*prm( 52)
         case(-1)
            f( 90)=x(110)-prm( 51)*prm( 52)
      end select
      select case (z( 34))
         case(0)
            f( 91)=x(110)
         case(1)
            f( 91)=x( 89)-prm( 50)
         case(-1)
            f( 91)=x( 89)-prm( 51)
      end select

!& algeq
      f( 92)=x( 92) - 1

!& algeq
      f( 93)=x( 93)

!& algeq
      f( 94)=x( 90) - x( 42) + prm( 19)

!& swsign
      select case (z( 35))
         case(1)
            f( 95)=x( 91)-x( 92)
         case(2)
            f( 95)=x( 91)-x( 93)
      end select

!& tf1p
      f( 96)=(-x( 94)+1.*x( 91))

!& algeq				! Frequency droop control
      f( 97)=x( 95) + (x( 42)/50) - prm( 58)

!& db
      select case (z( 36))
         case(0)
            f( 98)=x( 96)
         case(-1)
            f( 98)=x( 96)-0.-1.*(x( 95)-prm( 34))
         case(1)
            f( 98)=x( 96)-0.-1.*(x( 95)-prm( 35))
      end select

!& algeq
      f( 99)=x( 97) - x( 96)*prm( 37)

!& algeq
      f(100)=x( 98) - x( 96)*prm( 36)

!& lim
      select case (z( 37))
         case(0)
            f(101)=x( 99)-x( 97)
         case(-1)
            f(101)=x( 99)-(-99999.)
         case(1)
            f(101)=x( 99)-0.00001
      end select

!& lim
      select case (z( 38))
         case(0)
            f(102)=x(100)-x( 98)
         case(-1)
            f(102)=x(100)-0.0001
         case(1)
            f(102)=x(100)-99999.
      end select

!& algeq
      f(103)=x(101)-x(100)-x( 99)

!& algeq
      f(104)=x( 49)+prm( 41)-x(101)

!& algeq
      f(105)=x( 21) - x( 38)*cos(x(  6)) - x( 39)*sin(x(  6))

!& algeq
      f(106)=x( 20) + x( 38)*sin(x(  6)) - x( 39)*cos(x(  6))

!& algeq
      f(107)=x( 22) - x( 21)*x(  9)

!& algeq
      f(108)=x( 23) - x( 21)*x( 10)

!& algeq
      f(109)=x(  1) - x(  9)*cos(x(  6)) - x( 10)*sin(x(  6))

!& algeq
      f(110)=x(  2) - x(  9)*sin(x(  6)) + x( 10)*cos(x(  6))

!........................................................................................
   case (update_disc)

!& algeq

!& algeq

!& algeq

!& tf1p

!& max1v1c
      select case (z(  1))
         case(1)
            if(x(  7)>0.01)then
               z(  1)=2
            endif
         case(2)
            if(0.01>x(  7))then
               z(  1)=1
            endif
      end select

!& algeq

!& algeq

!& limvb
      select case (z(  2))
         case(0)
            if(x( 17)>x( 15))then
               z(  2)=1
            elseif(x( 17)<x( 16))then
               z(  2)=-1
            endif
         case(-1)
            if(x( 17)>x( 16))then
               z(  2)=0
            endif
         case(1)
            if(x( 17)<x( 15))then
               z(  2)=0
            endif
      end select

!& algeq

!& tf1p2lim
      select case (z(  3))
         case(0)
            if(x(103)>prm( 55)*prm(  4))then
               z(  3)=1
            elseif(x(103)<prm( 48)*prm(  4))then
               z(  3)=-1
            endif
         case(1)
            if(1*x( 11)-x( 29)<prm( 55)*prm(  4))then
               z(  3)= 0
            endif
         case(-1)
            if(1*x( 11)-x( 29)>prm( 48)*prm(  4))then
               z(  3)= 0
            endif
      end select
      select case (z(  4))
         case(0)
            if(x( 29)>prm( 47))then
               z(  4)=1
               eqtyp( 11)=0
            elseif(x( 29)<prm( 48))then
               z(  4)=-1
               eqtyp( 11)=0
            endif
         case(1)
            if (x(103)<0.)then
               z(  4)= 0
               eqtyp( 11)= 29
            endif
         case(-1)
            if(x(103)>0.)then
               z(  4)= 0
               eqtyp( 11)= 29
            endif
      end select

!& algeq 				  ! Active - reactive power priority

!& algeq

!& algeq

!& limvb
      select case (z(  5))
         case(0)
            if(x( 28)>x( 13))then
               z(  5)=1
            elseif(x( 28)<x( 14))then
               z(  5)=-1
            endif
         case(-1)
            if(x( 28)>x( 14))then
               z(  5)=0
            endif
         case(1)
            if(x( 28)<x( 13))then
               z(  5)=0
            endif
      end select

!& algeq						!  Over/Under voltage/frequency flags

!& tf1p

!& algeq 				  ! Active - reactive power priority

!& db 						!Reactive current injection
      select case (z(  6))
         case(0)
            if(x(  7)>prm( 29))then
               z(  6)=1
            elseif(x(  7)<prm( 28))then
               z(  6)=-1
            endif
         case(-1)
            if(x(  7)>prm( 28))then
               z(  6)=0
            endif
         case(1)
            if(x(  7)<prm( 29))then
               z(  6)=0
            endif
      end select

!& algeq   						!  Low voltage ride-through and LV protection flag

!& timer5
      if(z(  7) == -1)then
         if(x( 31) >= (-prm( 12)))then
            z(  7)=0
            eqtyp( 22)=104
         endif
      else
         if(x( 31) < (-prm( 12)))then
            z(  7)=-1
            eqtyp( 22)=0
         endif
      endif
      if(z(  7) == 0)then
         if(x( 31) > (-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))then
            if(x(104) > 0.)then
               z(  7)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))then
            if(x(104) > prm(  6)+(0.-prm(  6))*(x( 31)-(-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))))then
               z(  7)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))then
            if(x(104) > prm(  6)+(prm(  6)-prm(  6))*(x( 31)-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 13) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))))then
               z(  7)=1
            endif
         elseif(x( 31) > (-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))then
            if(x(104) > prm(  8)+(prm(  6)-prm(  8))*(x( 31)-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33))))/((-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))-(-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))))then
               z(  7)=1
            endif
         elseif(x( 31) > (-prm( 12)))then
            if(x(104) > prm(  7)+(prm(  8)-prm(  7))*(x( 31)-(-prm( 12)))/((-(prm( 31)*prm( 14) + (1-prm( 31))*prm( 33)))-(-prm( 12))))then
               z(  7)=1
            endif
         endif
      endif

!& algeq 		

!& hyst
      if (z(  8) == -1)then
         if(x( 26)>1.1)then
            z(  8)=1
         endif
      else
         if(x( 26)<0.9)then
            z(  8)=-1
         endif
      endif

!& algeq

!& inlim
      if (1>= 0.005)then
         select case (z(  9))
            case(0)
               if(x( 62)<0.)then
                  z(  9)=-1
                  eqtyp( 26)=0
               elseif(x( 62)>5)then
                  z(  9)= 1
                  eqtyp( 26)=0
               endif
            case(1)
               if(x( 61)<0.)then
                  z(  9)=0
                  eqtyp( 26)= 62
               endif
            case(-1)
               if(x( 61)>0.)then
                  z(  9)=0
                  eqtyp( 26)= 62
               endif
         end select
      else
         select case (z(  9))
            case(0)
               if(x( 62)<0.)then
                  z(  9)=-1
               elseif(x( 62)>5)then
                  z(  9)= 1
               endif
            case(1)
               if(x( 61)<5)then
                  z(  9)=0
               endif
            case(-1)
               if(x( 61)>0.)then
                  z(  9)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 10))
         case(1)
            if(x( 67)<0.)then
               z( 10)=2
            endif
         case(2)
            if(x( 67)>=0.)then
               z( 10)=1
            endif
      end select

!& tf1p2lim
      select case (z( 11))
         case(0)
            if(x(105)>prm( 56)*prm( 52))then
               z( 11)=1
            elseif(x(105)<prm( 48)*prm( 52))then
               z( 11)=-1
            endif
         case(1)
            if(1*x( 64)-x( 63)<prm( 56)*prm( 52))then
               z( 11)= 0
            endif
         case(-1)
            if(1*x( 64)-x( 63)>prm( 48)*prm( 52))then
               z( 11)= 0
            endif
      end select
      select case (z( 12))
         case(0)
            if(x( 63)>prm( 47))then
               z( 12)=1
               eqtyp( 32)=0
            elseif(x( 63)<prm( 49))then
               z( 12)=-1
               eqtyp( 32)=0
            endif
         case(1)
            if (x(105)<0.)then
               z( 12)= 0
               eqtyp( 32)= 63
            endif
         case(-1)
            if(x(105)>0.)then
               z( 12)= 0
               eqtyp( 32)= 63
            endif
      end select

!& algeq								! High voltage ride-through and HV protection flag

!& pwlin4
      if(x( 32)<(-999))then
         z( 13)=1
      elseif(x( 32)>=999)then
         z( 13)=  3
      elseif((-999)<=x( 32) .and. x( 32)<0.)then
         z( 13)=  1
      elseif(0.<=x( 32) .and. x( 32)<0.)then
         z( 13)=  2
      elseif(0.<=x( 32) .and. x( 32)<999)then
         z( 13)=  3
      endif

!& inlim
      if (1>= 0.005)then
         select case (z( 14))
            case(0)
               if(x( 69)<0.)then
                  z( 14)=-1
                  eqtyp( 35)=0
               elseif(x( 69)>5)then
                  z( 14)= 1
                  eqtyp( 35)=0
               endif
            case(1)
               if(x( 27)<0.)then
                  z( 14)=0
                  eqtyp( 35)= 69
               endif
            case(-1)
               if(x( 27)>0.)then
                  z( 14)=0
                  eqtyp( 35)= 69
               endif
         end select
      else
         select case (z( 14))
            case(0)
               if(x( 69)<0.)then
                  z( 14)=-1
               elseif(x( 69)>5)then
                  z( 14)= 1
               endif
            case(1)
               if(x( 27)<5)then
                  z( 14)=0
               endif
            case(-1)
               if(x( 27)>0.)then
                  z( 14)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 15))
         case(1)
            if(x( 74)<0.)then
               z( 15)=2
            endif
         case(2)
            if(x( 74)>=0.)then
               z( 15)=1
            endif
      end select

!& algeq

!& tf1p2lim
      select case (z( 16))
         case(0)
            if(x(106)>prm( 57)*prm( 52))then
               z( 16)=1
            elseif(x(106)<prm( 48)*prm( 52))then
               z( 16)=-1
            endif
         case(1)
            if(1*x(102)-x( 70)<prm( 57)*prm( 52))then
               z( 16)= 0
            endif
         case(-1)
            if(1*x(102)-x( 70)>prm( 48)*prm( 52))then
               z( 16)= 0
            endif
      end select
      select case (z( 17))
         case(0)
            if(x( 70)>prm( 47))then
               z( 17)=1
               eqtyp( 43)=0
            elseif(x( 70)<prm( 49))then
               z( 17)=-1
               eqtyp( 43)=0
            endif
         case(1)
            if (x(106)<0.)then
               z( 17)= 0
               eqtyp( 43)= 70
            endif
         case(-1)
            if(x(106)>0.)then
               z( 17)= 0
               eqtyp( 43)= 70
            endif
      end select

!& algeq 						! PLL control loop

!& algeq

!& algeq

!& swsign
      select case (z( 18))
         case(1)
            if(x( 34)<0.)then
               z( 18)=2
            endif
         case(2)
            if(x( 34)>=0.)then
               z( 18)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 19))
         case(1)
            if(x( 59)<0.)then
               z( 19)=2
            endif
         case(2)
            if(x( 59)>=0.)then
               z( 19)=1
            endif
      end select

!& int

!& pictl

!& algeq

!& algeq

!& tf1p

!& tf1p

!& algeq

!& algeq

!& tf1p

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 20))
         case(1)
            if(x( 50)<0.)then
               z( 20)=2
            endif
         case(2)
            if(x( 50)>=0.)then
               z( 20)=1
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 21))
         case(1)
            if(x( 53)<0.)then
               z( 21)=2
            endif
         case(2)
            if(x( 53)>=0.)then
               z( 21)=1
            endif
      end select

!& hyst
      if (z( 22) == -1)then
         if(x( 43)>1.1)then
            z( 22)=1
         endif
      else
         if(x( 43)<0.9)then
            z( 22)=-1
         endif
      endif

!& hyst
      if (z( 23) == -1)then
         if(x( 45)>1.1)then
            z( 23)=1
         endif
      else
         if(x( 45)<0.9)then
            z( 23)=-1
         endif
      endif

!& algeq						! Low frequency protection flag and reconnection

!& inlim
      if (1>= 0.005)then
         select case (z( 24))
            case(0)
               if(x( 76)<0.)then
                  z( 24)=-1
                  eqtyp( 73)=0
               elseif(x( 76)>5)then
                  z( 24)= 1
                  eqtyp( 73)=0
               endif
            case(1)
               if(x( 75)<0.)then
                  z( 24)=0
                  eqtyp( 73)= 76
               endif
            case(-1)
               if(x( 75)>0.)then
                  z( 24)=0
                  eqtyp( 73)= 76
               endif
         end select
      else
         select case (z( 24))
            case(0)
               if(x( 76)<0.)then
                  z( 24)=-1
               elseif(x( 76)>5)then
                  z( 24)= 1
               endif
            case(1)
               if(x( 75)<5)then
                  z( 24)=0
               endif
            case(-1)
               if(x( 75)>0.)then
                  z( 24)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 25))
         case(1)
            if(x( 81)<0.)then
               z( 25)=2
            endif
         case(2)
            if(x( 81)>=0.)then
               z( 25)=1
            endif
      end select

!& tf1p2lim
      select case (z( 26))
         case(0)
            if(x(108)>prm( 57)*prm( 52))then
               z( 26)=1
            elseif(x(108)<prm( 48)*prm( 52))then
               z( 26)=-1
            endif
         case(1)
            if(1*x( 78)-x( 77)<prm( 57)*prm( 52))then
               z( 26)= 0
            endif
         case(-1)
            if(1*x( 78)-x( 77)>prm( 48)*prm( 52))then
               z( 26)= 0
            endif
      end select
      select case (z( 27))
         case(0)
            if(x( 77)>prm( 47))then
               z( 27)=1
               eqtyp( 79)=0
            elseif(x( 77)<prm( 49))then
               z( 27)=-1
               eqtyp( 79)=0
            endif
         case(1)
            if (x(108)<0.)then
               z( 27)= 0
               eqtyp( 79)= 77
            endif
         case(-1)
            if(x(108)>0.)then
               z( 27)= 0
               eqtyp( 79)= 77
            endif
      end select

!& algeq								! High frequency protection flag and reconnection

!& inlim
      if (1>= 0.005)then
         select case (z( 28))
            case(0)
               if(x( 83)<0.)then
                  z( 28)=-1
                  eqtyp( 81)=0
               elseif(x( 83)>5)then
                  z( 28)= 1
                  eqtyp( 81)=0
               endif
            case(1)
               if(x( 82)<0.)then
                  z( 28)=0
                  eqtyp( 81)= 83
               endif
            case(-1)
               if(x( 82)>0.)then
                  z( 28)=0
                  eqtyp( 81)= 83
               endif
         end select
      else
         select case (z( 28))
            case(0)
               if(x( 83)<0.)then
                  z( 28)=-1
               elseif(x( 83)>5)then
                  z( 28)= 1
               endif
            case(1)
               if(x( 82)<5)then
                  z( 28)=0
               endif
            case(-1)
               if(x( 82)>0.)then
                  z( 28)=0
               endif
         end select
      endif

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 29))
         case(1)
            if(x( 88)<0.)then
               z( 29)=2
            endif
         case(2)
            if(x( 88)>=0.)then
               z( 29)=1
            endif
      end select

!& tf1p2lim
      select case (z( 30))
         case(0)
            if(x(109)>prm( 57)*prm( 52))then
               z( 30)=1
            elseif(x(109)<prm( 48)*prm( 52))then
               z( 30)=-1
            endif
         case(1)
            if(1*x( 78)-x( 84)<prm( 57)*prm( 52))then
               z( 30)= 0
            endif
         case(-1)
            if(1*x( 78)-x( 84)>prm( 48)*prm( 52))then
               z( 30)= 0
            endif
      end select
      select case (z( 31))
         case(0)
            if(x( 84)>prm( 47))then
               z( 31)=1
               eqtyp( 87)=0
            elseif(x( 84)<prm( 49))then
               z( 31)=-1
               eqtyp( 87)=0
            endif
         case(1)
            if (x(109)<0.)then
               z( 31)= 0
               eqtyp( 87)= 84
            endif
         case(-1)
            if(x(109)>0.)then
               z( 31)= 0
               eqtyp( 87)= 84
            endif
      end select

!& algeq						!Previous frequency control

!& lim
      select case (z( 32))
         case(0)
            if(x( 47)>999)then
               z( 32)=1
            elseif(x( 47)<0)then
               z( 32)=-1
            endif
         case(-1)
            if(x( 47)>0)then
               z( 32)=0
            endif
         case(1)
            if(x( 47)<999)then
               z( 32)=0
            endif
      end select

!& tf1p2lim
      select case (z( 33))
         case(0)
            if(x(110)>prm( 50)*prm( 52))then
               z( 33)=1
            elseif(x(110)<prm( 51)*prm( 52))then
               z( 33)=-1
            endif
         case(1)
            if(1*x( 48)-x( 89)<prm( 50)*prm( 52))then
               z( 33)= 0
            endif
         case(-1)
            if(1*x( 48)-x( 89)>prm( 51)*prm( 52))then
               z( 33)= 0
            endif
      end select
      select case (z( 34))
         case(0)
            if(x( 89)>prm( 50))then
               z( 34)=1
               eqtyp( 91)=0
            elseif(x( 89)<prm( 51))then
               z( 34)=-1
               eqtyp( 91)=0
            endif
         case(1)
            if (x(110)<0.)then
               z( 34)= 0
               eqtyp( 91)= 89
            endif
         case(-1)
            if(x(110)>0.)then
               z( 34)= 0
               eqtyp( 91)= 89
            endif
      end select

!& algeq

!& algeq

!& algeq

!& swsign
      select case (z( 35))
         case(1)
            if(x( 90)<0.)then
               z( 35)=2
            endif
         case(2)
            if(x( 90)>=0.)then
               z( 35)=1
            endif
      end select

!& tf1p

!& algeq				! Frequency droop control

!& db
      select case (z( 36))
         case(0)
            if(x( 95)>prm( 35))then
               z( 36)=1
            elseif(x( 95)<prm( 34))then
               z( 36)=-1
            endif
         case(-1)
            if(x( 95)>prm( 34))then
               z( 36)=0
            endif
         case(1)
            if(x( 95)<prm( 35))then
               z( 36)=0
            endif
      end select

!& algeq

!& algeq

!& lim
      select case (z( 37))
         case(0)
            if(x( 97)>0.00001)then
               z( 37)=1
            elseif(x( 97)<(-99999.))then
               z( 37)=-1
            endif
         case(-1)
            if(x( 97)>(-99999.))then
               z( 37)=0
            endif
         case(1)
            if(x( 97)<0.00001)then
               z( 37)=0
            endif
      end select

!& lim
      select case (z( 38))
         case(0)
            if(x( 98)>99999.)then
               z( 38)=1
            elseif(x( 98)<0.0001)then
               z( 38)=-1
            endif
         case(-1)
            if(x( 98)>0.0001)then
               z( 38)=0
            endif
         case(1)
            if(x( 98)<99999.)then
               z( 38)=0
            endif
      end select

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq

!& algeq
   end select

end subroutine inj_IBG3
