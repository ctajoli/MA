inj
IBG22

%data 

Imax              
IN
Iprate
Tg
Tm
tLVRT1
tLVRT2
tLVRTint
Vmax
tau              ! response time of the PLL in milliseconds
Vminpll          ! Voltage magnitude under which the PLL is blocked
a 
Vmin
Vint
fmin
fmax
fstart
b
fr
Tr      ! Time after which units are allowed to reconnect to the network
Re 
Xe
CM1
kRCI
kRCA
m
n
dbmin
dbmax
HVRT  
LVRT
CM2   
Vtrip
fdbup
fdbdn
Rup
Rdn



%parameters

vxlv = [vx] + {Re}*[ix] - {Xe}*[iy]
vylv = [vy] + {Re}*[iy] + {Xe}*[ix]
Vref = dsqrt({vxlv}**2 + {vylv}**2)
Pext = -{vxlv}*[ix]-{vylv}*[iy]
Qext = +{vxlv}*[iy]-{vylv}*[ix]
Iqref = -{Qext}/{Vref}
Ipref = -{Pext}/{Vref}
kpll = 10/({tau}*0.001)
theta_PLL = atan({vylv}/{vxlv})
Uplim = 9999
Downlim = -9999
Downlimdisc = 0
UplimdeltaP = 9999
DownlimdeltaP = 0
Tlim = 0.01
Uplimdis = 0
downlimdis = -9999
ratemax = {Iprate}*{IN}
rate = 0.1
fref = 1


%states

vxl = {vxlv}
vyl = {vylv}
Vt = {Vref}
PLLPhaseAngle = {theta_PLL}
Vm = {Vref}
x2 = {Vref}
Ip = {Ipref}
Iq = {Iqref}
Ipcmd = {Ipref}
Iqcmd = {Iqref}
Iqmax = dsqrt({Imax}**2 - {Ipref}**2 )
Iqmin = - dsqrt({Imax}**2 - {Ipref}**2 )
Ipmax = {IN}
Ipmin = -0.001
x4 = {Ipref}
DeltaW = 0
DeltaWf =  0
vq = -{vxlv}*sin({theta_PLL}) + {vylv}*cos({theta_PLL})
vd = {vxlv}*cos({theta_PLL}) + {vylv}*sin({theta_PLL}) 
Pgen = -{Pext}
Qgen = -{Qext}
Iqext = 0
Fvl = 1
Fvli = 1
Fvhi = 1                              
Fvh = 1  
z1 = 0.    
x5 = {Iqref}   
Iptemp = {Ipref}
Iqtemp = {Iqref}
x10 = -{Vref}  
x11 = {Vref} - {Vmax}
z = 0. 
deltaV = {Vref} - {dbmin}
Pflag = 1
Pflaga = 1                                                        ! Switch input a
Pflagb = 0.                                                       ! Switch input b
vxlm = {vxlv}
vylm = {vylv}
omegam = -0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1
fm = 50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)  
fmfilt = 50*(-0.5*({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)
Ffli = 1.                                                                                                               ! Current multiplier (input of hysteresis)
Ffl = 1. 
Ffhi = 1.
Ffh = 1.   
deltaP = {b}*-{Pext}*(50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1)  - {fstart})/50 
deltaPfin = 0                                                                                                           ! Active power correction
Ptot = -{Pext}
deltafl = 50*[omega] - {fmin}
flagla = 1
flaglb = 0
deltafh = {fmax} - 50*[omega] 
flagha = 1
flaghb = 0
PLLmulta = 1
PLLmultb = 0
mult = 1
deltaVPLL = {Vref} - {Vminpll}
wpll = -({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL}))*{kpll} + 2*pi*50 
g = 0
tr = 0
Fr = 1
Frtemp = 1
fvla = 1
fvlb = 1
deltafvl = -{Tr}
deltaPlim = 0
deltafcomp = 50*(-({vxlv}*sin({theta_PLL}) - {vylv}*cos({theta_PLL})) + 1) - {fr}
fcomp = 0
fa = 1
fb = 0
fcompf = 0
w1 = 0
w2 = 0
w3 = 0
w4 = 0
w5 = 0
w6 = 0
w7 = 0


%observables

Ip 
Iq
Pgen
Qgen
Vm
PLLPhaseAngle
fmfilt
Fr
Fvh
Ffl
Ffh
Frtemp
Ipcmd
Vt
wpll
vq

%models

& algeq
[vx] + {Re}*[ix] - {Xe}*[iy] - [vxl]
& algeq
[vy] + {Re}*[iy] + {Xe}*[ix] - [vyl]
& algeq
[Vt] - dsqrt([vxl]**2 + [vyl]**2)
& tf1p
Vt
Vm
1.
{Tm}
& max1v1c
Vm
x2
0.01
& algeq
[Ptot]/[x2] - [x4]
& algeq
[Ipmin] + 0.001
& limvb
x4
Ipmin
Ipmax
Ipcmd
& algeq
[Iptemp]*[Fr]*[Fvh]*[Ffl]*[Ffh] - [Ip]
& tf1p2lim
Ipcmd
Iptemp
1
{Tg}
{Downlim}
{Uplim}
{Downlim}
{ratemax}


& algeq                            
[Iqmax] -[Pflag]*dsqrt(max(0.d0,{Imax}**2 - [Ipcmd]**2)) - (1-[Pflag])*({CM2}*{Imax}*0.707 + (1-{CM2})*{Imax}) 
& algeq 
[Iqmin] + [Pflag]*dsqrt(max(0.d0,{Imax}**2 - [Ipcmd]**2)) + (1-[Pflag])*{Imax} 
& algeq
-[Iqext] + {Iqref} - [x5]
& limvb
x5
Iqmin
Iqmax
Iqcmd
& algeq
[Iqtemp]*[Fr]*[Fvh]*[Ffl]*[Ffh] - [Iq]
& tf1p
Iqcmd
Iqtemp
1.
{Tg}
& algeq
[Ipmax] - [Pflag]*{IN} -  (1-[Pflag])*dsqrt(max(0.d0,{Imax}**2 - [Iqcmd]**2)) 
& db                          
Vm
Iqext
{dbmin}
-{m}*{Imax}*({CM1} + {CM2})
{kRCI}*{Imax}*({CM1} + {CM2} )
{dbmax}
{n}*{Imax}
{kRCA}*{Imax}*{HVRT}
& algeq                            
[Vm] + [x10]   
& timer5                       
x10
z
-{a}
{tLVRT2}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRTint}
-({LVRT}*{Vint} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
{tLVRT1}
-({LVRT}*{Vmin} + (1-{LVRT})*{Vtrip})
0.


& algeq 
[Fvli] -1 + [z] 
& hyst                             
Fvli
Fvl
1.1
0.
1.
0.9
1.
0.
1.
& algeq
[g] - 1 + [Fvl]
& inlim
g
tr
1
0.
5
& algeq
[fvla] - 1 
& algeq
[fvlb] - 1 + [g]
& algeq
[deltafvl] + {Tr} - [tr]
& swsign
fvla
fvlb
deltafvl
Frtemp
& tf1p2lim
Frtemp
Fr
1
{Tlim}
{Downlimdisc}
{Uplim}
{Downlim}
{rate}

& algeq
[x11] - [Vm] + {Vmax}
& pwlin4                           
x11
z1
-999
0.
0.
0.
0.
1.
999
1.
& algeq 
[Fvhi] -1 + [z1] 
& hyst
Fvhi
Fvh
1.1
0.
1.
0.9
1.
0.
1.


& algeq 
[deltaV] - [Vm] + {dbmin}
& algeq 
[Pflaga] - 1 
& algeq 
[Pflagb] 
& swsign                           
Pflaga
Pflagb
deltaV
Pflag
& algeq
[PLLmulta] - 1
& algeq
[PLLmultb]
& algeq
[deltaVPLL] - [Vm] + {Vminpll}
& swsign
PLLmulta
PLLmultb
deltaVPLL
mult
& int                              
DeltaWf
PLLPhaseAngle
1.
& pictl
vq
wpll
25/({tau}*0.001)**2
10/({tau}*0.001)
& algeq
[DeltaW] - [wpll] + [omega]*2*pi*50
& algeq
[DeltaWf] - [DeltaW]*[mult]
& tf1p
vxl
vxlm
1.
{Tm}
& tf1p
vyl
vylm
1.
{Tm}


& algeq
[omegam] - [wpll]/(2*pi*50)
& algeq
[fm] - 50*[omegam]
& tf1p
fm
fmfilt
1.
1.
& algeq
[flagla] - 1
& algeq 
[flaglb] 
& algeq 
[deltafl] - [fmfilt] + {fmin}
& swsign
flagla
flaglb
deltafl
Ffli
& algeq
[flagha] - 1
& algeq 
[flaghb]
& algeq 
[deltafh] - {fmax} + [fmfilt]
& swsign
flagha
flaghb
deltafh
Ffhi
& hyst
Ffli
Ffl
1.1
0.
1.
0.9
1.
0.
1.
& hyst
Ffhi
Ffh
1.1
0.
1.
0.9
1.
0.
1.


& algeq
[deltaP] + {b}*{Pext}*([fmfilt] - {fstart})/50
& lim
deltaP
deltaPfin
0
999
& tf1p2lim
deltaPfin
deltaPlim
1
{Tlim}
{DownlimdeltaP}
{UplimdeltaP}
{DownlimdeltaP}
{UplimdeltaP}
& algeq
[fa] - 1
& algeq
[fb] 
& algeq
[deltafcomp] - [fmfilt] + {fr}
& swsign
fa
fb
deltafcomp
fcomp
& tf1p
fcomp
fcompf
1.
1



& algeq
[w1] + ([fmfilt]/50) - {fref}
& db
w1
w2
{fdbup}
0.
1.
{fdbdn}
0.
1.
& algeq
[w3] - [w2]*{Rdn}
& algeq
[w4] - [w2]*{Rup}
& lim
w3
w5
-99999.
-0.00001
& lim
w4
w6
0.00001
99999.
& algeq
[w7]-[w6]-[w5]
& algeq
[Ptot]+{Pext}-[w7]



& algeq
[vd] - [vxlm]*cos([PLLPhaseAngle]) - [vylm]*sin([PLLPhaseAngle]) 
& algeq
[vq] + [vxlm]*sin([PLLPhaseAngle]) - [vylm]*cos([PLLPhaseAngle]) 
& algeq
[Pgen] - [vd]*[Ip]
& algeq
[Qgen] - [vd]*[Iq]
& algeq
[ix] - [Ip]*cos([PLLPhaseAngle]) - [Iq]*sin([PLLPhaseAngle])
& algeq
[iy] - [Ip]*sin([PLLPhaseAngle]) + [Iq]*cos([PLLPhaseAngle])








